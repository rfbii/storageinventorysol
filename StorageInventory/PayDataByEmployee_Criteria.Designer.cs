﻿namespace PayrollFieldDataDesktop
{
    partial class PayDataByEmployee_Criteria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatePickerGroupBox = new System.Windows.Forms.GroupBox();
            this.DatePickerEndWeekLbl = new System.Windows.Forms.Label();
            this.DatePickerEndYearLbl = new System.Windows.Forms.Label();
            this.DatePickerStartWeekLbl = new System.Windows.Forms.Label();
            this.DatePickerStartYearLbl = new System.Windows.Forms.Label();
            this.DatePickerEndWeekTxt = new System.Windows.Forms.TextBox();
            this.DatePickerEndYearTxt = new System.Windows.Forms.TextBox();
            this.DatePickerStartWeekTxt = new System.Windows.Forms.TextBox();
            this.DatePickerStartYearTxt = new System.Windows.Forms.TextBox();
            this.DatePickerThruDateLbl = new System.Windows.Forms.Label();
            this.DatePickerThruDateTxt = new System.Windows.Forms.TextBox();
            this.DatePickerFromDateTxt = new System.Windows.Forms.TextBox();
            this.DatePickerFromDateLbl = new System.Windows.Forms.Label();
            this.DatePickerPresetsLbl = new System.Windows.Forms.Label();
            this.DatePickerPresetsCbx = new System.Windows.Forms.ComboBox();
            this.exitBtn = new System.Windows.Forms.Button();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.titleLbl = new System.Windows.Forms.Label();
            this.cropGroupBox = new System.Windows.Forms.GroupBox();
            this.cropsAvailableLst = new System.Windows.Forms.ListBox();
            this.cropsSelectedLbl = new System.Windows.Forms.Label();
            this.cropsSelectedLst = new System.Windows.Forms.ListBox();
            this.cropsAvailableLbl = new System.Windows.Forms.Label();
            this.cropAddBtn = new System.Windows.Forms.Button();
            this.cropRemoveBtn = new System.Windows.Forms.Button();
            this.jobGroupBox = new System.Windows.Forms.GroupBox();
            this.jobsAvailableLst = new System.Windows.Forms.ListBox();
            this.jobsSelectedLbl = new System.Windows.Forms.Label();
            this.jobsSelectedLst = new System.Windows.Forms.ListBox();
            this.jobsAvailableLbl = new System.Windows.Forms.Label();
            this.jobAddBtn = new System.Windows.Forms.Button();
            this.jobRemoveBtn = new System.Windows.Forms.Button();
            this.fieldGroupBox = new System.Windows.Forms.GroupBox();
            this.fieldsAvailableLst = new System.Windows.Forms.ListBox();
            this.fieldsSelectedLbl = new System.Windows.Forms.Label();
            this.fieldsSelectedLst = new System.Windows.Forms.ListBox();
            this.fieldsAvailableLbl = new System.Windows.Forms.Label();
            this.fieldAddBtn = new System.Windows.Forms.Button();
            this.fieldRemoveBtn = new System.Windows.Forms.Button();
            this.runBtn = new System.Windows.Forms.Button();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.makePDFBtn = new System.Windows.Forms.Button();
            this.makeExcelBtn = new System.Windows.Forms.Button();
            this.DatePickerGroupBox.SuspendLayout();
            this.cropGroupBox.SuspendLayout();
            this.jobGroupBox.SuspendLayout();
            this.fieldGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatePickerGroupBox
            // 
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndWeekLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndYearLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartWeekLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartYearLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndWeekTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndYearTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartWeekTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartYearTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerThruDateLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerThruDateTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerFromDateTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerFromDateLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerPresetsLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerPresetsCbx);
            this.DatePickerGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerGroupBox.Location = new System.Drawing.Point(64, 44);
            this.DatePickerGroupBox.Name = "DatePickerGroupBox";
            this.DatePickerGroupBox.Size = new System.Drawing.Size(684, 95);
            this.DatePickerGroupBox.TabIndex = 0;
            this.DatePickerGroupBox.TabStop = false;
            // 
            // DatePickerEndWeekLbl
            // 
            this.DatePickerEndWeekLbl.AutoSize = true;
            this.DatePickerEndWeekLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndWeekLbl.Location = new System.Drawing.Point(619, 11);
            this.DatePickerEndWeekLbl.Name = "DatePickerEndWeekLbl";
            this.DatePickerEndWeekLbl.Size = new System.Drawing.Size(47, 18);
            this.DatePickerEndWeekLbl.TabIndex = 13;
            this.DatePickerEndWeekLbl.Text = "Week";
            // 
            // DatePickerEndYearLbl
            // 
            this.DatePickerEndYearLbl.AutoSize = true;
            this.DatePickerEndYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndYearLbl.Location = new System.Drawing.Point(518, 11);
            this.DatePickerEndYearLbl.Name = "DatePickerEndYearLbl";
            this.DatePickerEndYearLbl.Size = new System.Drawing.Size(68, 18);
            this.DatePickerEndYearLbl.TabIndex = 12;
            this.DatePickerEndYearLbl.Text = "End Year";
            // 
            // DatePickerStartWeekLbl
            // 
            this.DatePickerStartWeekLbl.AutoSize = true;
            this.DatePickerStartWeekLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartWeekLbl.Location = new System.Drawing.Point(444, 11);
            this.DatePickerStartWeekLbl.Name = "DatePickerStartWeekLbl";
            this.DatePickerStartWeekLbl.Size = new System.Drawing.Size(47, 18);
            this.DatePickerStartWeekLbl.TabIndex = 11;
            this.DatePickerStartWeekLbl.Text = "Week";
            // 
            // DatePickerStartYearLbl
            // 
            this.DatePickerStartYearLbl.AutoSize = true;
            this.DatePickerStartYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartYearLbl.Location = new System.Drawing.Point(340, 11);
            this.DatePickerStartYearLbl.Name = "DatePickerStartYearLbl";
            this.DatePickerStartYearLbl.Size = new System.Drawing.Size(73, 18);
            this.DatePickerStartYearLbl.TabIndex = 9;
            this.DatePickerStartYearLbl.Text = "Start Year";
            // 
            // DatePickerEndWeekTxt
            // 
            this.DatePickerEndWeekTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndWeekTxt.Location = new System.Drawing.Point(615, 33);
            this.DatePickerEndWeekTxt.Name = "DatePickerEndWeekTxt";
            this.DatePickerEndWeekTxt.Size = new System.Drawing.Size(54, 24);
            this.DatePickerEndWeekTxt.TabIndex = 6;
            // 
            // DatePickerEndYearTxt
            // 
            this.DatePickerEndYearTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndYearTxt.Location = new System.Drawing.Point(502, 33);
            this.DatePickerEndYearTxt.Name = "DatePickerEndYearTxt";
            this.DatePickerEndYearTxt.Size = new System.Drawing.Size(105, 24);
            this.DatePickerEndYearTxt.TabIndex = 5;
            // 
            // DatePickerStartWeekTxt
            // 
            this.DatePickerStartWeekTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartWeekTxt.Location = new System.Drawing.Point(440, 33);
            this.DatePickerStartWeekTxt.Name = "DatePickerStartWeekTxt";
            this.DatePickerStartWeekTxt.Size = new System.Drawing.Size(54, 24);
            this.DatePickerStartWeekTxt.TabIndex = 4;
            // 
            // DatePickerStartYearTxt
            // 
            this.DatePickerStartYearTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartYearTxt.Location = new System.Drawing.Point(328, 33);
            this.DatePickerStartYearTxt.Name = "DatePickerStartYearTxt";
            this.DatePickerStartYearTxt.Size = new System.Drawing.Size(105, 24);
            this.DatePickerStartYearTxt.TabIndex = 3;
            // 
            // DatePickerThruDateLbl
            // 
            this.DatePickerThruDateLbl.AutoSize = true;
            this.DatePickerThruDateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerThruDateLbl.Location = new System.Drawing.Point(415, 68);
            this.DatePickerThruDateLbl.Name = "DatePickerThruDateLbl";
            this.DatePickerThruDateLbl.Size = new System.Drawing.Size(73, 18);
            this.DatePickerThruDateLbl.TabIndex = 14;
            this.DatePickerThruDateLbl.Text = "Thru Date";
            // 
            // DatePickerThruDateTxt
            // 
            this.DatePickerThruDateTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerThruDateTxt.Location = new System.Drawing.Point(494, 66);
            this.DatePickerThruDateTxt.Name = "DatePickerThruDateTxt";
            this.DatePickerThruDateTxt.Size = new System.Drawing.Size(144, 24);
            this.DatePickerThruDateTxt.TabIndex = 2;
            // 
            // DatePickerFromDateTxt
            // 
            this.DatePickerFromDateTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerFromDateTxt.Location = new System.Drawing.Point(164, 66);
            this.DatePickerFromDateTxt.Name = "DatePickerFromDateTxt";
            this.DatePickerFromDateTxt.Size = new System.Drawing.Size(144, 24);
            this.DatePickerFromDateTxt.TabIndex = 1;
            // 
            // DatePickerFromDateLbl
            // 
            this.DatePickerFromDateLbl.AutoSize = true;
            this.DatePickerFromDateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerFromDateLbl.Location = new System.Drawing.Point(79, 69);
            this.DatePickerFromDateLbl.Name = "DatePickerFromDateLbl";
            this.DatePickerFromDateLbl.Size = new System.Drawing.Size(79, 18);
            this.DatePickerFromDateLbl.TabIndex = 8;
            this.DatePickerFromDateLbl.Text = "From Date";
            // 
            // DatePickerPresetsLbl
            // 
            this.DatePickerPresetsLbl.AutoSize = true;
            this.DatePickerPresetsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerPresetsLbl.Location = new System.Drawing.Point(6, 11);
            this.DatePickerPresetsLbl.Name = "DatePickerPresetsLbl";
            this.DatePickerPresetsLbl.Size = new System.Drawing.Size(169, 18);
            this.DatePickerPresetsLbl.TabIndex = 7;
            this.DatePickerPresetsLbl.Text = "Date Selections for Date";
            // 
            // DatePickerPresetsCbx
            // 
            this.DatePickerPresetsCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DatePickerPresetsCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerPresetsCbx.FormattingEnabled = true;
            this.DatePickerPresetsCbx.Location = new System.Drawing.Point(6, 33);
            this.DatePickerPresetsCbx.Name = "DatePickerPresetsCbx";
            this.DatePickerPresetsCbx.Size = new System.Drawing.Size(271, 26);
            this.DatePickerPresetsCbx.TabIndex = 0;
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(446, 486);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(95, 35);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Text = "&Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(323, 29);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(223, 20);
            this.subtitleLbl.TabIndex = 8;
            this.subtitleLbl.Text = "Pay Data By Employee Report";
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLbl.Location = new System.Drawing.Point(259, 5);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(352, 24);
            this.titleLbl.TabIndex = 7;
            this.titleLbl.Text = "KeyCentral® Easy Payroll Data Collection";
            // 
            // cropGroupBox
            // 
            this.cropGroupBox.Controls.Add(this.cropsAvailableLst);
            this.cropGroupBox.Controls.Add(this.cropsSelectedLbl);
            this.cropGroupBox.Controls.Add(this.cropsSelectedLst);
            this.cropGroupBox.Controls.Add(this.cropsAvailableLbl);
            this.cropGroupBox.Controls.Add(this.cropAddBtn);
            this.cropGroupBox.Controls.Add(this.cropRemoveBtn);
            this.cropGroupBox.Location = new System.Drawing.Point(18, 138);
            this.cropGroupBox.Name = "cropGroupBox";
            this.cropGroupBox.Size = new System.Drawing.Size(792, 117);
            this.cropGroupBox.TabIndex = 2;
            this.cropGroupBox.TabStop = false;
            // 
            // cropsAvailableLst
            // 
            this.cropsAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsAvailableLst.FormattingEnabled = true;
            this.cropsAvailableLst.ItemHeight = 18;
            this.cropsAvailableLst.Sorted = true;
            this.cropsAvailableLst.Location = new System.Drawing.Point(6, 27);
            this.cropsAvailableLst.Name = "cropsAvailableLst";
            this.cropsAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.cropsAvailableLst.TabIndex = 0;
            // 
            // cropsSelectedLbl
            // 
            this.cropsSelectedLbl.AutoSize = true;
            this.cropsSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsSelectedLbl.Location = new System.Drawing.Point(471, 8);
            this.cropsSelectedLbl.Name = "cropsSelectedLbl";
            this.cropsSelectedLbl.Size = new System.Drawing.Size(110, 18);
            this.cropsSelectedLbl.TabIndex = 5;
            this.cropsSelectedLbl.Text = "Selected Crops";
            // 
            // cropsSelectedLst
            // 
            this.cropsSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsSelectedLst.FormattingEnabled = true;
            this.cropsSelectedLst.ItemHeight = 18;
            this.cropsSelectedLst.Sorted = true;
            this.cropsSelectedLst.Location = new System.Drawing.Point(474, 27);
            this.cropsSelectedLst.Name = "cropsSelectedLst";
            this.cropsSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.cropsSelectedLst.TabIndex = 1;
            // 
            // cropsAvailableLbl
            // 
            this.cropsAvailableLbl.AutoSize = true;
            this.cropsAvailableLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsAvailableLbl.Location = new System.Drawing.Point(6, 8);
            this.cropsAvailableLbl.Name = "cropsAvailableLbl";
            this.cropsAvailableLbl.Size = new System.Drawing.Size(110, 18);
            this.cropsAvailableLbl.TabIndex = 4;
            this.cropsAvailableLbl.Text = "Available Crops";
            // 
            // cropAddBtn
            // 
            this.cropAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropAddBtn.Location = new System.Drawing.Point(348, 27);
            this.cropAddBtn.Name = "cropAddBtn";
            this.cropAddBtn.Size = new System.Drawing.Size(95, 35);
            this.cropAddBtn.TabIndex = 2;
            this.cropAddBtn.Text = ">";
            this.cropAddBtn.UseVisualStyleBackColor = true;
            // 
            // cropRemoveBtn
            // 
            this.cropRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropRemoveBtn.Location = new System.Drawing.Point(348, 68);
            this.cropRemoveBtn.Name = "cropRemoveBtn";
            this.cropRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.cropRemoveBtn.TabIndex = 3;
            this.cropRemoveBtn.Text = "<";
            this.cropRemoveBtn.UseVisualStyleBackColor = true;
            // 
            // jobGroupBox
            // 
            this.jobGroupBox.Controls.Add(this.jobsAvailableLst);
            this.jobGroupBox.Controls.Add(this.jobsSelectedLbl);
            this.jobGroupBox.Controls.Add(this.jobsSelectedLst);
            this.jobGroupBox.Controls.Add(this.jobsAvailableLbl);
            this.jobGroupBox.Controls.Add(this.jobAddBtn);
            this.jobGroupBox.Controls.Add(this.jobRemoveBtn);
            this.jobGroupBox.Location = new System.Drawing.Point(18, 253);
            this.jobGroupBox.Name = "jobGroupBox";
            this.jobGroupBox.Size = new System.Drawing.Size(792, 117);
            this.jobGroupBox.TabIndex = 3;
            this.jobGroupBox.TabStop = false;
            // 
            // jobsAvailableLst
            // 
            this.jobsAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsAvailableLst.FormattingEnabled = true;
            this.jobsAvailableLst.ItemHeight = 18;
            this.jobsAvailableLst.Sorted = true;
            this.jobsAvailableLst.Location = new System.Drawing.Point(6, 27);
            this.jobsAvailableLst.Name = "jobsAvailableLst";
            this.jobsAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.jobsAvailableLst.TabIndex = 0;
            // 
            // jobsSelectedLbl
            // 
            this.jobsSelectedLbl.AutoSize = true;
            this.jobsSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsSelectedLbl.Location = new System.Drawing.Point(471, 8);
            this.jobsSelectedLbl.Name = "jobsSelectedLbl";
            this.jobsSelectedLbl.Size = new System.Drawing.Size(102, 18);
            this.jobsSelectedLbl.TabIndex = 5;
            this.jobsSelectedLbl.Text = "Selected Jobs";
            // 
            // jobsSelectedLst
            // 
            this.jobsSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsSelectedLst.FormattingEnabled = true;
            this.jobsSelectedLst.ItemHeight = 18;
            this.jobsSelectedLst.Sorted = true;
            this.jobsSelectedLst.Location = new System.Drawing.Point(474, 27);
            this.jobsSelectedLst.Name = "jobsSelectedLst";
            this.jobsSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.jobsSelectedLst.TabIndex = 1;
            // 
            // jobsAvailableLbl
            // 
            this.jobsAvailableLbl.AutoSize = true;
            this.jobsAvailableLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsAvailableLbl.Location = new System.Drawing.Point(3, 8);
            this.jobsAvailableLbl.Name = "jobsAvailableLbl";
            this.jobsAvailableLbl.Size = new System.Drawing.Size(102, 18);
            this.jobsAvailableLbl.TabIndex = 4;
            this.jobsAvailableLbl.Text = "Available Jobs";
            // 
            // jobAddBtn
            // 
            this.jobAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobAddBtn.Location = new System.Drawing.Point(348, 27);
            this.jobAddBtn.Name = "jobAddBtn";
            this.jobAddBtn.Size = new System.Drawing.Size(95, 35);
            this.jobAddBtn.TabIndex = 2;
            this.jobAddBtn.Text = ">";
            this.jobAddBtn.UseVisualStyleBackColor = true;
            // 
            // jobRemoveBtn
            // 
            this.jobRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobRemoveBtn.Location = new System.Drawing.Point(348, 68);
            this.jobRemoveBtn.Name = "jobRemoveBtn";
            this.jobRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.jobRemoveBtn.TabIndex = 3;
            this.jobRemoveBtn.Text = "<";
            this.jobRemoveBtn.UseVisualStyleBackColor = true;
            // 
            // fieldGroupBox
            // 
            this.fieldGroupBox.Controls.Add(this.fieldsAvailableLst);
            this.fieldGroupBox.Controls.Add(this.fieldsSelectedLbl);
            this.fieldGroupBox.Controls.Add(this.fieldsSelectedLst);
            this.fieldGroupBox.Controls.Add(this.fieldsAvailableLbl);
            this.fieldGroupBox.Controls.Add(this.fieldAddBtn);
            this.fieldGroupBox.Controls.Add(this.fieldRemoveBtn);
            this.fieldGroupBox.Location = new System.Drawing.Point(18, 368);
            this.fieldGroupBox.Name = "fieldGroupBox";
            this.fieldGroupBox.Size = new System.Drawing.Size(792, 117);
            this.fieldGroupBox.TabIndex = 4;
            this.fieldGroupBox.TabStop = false;
            // 
            // fieldsAvailableLst
            // 
            this.fieldsAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsAvailableLst.FormattingEnabled = true;
            this.fieldsAvailableLst.ItemHeight = 18;
            this.fieldsAvailableLst.Sorted = true;
            this.fieldsAvailableLst.Location = new System.Drawing.Point(6, 27);
            this.fieldsAvailableLst.Name = "fieldsAvailableLst";
            this.fieldsAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.fieldsAvailableLst.TabIndex = 0;
            // 
            // fieldsSelectedLbl
            // 
            this.fieldsSelectedLbl.AutoSize = true;
            this.fieldsSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsSelectedLbl.Location = new System.Drawing.Point(471, 8);
            this.fieldsSelectedLbl.Name = "fieldsSelectedLbl";
            this.fieldsSelectedLbl.Size = new System.Drawing.Size(108, 18);
            this.fieldsSelectedLbl.TabIndex = 5;
            this.fieldsSelectedLbl.Text = "Selected Fields";
            this.fieldsSelectedLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldsSelectedLst
            // 
            this.fieldsSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsSelectedLst.FormattingEnabled = true;
            this.fieldsSelectedLst.ItemHeight = 18;
            this.fieldsSelectedLst.Sorted = true;
            this.fieldsSelectedLst.Location = new System.Drawing.Point(474, 27);
            this.fieldsSelectedLst.Name = "fieldsSelectedLst";
            this.fieldsSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.fieldsSelectedLst.TabIndex = 1;
            // 
            // fieldsAvailableLbl
            // 
            this.fieldsAvailableLbl.AutoSize = true;
            this.fieldsAvailableLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsAvailableLbl.Location = new System.Drawing.Point(3, 8);
            this.fieldsAvailableLbl.Name = "fieldsAvailableLbl";
            this.fieldsAvailableLbl.Size = new System.Drawing.Size(108, 18);
            this.fieldsAvailableLbl.TabIndex = 4;
            this.fieldsAvailableLbl.Text = "Available Fields";
            // 
            // fieldAddBtn
            // 
            this.fieldAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldAddBtn.Location = new System.Drawing.Point(348, 27);
            this.fieldAddBtn.Name = "fieldAddBtn";
            this.fieldAddBtn.Size = new System.Drawing.Size(95, 35);
            this.fieldAddBtn.TabIndex = 2;
            this.fieldAddBtn.Text = ">";
            this.fieldAddBtn.UseVisualStyleBackColor = true;
            // 
            // fieldRemoveBtn
            // 
            this.fieldRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldRemoveBtn.Location = new System.Drawing.Point(348, 68);
            this.fieldRemoveBtn.Name = "fieldRemoveBtn";
            this.fieldRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.fieldRemoveBtn.TabIndex = 3;
            this.fieldRemoveBtn.Text = "<";
            this.fieldRemoveBtn.UseVisualStyleBackColor = true;
            // 
            // runBtn
            // 
            this.runBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runBtn.Location = new System.Drawing.Point(719, 486);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(95, 35);
            this.runBtn.TabIndex = 6;
            this.runBtn.Text = "&Run";
            this.runBtn.UseVisualStyleBackColor = true;
            this.runBtn.Visible = false;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 5);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 9;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // makePDFBtn
            // 
            this.makePDFBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.makePDFBtn.Location = new System.Drawing.Point(293, 486);
            this.makePDFBtn.Name = "makePDFBtn";
            this.makePDFBtn.Size = new System.Drawing.Size(147, 35);
            this.makePDFBtn.TabIndex = 10;
            this.makePDFBtn.Text = "Make PDF Report";
            this.makePDFBtn.UseVisualStyleBackColor = true;
            this.makePDFBtn.Click += new System.EventHandler(this.makePDFBtn_Click);
            // 
            // makeExcelBtn
            // 
            this.makeExcelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.makeExcelBtn.Location = new System.Drawing.Point(24, 486);
            this.makeExcelBtn.Name = "makeExcelBtn";
            this.makeExcelBtn.Size = new System.Drawing.Size(96, 35);
            this.makeExcelBtn.TabIndex = 11;
            this.makeExcelBtn.Text = "Make Excel";
            this.makeExcelBtn.UseVisualStyleBackColor = true;
            this.makeExcelBtn.Visible = false;
            this.makeExcelBtn.Click += new System.EventHandler(this.makeExcelBtn_Click);
            // 
            // PayDataByEmployee_Criteria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(839, 524);
            this.Controls.Add(this.makeExcelBtn);
            this.Controls.Add(this.makePDFBtn);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.DatePickerGroupBox);
            this.Controls.Add(this.cropGroupBox);
            this.Controls.Add(this.jobGroupBox);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.fieldGroupBox);
            this.Controls.Add(this.titleLbl);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PayDataByEmployee_Criteria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Easy Payroll Data Collection - Pay Data Report";
            this.DatePickerGroupBox.ResumeLayout(false);
            this.DatePickerGroupBox.PerformLayout();
            this.cropGroupBox.ResumeLayout(false);
            this.cropGroupBox.PerformLayout();
            this.jobGroupBox.ResumeLayout(false);
            this.jobGroupBox.PerformLayout();
            this.fieldGroupBox.ResumeLayout(false);
            this.fieldGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox DatePickerGroupBox;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label DatePickerPresetsLbl;
        private System.Windows.Forms.ComboBox DatePickerPresetsCbx;
        private System.Windows.Forms.TextBox DatePickerThruDateTxt;
        private System.Windows.Forms.TextBox DatePickerFromDateTxt;
        private System.Windows.Forms.Label DatePickerFromDateLbl;
        private System.Windows.Forms.TextBox DatePickerEndWeekTxt;
        private System.Windows.Forms.TextBox DatePickerEndYearTxt;
        private System.Windows.Forms.TextBox DatePickerStartWeekTxt;
        private System.Windows.Forms.TextBox DatePickerStartYearTxt;
		private System.Windows.Forms.Label DatePickerThruDateLbl;
        private System.Windows.Forms.Label DatePickerStartYearLbl;
        private System.Windows.Forms.Label DatePickerEndWeekLbl;
        private System.Windows.Forms.Label DatePickerEndYearLbl;
        private System.Windows.Forms.Label DatePickerStartWeekLbl;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Label titleLbl;
        private System.Windows.Forms.GroupBox cropGroupBox;
        private System.Windows.Forms.ListBox cropsAvailableLst;
        private System.Windows.Forms.Label cropsSelectedLbl;
        private System.Windows.Forms.ListBox cropsSelectedLst;
        private System.Windows.Forms.Label cropsAvailableLbl;
        private System.Windows.Forms.Button cropAddBtn;
        private System.Windows.Forms.Button cropRemoveBtn;
        private System.Windows.Forms.GroupBox jobGroupBox;
        private System.Windows.Forms.ListBox jobsAvailableLst;
        private System.Windows.Forms.Label jobsSelectedLbl;
        private System.Windows.Forms.ListBox jobsSelectedLst;
        private System.Windows.Forms.Label jobsAvailableLbl;
        private System.Windows.Forms.Button jobAddBtn;
        private System.Windows.Forms.Button jobRemoveBtn;
        private System.Windows.Forms.GroupBox fieldGroupBox;
        private System.Windows.Forms.ListBox fieldsAvailableLst;
        private System.Windows.Forms.Label fieldsSelectedLbl;
        private System.Windows.Forms.ListBox fieldsSelectedLst;
        private System.Windows.Forms.Label fieldsAvailableLbl;
        private System.Windows.Forms.Button fieldAddBtn;
        private System.Windows.Forms.Button fieldRemoveBtn;
        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.Button makePDFBtn;
        private System.Windows.Forms.Button makeExcelBtn;
    }
}