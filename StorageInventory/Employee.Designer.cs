﻿namespace StorageInventory
{
    partial class Employee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitBtn = new System.Windows.Forms.Button();
            this.codeTxt = new System.Windows.Forms.TextBox();
            this.codeLbl = new System.Windows.Forms.Label();
            this.nameLbl = new System.Windows.Forms.Label();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.savedEmployeesLst = new System.Windows.Forms.ListBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.savedEmployeesLbl = new System.Windows.Forms.Label();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.saveEditedBtn = new System.Windows.Forms.Button();
            this.enableDisableBtn = new System.Windows.Forms.Button();
            this.keyTxt = new System.Windows.Forms.TextBox();
            this.activeRdo = new System.Windows.Forms.RadioButton();
            this.inactiveRdo = new System.Windows.Forms.RadioButton();
            this.preTaxAdjustmentKeyTxt = new System.Windows.Forms.TextBox();
            this.deletePreTaxAdjustmentBtn = new System.Windows.Forms.Button();
            this.saveEditedPreTaxAdjustmentBtn = new System.Windows.Forms.Button();
            this.editPreTaxAdjustmentDescriptionLbl = new System.Windows.Forms.Label();
            this.editPreTaxAdjustmentDescriptionTxt = new System.Windows.Forms.TextBox();
            this.editPreTaxAdjustmentCodeLbl = new System.Windows.Forms.Label();
            this.editPreTaxAdjustmentCodeTxt = new System.Windows.Forms.TextBox();
            this.savedPreTaxAdjustmentsLbl = new System.Windows.Forms.Label();
            this.saveNewPreTaxAdjustmentBtn = new System.Windows.Forms.Button();
            this.savedPreTaxAdjustmentsLst = new System.Windows.Forms.ListBox();
            this.newPreTaxAdjustmentDescriptionLbl = new System.Windows.Forms.Label();
            this.newPreTaxAdjustmentDescriptionTxt = new System.Windows.Forms.TextBox();
            this.newPreTaxAdjustmentCodeLbl = new System.Windows.Forms.Label();
            this.newPreTaxAdjustmentCodeTxt = new System.Windows.Forms.TextBox();
            this.newPreTaxAdjustmentAmountTxt = new System.Windows.Forms.TextBox();
            this.editPreTaxAdjustmentAmountTxt = new System.Windows.Forms.TextBox();
            this.newPreTaxAdjustmentAmountLbl = new System.Windows.Forms.Label();
            this.editPreTaxAdjustmentAmountLbl = new System.Windows.Forms.Label();
            this.hourlyWageLbl = new System.Windows.Forms.Label();
            this.hourlyWageTxt = new System.Windows.Forms.TextBox();
            this.overtimeByAverageWageChk = new System.Windows.Forms.CheckBox();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.address1Lbl = new System.Windows.Forms.Label();
            this.address1Txt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.address2Txt = new System.Windows.Forms.TextBox();
            this.warningPnl = new System.Windows.Forms.Panel();
            this.warnNoBtn = new System.Windows.Forms.Button();
            this.warnYesBtn = new System.Windows.Forms.Button();
            this.warningLbl = new System.Windows.Forms.Label();
            this.newEmployeeBtn = new System.Windows.Forms.Button();
            this.deleteBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.groupTxt = new System.Windows.Forms.TextBox();
            this.cityLbl = new System.Windows.Forms.Label();
            this.city2Txt = new System.Windows.Forms.TextBox();
            this.stateLbl = new System.Windows.Forms.Label();
            this.state2Txt = new System.Windows.Forms.TextBox();
            this.zip2Lbl = new System.Windows.Forms.Label();
            this.zip2Txt = new System.Windows.Forms.TextBox();
            this.SSNLbl = new System.Windows.Forms.Label();
            this.SSN2Txt = new System.Windows.Forms.TextBox();
            this.overtimeNoRdo = new System.Windows.Forms.RadioButton();
            this.overtimeYesRdo = new System.Windows.Forms.RadioButton();
            this.overtimeLbl = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.employeeListBtn = new System.Windows.Forms.Button();
            this.MakeExcelBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.warningPnl.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(469, 532);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(92, 34);
            this.exitBtn.TabIndex = 15;
            this.exitBtn.Text = "E&xit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // codeTxt
            // 
            this.codeTxt.AcceptsTab = true;
            this.codeTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeTxt.Location = new System.Drawing.Point(449, 73);
            this.codeTxt.MaxLength = 16;
            this.codeTxt.Name = "codeTxt";
            this.codeTxt.Size = new System.Drawing.Size(183, 24);
            this.codeTxt.TabIndex = 0;
            this.codeTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.codeTxt_KeyDown);
            // 
            // codeLbl
            // 
            this.codeLbl.AutoSize = true;
            this.codeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeLbl.Location = new System.Drawing.Point(399, 76);
            this.codeLbl.Name = "codeLbl";
            this.codeLbl.Size = new System.Drawing.Size(48, 18);
            this.codeLbl.TabIndex = 101;
            this.codeLbl.Text = "Code:";
            this.codeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameLbl.Location = new System.Drawing.Point(395, 106);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(52, 18);
            this.nameLbl.TabIndex = 102;
            this.nameLbl.Text = "Name:";
            this.nameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // nameTxt
            // 
            this.nameTxt.AcceptsTab = true;
            this.nameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nameTxt.Location = new System.Drawing.Point(449, 103);
            this.nameTxt.MaxLength = 32;
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(249, 24);
            this.nameTxt.TabIndex = 1;
            this.nameTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.nameTxt_KeyDown);
            // 
            // savedEmployeesLst
            // 
            this.savedEmployeesLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedEmployeesLst.FormattingEnabled = true;
            this.savedEmployeesLst.ItemHeight = 18;
            this.savedEmployeesLst.Location = new System.Drawing.Point(12, 86);
            this.savedEmployeesLst.Name = "savedEmployeesLst";
            this.savedEmployeesLst.Size = new System.Drawing.Size(306, 184);
            this.savedEmployeesLst.Sorted = true;
            this.savedEmployeesLst.TabIndex = 20;
            this.savedEmployeesLst.SelectedIndexChanged += new System.EventHandler(this.savedEmployeesLst_SelectedIndexChanged);
            // 
            // saveBtn
            // 
            this.saveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveBtn.Location = new System.Drawing.Point(342, 420);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(121, 29);
            this.saveBtn.TabIndex = 11;
            this.saveBtn.Text = "&Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            this.saveBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.saveBtn_KeyDown);
            // 
            // savedEmployeesLbl
            // 
            this.savedEmployeesLbl.AutoSize = true;
            this.savedEmployeesLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedEmployeesLbl.Location = new System.Drawing.Point(12, 62);
            this.savedEmployeesLbl.Name = "savedEmployeesLbl";
            this.savedEmployeesLbl.Size = new System.Drawing.Size(127, 18);
            this.savedEmployeesLbl.TabIndex = 24;
            this.savedEmployeesLbl.Text = "Saved Customers";
            this.savedEmployeesLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(415, 47);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(78, 20);
            this.subtitleLbl.TabIndex = 23;
            this.subtitleLbl.Text = "Customer";
            // 
            // saveEditedBtn
            // 
            this.saveEditedBtn.Enabled = false;
            this.saveEditedBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveEditedBtn.Location = new System.Drawing.Point(16, 534);
            this.saveEditedBtn.Name = "saveEditedBtn";
            this.saveEditedBtn.Size = new System.Drawing.Size(127, 30);
            this.saveEditedBtn.TabIndex = 131;
            this.saveEditedBtn.Text = "Save &Edited Employee";
            this.saveEditedBtn.UseVisualStyleBackColor = true;
            this.saveEditedBtn.Visible = false;
            // 
            // enableDisableBtn
            // 
            this.enableDisableBtn.Enabled = false;
            this.enableDisableBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableDisableBtn.Location = new System.Drawing.Point(149, 534);
            this.enableDisableBtn.Name = "enableDisableBtn";
            this.enableDisableBtn.Size = new System.Drawing.Size(103, 30);
            this.enableDisableBtn.TabIndex = 141;
            this.enableDisableBtn.Text = "&Disable";
            this.enableDisableBtn.UseVisualStyleBackColor = true;
            this.enableDisableBtn.Visible = false;
            // 
            // keyTxt
            // 
            this.keyTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keyTxt.Location = new System.Drawing.Point(16, 33);
            this.keyTxt.Name = "keyTxt";
            this.keyTxt.Size = new System.Drawing.Size(64, 24);
            this.keyTxt.TabIndex = 33;
            this.keyTxt.Visible = false;
            // 
            // activeRdo
            // 
            this.activeRdo.AutoSize = true;
            this.activeRdo.Checked = true;
            this.activeRdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.activeRdo.Location = new System.Drawing.Point(1, 7);
            this.activeRdo.Name = "activeRdo";
            this.activeRdo.Size = new System.Drawing.Size(65, 22);
            this.activeRdo.TabIndex = 25;
            this.activeRdo.TabStop = true;
            this.activeRdo.Text = "Active";
            this.activeRdo.UseVisualStyleBackColor = true;
            this.activeRdo.CheckedChanged += new System.EventHandler(this.activeRdo_CheckedChanged);
            // 
            // inactiveRdo
            // 
            this.inactiveRdo.AutoSize = true;
            this.inactiveRdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inactiveRdo.Location = new System.Drawing.Point(84, 7);
            this.inactiveRdo.Name = "inactiveRdo";
            this.inactiveRdo.Size = new System.Drawing.Size(75, 22);
            this.inactiveRdo.TabIndex = 26;
            this.inactiveRdo.Text = "Inactive";
            this.inactiveRdo.UseVisualStyleBackColor = true;
            this.inactiveRdo.CheckedChanged += new System.EventHandler(this.inactiveRdo_CheckedChanged);
            // 
            // preTaxAdjustmentKeyTxt
            // 
            this.preTaxAdjustmentKeyTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.preTaxAdjustmentKeyTxt.Location = new System.Drawing.Point(324, 201);
            this.preTaxAdjustmentKeyTxt.Name = "preTaxAdjustmentKeyTxt";
            this.preTaxAdjustmentKeyTxt.Size = new System.Drawing.Size(64, 24);
            this.preTaxAdjustmentKeyTxt.TabIndex = 43;
            this.preTaxAdjustmentKeyTxt.Visible = false;
            // 
            // deletePreTaxAdjustmentBtn
            // 
            this.deletePreTaxAdjustmentBtn.Enabled = false;
            this.deletePreTaxAdjustmentBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deletePreTaxAdjustmentBtn.Location = new System.Drawing.Point(792, 205);
            this.deletePreTaxAdjustmentBtn.Name = "deletePreTaxAdjustmentBtn";
            this.deletePreTaxAdjustmentBtn.Size = new System.Drawing.Size(103, 30);
            this.deletePreTaxAdjustmentBtn.TabIndex = 18;
            this.deletePreTaxAdjustmentBtn.Text = "De&lete";
            this.deletePreTaxAdjustmentBtn.UseVisualStyleBackColor = true;
            this.deletePreTaxAdjustmentBtn.Visible = false;
            this.deletePreTaxAdjustmentBtn.Click += new System.EventHandler(this.deletePreTaxAdjustmentBtn_Click);
            // 
            // saveEditedPreTaxAdjustmentBtn
            // 
            this.saveEditedPreTaxAdjustmentBtn.Enabled = false;
            this.saveEditedPreTaxAdjustmentBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveEditedPreTaxAdjustmentBtn.Location = new System.Drawing.Point(607, 205);
            this.saveEditedPreTaxAdjustmentBtn.Name = "saveEditedPreTaxAdjustmentBtn";
            this.saveEditedPreTaxAdjustmentBtn.Size = new System.Drawing.Size(169, 30);
            this.saveEditedPreTaxAdjustmentBtn.TabIndex = 17;
            this.saveEditedPreTaxAdjustmentBtn.Text = "Save Edited Pre-&Tax Adjustment";
            this.saveEditedPreTaxAdjustmentBtn.UseVisualStyleBackColor = true;
            this.saveEditedPreTaxAdjustmentBtn.Visible = false;
            this.saveEditedPreTaxAdjustmentBtn.Click += new System.EventHandler(this.saveEditedPreTaxAdjustmentBtn_Click);
            // 
            // editPreTaxAdjustmentDescriptionLbl
            // 
            this.editPreTaxAdjustmentDescriptionLbl.AutoSize = true;
            this.editPreTaxAdjustmentDescriptionLbl.Enabled = false;
            this.editPreTaxAdjustmentDescriptionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editPreTaxAdjustmentDescriptionLbl.Location = new System.Drawing.Point(574, 133);
            this.editPreTaxAdjustmentDescriptionLbl.Name = "editPreTaxAdjustmentDescriptionLbl";
            this.editPreTaxAdjustmentDescriptionLbl.Size = new System.Drawing.Size(160, 36);
            this.editPreTaxAdjustmentDescriptionLbl.TabIndex = 40;
            this.editPreTaxAdjustmentDescriptionLbl.Text = "Edit Pre-Tax\r\nAdjustment Description";
            this.editPreTaxAdjustmentDescriptionLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.editPreTaxAdjustmentDescriptionLbl.Visible = false;
            // 
            // editPreTaxAdjustmentDescriptionTxt
            // 
            this.editPreTaxAdjustmentDescriptionTxt.AcceptsReturn = true;
            this.editPreTaxAdjustmentDescriptionTxt.AcceptsTab = true;
            this.editPreTaxAdjustmentDescriptionTxt.Enabled = false;
            this.editPreTaxAdjustmentDescriptionTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editPreTaxAdjustmentDescriptionTxt.Location = new System.Drawing.Point(529, 175);
            this.editPreTaxAdjustmentDescriptionTxt.MaxLength = 32;
            this.editPreTaxAdjustmentDescriptionTxt.Name = "editPreTaxAdjustmentDescriptionTxt";
            this.editPreTaxAdjustmentDescriptionTxt.Size = new System.Drawing.Size(249, 24);
            this.editPreTaxAdjustmentDescriptionTxt.TabIndex = 15;
            this.editPreTaxAdjustmentDescriptionTxt.Visible = false;
            // 
            // editPreTaxAdjustmentCodeLbl
            // 
            this.editPreTaxAdjustmentCodeLbl.AutoSize = true;
            this.editPreTaxAdjustmentCodeLbl.Enabled = false;
            this.editPreTaxAdjustmentCodeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editPreTaxAdjustmentCodeLbl.Location = new System.Drawing.Point(349, 136);
            this.editPreTaxAdjustmentCodeLbl.Name = "editPreTaxAdjustmentCodeLbl";
            this.editPreTaxAdjustmentCodeLbl.Size = new System.Drawing.Size(121, 36);
            this.editPreTaxAdjustmentCodeLbl.TabIndex = 39;
            this.editPreTaxAdjustmentCodeLbl.Text = "Edit Pre-Tax\r\nAdjustment Code";
            this.editPreTaxAdjustmentCodeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.editPreTaxAdjustmentCodeLbl.Visible = false;
            // 
            // editPreTaxAdjustmentCodeTxt
            // 
            this.editPreTaxAdjustmentCodeTxt.AcceptsReturn = true;
            this.editPreTaxAdjustmentCodeTxt.AcceptsTab = true;
            this.editPreTaxAdjustmentCodeTxt.Enabled = false;
            this.editPreTaxAdjustmentCodeTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editPreTaxAdjustmentCodeTxt.Location = new System.Drawing.Point(324, 175);
            this.editPreTaxAdjustmentCodeTxt.MaxLength = 16;
            this.editPreTaxAdjustmentCodeTxt.Name = "editPreTaxAdjustmentCodeTxt";
            this.editPreTaxAdjustmentCodeTxt.Size = new System.Drawing.Size(183, 24);
            this.editPreTaxAdjustmentCodeTxt.TabIndex = 14;
            this.editPreTaxAdjustmentCodeTxt.Visible = false;
            this.editPreTaxAdjustmentCodeTxt.Leave += new System.EventHandler(this.editPreTaxAdjustmentCodeTxt_Leave);
            // 
            // savedPreTaxAdjustmentsLbl
            // 
            this.savedPreTaxAdjustmentsLbl.AutoSize = true;
            this.savedPreTaxAdjustmentsLbl.Enabled = false;
            this.savedPreTaxAdjustmentsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedPreTaxAdjustmentsLbl.Location = new System.Drawing.Point(12, 24);
            this.savedPreTaxAdjustmentsLbl.Name = "savedPreTaxAdjustmentsLbl";
            this.savedPreTaxAdjustmentsLbl.Size = new System.Drawing.Size(190, 18);
            this.savedPreTaxAdjustmentsLbl.TabIndex = 34;
            this.savedPreTaxAdjustmentsLbl.Text = "Saved Pre-Tax Adjustments";
            this.savedPreTaxAdjustmentsLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.savedPreTaxAdjustmentsLbl.Visible = false;
            // 
            // saveNewPreTaxAdjustmentBtn
            // 
            this.saveNewPreTaxAdjustmentBtn.Enabled = false;
            this.saveNewPreTaxAdjustmentBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveNewPreTaxAdjustmentBtn.Location = new System.Drawing.Point(726, 71);
            this.saveNewPreTaxAdjustmentBtn.Name = "saveNewPreTaxAdjustmentBtn";
            this.saveNewPreTaxAdjustmentBtn.Size = new System.Drawing.Size(169, 28);
            this.saveNewPreTaxAdjustmentBtn.TabIndex = 13;
            this.saveNewPreTaxAdjustmentBtn.Text = "Save New &Pre-Tax Adjustment";
            this.saveNewPreTaxAdjustmentBtn.UseVisualStyleBackColor = true;
            this.saveNewPreTaxAdjustmentBtn.Visible = false;
            this.saveNewPreTaxAdjustmentBtn.Click += new System.EventHandler(this.saveNewPreTaxAdjustmentBtn_Click);
            // 
            // savedPreTaxAdjustmentsLst
            // 
            this.savedPreTaxAdjustmentsLst.Enabled = false;
            this.savedPreTaxAdjustmentsLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedPreTaxAdjustmentsLst.FormattingEnabled = true;
            this.savedPreTaxAdjustmentsLst.ItemHeight = 18;
            this.savedPreTaxAdjustmentsLst.Location = new System.Drawing.Point(12, 47);
            this.savedPreTaxAdjustmentsLst.Name = "savedPreTaxAdjustmentsLst";
            this.savedPreTaxAdjustmentsLst.Size = new System.Drawing.Size(306, 184);
            this.savedPreTaxAdjustmentsLst.TabIndex = 21;
            this.savedPreTaxAdjustmentsLst.Visible = false;
            this.savedPreTaxAdjustmentsLst.SelectedIndexChanged += new System.EventHandler(this.savedPreTaxAdjustmentsLst_SelectedIndexChanged);
            // 
            // newPreTaxAdjustmentDescriptionLbl
            // 
            this.newPreTaxAdjustmentDescriptionLbl.AutoSize = true;
            this.newPreTaxAdjustmentDescriptionLbl.Enabled = false;
            this.newPreTaxAdjustmentDescriptionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPreTaxAdjustmentDescriptionLbl.Location = new System.Drawing.Point(574, 1);
            this.newPreTaxAdjustmentDescriptionLbl.Name = "newPreTaxAdjustmentDescriptionLbl";
            this.newPreTaxAdjustmentDescriptionLbl.Size = new System.Drawing.Size(160, 36);
            this.newPreTaxAdjustmentDescriptionLbl.TabIndex = 36;
            this.newPreTaxAdjustmentDescriptionLbl.Text = "New Pre-Tax\r\nAdjustment Description";
            this.newPreTaxAdjustmentDescriptionLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newPreTaxAdjustmentDescriptionLbl.Visible = false;
            // 
            // newPreTaxAdjustmentDescriptionTxt
            // 
            this.newPreTaxAdjustmentDescriptionTxt.AcceptsReturn = true;
            this.newPreTaxAdjustmentDescriptionTxt.AcceptsTab = true;
            this.newPreTaxAdjustmentDescriptionTxt.Enabled = false;
            this.newPreTaxAdjustmentDescriptionTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPreTaxAdjustmentDescriptionTxt.Location = new System.Drawing.Point(529, 40);
            this.newPreTaxAdjustmentDescriptionTxt.MaxLength = 32;
            this.newPreTaxAdjustmentDescriptionTxt.Name = "newPreTaxAdjustmentDescriptionTxt";
            this.newPreTaxAdjustmentDescriptionTxt.Size = new System.Drawing.Size(249, 24);
            this.newPreTaxAdjustmentDescriptionTxt.TabIndex = 11;
            this.newPreTaxAdjustmentDescriptionTxt.Visible = false;
            this.newPreTaxAdjustmentDescriptionTxt.Enter += new System.EventHandler(this.newPreTaxAdjustmentDescriptionTxt_Enter);
            // 
            // newPreTaxAdjustmentCodeLbl
            // 
            this.newPreTaxAdjustmentCodeLbl.AutoSize = true;
            this.newPreTaxAdjustmentCodeLbl.Enabled = false;
            this.newPreTaxAdjustmentCodeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPreTaxAdjustmentCodeLbl.Location = new System.Drawing.Point(349, 1);
            this.newPreTaxAdjustmentCodeLbl.Name = "newPreTaxAdjustmentCodeLbl";
            this.newPreTaxAdjustmentCodeLbl.Size = new System.Drawing.Size(121, 36);
            this.newPreTaxAdjustmentCodeLbl.TabIndex = 35;
            this.newPreTaxAdjustmentCodeLbl.Text = "New Pre-Tax\r\nAdjustment Code";
            this.newPreTaxAdjustmentCodeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newPreTaxAdjustmentCodeLbl.Visible = false;
            // 
            // newPreTaxAdjustmentCodeTxt
            // 
            this.newPreTaxAdjustmentCodeTxt.AcceptsTab = true;
            this.newPreTaxAdjustmentCodeTxt.Enabled = false;
            this.newPreTaxAdjustmentCodeTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPreTaxAdjustmentCodeTxt.Location = new System.Drawing.Point(332, 40);
            this.newPreTaxAdjustmentCodeTxt.MaxLength = 16;
            this.newPreTaxAdjustmentCodeTxt.Name = "newPreTaxAdjustmentCodeTxt";
            this.newPreTaxAdjustmentCodeTxt.Size = new System.Drawing.Size(183, 24);
            this.newPreTaxAdjustmentCodeTxt.TabIndex = 10;
            this.newPreTaxAdjustmentCodeTxt.Visible = false;
            this.newPreTaxAdjustmentCodeTxt.Enter += new System.EventHandler(this.newPreTaxAdjustmentCodeTxt_Enter);
            this.newPreTaxAdjustmentCodeTxt.Leave += new System.EventHandler(this.newPreTaxAdjustmentCodeTxt_Leave);
            // 
            // newPreTaxAdjustmentAmountTxt
            // 
            this.newPreTaxAdjustmentAmountTxt.AcceptsTab = true;
            this.newPreTaxAdjustmentAmountTxt.Enabled = false;
            this.newPreTaxAdjustmentAmountTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPreTaxAdjustmentAmountTxt.Location = new System.Drawing.Point(792, 40);
            this.newPreTaxAdjustmentAmountTxt.MaxLength = 16;
            this.newPreTaxAdjustmentAmountTxt.Name = "newPreTaxAdjustmentAmountTxt";
            this.newPreTaxAdjustmentAmountTxt.Size = new System.Drawing.Size(89, 24);
            this.newPreTaxAdjustmentAmountTxt.TabIndex = 12;
            this.newPreTaxAdjustmentAmountTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.newPreTaxAdjustmentAmountTxt.Visible = false;
            this.newPreTaxAdjustmentAmountTxt.Enter += new System.EventHandler(this.newPreTaxAdjustmentAmountTxt_Enter);
            this.newPreTaxAdjustmentAmountTxt.Leave += new System.EventHandler(this.newPreTaxAdjustmentAmountTxt_Leave);
            // 
            // editPreTaxAdjustmentAmountTxt
            // 
            this.editPreTaxAdjustmentAmountTxt.AcceptsTab = true;
            this.editPreTaxAdjustmentAmountTxt.Enabled = false;
            this.editPreTaxAdjustmentAmountTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editPreTaxAdjustmentAmountTxt.Location = new System.Drawing.Point(792, 175);
            this.editPreTaxAdjustmentAmountTxt.MaxLength = 16;
            this.editPreTaxAdjustmentAmountTxt.Name = "editPreTaxAdjustmentAmountTxt";
            this.editPreTaxAdjustmentAmountTxt.Size = new System.Drawing.Size(89, 24);
            this.editPreTaxAdjustmentAmountTxt.TabIndex = 16;
            this.editPreTaxAdjustmentAmountTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editPreTaxAdjustmentAmountTxt.Visible = false;
            this.editPreTaxAdjustmentAmountTxt.Leave += new System.EventHandler(this.editPreTaxAdjustmentAmountTxt_Leave);
            // 
            // newPreTaxAdjustmentAmountLbl
            // 
            this.newPreTaxAdjustmentAmountLbl.AutoSize = true;
            this.newPreTaxAdjustmentAmountLbl.Enabled = false;
            this.newPreTaxAdjustmentAmountLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newPreTaxAdjustmentAmountLbl.Location = new System.Drawing.Point(765, 1);
            this.newPreTaxAdjustmentAmountLbl.Name = "newPreTaxAdjustmentAmountLbl";
            this.newPreTaxAdjustmentAmountLbl.Size = new System.Drawing.Size(136, 36);
            this.newPreTaxAdjustmentAmountLbl.TabIndex = 37;
            this.newPreTaxAdjustmentAmountLbl.Text = "New Pre-Tax\r\nAdjustment Amount";
            this.newPreTaxAdjustmentAmountLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.newPreTaxAdjustmentAmountLbl.Visible = false;
            // 
            // editPreTaxAdjustmentAmountLbl
            // 
            this.editPreTaxAdjustmentAmountLbl.AutoSize = true;
            this.editPreTaxAdjustmentAmountLbl.Enabled = false;
            this.editPreTaxAdjustmentAmountLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editPreTaxAdjustmentAmountLbl.Location = new System.Drawing.Point(765, 133);
            this.editPreTaxAdjustmentAmountLbl.Name = "editPreTaxAdjustmentAmountLbl";
            this.editPreTaxAdjustmentAmountLbl.Size = new System.Drawing.Size(136, 36);
            this.editPreTaxAdjustmentAmountLbl.TabIndex = 41;
            this.editPreTaxAdjustmentAmountLbl.Text = "Edit Pre-Tax\r\nAdjustment Amount";
            this.editPreTaxAdjustmentAmountLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.editPreTaxAdjustmentAmountLbl.Visible = false;
            // 
            // hourlyWageLbl
            // 
            this.hourlyWageLbl.AutoSize = true;
            this.hourlyWageLbl.Enabled = false;
            this.hourlyWageLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourlyWageLbl.Location = new System.Drawing.Point(345, 316);
            this.hourlyWageLbl.Name = "hourlyWageLbl";
            this.hourlyWageLbl.Size = new System.Drawing.Size(98, 18);
            this.hourlyWageLbl.TabIndex = 109;
            this.hourlyWageLbl.Text = "Hourly Wage:";
            this.hourlyWageLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.hourlyWageLbl.Visible = false;
            // 
            // hourlyWageTxt
            // 
            this.hourlyWageTxt.AcceptsTab = true;
            this.hourlyWageTxt.Enabled = false;
            this.hourlyWageTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hourlyWageTxt.Location = new System.Drawing.Point(449, 313);
            this.hourlyWageTxt.MaxLength = 16;
            this.hourlyWageTxt.Name = "hourlyWageTxt";
            this.hourlyWageTxt.Size = new System.Drawing.Size(89, 24);
            this.hourlyWageTxt.TabIndex = 8;
            this.hourlyWageTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.hourlyWageTxt.Visible = false;
            this.hourlyWageTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.hourlyWageTxt_KeyDown);
            // 
            // overtimeByAverageWageChk
            // 
            this.overtimeByAverageWageChk.AutoSize = true;
            this.overtimeByAverageWageChk.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overtimeByAverageWageChk.Location = new System.Drawing.Point(666, 12);
            this.overtimeByAverageWageChk.Name = "overtimeByAverageWageChk";
            this.overtimeByAverageWageChk.Size = new System.Drawing.Size(229, 22);
            this.overtimeByAverageWageChk.TabIndex = 115;
            this.overtimeByAverageWageChk.Text = "Pay Overtime For This Worker";
            this.overtimeByAverageWageChk.UseVisualStyleBackColor = true;
            this.overtimeByAverageWageChk.Visible = false;
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 9);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 44;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.editPreTaxAdjustmentAmountLbl);
            this.panel1.Controls.Add(this.newPreTaxAdjustmentAmountLbl);
            this.panel1.Controls.Add(this.editPreTaxAdjustmentAmountTxt);
            this.panel1.Controls.Add(this.newPreTaxAdjustmentAmountTxt);
            this.panel1.Controls.Add(this.preTaxAdjustmentKeyTxt);
            this.panel1.Controls.Add(this.deletePreTaxAdjustmentBtn);
            this.panel1.Controls.Add(this.saveEditedPreTaxAdjustmentBtn);
            this.panel1.Controls.Add(this.editPreTaxAdjustmentDescriptionLbl);
            this.panel1.Controls.Add(this.editPreTaxAdjustmentDescriptionTxt);
            this.panel1.Controls.Add(this.editPreTaxAdjustmentCodeLbl);
            this.panel1.Controls.Add(this.editPreTaxAdjustmentCodeTxt);
            this.panel1.Controls.Add(this.savedPreTaxAdjustmentsLbl);
            this.panel1.Controls.Add(this.saveNewPreTaxAdjustmentBtn);
            this.panel1.Controls.Add(this.savedPreTaxAdjustmentsLst);
            this.panel1.Controls.Add(this.newPreTaxAdjustmentDescriptionLbl);
            this.panel1.Controls.Add(this.newPreTaxAdjustmentDescriptionTxt);
            this.panel1.Controls.Add(this.newPreTaxAdjustmentCodeLbl);
            this.panel1.Controls.Add(this.newPreTaxAdjustmentCodeTxt);
            this.panel1.Location = new System.Drawing.Point(27, 329);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(251, 188);
            this.panel1.TabIndex = 45;
            this.panel1.Visible = false;
            // 
            // address1Lbl
            // 
            this.address1Lbl.AutoSize = true;
            this.address1Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address1Lbl.Location = new System.Drawing.Point(365, 136);
            this.address1Lbl.Name = "address1Lbl";
            this.address1Lbl.Size = new System.Drawing.Size(78, 18);
            this.address1Lbl.TabIndex = 103;
            this.address1Lbl.Text = "Address 1:";
            this.address1Lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // address1Txt
            // 
            this.address1Txt.AcceptsTab = true;
            this.address1Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address1Txt.Location = new System.Drawing.Point(449, 133);
            this.address1Txt.MaxLength = 32;
            this.address1Txt.Name = "address1Txt";
            this.address1Txt.Size = new System.Drawing.Size(249, 24);
            this.address1Txt.TabIndex = 2;
            this.address1Txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.address1Txt_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(365, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 18);
            this.label1.TabIndex = 104;
            this.label1.Text = "Address 2:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // address2Txt
            // 
            this.address2Txt.AcceptsTab = true;
            this.address2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address2Txt.Location = new System.Drawing.Point(449, 163);
            this.address2Txt.MaxLength = 32;
            this.address2Txt.Name = "address2Txt";
            this.address2Txt.Size = new System.Drawing.Size(249, 24);
            this.address2Txt.TabIndex = 3;
            this.address2Txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.address2Txt_KeyDown);
            // 
            // warningPnl
            // 
            this.warningPnl.Controls.Add(this.warnNoBtn);
            this.warningPnl.Controls.Add(this.warnYesBtn);
            this.warningPnl.Controls.Add(this.warningLbl);
            this.warningPnl.Location = new System.Drawing.Point(724, 243);
            this.warningPnl.Name = "warningPnl";
            this.warningPnl.Size = new System.Drawing.Size(157, 165);
            this.warningPnl.TabIndex = 58;
            this.warningPnl.Visible = false;
            // 
            // warnNoBtn
            // 
            this.warnNoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warnNoBtn.Location = new System.Drawing.Point(32, 115);
            this.warnNoBtn.Name = "warnNoBtn";
            this.warnNoBtn.Size = new System.Drawing.Size(89, 26);
            this.warnNoBtn.TabIndex = 2;
            this.warnNoBtn.Text = "No";
            this.warnNoBtn.UseVisualStyleBackColor = true;
            this.warnNoBtn.Click += new System.EventHandler(this.warnNoBtn_Click);
            // 
            // warnYesBtn
            // 
            this.warnYesBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warnYesBtn.Location = new System.Drawing.Point(32, 70);
            this.warnYesBtn.Name = "warnYesBtn";
            this.warnYesBtn.Size = new System.Drawing.Size(89, 26);
            this.warnYesBtn.TabIndex = 1;
            this.warnYesBtn.Text = "Yes Delete";
            this.warnYesBtn.UseVisualStyleBackColor = true;
            this.warnYesBtn.Click += new System.EventHandler(this.warnYesBtn_Click);
            // 
            // warningLbl
            // 
            this.warningLbl.AutoSize = true;
            this.warningLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLbl.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.warningLbl.Location = new System.Drawing.Point(3, 25);
            this.warningLbl.Name = "warningLbl";
            this.warningLbl.Size = new System.Drawing.Size(147, 26);
            this.warningLbl.TabIndex = 0;
            this.warningLbl.Text = "Are you sure?";
            // 
            // newEmployeeBtn
            // 
            this.newEmployeeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newEmployeeBtn.Location = new System.Drawing.Point(596, 420);
            this.newEmployeeBtn.Name = "newEmployeeBtn";
            this.newEmployeeBtn.Size = new System.Drawing.Size(121, 29);
            this.newEmployeeBtn.TabIndex = 100;
            this.newEmployeeBtn.Text = "&New";
            this.newEmployeeBtn.UseVisualStyleBackColor = true;
            this.newEmployeeBtn.Click += new System.EventHandler(this.newEmployeeBtn_Click);
            // 
            // deleteBtn
            // 
            this.deleteBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteBtn.Location = new System.Drawing.Point(469, 420);
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(121, 29);
            this.deleteBtn.TabIndex = 12;
            this.deleteBtn.Text = "Delete";
            this.deleteBtn.UseVisualStyleBackColor = true;
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(387, 348);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 110;
            this.label2.Text = "Group:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // groupTxt
            // 
            this.groupTxt.AcceptsTab = true;
            this.groupTxt.Enabled = false;
            this.groupTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupTxt.Location = new System.Drawing.Point(449, 345);
            this.groupTxt.MaxLength = 16;
            this.groupTxt.Name = "groupTxt";
            this.groupTxt.Size = new System.Drawing.Size(89, 24);
            this.groupTxt.TabIndex = 9;
            this.groupTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.groupTxt.Visible = false;
            this.groupTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Group_KeyDown);
            // 
            // cityLbl
            // 
            this.cityLbl.AutoSize = true;
            this.cityLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityLbl.Location = new System.Drawing.Point(406, 196);
            this.cityLbl.Name = "cityLbl";
            this.cityLbl.Size = new System.Drawing.Size(37, 18);
            this.cityLbl.TabIndex = 112;
            this.cityLbl.Text = "City:";
            this.cityLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // city2Txt
            // 
            this.city2Txt.AcceptsTab = true;
            this.city2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.city2Txt.Location = new System.Drawing.Point(449, 193);
            this.city2Txt.MaxLength = 32;
            this.city2Txt.Name = "city2Txt";
            this.city2Txt.Size = new System.Drawing.Size(183, 24);
            this.city2Txt.TabIndex = 4;
            this.city2Txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.city2Txt_KeyDown);
            // 
            // stateLbl
            // 
            this.stateLbl.AutoSize = true;
            this.stateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stateLbl.Location = new System.Drawing.Point(397, 226);
            this.stateLbl.Name = "stateLbl";
            this.stateLbl.Size = new System.Drawing.Size(46, 18);
            this.stateLbl.TabIndex = 114;
            this.stateLbl.Text = "State:";
            this.stateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // state2Txt
            // 
            this.state2Txt.AcceptsTab = true;
            this.state2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.state2Txt.Location = new System.Drawing.Point(449, 223);
            this.state2Txt.MaxLength = 32;
            this.state2Txt.Name = "state2Txt";
            this.state2Txt.Size = new System.Drawing.Size(45, 24);
            this.state2Txt.TabIndex = 5;
            this.state2Txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.state2Txt_KeyDown);
            // 
            // zip2Lbl
            // 
            this.zip2Lbl.AutoSize = true;
            this.zip2Lbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zip2Lbl.Location = new System.Drawing.Point(411, 256);
            this.zip2Lbl.Name = "zip2Lbl";
            this.zip2Lbl.Size = new System.Drawing.Size(32, 18);
            this.zip2Lbl.TabIndex = 116;
            this.zip2Lbl.Text = "Zip:";
            this.zip2Lbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // zip2Txt
            // 
            this.zip2Txt.AcceptsTab = true;
            this.zip2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zip2Txt.Location = new System.Drawing.Point(449, 253);
            this.zip2Txt.MaxLength = 32;
            this.zip2Txt.Name = "zip2Txt";
            this.zip2Txt.Size = new System.Drawing.Size(93, 24);
            this.zip2Txt.TabIndex = 6;
            this.zip2Txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.zip2Txt_KeyDown);
            // 
            // SSNLbl
            // 
            this.SSNLbl.AutoSize = true;
            this.SSNLbl.Enabled = false;
            this.SSNLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSNLbl.Location = new System.Drawing.Point(400, 286);
            this.SSNLbl.Name = "SSNLbl";
            this.SSNLbl.Size = new System.Drawing.Size(43, 18);
            this.SSNLbl.TabIndex = 118;
            this.SSNLbl.Text = "SSN:";
            this.SSNLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SSNLbl.Visible = false;
            // 
            // SSN2Txt
            // 
            this.SSN2Txt.AcceptsTab = true;
            this.SSN2Txt.Enabled = false;
            this.SSN2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSN2Txt.Location = new System.Drawing.Point(449, 283);
            this.SSN2Txt.MaxLength = 32;
            this.SSN2Txt.Name = "SSN2Txt";
            this.SSN2Txt.Size = new System.Drawing.Size(249, 24);
            this.SSN2Txt.TabIndex = 7;
            this.SSN2Txt.Visible = false;
            this.SSN2Txt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ssn2Txt_KeyDown);
            // 
            // overtimeNoRdo
            // 
            this.overtimeNoRdo.AutoSize = true;
            this.overtimeNoRdo.Checked = true;
            this.overtimeNoRdo.Enabled = false;
            this.overtimeNoRdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overtimeNoRdo.Location = new System.Drawing.Point(95, 7);
            this.overtimeNoRdo.Name = "overtimeNoRdo";
            this.overtimeNoRdo.Size = new System.Drawing.Size(46, 22);
            this.overtimeNoRdo.TabIndex = 143;
            this.overtimeNoRdo.TabStop = true;
            this.overtimeNoRdo.Text = "No";
            this.overtimeNoRdo.UseVisualStyleBackColor = true;
            this.overtimeNoRdo.Visible = false;
            // 
            // overtimeYesRdo
            // 
            this.overtimeYesRdo.AutoSize = true;
            this.overtimeYesRdo.Enabled = false;
            this.overtimeYesRdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overtimeYesRdo.Location = new System.Drawing.Point(6, 5);
            this.overtimeYesRdo.Name = "overtimeYesRdo";
            this.overtimeYesRdo.Size = new System.Drawing.Size(51, 22);
            this.overtimeYesRdo.TabIndex = 142;
            this.overtimeYesRdo.Text = "Yes";
            this.overtimeYesRdo.UseVisualStyleBackColor = true;
            this.overtimeYesRdo.Visible = false;
            // 
            // overtimeLbl
            // 
            this.overtimeLbl.AutoSize = true;
            this.overtimeLbl.Enabled = false;
            this.overtimeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.overtimeLbl.Location = new System.Drawing.Point(371, 376);
            this.overtimeLbl.Name = "overtimeLbl";
            this.overtimeLbl.Size = new System.Drawing.Size(72, 18);
            this.overtimeLbl.TabIndex = 144;
            this.overtimeLbl.Text = "Overtime:";
            this.overtimeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.overtimeLbl.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.activeRdo);
            this.groupBox1.Controls.Add(this.inactiveRdo);
            this.groupBox1.Location = new System.Drawing.Point(167, 47);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(219, 33);
            this.groupBox1.TabIndex = 145;
            this.groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.overtimeYesRdo);
            this.groupBox2.Controls.Add(this.overtimeNoRdo);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(449, 375);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 33);
            this.groupBox2.TabIndex = 146;
            this.groupBox2.TabStop = false;
            this.groupBox2.Visible = false;
            // 
            // employeeListBtn
            // 
            this.employeeListBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeListBtn.Location = new System.Drawing.Point(280, 532);
            this.employeeListBtn.Name = "employeeListBtn";
            this.employeeListBtn.Size = new System.Drawing.Size(150, 34);
            this.employeeListBtn.TabIndex = 147;
            this.employeeListBtn.Text = "Make Customer List";
            this.employeeListBtn.UseVisualStyleBackColor = true;
            this.employeeListBtn.Click += new System.EventHandler(this.employeeListBtn_Click);
            // 
            // MakeExcelBtn
            // 
            this.MakeExcelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MakeExcelBtn.Location = new System.Drawing.Point(280, 483);
            this.MakeExcelBtn.Name = "MakeExcelBtn";
            this.MakeExcelBtn.Size = new System.Drawing.Size(150, 34);
            this.MakeExcelBtn.TabIndex = 148;
            this.MakeExcelBtn.Text = "Make Excel File";
            this.MakeExcelBtn.UseVisualStyleBackColor = true;
            this.MakeExcelBtn.Click += new System.EventHandler(this.MakeExcelBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___100x20_pix_;
            this.pictureBox1.Location = new System.Drawing.Point(349, -13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(210, 63);
            this.pictureBox1.TabIndex = 149;
            this.pictureBox1.TabStop = false;
            // 
            // Employee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(909, 572);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.MakeExcelBtn);
            this.Controls.Add(this.employeeListBtn);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.overtimeLbl);
            this.Controls.Add(this.SSNLbl);
            this.Controls.Add(this.SSN2Txt);
            this.Controls.Add(this.zip2Lbl);
            this.Controls.Add(this.zip2Txt);
            this.Controls.Add(this.stateLbl);
            this.Controls.Add(this.state2Txt);
            this.Controls.Add(this.cityLbl);
            this.Controls.Add(this.city2Txt);
            this.Controls.Add(this.deleteBtn);
            this.Controls.Add(this.newEmployeeBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.warningPnl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupTxt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.address2Txt);
            this.Controls.Add(this.address1Lbl);
            this.Controls.Add(this.address1Txt);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.overtimeByAverageWageChk);
            this.Controls.Add(this.hourlyWageLbl);
            this.Controls.Add(this.hourlyWageTxt);
            this.Controls.Add(this.keyTxt);
            this.Controls.Add(this.enableDisableBtn);
            this.Controls.Add(this.saveEditedBtn);
            this.Controls.Add(this.savedEmployeesLbl);
            this.Controls.Add(this.savedEmployeesLst);
            this.Controls.Add(this.nameLbl);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.codeLbl);
            this.Controls.Add(this.codeTxt);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Employee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Storage Inventory - Customer";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.warningPnl.ResumeLayout(false);
            this.warningPnl.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.TextBox codeTxt;
        private System.Windows.Forms.Label codeLbl;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.ListBox savedEmployeesLst;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label savedEmployeesLbl;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Button saveEditedBtn;
        private System.Windows.Forms.Button enableDisableBtn;
        private System.Windows.Forms.TextBox keyTxt;
        private System.Windows.Forms.RadioButton activeRdo;
        private System.Windows.Forms.RadioButton inactiveRdo;
		private System.Windows.Forms.TextBox preTaxAdjustmentKeyTxt;
		private System.Windows.Forms.Button deletePreTaxAdjustmentBtn;
		private System.Windows.Forms.Button saveEditedPreTaxAdjustmentBtn;
		private System.Windows.Forms.Label editPreTaxAdjustmentDescriptionLbl;
		private System.Windows.Forms.TextBox editPreTaxAdjustmentDescriptionTxt;
		private System.Windows.Forms.Label editPreTaxAdjustmentCodeLbl;
		private System.Windows.Forms.TextBox editPreTaxAdjustmentCodeTxt;
		private System.Windows.Forms.Label savedPreTaxAdjustmentsLbl;
		private System.Windows.Forms.Button saveNewPreTaxAdjustmentBtn;
		private System.Windows.Forms.ListBox savedPreTaxAdjustmentsLst;
		private System.Windows.Forms.Label newPreTaxAdjustmentDescriptionLbl;
		private System.Windows.Forms.TextBox newPreTaxAdjustmentDescriptionTxt;
		private System.Windows.Forms.Label newPreTaxAdjustmentCodeLbl;
		private System.Windows.Forms.TextBox newPreTaxAdjustmentCodeTxt;
		private System.Windows.Forms.TextBox newPreTaxAdjustmentAmountTxt;
		private System.Windows.Forms.TextBox editPreTaxAdjustmentAmountTxt;
		private System.Windows.Forms.Label newPreTaxAdjustmentAmountLbl;
		private System.Windows.Forms.Label editPreTaxAdjustmentAmountLbl;
		private System.Windows.Forms.Label hourlyWageLbl;
        private System.Windows.Forms.TextBox hourlyWageTxt;
		private System.Windows.Forms.CheckBox overtimeByAverageWageChk;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.Panel panel1;
        internal System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.Label address1Lbl;
        internal System.Windows.Forms.TextBox address1Txt;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.TextBox address2Txt;
        private System.Windows.Forms.Panel warningPnl;
        private System.Windows.Forms.Button warnNoBtn;
        private System.Windows.Forms.Button warnYesBtn;
        private System.Windows.Forms.Label warningLbl;
        private System.Windows.Forms.Button newEmployeeBtn;
        private System.Windows.Forms.Button deleteBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox groupTxt;
        private System.Windows.Forms.Label cityLbl;
        internal System.Windows.Forms.TextBox city2Txt;
        private System.Windows.Forms.Label stateLbl;
        internal System.Windows.Forms.TextBox state2Txt;
        private System.Windows.Forms.Label zip2Lbl;
        internal System.Windows.Forms.TextBox zip2Txt;
        private System.Windows.Forms.Label SSNLbl;
        internal System.Windows.Forms.TextBox SSN2Txt;
        private System.Windows.Forms.RadioButton overtimeNoRdo;
        private System.Windows.Forms.RadioButton overtimeYesRdo;
        private System.Windows.Forms.Label overtimeLbl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button employeeListBtn;
        private System.Windows.Forms.Button MakeExcelBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}