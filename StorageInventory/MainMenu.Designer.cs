﻿namespace StorageInventory
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitBtn = new System.Windows.Forms.Button();
            this.titleLbl = new System.Windows.Forms.Label();
            this.payDataReportBtn = new System.Windows.Forms.Button();
            this.fieldBtn = new System.Windows.Forms.Button();
            this.jobBtn = new System.Windows.Forms.Button();
            this.employeeBtn = new System.Windows.Forms.Button();
            this.cropBtn = new System.Windows.Forms.Button();
            this.payDetailBtn = new System.Windows.Forms.Button();
            this.setPayrollOptionsBtn = new System.Windows.Forms.Button();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.editInventoryDetailBtn = new System.Windows.Forms.Button();
            this.payDataByEmployeeBtn = new System.Windows.Forms.Button();
            this.VersionData = new System.Windows.Forms.Label();
            this.WorkOrderBtn = new System.Windows.Forms.Button();
            this.LabelPrintingBtn = new System.Windows.Forms.Button();
            this.ShippingOrderBtn = new System.Windows.Forms.Button();
            this.RoomReportBtn = new System.Windows.Forms.Button();
            this.growerBlockBtn = new System.Windows.Forms.Button();
            this.MultipleRoomMoveBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(398, 350);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(112, 34);
            this.exitBtn.TabIndex = 8;
            this.exitBtn.Text = "E&xit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLbl.Location = new System.Drawing.Point(376, 99);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(156, 24);
            this.titleLbl.TabIndex = 9;
            this.titleLbl.Text = "Storage Inventory";
            // 
            // payDataReportBtn
            // 
            this.payDataReportBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payDataReportBtn.Location = new System.Drawing.Point(745, 243);
            this.payDataReportBtn.Name = "payDataReportBtn";
            this.payDataReportBtn.Size = new System.Drawing.Size(183, 34);
            this.payDataReportBtn.TabIndex = 6;
            this.payDataReportBtn.Text = "Storage Inventory Report";
            this.payDataReportBtn.UseVisualStyleBackColor = true;
            this.payDataReportBtn.Click += new System.EventHandler(this.payDataReportBtn_Click);
            // 
            // fieldBtn
            // 
            this.fieldBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldBtn.Location = new System.Drawing.Point(378, 137);
            this.fieldBtn.Name = "fieldBtn";
            this.fieldBtn.Size = new System.Drawing.Size(181, 34);
            this.fieldBtn.TabIndex = 2;
            this.fieldBtn.Text = "Where Input/Edit";
            this.fieldBtn.UseVisualStyleBackColor = true;
            this.fieldBtn.Click += new System.EventHandler(this.fieldBtn_Click);
            // 
            // jobBtn
            // 
            this.jobBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobBtn.Location = new System.Drawing.Point(208, 137);
            this.jobBtn.Name = "jobBtn";
            this.jobBtn.Size = new System.Drawing.Size(157, 34);
            this.jobBtn.TabIndex = 1;
            this.jobBtn.Text = "Box Type Input/Edit";
            this.jobBtn.UseVisualStyleBackColor = true;
            this.jobBtn.Click += new System.EventHandler(this.jobBtn_Click);
            // 
            // employeeBtn
            // 
            this.employeeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeBtn.Location = new System.Drawing.Point(576, 137);
            this.employeeBtn.Name = "employeeBtn";
            this.employeeBtn.Size = new System.Drawing.Size(157, 34);
            this.employeeBtn.TabIndex = 3;
            this.employeeBtn.Text = "Customer Input/Edit";
            this.employeeBtn.UseVisualStyleBackColor = true;
            this.employeeBtn.Click += new System.EventHandler(this.employeeBtn_Click);
            // 
            // cropBtn
            // 
            this.cropBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropBtn.Location = new System.Drawing.Point(19, 137);
            this.cropBtn.Name = "cropBtn";
            this.cropBtn.Size = new System.Drawing.Size(173, 34);
            this.cropBtn.TabIndex = 0;
            this.cropBtn.Text = "Crop Input/Edit";
            this.cropBtn.UseVisualStyleBackColor = true;
            this.cropBtn.Click += new System.EventHandler(this.cropBtn_Click);
            // 
            // payDetailBtn
            // 
            this.payDetailBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payDetailBtn.Location = new System.Drawing.Point(745, 137);
            this.payDetailBtn.Name = "payDetailBtn";
            this.payDetailBtn.Size = new System.Drawing.Size(186, 34);
            this.payDetailBtn.TabIndex = 4;
            this.payDetailBtn.Text = "Inventory Detail Input/Edit";
            this.payDetailBtn.UseVisualStyleBackColor = true;
            this.payDetailBtn.Click += new System.EventHandler(this.payDetailBtn_Click);
            // 
            // setPayrollOptionsBtn
            // 
            this.setPayrollOptionsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setPayrollOptionsBtn.Location = new System.Drawing.Point(208, 190);
            this.setPayrollOptionsBtn.Name = "setPayrollOptionsBtn";
            this.setPayrollOptionsBtn.Size = new System.Drawing.Size(157, 34);
            this.setPayrollOptionsBtn.TabIndex = 5;
            this.setPayrollOptionsBtn.Text = "Set Printer Options";
            this.setPayrollOptionsBtn.UseVisualStyleBackColor = true;
            this.setPayrollOptionsBtn.Click += new System.EventHandler(this.setPayrollOptionsBtn_Click);
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(19, 13);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 10;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // editInventoryDetailBtn
            // 
            this.editInventoryDetailBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.editInventoryDetailBtn.Location = new System.Drawing.Point(745, 190);
            this.editInventoryDetailBtn.Name = "editInventoryDetailBtn";
            this.editInventoryDetailBtn.Size = new System.Drawing.Size(186, 34);
            this.editInventoryDetailBtn.TabIndex = 11;
            this.editInventoryDetailBtn.Text = "Edit Inventory Detail";
            this.editInventoryDetailBtn.UseVisualStyleBackColor = true;
            this.editInventoryDetailBtn.Click += new System.EventHandler(this.editInventoryDetailBtn_Click);
            // 
            // payDataByEmployeeBtn
            // 
            this.payDataByEmployeeBtn.Enabled = false;
            this.payDataByEmployeeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payDataByEmployeeBtn.Location = new System.Drawing.Point(675, 309);
            this.payDataByEmployeeBtn.Name = "payDataByEmployeeBtn";
            this.payDataByEmployeeBtn.Size = new System.Drawing.Size(256, 34);
            this.payDataByEmployeeBtn.TabIndex = 12;
            this.payDataByEmployeeBtn.Text = "Pay Data By Employee Report";
            this.payDataByEmployeeBtn.UseVisualStyleBackColor = true;
            this.payDataByEmployeeBtn.Visible = false;
            this.payDataByEmployeeBtn.Click += new System.EventHandler(this.payDataByEmployeeBtn_Click);
            // 
            // VersionData
            // 
            this.VersionData.Location = new System.Drawing.Point(641, 13);
            this.VersionData.Name = "VersionData";
            this.VersionData.Size = new System.Drawing.Size(248, 23);
            this.VersionData.TabIndex = 14;
            this.VersionData.Text = "versionLbl";
            this.VersionData.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.VersionData.UseMnemonic = false;
            // 
            // WorkOrderBtn
            // 
            this.WorkOrderBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.WorkOrderBtn.Location = new System.Drawing.Point(19, 243);
            this.WorkOrderBtn.Name = "WorkOrderBtn";
            this.WorkOrderBtn.Size = new System.Drawing.Size(173, 34);
            this.WorkOrderBtn.TabIndex = 16;
            this.WorkOrderBtn.Text = "Receiving Product";
            this.WorkOrderBtn.UseVisualStyleBackColor = true;
            this.WorkOrderBtn.Click += new System.EventHandler(this.WorkOrderBtn_Click);
            // 
            // LabelPrintingBtn
            // 
            this.LabelPrintingBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.LabelPrintingBtn.Location = new System.Drawing.Point(378, 243);
            this.LabelPrintingBtn.Name = "LabelPrintingBtn";
            this.LabelPrintingBtn.Size = new System.Drawing.Size(181, 34);
            this.LabelPrintingBtn.TabIndex = 17;
            this.LabelPrintingBtn.Text = "Label Printing";
            this.LabelPrintingBtn.UseVisualStyleBackColor = true;
            this.LabelPrintingBtn.Click += new System.EventHandler(this.LabelPrintingBtn_Click);
            // 
            // ShippingOrderBtn
            // 
            this.ShippingOrderBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ShippingOrderBtn.Location = new System.Drawing.Point(208, 243);
            this.ShippingOrderBtn.Name = "ShippingOrderBtn";
            this.ShippingOrderBtn.Size = new System.Drawing.Size(157, 34);
            this.ShippingOrderBtn.TabIndex = 15;
            this.ShippingOrderBtn.Text = "Shipping Product";
            this.ShippingOrderBtn.UseVisualStyleBackColor = true;
            this.ShippingOrderBtn.Click += new System.EventHandler(this.ShippingOrderBtn_Click);
            // 
            // RoomReportBtn
            // 
            this.RoomReportBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.RoomReportBtn.Location = new System.Drawing.Point(576, 243);
            this.RoomReportBtn.Name = "RoomReportBtn";
            this.RoomReportBtn.Size = new System.Drawing.Size(157, 34);
            this.RoomReportBtn.TabIndex = 19;
            this.RoomReportBtn.Text = "Room Report";
            this.RoomReportBtn.UseVisualStyleBackColor = true;
            this.RoomReportBtn.Click += new System.EventHandler(this.RoomReportBtn_Click);
            // 
            // growerBlockBtn
            // 
            this.growerBlockBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerBlockBtn.Location = new System.Drawing.Point(19, 190);
            this.growerBlockBtn.Name = "growerBlockBtn";
            this.growerBlockBtn.Size = new System.Drawing.Size(174, 34);
            this.growerBlockBtn.TabIndex = 20;
            this.growerBlockBtn.Text = "Grower Block Input/Edit";
            this.growerBlockBtn.UseVisualStyleBackColor = true;
            this.growerBlockBtn.Click += new System.EventHandler(this.growerBlockBtn_Click);
            // 
            // MultipleRoomMoveBtn
            // 
            this.MultipleRoomMoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.MultipleRoomMoveBtn.Location = new System.Drawing.Point(547, 190);
            this.MultipleRoomMoveBtn.Name = "MultipleRoomMoveBtn";
            this.MultipleRoomMoveBtn.Size = new System.Drawing.Size(186, 34);
            this.MultipleRoomMoveBtn.TabIndex = 21;
            this.MultipleRoomMoveBtn.Text = "Multiple Tag Room Move";
            this.MultipleRoomMoveBtn.UseVisualStyleBackColor = true;
            this.MultipleRoomMoveBtn.Click += new System.EventHandler(this.multipleRoomMoveBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___200x40_pix_;
            this.pictureBox1.Location = new System.Drawing.Point(244, -25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(420, 121);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(948, 396);
            this.Controls.Add(this.MultipleRoomMoveBtn);
            this.Controls.Add(this.growerBlockBtn);
            this.Controls.Add(this.RoomReportBtn);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ShippingOrderBtn);
            this.Controls.Add(this.LabelPrintingBtn);
            this.Controls.Add(this.WorkOrderBtn);
            this.Controls.Add(this.VersionData);
            this.Controls.Add(this.payDataByEmployeeBtn);
            this.Controls.Add(this.editInventoryDetailBtn);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.setPayrollOptionsBtn);
            this.Controls.Add(this.payDataReportBtn);
            this.Controls.Add(this.payDetailBtn);
            this.Controls.Add(this.cropBtn);
            this.Controls.Add(this.employeeBtn);
            this.Controls.Add(this.titleLbl);
            this.Controls.Add(this.jobBtn);
            this.Controls.Add(this.fieldBtn);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Storage Inventory - Main Menu";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label titleLbl;
        private System.Windows.Forms.Button payDataReportBtn;
        private System.Windows.Forms.Button fieldBtn;
        private System.Windows.Forms.Button jobBtn;
        private System.Windows.Forms.Button employeeBtn;
        private System.Windows.Forms.Button cropBtn;
        private System.Windows.Forms.Button payDetailBtn;
        private System.Windows.Forms.Button setPayrollOptionsBtn;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.Button editInventoryDetailBtn;
        private System.Windows.Forms.Button payDataByEmployeeBtn;
        private System.Windows.Forms.Label VersionData;
        private System.Windows.Forms.Button WorkOrderBtn;
        private System.Windows.Forms.Button LabelPrintingBtn;
        private System.Windows.Forms.Button ShippingOrderBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button RoomReportBtn;
        private System.Windows.Forms.Button growerBlockBtn;
        private System.Windows.Forms.Button MultipleRoomMoveBtn;
    }
}

