﻿namespace PayrollFieldDataDesktop
{
    partial class MakePayEndDate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PayThruDate = new System.Windows.Forms.DateTimePicker();
            this.labelSelectEndDate = new System.Windows.Forms.Label();
            this.buttonStartProcess = new System.Windows.Forms.Button();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PayThruDate
            // 
            this.PayThruDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.PayThruDate.Location = new System.Drawing.Point(60, 115);
            this.PayThruDate.Name = "PayThruDate";
            this.PayThruDate.Size = new System.Drawing.Size(165, 20);
            this.PayThruDate.TabIndex = 0;
            this.PayThruDate.Value = new System.DateTime(2012, 11, 12, 16, 15, 53, 0);
            this.PayThruDate.ValueChanged += new System.EventHandler(this.PayThruDate_ValueChanged);
            // 
            // labelSelectEndDate
            // 
            this.labelSelectEndDate.AutoSize = true;
            this.labelSelectEndDate.Location = new System.Drawing.Point(60, 79);
            this.labelSelectEndDate.Name = "labelSelectEndDate";
            this.labelSelectEndDate.Size = new System.Drawing.Size(165, 13);
            this.labelSelectEndDate.TabIndex = 1;
            this.labelSelectEndDate.Text = "Please Select Pay Through Date:";
            this.labelSelectEndDate.Click += new System.EventHandler(this.labelSelectEndDate_Click);
            // 
            // buttonStartProcess
            // 
            this.buttonStartProcess.Location = new System.Drawing.Point(60, 158);
            this.buttonStartProcess.Name = "buttonStartProcess";
            this.buttonStartProcess.Size = new System.Drawing.Size(165, 23);
            this.buttonStartProcess.TabIndex = 2;
            this.buttonStartProcess.Text = "Start Payroll Processing >>>";
            this.buttonStartProcess.UseVisualStyleBackColor = true;
            this.buttonStartProcess.Click += new System.EventHandler(this.buttonStartProcess_Click);
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(24, 9);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 3;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CompanyNameData.UseMnemonic = false;
            // 
            // MakePayEndDate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.buttonStartProcess);
            this.Controls.Add(this.labelSelectEndDate);
            this.Controls.Add(this.PayThruDate);
            this.Location = new System.Drawing.Point(10, 200);
            this.Name = "MakePayEndDate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Enter Pay Through Date";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker PayThruDate;
        private System.Windows.Forms.Label labelSelectEndDate;
        private System.Windows.Forms.Button buttonStartProcess;
        private System.Windows.Forms.Label CompanyNameData;
    }
}