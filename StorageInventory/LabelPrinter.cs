﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Management;
using SoftekBarcodeMakerLib2;
using System.Drawing.Printing;

namespace StorageInventory
{
    public partial class LabelPrinter : Form
    {

        #region Vars

        private MainMenu.CropsData cropsData;
        private MainMenu.JobsData jobsData;
        private MainMenu.FieldsData fieldsData;
        private MainMenu.EmployeesData employeesData;
        private MainMenu.PayrollOptionsData payrollOptionsData;
        private MainMenu.Grower_Blocks_Data growerBlocksData;
        private ArrayList EmployeesArl;
        private ArrayList CropsArl;
        private ArrayList FieldsArl;
        private ArrayList JobsArl;
        private ArrayList GrowerBlockArl;
        private Printer printer;
        private string xmlFileName_Crops;
        private string xmlFileName_Jobs;
        private string xmlFileName_Fields;
        private string xmlFileName_Employees;
        private string xmlFileName_payrollOptionsData;
        private string xmlFileName_GrowerBlockData;
        private int tagCountInt;
        private string timeStampStr;

        //Program option variables.
        private char delimiter;
        private string delimiterWithSpaces;
       
        #endregion


        public LabelPrinter()
        {
//Initialize the program option variables
			delimiter = '~';
			delimiterWithSpaces = " " + delimiter.ToString() + " ";
			

			//Default-Initialize
			InitializeComponent();

			//Initialize object variables.
			cropsData = new MainMenu.CropsData();
			jobsData = new MainMenu.JobsData();
			fieldsData = new MainMenu.FieldsData();
            employeesData = new MainMenu.EmployeesData();
            payrollOptionsData = new MainMenu.PayrollOptionsData();
			//Set the XML file names
			xmlFileName_Crops = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
			xmlFileName_Jobs = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
			xmlFileName_Fields = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
            xmlFileName_Employees = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
            xmlFileName_payrollOptionsData = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayrollOptionsData.xml";
            xmlFileName_GrowerBlockData = Path.GetDirectoryName(Application.ExecutablePath) + @"\Grower_Block_Data.xml";
            //Load all of our data from XML
            FillCropDataFromXML();
			FillFieldDataFromXML();
			FillJobDataFromXML();
            FillEmployeeDataFromXML();
            FillPayrollOptionsDataFromXML();
            FillGrowerBlockDataFromXML();
            //Set the dropdown boxes to auto-complete
            cropCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			cropCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
			jobCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			jobCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
			fieldCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			fieldCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            employeeCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            employeeCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            growerCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            growerCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            //Fill the dropdown lists
            FillCropsDropdown();
			FillJobsDropdown();
			FillFieldsDropdown();
            FillEmployeesDropdown();
            FillGrowerBlockDropdown();
			
            CompanyNameData.Text = MainMenu.CompanyStr;
		}

        #region Helper Methods
        //Get Data
        private void FillCropDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Crops);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Crops, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
       
        private void FillJobDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Jobs);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Jobs, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillFieldDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Fields);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Fields, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillEmployeeDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Employees);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Employees, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillPayrollOptionsDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_payrollOptionsData);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayrollOptionsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_payrollOptionsData, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                payrollOptionsData = (MainMenu.PayrollOptionsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillGrowerBlockDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_GrowerBlockData);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.Grower_Blocks_Data));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_GrowerBlockData, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                growerBlocksData = (MainMenu.Grower_Blocks_Data)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        //Fill DropDown Functions

        private void FillCropsDropdown()
        {
            //Clear the existing list
            cropCbx.Items.Clear();
            cropCbx.SelectedIndex = -1;

            CropsArl = new ArrayList();

            //Fill the crop data object
            FillCropDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
            {
                MainMenu.CropData crop = (MainMenu.CropData)cropsData.CropsDataArl[i];

                if (crop.ActiveBln)
                {
                    cropCbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    CropsArl.Add(crop);
                }
            }
        }
        private void FillJobsDropdown()
        {
            //Clear the existing list
            jobCbx.Items.Clear();
            jobCbx.SelectedIndex = -1;

            JobsArl = new ArrayList();

            //Fill the job data object
            FillJobDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
            {
                MainMenu.JobData job = (MainMenu.JobData)jobsData.JobsDataArl[i];

                if (job.ActiveBln)
                {
                    jobCbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    JobsArl.Add(job);
                }
            }
        }
        private void FillFieldsDropdown()
        {
            //Clear the existing list
            fieldCbx.Items.Clear();
            fieldCbx.SelectedIndex = -1;

            FieldsArl = new ArrayList();

            //Fill the field data object
            FillFieldDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
            {
                MainMenu.FieldData field = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

                if (field.ActiveBln)
                {
                    fieldCbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    FieldsArl.Add(field);
                }
            }
        }
        private void FillEmployeesDropdown()
        {
            //Clear the existing list
            employeeCbx.Items.Clear();
            employeeCbx.SelectedIndex = -1;

            //editEmployeeCbx.Items.Clear();
            //editEmployeeCbx.SelectedIndex = -1;

            EmployeesArl = new ArrayList();

            //Fill the employee data object
            FillEmployeeDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
            {
                MainMenu.EmployeeData employee = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                if (employee.ActiveBln)
                {

                    employeeCbx.Items.Add(employee.CodeStr + delimiterWithSpaces + employee.NameStr);
                    //employeeCbx.DisplayMember = employee.CodeStr + delimiterWithSpaces + employee.NameStr;
                    //employeeCbx.DataSource = employee;
                    //editEmployeeCbx.Items.Add(employee.CodeStr + delimiterWithSpaces + employee.NameStr);
                    //editEmployeeCbx.DisplayMember = employee.CodeStr + delimiterWithSpaces + employee.NameStr;
                    //editEmployeeCbx.DataSource = employee;

                    EmployeesArl.Add(employee);
                }
            }
        }
        private void FillGrowerBlockDropdown()
        {
            //Clear the existing list
            growerCbx.Items.Clear();
            cropCbx.SelectedIndex = -1;

            GrowerBlockArl = new ArrayList();

            //Fill the crop data object
            FillGrowerBlockDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < growerBlocksData.GrowerBlocksDataArl.Count; i++)
            {
                MainMenu.GrowerBlockData grower = (MainMenu.GrowerBlockData)growerBlocksData.GrowerBlocksDataArl[i];

                if (grower.ActiveBln)
                {
                    growerCbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    GrowerBlockArl.Add(grower);
                }
            }
        }
        //Adjust ComboBox dropdown list width
        private static void SetComboScrollWidth(object sender)
        {
            //CREDIT - this code was found at: http://rajeshkm.blogspot.com/2006/11/adjust-combobox-drop-down-list-width-c.html
            //      THANKS Rajesh!
            try
            {
                ComboBox senderComboBox = (ComboBox)sender;
                int width = senderComboBox.Width;
                Graphics g = senderComboBox.CreateGraphics();
                Font font = senderComboBox.Font;

                //checks if a scrollbar will be displayed.
                //If yes, then get its width to adjust the size of the drop down list.
                int vertScrollBarWidth = (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems) ? SystemInformation.VerticalScrollBarWidth : 0;

                //Loop through list items and check size of each items.
                //set the width of the drop down list to the width of the largest item.

                int newWidth;

                foreach (string s in ((ComboBox)sender).Items)
                {
                    if (s != null)
                    {
                        newWidth = (int)g.MeasureString(s.Trim(), font).Width + vertScrollBarWidth;

                        if (width < newWidth)
                        {
                            width = newWidth;
                        }
                    }
                }

                senderComboBox.DropDownWidth = width;
            }
            catch (Exception objException)
            {
                //Catch objException
            }
        }

        //Validation Function(s)
        private void ValidateQuantity(TextBox textBoxToCheck, string textBoxName)
        {
            try
            {
                decimal temp = decimal.Parse(textBoxToCheck.Text);

                if (textBoxToCheck.Text != "" && textBoxToCheck.Text.IndexOf(",") == -1)
                {
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
                else if (textBoxToCheck.Text.IndexOf(",") != -1)
                {
                    textBoxToCheck.Text.Replace(",", "");
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show(this, "Invalid " + textBoxName + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxToCheck.Focus();
                textBoxToCheck.SelectAll();
            }
        }

        //Formating Function(s)
        private static string DisplayFormat(decimal value)
        {
            if (value == 0)
                return value.ToString();
            else
                return decimal.Round(value, 2).ToString("#,###,##0.##");
        }
        
       
        private void EnableAllTextFields()
        {
            
            cropCbx.Enabled = true;
            jobCbx.Enabled = true;
            fieldCbx.Enabled = true;
            priceTxt.Enabled = true;
            growerCbx.Enabled = true;
        }
        private void ResetScreen()
        {
             cropCbx.SelectedIndex = -1;
             jobCbx.SelectedIndex = -1;
             fieldCbx.SelectedIndex = -1;
             priceTxt.Text = "0";
            growerCbx.SelectedIndex = -1;
        }
        public static string ParseTildaString(string stringToParse, int field)
        {
            string[] stringSplit = stringToParse.Split('~');

            if (stringSplit.Length <= field)
                return "";
            else
                return stringSplit[field];
        }
        #endregion

        private void printReportBtn_Click(object sender, EventArgs e)
        {
            timeStampStr = DateTime.Today.ToShortDateString() + "-" + DateTime.Now.ToLongTimeString();
            tagCountInt = Convert.ToInt32(priceTxt.Text);
            for (int i = 0; i < tagCountInt; i++)
            {
                printer = new Printer();
                printer.PrintPage += new PrintPageEventHandler(PrintBarcode);
                printer.PrintDirectToPrinter(payrollOptionsData.LabelPrinterStr.Trim());
            }
        }

        private void PrintBarcode(object sender, PrintPageEventArgs e)
        {
                       
                    #region Make Label
                    int currentX = 10;
                    int currentY = 5;
                    int lineHeight = 15;

            //website for inches to pixel conversion: http://www.classical-webdesigns.co.uk/resources/pixelinchconvert.html

            //4 in. x 2 in. = 400 px X 200 px
            //e.Graphics.DrawRectangle(new Pen(Brushes.Black), 0, 0, totalWidth, totalHeight);

            //float totalWidth = e.PageSettings.PaperSize.Width;
            //float totalHeight = e.PageSettings.PaperSize.Height;
            //float totalWidth = 400; //4 inches
            //float totalHeight = 200;//2 inches
            #region Normal code
            //Company name
            e.Graphics.DrawString(ParseTildaString(employeeCbx.SelectedItem.ToString(), 1).Trim(), new Font("Arial", 20), Brushes.Black, new Point(currentX, currentY));


            //Company code
            currentX = 10;
            currentY = 30;
            e.Graphics.DrawString(ParseTildaString(employeeCbx.SelectedItem.ToString(), 0).Trim(), new Font("Arial", 80), Brushes.Black, new Point(currentX, currentY));

            //Variety Code
            currentX = 10;
            currentY = 120;
            e.Graphics.DrawString(ParseTildaString(cropCbx.SelectedItem.ToString(), 0).Trim(), new Font("Arial", 100), Brushes.Black, new Point(currentX, currentY));

            //Variety Description
            currentX = 10;
            currentY = 240;
            e.Graphics.DrawString(ParseTildaString(cropCbx.SelectedItem.ToString(), 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            //Grower Block Label
            currentX = 10;
            currentY = 300;
            e.Graphics.DrawString(ParseTildaString(growerCbx.SelectedItem.ToString(), 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            //box Label
            currentX = 10;
            currentY = 360;
            e.Graphics.DrawString(boxLabelTxt.Text.ToString(), new Font("Arial", 50), Brushes.Black, new Point(currentX, currentY));
            //Box Type Description
            currentX = 10;
            currentY = 420;
            e.Graphics.DrawString(ParseTildaString(jobCbx.SelectedItem.ToString(), 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            //Where Description
            currentX = 10;
            currentY = 480;
            e.Graphics.DrawString(ParseTildaString(fieldCbx.SelectedItem.ToString(), 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            //Where Time Stamp
            currentX = 10;
            currentY = 540;
            e.Graphics.DrawString(timeStampStr, new Font("Arial", 20), Brushes.Black, new Point(currentX, currentY));
            #endregion
            #region Berrybrook Code
            ////Company code
            //currentX = 10;
            //currentY = 30;
            //e.Graphics.DrawString(ParseTildaString(employeeCbx.SelectedItem.ToString(), 0).Trim(), new Font("Arial", 50), Brushes.Black, new Point(currentX, currentY));

            ////Variety Code
            //currentX = 10;
            //currentY = 100;
            //e.Graphics.DrawString(ParseTildaString(cropCbx.SelectedItem.ToString(), 0).Trim(), new Font("Arial", 50), Brushes.Black, new Point(currentX, currentY));

            ////Variety Description
            //currentX = 10;
            //currentY = 280;
            ////e.Graphics.DrawString(ParseTildaString(cropCbx.SelectedItem.ToString(), 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            ////box Label
            //currentX = 10;
            //currentY = 170;
            //e.Graphics.DrawString(boxLabelTxt.Text.ToString(), new Font("Arial", 30), Brushes.Black, new Point(currentX, currentY));
            ////Box Type Description
            //currentX = 10;
            //currentY = 230;
            //e.Graphics.DrawString(ParseTildaString(jobCbx.SelectedItem.ToString(), 0).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            ////Where Description
            //currentX = 10;
            //currentY = 300;
            //e.Graphics.DrawString(ParseTildaString(fieldCbx.SelectedItem.ToString(), 0).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            ////Where Time Stamp
            //currentX = 10;
            //currentY = 360;
            //e.Graphics.DrawString(timeStampStr, new Font("Arial", 20), Brushes.Black, new Point(currentX, currentY));
            #endregion
            currentX = 300;
                    #region Sample Code
                    ////Date
                    //if (label.ReceiveDateDtm != DateTime.MinValue)
                    //{
                    //    string yearStr = label.ReceiveDateDtm.Year.ToString().Substring(2, 2);
                    //    string dayStr = label.ReceiveDateDtm.Day.ToString();
                    //    string monthStr = "";

                    //    switch (label.ReceiveDateDtm.Month)
                    //    {
                    //        case 1:
                    //            monthStr = "JA";
                    //            break;

                    //        case 2:
                    //            monthStr = "FE";
                    //            break;

                    //        case 3:
                    //            monthStr = "MR";
                    //            break;

                    //        case 4:
                    //            monthStr = "AI";
                    //            break;

                    //        case 5:
                    //            monthStr = "MA";
                    //            break;

                    //        case 6:
                    //            monthStr = "JN";
                    //            break;

                    //        case 7:
                    //            monthStr = "JL";
                    //            break;

                    //        case 8:
                    //            monthStr = "AU";
                    //            break;

                    //        case 9:
                    //            monthStr = "SE";
                    //            break;

                    //        case 10:
                    //            monthStr = "OC";
                    //            break;

                    //        case 11:
                    //            monthStr = "NO";
                    //            break;

                    //        case 12:
                    //            monthStr = "DE";
                    //            break;
                    //    }

                    //    string receiveDateStr = yearStr + " " + monthStr + " " + dayStr;

                    //    e.Graphics.DrawString(receiveDateStr, new Font("Arial", 12), Brushes.Black, new Point(currentX, currentY));
                    //}

                    //currentX = 0;
                    //currentY += lineHeight + 5;

                    ////Horizontal line
                    //e.Graphics.DrawLine(new Pen(Brushes.Black), currentX, currentY, totalWidth, currentY);

                    //lineHeight = 30;

                    ////Variety
                    //e.Graphics.DrawString(label.VarietyNameStr, new Font("Arial", 16, FontStyle.Bold), Brushes.Black, new Point(currentX, currentY));

                    ////currentX = 300;
                    //currentX = Convert.ToInt32(totalWidth - (e.Graphics.MeasureString(label.StyleNameStr, new Font("Arial", 16)).Width)) - 20;

                    ////Style
                    //e.Graphics.DrawString(label.StyleNameStr, new Font("Arial", 16), Brushes.Black, new Point(currentX, currentY));

                    //currentX = 0;
                    //currentY += lineHeight - 10;

                    ////Commodity
                    //e.Graphics.DrawString(label.CommodityNameStr, new Font("Arial", 16, FontStyle.Bold), Brushes.Black, new Point(currentX, currentY));

                    ////currentX = 300;
                    //currentX = Convert.ToInt32(totalWidth - (e.Graphics.MeasureString(label.SizeNameStr, new Font("Arial", 16)).Width)) - 20;

                    ////Size
                    //e.Graphics.DrawString(label.SizeNameStr, new Font("Arial", 16), Brushes.Black, new Point(currentX, currentY));

                    //currentX = 0;
                    //currentY += lineHeight;

                    ////Vertical line
                    //e.Graphics.DrawLine(new Pen(Brushes.Black), (totalWidth / 2), -3, (totalWidth / 2), currentY - 5);

                    //currentX = Convert.ToInt32((totalWidth / 2) - (bitmap.Width / 2));

                    ////Barcode
                    //e.Graphics.DrawImage(bitmap, new Point(currentX - 10, currentY - 7));

                    ////currentX = 50;
                    //currentX = Convert.ToInt32((totalWidth / 2) - (e.Graphics.MeasureString("(01)" + gtinNumberStr + "(10)" + lotIdOrPoNumberStr, new Font("Arial", 8)).Width / 2));
                    ////currentX = Convert.ToInt32((totalWidth / 2) - (e.Graphics.MeasureString(gtinNumberStr + lotIdStr, new Font("Arial", 14)).Width / 2));

                    //currentY += bitmap.Height;

                    ////GTIN Number
                    //e.Graphics.DrawString("(01)" + gtinNumberStr + "(10)" + lotIdOrPoNumberStr, new Font("Arial", 8), Brushes.Black, new Point(currentX, currentY));
                    ////e.Graphics.DrawString(gtinNumberStr + lotIdStr, new Font("Arial", 14), Brushes.Black, new Point(currentX, currentY));
                    #endregion
                   
                    #endregion

                    //printArlIndexInt++;

                    //pageCurrentInt++;

                  
                    //if (pageCurrentInt < printArl.Count)
                    //{
                    //    e.HasMorePages = true;
                    //}
                    
               
           
        }
        private void priceTxt_TextChanged(object sender, EventArgs e)
        {
             
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }


       





        
    }
}
