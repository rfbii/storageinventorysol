﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.EnterpriseServices;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;

namespace PayrollFieldDataDesktop
{
	public partial class PayDataByEmployee_Criteria : Form
    {
        #region Vars
        string makeTypeStr = string.Empty;
        private string xmlFileName_Employees;
        public string commentStr = string.Empty;
        #endregion

        #region Constructor
        public PayDataByEmployee_Criteria()
		{
			//DEFAULT - Initialize Form.
			InitializeComponent();

			//Initialize the DatePicker.
			ConstructDatePicker();

			//Initialize selector(s).
			ConstructSelector_Employee();
			ConstructSelector_Crop();
			ConstructSelector_Job();
			ConstructSelector_Field();

			Construct_PayDetailData();
            CompanyNameData.Text = MainMenu.CompanyStr;
			//Make form non-resizable.
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;

            employeesData = new MainMenu.EmployeesData();
            xmlFileName_Employees = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
            FillEmployeeDataFromXML();

		}
		#endregion

		#region Form Functions
        private void runBtn_Click(object sender, EventArgs e)
        {
            commentStr = string.Empty;

            #region Filter out the selections
            SelectedPayDetailsDataByEmployeeArl = new ArrayList();
            
            if (DatePickerFromDateTxt.Text.ToString() != "")
            {
                commentStr += "Date: ";
                commentStr += DatePickerFromDateTxt.Text.ToString();
            }
            if (DatePickerThruDateTxt.Text.ToString() != "")
            {
                commentStr += " To ";
                commentStr += DatePickerThruDateTxt.Text.ToString();
            }
            

            for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
            {
                MainMenu.PayDetailData payDetailData = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];

                if (payDetailData.ActiveBln)
                {
                    bool addPayDetailDataBln = true;

                    if (addPayDetailDataBln && SelectedCropsArl.Count > 0)
                    {
                        for (int j = 0; j < SelectedCropsArl.Count; j++)
                        {
                            MainMenu.CropData cropData = (MainMenu.CropData)SelectedCropsArl[j];

                            if (payDetailData.CropKeyInt == cropData.KeyInt)
                            {
                                addPayDetailDataBln = true;

                                break;
                            }
                            else
                            {
                                addPayDetailDataBln = false;
                            }
                        }
                    }

                    if (addPayDetailDataBln && SelectedJobsArl.Count > 0)
                    {
                        for (int j = 0; j < SelectedJobsArl.Count; j++)
                        {
                            MainMenu.JobData jobData = (MainMenu.JobData)SelectedJobsArl[j];

                            if (payDetailData.JobKeyInt == jobData.KeyInt)
                            {
                                addPayDetailDataBln = true;

                                break;
                            }
                            else
                            {
                                addPayDetailDataBln = false;
                            }
                        }
                    }

                    if (addPayDetailDataBln && SelectedFieldsArl.Count > 0)
                    {
                        for (int j = 0; j < SelectedFieldsArl.Count; j++)
                        {
                            MainMenu.FieldData fieldData = (MainMenu.FieldData)SelectedFieldsArl[j];

                            if (payDetailData.FieldKeyInt == fieldData.KeyInt)
                            {
                                addPayDetailDataBln = true;

                                break;
                            }
                            else
                            {
                                addPayDetailDataBln = false;
                            }
                        }
                    }

                    if (addPayDetailDataBln && DatePickerFromDateTxt.Text.Trim() != "" && DatePickerThruDateTxt.Text.Trim() != "")
                    {
                        if (Convert.ToDateTime(DatePickerFromDateTxt.Text.Trim()) <= payDetailData.DateDtm &&
                            Convert.ToDateTime(DatePickerThruDateTxt.Text.Trim()) >= payDetailData.DateDtm)
                        {
                            addPayDetailDataBln = true;
                        }
                        else
                        {
                            addPayDetailDataBln = false;
                        }
                    }

                    if (addPayDetailDataBln)
                    {
                        SelectedPayDetailsDataByEmployeeArl.Add(payDetailData);
                    }

                   
                }
            }

           
            if (cropsSelectedLst.Items.Count != 0)
            {
                if (commentStr != "")
                {
                    commentStr += "; Crops: ";
                }
                else
                {
                    commentStr += "Crops: ";
                }
                for (int p = 0; p < cropsSelectedLst.Items.Count; p++)
                {
                    if (p != cropsSelectedLst.Items.Count - 1)
                    {
                        commentStr += cropsSelectedLst.Items[p].ToString() + ", ";
                    }
                    else
                    {
                        commentStr += cropsSelectedLst.Items[p].ToString();
                    }
                     
                }
            }
        

            if (jobsSelectedLst.Items.Count != 0)
                {
                    if (cropsSelectedLst.Items.Count != 0 || commentStr != "")
                    {
                        commentStr += "; Jobs: ";
                    }
                    else
                    {
                        commentStr += "Jobs: ";
                    }
                    
                    for (int p = 0; p < jobsSelectedLst.Items.Count; p++)
                    {
                        if (p != jobsSelectedLst.Items.Count - 1)
                        {
                            commentStr += jobsSelectedLst.Items[p].ToString() + ", ";
                        }
                        else
                        {
                            commentStr += jobsSelectedLst.Items[p].ToString();
                        }

                    }
                }
            
                if (fieldsSelectedLst.Items.Count != 0)
                {
                    if (jobsSelectedLst.Items.Count != 0 || commentStr != "")
                    {
                        commentStr += "; Fields: ";
                    }
                    else
                    {
                        commentStr += "Fields: ";
                    }
                    
                    for (int p = 0; p < fieldsSelectedLst.Items.Count; p++)
                    {
                        if (p != fieldsSelectedLst.Items.Count - 1)
                        {
                            commentStr += fieldsSelectedLst.Items[p].ToString() + ", ";
                        }
                        else
                        {
                            commentStr += fieldsSelectedLst.Items[p].ToString();
                        }

                    }
                }
                if (cropsSelectedLst.Items.Count != 0 || jobsSelectedLst.Items.Count != 0 || fieldsSelectedLst.Items.Count != 0)
                {
                    commentStr += ".";
                }
            
            #endregion
            ArrayList payReportTotalsByEmployeeArl = new ArrayList();
            for (int j = 0; j < SelectedPayDetailsDataByEmployeeArl.Count; j++)
            {
                MainMenu.PayDetailData tempPayDetail = (MainMenu.PayDetailData)SelectedPayDetailsDataByEmployeeArl[j];
                
                MainMenu.PayReportDataByEmployee tempTotalPayByEmployee = FindOrAddPayReportDataByEmployee(payReportTotalsByEmployeeArl, FindCode(tempPayDetail.EmployeeKeyInt.ToString()), FindDescription(tempPayDetail.EmployeeKeyInt.ToString()));

                tempTotalPayByEmployee.totalHoursDbl += tempPayDetail.HoursDbl;
                tempTotalPayByEmployee.totalPayDbl += (tempPayDetail.PayUnitDbl * tempPayDetail.PriceDbl);
                tempTotalPayByEmployee.totalPayUnitsDbl += tempPayDetail.PayUnitDbl;
                tempTotalPayByEmployee.totalYieldUnitsDbl += tempPayDetail.YieldUnitDbl;
            }

            if (payReportTotalsByEmployeeArl.Count > 0)
            {
                if (makeTypeStr == "PDF")
                {
                    //check to see what we are running =  PDF OR EXCEL (WE ARE ONLY MAKING A PDF)
                    #region Make PDF
                    string timeStampStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".").Replace(" ", "_");
                    string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Pay Data\";
                    string fileNameStr = "Pay_Data_By_Employee_Report_" + timeStampStr + ".pdf";
                    //string fileNameStr = "Pay_Data_By_Employee_Report.pdf";

                    PayDataByEmployee_Report payDataByEmployee_Report = new PayDataByEmployee_Report();
                    payDataByEmployee_Report.Main(saveFolderStr, fileNameStr, commentStr, payReportTotalsByEmployeeArl);
                    #endregion

                    #region Open PDF
                    Process.Start(saveFolderStr + fileNameStr);
                    #endregion

                    #region Update XML
                    //Serialize (convert an object instance to an XML document):
                    XmlSerializer xmlSerializer = new XmlSerializer(payDetailsData.GetType());
                    // Create an XmlTextWriter using a FileStream.
                    Stream fileStream = new FileStream(PayDetailDataXML, FileMode.Create);
                    XmlWriter xmlWriter = new XmlTextWriter(fileStream, Encoding.Unicode);
                    // Serialize using the XmlTextWriter.
                    xmlSerializer.Serialize(xmlWriter, payDetailsData);
                    xmlWriter.Flush();
                    xmlWriter.Close();
                    #endregion

                }
                #region Excel - Not using
                //else if (makeTypeStr == "Excel") //(NOT BEING UTILIZED)
                //{
                //    string saveFolderExcelStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Pay Data\";
                //    //DisplayHTML(saveFolderExcelStr, SelectedPayDetailsDataArl);
                //}
                #endregion
            }
            else
            {
                MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
		private void exitBtn_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		#endregion

		#region DatePicker
		// ~!~!~   INSTRUCTIONS....   ~!~!~ 
		//
		// To use this DatePicker on another form:
		//      1. Copy this whole DatePicker #region to the new form.
		//      2. Call this constructor -"ConstructDatePicker()" to the new form's constructor.
		//      3. Copy the Grouped Box from this designer view to the new form's designer view.

		#region Constructor
		protected void ConstructDatePicker()
		{
			//Fill the Combobox (DropdownList) with preset date ranges.
			DatePicker_presetsCbx_FillPresetDateRanges();

			//Link the GUI actions/events to the proper functions.
			this.DatePickerPresetsCbx.SelectedIndexChanged += new System.EventHandler(this.DatePicker_presetsCbx_SelectedIndexChanged);
			this.DatePickerEndWeekTxt.TextChanged += new System.EventHandler(this.DatePicker_endYearOrWeek_TextChanged);
			this.DatePickerEndYearTxt.TextChanged += new System.EventHandler(this.DatePicker_endYearOrWeek_TextChanged);
			this.DatePickerStartWeekTxt.TextChanged += new System.EventHandler(this.DatePicker_startYearOrWeek_TextChanged);
			this.DatePickerStartYearTxt.TextChanged += new System.EventHandler(this.DatePicker_startYearOrWeek_TextChanged);
		}
		#endregion

		#region Functions
		#region Events
		private void DatePicker_presetsCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			DatePickerFromDateTxt.Text = " ";
			DatePickerThruDateTxt.Text = " ";

			switch (DatePickerPresetsCbx.SelectedIndex)
			{
				case 0: /*Custom*/
					break;
				case 1: /*Today*/
					DatePickerFromDateTxt.Text = System.DateTime.Now.ToShortDateString();
					DatePickerThruDateTxt.Text = System.DateTime.Now.ToShortDateString();
					ClearCustomEntries();
					break;
				case 2: /*Yesterday*/
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					ClearCustomEntries();
					break;
				case 3: /*Tomorrow*/
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
					ClearCustomEntries();
					break;
				case 4: /*This Week*/
					ThisWeek();
					ClearCustomEntries();
					break;
				case 5: /*This Week Thru Today*/
					ThisWeekThruToday();
					ClearCustomEntries();
					break;
				case 6: /*Last Week*/
					LastWeek();
					ClearCustomEntries();
					break;
				case 7: /*Last 2 Weeks*/
					Last2Weeks();
					ClearCustomEntries();
					break;
				case 8: /*Last 4 Weeks*/
					Last4Weeks();
					ClearCustomEntries();
					break;
				case 9: /*Last 13 Weeks*/
					Last13Weeks();
					ClearCustomEntries();
					break;
				case 10: /*Last 26 Weeks*/
					Last26Weeks();
					ClearCustomEntries();
					break;
				case 11: /*Last 52 Weeks*/
					Last52Weeks();
					ClearCustomEntries();
					break;
				case 12: /*This Month*/
					ThisMonth();
					ClearCustomEntries();
					break;
				case 13: /*This Month thru Today*/
					ThisMonthThruToday();
					ClearCustomEntries();
					break;
				case 14: /*This Month Year Ago*/
					ThisMonthYearAgo();
					ClearCustomEntries();
					break;
				case 15: /*Last Month"*/
					LastMonth();
					ClearCustomEntries();
					break;
				case 16: /*Last Month Year Ago*/
					LastMonthYearAgo();
					ClearCustomEntries();
					break;
				case 17: /*This Year*/
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "12/31/" + (System.DateTime.Today.Year);
					ClearCustomEntries();
					break;
				case 18: /*This Year Thru Today*/
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = System.DateTime.Now.ToShortDateString();
					ClearCustomEntries();
					break;
				case 19: /*Last Year*/
					DatePickerFromDateTxt.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
					ClearCustomEntries();
					break;
			}
		}
		private void DatePicker_startYearOrWeek_TextChanged(object senderIn, EventArgs e)
		{
			//Setup validatable variables.
			int yearEntered, weekEntered;

			//Try to Parse Integers from what the user has entered in the text field.
			if (int.TryParse(DatePickerStartWeekTxt.Text, out weekEntered) && int.TryParse(DatePickerStartYearTxt.Text, out yearEntered)
				&& weekEntered <= 52 && weekEntered >= 1 && yearEntered >= 1 && yearEntered <= 3000)
			{
				//Set the object type.
                System.Web.UI.WebControls.TextBox sender = (System.Web.UI.WebControls.TextBox)senderIn;

				if (sender.Text != "")/*Set the preset list to 'custom'. */
					DatePickerPresetsCbx.SelectedIndex = 0;

				//Create the proper date variable.
				DateTime startDate = new DateTime(yearEntered, 1, 1);/*Set to January 1st of whatever year*/
				startDate = startDate.AddDays(FirstMondayOfTheYear(startDate));
				startDate = startDate.AddDays((weekEntered - 1) * 7);
				startDate.AddDays(weekEntered * 7);

				//Set the GUI with a string from this date.
				DatePickerFromDateTxt.Text = startDate.ToShortDateString();
			}
		}
		private void DatePicker_endYearOrWeek_TextChanged(object senderIn, EventArgs e)
		{
			//Setup validatable variables.
			int yearEntered, weekEntered;

			//Try to Parse Integers from what the user has entered in the text field.
			if (int.TryParse(DatePickerEndWeekTxt.Text, out weekEntered) && int.TryParse(DatePickerEndYearTxt.Text, out yearEntered)
				&& weekEntered <= 52 && weekEntered >= 1 && yearEntered >= 1 && yearEntered <= 3000)
			{
				//Set the object type.
                System.Web.UI.WebControls.TextBox sender = (System.Web.UI.WebControls.TextBox)senderIn;

				if (sender.Text != "")/*Set the preset list to 'custom'. */
					DatePickerPresetsCbx.SelectedIndex = 0;

				//Create the proper date variable.
				DateTime endDate = new DateTime(yearEntered, 1, 1);/*Set to January 1st of whatever year*/
				endDate = endDate.AddDays(FirstMondayOfTheYear(endDate));
				endDate = endDate.AddDays((weekEntered - 1) * 7);
				endDate.AddDays(weekEntered * 7);

				//Set the GUI with a string from this date.
				DatePickerThruDateTxt.Text = endDate.ToShortDateString();
			}
		}
		#endregion

		#region Date Getter Functions
		private void ThisWeek()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+6)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+5)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+4)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+3)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+2)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
					break;
			}
		}
		private void ThisWeekThruToday()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
			}
		}
		private void LastWeek()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-13)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-8)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-9)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-10)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-11)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-12)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last2Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-20)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-14)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-15)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-16)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-17)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-18)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-19)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last4Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-34)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-28)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-29)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-30)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-31)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-32)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-33)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last13Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-97)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-91)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-92)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-93)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-94)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-95)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-96)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last26Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-188)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-182)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-183)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-184)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-184)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-186)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-187)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last52Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-370)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-364)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-365)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-366)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-367)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-368)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-369)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void ThisMonth()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "1/31/" + (System.DateTime.Today.Year);
					break;
				case 2:
					if (System.DateTime.IsLeapYear(System.DateTime.Today.Year))
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/29/" + (System.DateTime.Today.Year);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/28/" + (System.DateTime.Today.Year);
					}
					break;
				case 3:
					DatePickerFromDateTxt.Text = "3/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "3/31/" + (System.DateTime.Today.Year);
					break;
				case 4:
					DatePickerFromDateTxt.Text = "4/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "4/30/" + (System.DateTime.Today.Year);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "5/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "5/31/" + (System.DateTime.Today.Year);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "6/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "6/30/" + (System.DateTime.Today.Year);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "7/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "7/31/" + (System.DateTime.Today.Year);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "8/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "8/31/" + (System.DateTime.Today.Year);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "9/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "9/30/" + (System.DateTime.Today.Year);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "10/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "10/31/" + (System.DateTime.Today.Year);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "11/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "11/30/" + (System.DateTime.Today.Year);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "12/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "12/31/" + (System.DateTime.Today.Year);
					break;
			}
		}
		private void ThisMonthThruToday()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 2:
					DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 3:
					DatePickerFromDateTxt.Text = "3/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 4:
					DatePickerFromDateTxt.Text = "4/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 5:
					DatePickerFromDateTxt.Text = "5/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 6:
					DatePickerFromDateTxt.Text = "6/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 7:
					DatePickerFromDateTxt.Text = "7/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 8:
					DatePickerFromDateTxt.Text = "8/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 9:
					DatePickerFromDateTxt.Text = "9/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 10:
					DatePickerFromDateTxt.Text = "10/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 11:
					DatePickerFromDateTxt.Text = "11/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 12:
					DatePickerFromDateTxt.Text = "12/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
			}
		}
		private void ThisMonthYearAgo()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "1/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 2:
					if (System.DateTime.IsLeapYear((System.DateTime.Today.Year) - 1))
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/29/" + ((System.DateTime.Today.Year) - 1);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/28/" + ((System.DateTime.Today.Year) - 1);
					}
					break;
				case 3:
					DatePickerFromDateTxt.Text = "3/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "3/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 4:
					DatePickerFromDateTxt.Text = "4/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "4/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "5/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "5/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "6/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "6/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "7/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "7/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "8/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "8/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "9/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "9/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "10/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "10/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "11/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "11/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "12/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
					break;
			}
		}
		private void LastMonth()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "12/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 2:
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "1/31/" + (System.DateTime.Today.Year);
					break;
				case 3:
					if (System.DateTime.IsLeapYear(System.DateTime.Today.Year))
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/29/" + (System.DateTime.Today.Year);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/28/" + (System.DateTime.Today.Year);
					}
					break;
				case 4:
					DatePickerFromDateTxt.Text = "3/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "3/31/" + (System.DateTime.Today.Year);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "4/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "4/30/" + (System.DateTime.Today.Year);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "5/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "5/31/" + (System.DateTime.Today.Year);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "6/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "6/30/" + (System.DateTime.Today.Year);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "7/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "7/31/" + (System.DateTime.Today.Year);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "8/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "8/31/" + (System.DateTime.Today.Year);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "9/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "9/30/" + (System.DateTime.Today.Year);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "10/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "10/31/" + (System.DateTime.Today.Year);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "11/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "11/30/" + (System.DateTime.Today.Year);
					break;
			}
		}
		private void LastMonthYearAgo()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "12/1/" + ((System.DateTime.Today.Year) - 2);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 2);
					break;
				case 2:
					DatePickerFromDateTxt.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "1/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 3:
					if (System.DateTime.IsLeapYear((System.DateTime.Today.Year) - 1))
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/29/" + ((System.DateTime.Today.Year) - 1);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/28/" + ((System.DateTime.Today.Year) - 1);
					}
					break;
				case 4:
					DatePickerFromDateTxt.Text = "3/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "3/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "4/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "4/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "5/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "5/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "6/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "6/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "7/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "7/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "8/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "8/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "9/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "9/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "10/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "10/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "11/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "11/30/" + ((System.DateTime.Today.Year) - 1);
					break;
			}
		}
		private int FirstMondayOfTheYear(DateTime date)
		{
			DateTime jan1 = date.AddDays(-(date.DayOfYear - 1));

			if (jan1.DayOfWeek == 0)
				return 1;
			else if (((int)jan1.DayOfWeek) < 5)
				return -(((int)jan1.DayOfWeek) - 1);
			else
				return 8 - ((int)jan1.DayOfWeek);
		}
		#endregion

		#region Helper / General Functions
		private void DatePicker_presetsCbx_FillPresetDateRanges()
		{
			DatePickerPresetsCbx.Items.Add("Custom");
			DatePickerPresetsCbx.Items.Add("Today");
			DatePickerPresetsCbx.Items.Add("Yesterday");
			DatePickerPresetsCbx.Items.Add("Tomorrow");
			DatePickerPresetsCbx.Items.Add("This Week");
			DatePickerPresetsCbx.Items.Add("This Week Thru Today");
			DatePickerPresetsCbx.Items.Add("Last Week");
			DatePickerPresetsCbx.Items.Add("Last 2 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 4 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 13 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 26 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 52 Weeks");
			DatePickerPresetsCbx.Items.Add("This Month");
			DatePickerPresetsCbx.Items.Add("This Month thru Today");
			DatePickerPresetsCbx.Items.Add("This Month Year Ago");
			DatePickerPresetsCbx.Items.Add("Last Month");
			DatePickerPresetsCbx.Items.Add("Last Month Year Ago");
			DatePickerPresetsCbx.Items.Add("This Year");
			DatePickerPresetsCbx.Items.Add("This Year Thru Today");
			DatePickerPresetsCbx.Items.Add("Last Year");

			DatePickerPresetsCbx.SelectedIndex = 0;
		}
		private void ClearCustomEntries()
		{
			this.DatePickerStartYearTxt.Text = "";
			this.DatePickerStartWeekTxt.Text = "";
			this.DatePickerEndWeekTxt.Text = "";
			this.DatePickerEndYearTxt.Text = "";
		}
		#endregion
		#endregion
		#endregion

		#region Selector (Employee)
		// ~!~!~   INSTRUCTIONS....   ~!~!~ 
		//
		// To use this Selector for another data type or on another form:
		//      1. Copy this whole Selector (Employee) #region to the new form.
		//      2. Find & Rename all 'Employee' & 'employee' to match the new data.
		//      3. Call this constructor -"ConstructSelector_Employee()" to the new form's constructor.
		//      4. Copy the Grouped Box from this designer view to the new form's designer view.
		//      5. Rename all of the objects in the designer view - change 'Employee' to match new data.
		//      6. Depending - You may have to change the XML file name in the 'Vars' region.

		#region Vars
		string EmployeeXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
		MainMenu.EmployeesData employeesData;

		private ArrayList AvailableEmployeesArl = new ArrayList();
		private ArrayList SelectedEmployeesArl = new ArrayList();
		#endregion

		#region Constructor
		protected void ConstructSelector_Employee()
		{
			//Fill the available employees.
			FillAvailable_Employee();
		}
		#endregion

		#region Functions
		protected void FillAvailable_Employee()
		{
			FileInfo fileInfo = new FileInfo(EmployeeXML);

			if (fileInfo.Exists)
			{
				//Fill 'employeesData' from XML.

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(EmployeeXML, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
				fileStream.Close();

				//Fill the list from the data.
				for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
				{
					MainMenu.EmployeeData tempEmployeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

					if (tempEmployeeData.ActiveBln)
					{
						AvailableEmployeesArl.Add(tempEmployeeData);
					}
				}
			}
		}
		#endregion
		#endregion

		#region Selector (Crop)
		// ~!~!~   INSTRUCTIONS....   ~!~!~ 
		//
		// To use this Selector for another data type or on another form:
		//      1. Copy this whole Selector (Crop) #region to the new form.
		//      2. Find & Rename all 'Crop' & 'crop' to match the new data.
		//      3. Call this constructor -"ConstructSelector_Crop()" to the new form's constructor.
		//      4. Copy the Grouped Box from this designer view to the new form's designer view.
		//      5. Rename all of the objects in the designer view - change 'Crop' to match new data.
		//      6. Depending - You may have to change the XML file name in the 'Vars' region.

		#region Vars
		string CropXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
		MainMenu.CropsData cropsData;

		private ArrayList AvailableCropsArl = new ArrayList();
		private ArrayList SelectedCropsArl = new ArrayList();
		#endregion

		#region Constructor
		protected void ConstructSelector_Crop()
		{
			//Fill the available crops.
			FillAvailable_Crop();

			//Set attributes.
			this.cropsAvailableLst.SelectionMode = SelectionMode.MultiExtended;
			this.cropsSelectedLst.SelectionMode = SelectionMode.MultiExtended;

			//Link the GUI actions/events to the proper functions.
			this.cropAddBtn.Click += new System.EventHandler(this.AddClick_Crop);
			this.cropRemoveBtn.Click += new System.EventHandler(this.RemoveClick_Crop);
		}
		#endregion

		#region Functions
		protected void FillAvailable_Crop()
		{
			FileInfo fileInfo = new FileInfo(CropXML);

			if (fileInfo.Exists)
			{
				//Fill 'cropsData' from XML.

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(CropXML, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
				fileStream.Close();

				//Fill the list from the data.
				for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
				{
					MainMenu.CropData tempCropData = (MainMenu.CropData)cropsData.CropsDataArl[i];

					if (tempCropData.ActiveBln)
					{
						cropsAvailableLst.Items.Add(tempCropData.Print(" - "));
						AvailableCropsArl.Add(tempCropData);
					}
				}
			}
		}
		private void AddClick_Crop(object sender, EventArgs e)
		{
			//Reverse for loop so it doesn't disrupt the indexes as it goes.
			for (int i = this.cropsAvailableLst.SelectedIndices.Count - 1; i > -1; i--)
			{
				int indexInt = this.cropsAvailableLst.SelectedIndices[i];

				this.cropsSelectedLst.Items.Add(this.cropsAvailableLst.Items[indexInt]);
				this.cropsAvailableLst.Items.RemoveAt(indexInt);

				SelectedCropsArl.Add(AvailableCropsArl[indexInt]);
				AvailableCropsArl.RemoveAt(indexInt);
			}
		}
		private void RemoveClick_Crop(object sender, EventArgs e)
		{
			//Reverse for loop so it doesn't disrupt the indexes as it goes.
			for (int i = this.cropsSelectedLst.SelectedIndices.Count - 1; i > -1; i--)
			{
				int indexInt = this.cropsSelectedLst.SelectedIndices[i];

				this.cropsAvailableLst.Items.Add(this.cropsSelectedLst.Items[indexInt]);
				this.cropsSelectedLst.Items.RemoveAt(indexInt);

				AvailableCropsArl.Add(SelectedCropsArl[indexInt]);
				SelectedCropsArl.RemoveAt(indexInt);
			}
		}
		#endregion
		#endregion

		#region Selector (Job)
		// ~!~!~   INSTRUCTIONS....   ~!~!~ 
		//
		// To use this Selector for another data type or on another form:
		//      1. Copy this whole Selector (Job) #region to the new form.
		//      2. Find & Rename all 'Job' & 'job' to match the new data.
		//      3. Call this constructor -"ConstructSelector_Job()" to the new form's constructor.
		//      4. Copy the Grouped Box from this designer view to the new form's designer view.
		//      5. Rename all of the objects in the designer view - change 'Job' to match new data.
		//      6. Depending - You may have to change the XML file name in the 'Vars' region.

		#region Vars
		string JobXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
		MainMenu.JobsData jobsData;

		private ArrayList AvailableJobsArl = new ArrayList();
		private ArrayList SelectedJobsArl = new ArrayList();
		#endregion

		#region Constructor
		protected void ConstructSelector_Job()
		{
			//Fill the available jobs.
			FillAvailable_Job();

			//Set attributes.
			this.jobsAvailableLst.SelectionMode = SelectionMode.MultiExtended;
			this.jobsSelectedLst.SelectionMode = SelectionMode.MultiExtended;

			//Link the GUI actions/events to the proper functions.
			this.jobAddBtn.Click += new System.EventHandler(this.AddClick_Job);
			this.jobRemoveBtn.Click += new System.EventHandler(this.RemoveClick_Job);
		}
		#endregion

		#region Functions
		protected void FillAvailable_Job()
		{
			FileInfo fileInfo = new FileInfo(JobXML);

			if (fileInfo.Exists)
			{
				//Fill 'jobsData' from XML.

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(JobXML, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
				fileStream.Close();

				//Fill the list from the data.
				for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
				{
					MainMenu.JobData tempJobData = (MainMenu.JobData)jobsData.JobsDataArl[i];

					if (tempJobData.ActiveBln)
					{
						jobsAvailableLst.Items.Add(tempJobData.Print(" - "));
						AvailableJobsArl.Add(tempJobData);
					}
				}
			}
		}
		private void AddClick_Job(object sender, EventArgs e)
		{
			//Reverse for loop so it doesn't disrupt the indexes as it goes.
			for (int i = this.jobsAvailableLst.SelectedIndices.Count - 1; i > -1; i--)
			{
				int indexInt = this.jobsAvailableLst.SelectedIndices[i];

				this.jobsSelectedLst.Items.Add(this.jobsAvailableLst.Items[indexInt]);
				this.jobsAvailableLst.Items.RemoveAt(indexInt);

				SelectedJobsArl.Add(AvailableJobsArl[indexInt]);
				AvailableJobsArl.RemoveAt(indexInt);
			}
		}
		private void RemoveClick_Job(object sender, EventArgs e)
		{
			//Reverse for loop so it doesn't disrupt the indexes as it goes.
			for (int i = this.jobsSelectedLst.SelectedIndices.Count - 1; i > -1; i--)
			{
				int indexInt = this.jobsSelectedLst.SelectedIndices[i];

				this.jobsAvailableLst.Items.Add(this.jobsSelectedLst.Items[indexInt]);
				this.jobsSelectedLst.Items.RemoveAt(indexInt);

				AvailableJobsArl.Add(SelectedJobsArl[indexInt]);
				SelectedJobsArl.RemoveAt(indexInt);
			}
		}
		#endregion
		#endregion

		#region Selector (Field)
		// ~!~!~   INSTRUCTIONS....   ~!~!~ 
		//
		// To use this Selector for another data type or on another form:
		//      1. Copy this whole Selector (Field) #region to the new form.
		//      2. Find & Rename all 'Field' & 'field' to match the new data.
		//      3. Call this constructor -"ConstructSelector_Field()" to the new form's constructor.
		//      4. Copy the Grouped Box from this designer view to the new form's designer view.
		//      5. Rename all of the objects in the designer view - change 'Field' to match new data.
		//      6. Depending - You may have to change the XML file name in the 'Vars' region.

		#region Vars
		string FieldXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
		MainMenu.FieldsData fieldsData;

		private ArrayList AvailableFieldsArl = new ArrayList();
		private ArrayList SelectedFieldsArl = new ArrayList();
		#endregion

		#region Constructor
		protected void ConstructSelector_Field()
		{
			//Fill the available fields.
			FillAvailable_Field();

			//Set attributes.
			this.fieldsAvailableLst.SelectionMode = SelectionMode.MultiExtended;
			this.fieldsSelectedLst.SelectionMode = SelectionMode.MultiExtended;

			//Link the GUI actions/events to the proper functions.
			this.fieldAddBtn.Click += new System.EventHandler(this.AddClick_Field);
			this.fieldRemoveBtn.Click += new System.EventHandler(this.RemoveClick_Field);
		}
		#endregion

		#region Functions
		protected void FillAvailable_Field()
		{
			FileInfo fileInfo = new FileInfo(FieldXML);

			if (fileInfo.Exists)
			{
				//Fill 'fieldsData' from XML.

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(FieldXML, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
				fileStream.Close();

				//Fill the list from the data.
				for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
				{
					MainMenu.FieldData tempFieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

					if (tempFieldData.ActiveBln)
					{
						fieldsAvailableLst.Items.Add(tempFieldData.Print(" - "));
						AvailableFieldsArl.Add(tempFieldData);
					}
				}
			}
		}
		private void AddClick_Field(object sender, EventArgs e)
		{
			//Reverse for loop so it doesn't disrupt the indexes as it goes.
			for (int i = this.fieldsAvailableLst.SelectedIndices.Count - 1; i > -1; i--)
			{
				int indexInt = this.fieldsAvailableLst.SelectedIndices[i];

				this.fieldsSelectedLst.Items.Add(this.fieldsAvailableLst.Items[indexInt]);
				this.fieldsAvailableLst.Items.RemoveAt(indexInt);

				SelectedFieldsArl.Add(AvailableFieldsArl[indexInt]);
				AvailableFieldsArl.RemoveAt(indexInt);
			}
		}
		private void RemoveClick_Field(object sender, EventArgs e)
		{
			//Reverse for loop so it doesn't disrupt the indexes as it goes.
			for (int i = this.fieldsSelectedLst.SelectedIndices.Count - 1; i > -1; i--)
			{
				int indexInt = this.fieldsSelectedLst.SelectedIndices[i];

				this.fieldsAvailableLst.Items.Add(this.fieldsSelectedLst.Items[indexInt]);
				this.fieldsSelectedLst.Items.RemoveAt(indexInt);

				AvailableFieldsArl.Add(SelectedFieldsArl[indexInt]);
				SelectedFieldsArl.RemoveAt(indexInt);
			}
		}
		#endregion
		#endregion

		#region Pay Detail Data
		#region Vars
		string PayDetailDataXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayDetailData.xml";
		MainMenu.PayDetailsData payDetailsData;

		private ArrayList AvailablePayDetailsDataByEmployeeArl = new ArrayList();//not really needed
		private ArrayList SelectedPayDetailsDataByEmployeeArl = new ArrayList();
		#endregion

		#region Constructor
		protected void Construct_PayDetailData()
		{
			FileInfo fileInfo = new FileInfo(PayDetailDataXML);

			if (fileInfo.Exists)
			{
				//Fill 'payDetailsData' from XML.

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayDetailsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(PayDetailDataXML, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				payDetailsData = (MainMenu.PayDetailsData)serializer.Deserialize(xmlReader);
				fileStream.Close();

				//Fill the list from the data.
				for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
				{
					MainMenu.PayDetailData tempPayDetailData = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];

					
						AvailablePayDetailsDataByEmployeeArl.Add(tempPayDetailData);
					
				}
			}
		}
		#endregion

        private void makePDFBtn_Click(object sender, EventArgs e)
        {
            makeTypeStr = "PDF";
            runBtn_Click(sender, e);
        }
		#endregion

        private void makeExcelBtn_Click(object sender, EventArgs e)
        {
            makeTypeStr = "Excel";
            runBtn_Click(sender, e);
        }

        public void DisplayHTML(string excelFile, ArrayList SelectedPayDetailsDataArl)
        {
            #region Vars
            double totalHoursDbl = 0;
			double totalPayUnitDbl = 0;
			double totalYieldUnitDbl = 0;
			double totalPayDbl = 0;
            double totalMinPayDbl = 0;
            double totalOverPayDbl = 0;
            double overRegularTimeDbl = 0;

            string employeeNameStr = string.Empty;
            string employeeAddress1Str = string.Empty;
            string employeeAddress2Str = string.Empty;
            string employeeCityStr = string.Empty;
            string employeeStateStr = string.Empty;
            string employeeZipStr = string.Empty;
            string employeeSSNStr = string.Empty;
            string employeeHourlyWageStr = string.Empty;
            //string employeeOverTimeByAvgWageStr = string.Empty;
            string employeeOverTimeByEmployeeStr = string.Empty;
            string employeeActiveStr = string.Empty;
            string employeeGroupStr = string.Empty;
            string employeeSubInt1Str = string.Empty;
            string employeeSubStr1Str = string.Empty;
            string employeeSubInt2Str = string.Empty;
            string employeeSubStr2Str = string.Empty;
            string employeeSubInt3Str = string.Empty;
            string employeeSubStr3Str = string.Empty;
            string employeeSubInt4Str = string.Empty;
            string employeeSubStr4Str = string.Empty;
            string employeeSubInt5Str = string.Empty;
            string employeeSubStr5Str = string.Empty;

            string cropNameStr = string.Empty;
            string cropActiveStr = string.Empty;
            string cropSubInt1Str = string.Empty;
            string cropSubStr1Str = string.Empty;
            string cropSubInt2Str = string.Empty;
            string cropSubStr2Str = string.Empty;
            string cropSubInt3Str = string.Empty;
            string cropSubStr3Str = string.Empty;
            string cropSubInt4Str = string.Empty;
            string cropSubStr4Str = string.Empty;
            string cropSubInt5Str = string.Empty;
            string cropSubStr5Str = string.Empty;

            string jobNameStr = string.Empty;
            string jobActiveStr = string.Empty;
            string jobSubInt1Str = string.Empty;
            string jobSubStr1Str = string.Empty;
            string jobSubInt2Str = string.Empty;
            string jobSubStr2Str = string.Empty;
            string jobSubInt3Str = string.Empty;
            string jobSubStr3Str = string.Empty;
            string jobSubInt4Str = string.Empty;
            string jobSubStr4Str = string.Empty;
            string jobSubInt5Str = string.Empty;
            string jobSubStr5Str = string.Empty;

            string fieldNameStr = string.Empty;
            string fieldActiveStr = string.Empty;
            string fieldSubInt1Str = string.Empty;
            string fieldSubStr1Str = string.Empty;
            string fieldSubInt2Str = string.Empty;
            string fieldSubStr2Str = string.Empty;
            string fieldSubInt3Str = string.Empty;
            string fieldSubStr3Str = string.Empty;
            string fieldSubInt4Str = string.Empty;
            string fieldSubStr4Str = string.Empty;
            string fieldSubInt5Str = string.Empty;
            string fieldSubStr5Str = string.Empty;
            #endregion

            StreamWriter fileToWriteTo = null;
            string timeStampCodeStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".").Replace(" ", "_");
            fileToWriteTo = new StreamWriter(excelFile + "PayData" + timeStampCodeStr + ".csv");
            
            //Header line to write to file
            #region Header line to write to file
            string lineToWriteToFile = "";
            lineToWriteToFile = "Date, Code, Employee, Hours, YieldUnit, Crop, Job, Field, PayUnit, Price, Total, StartDate, EndDate, ReportDate" +
                                "Name, Address1, Adress2, City, State, Zip, SSNStr, HourlyWage, OverTimeByAvgWage, Active, Group, SubInt1, SubStr1, SubInt2, SubStr2, SubInt3, SubStr3, SubInt4, SubStr4, SubInt5, SubStr5" +
                                "Code, Descr, Active, SubInt1, SubStr1, SubInt2, SubStr2, SubInt3, SubStr3, SubInt4, SubStr4, SubInt5, SubStr5" +
                                "Code, Descr, Active, SubInt1, SubStr1, SubInt2, SubStr2, SubInt3, SubStr3, SubInt4, SubStr4, SubInt5, SubStr5" +
                                "Code, Descr, Active, SubInt1, SubStr1, SubInt2, SubStr2, SubInt3, SubStr3, SubInt4, SubStr4, SubInt5, SubStr5";

            fileToWriteTo.WriteLine(lineToWriteToFile);
            #endregion
            for (int i = 0; i < SelectedPayDetailsDataArl.Count; i++)
				{
					MainMenu.PayDetailData payDetailData = (MainMenu.PayDetailData)SelectedPayDetailsDataArl[i];
               
					string dateStr = "";

					if (payDetailData.DateDtm != DateTime.MinValue)
					{
						dateStr = payDetailData.DateDtm.ToShortDateString();
					}

                    string typeStr = payDetailData.TypeStr.ToString();
                    string startTimeStr = payDetailData.StartTimeDtm.ToShortTimeString();
                    string endTimeStr = payDetailData.EndTimeDtm.ToShortTimeString();
                    string reportDateStr = payDetailData.ReportDateDtm.ToShortDateString();
    
                    #region Employees
                    string employeeStr = "";

					for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
					{
						MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

						if (employeeData.KeyInt == payDetailData.EmployeeKeyInt)
						{
							employeeStr = employeeData.CodeStr;
                            //for Excel doc
                            employeeNameStr = employeeData.NameStr;
                            employeeAddress1Str = employeeData.Address1Str;
                            employeeAddress2Str = employeeData.Address2Str;
                            employeeCityStr = employeeData.CityStr;
                            employeeStateStr = employeeData.StateStr;
                            employeeZipStr = employeeData.ZipStr;
                            employeeSSNStr = employeeData.SSNStr;
                            employeeHourlyWageStr = employeeData.HourlyWageDbl.ToString();
                            employeeOverTimeByEmployeeStr = employeeData.OvertimePerEmployeeBln.ToString();
                            employeeActiveStr = employeeData.ActiveBln.ToString();
                            employeeGroupStr = employeeData.GroupStr;
                            employeeSubInt1Str = employeeData.SubInt1.ToString();
                            if (employeeSubInt1Str == "-1")
                            {
                                employeeSubInt1Str = string.Empty;
                            }
                            employeeSubStr1Str = employeeData.SubStr1;
                            employeeSubInt2Str = employeeData.SubInt1.ToString();
                            if (employeeSubInt2Str == "-1")
                            {
                                employeeSubInt2Str = string.Empty;
                            }
                            employeeSubStr2Str = employeeData.SubStr2;
                            employeeSubInt3Str = employeeData.SubInt3.ToString();
                            if (employeeSubInt3Str == "-1")
                            {
                                employeeSubInt3Str = string.Empty;
                            }
                            employeeSubStr3Str = employeeData.SubStr3;
                            employeeSubInt4Str = employeeData.SubInt4.ToString();
                            if (employeeSubInt4Str == "-1")
                            {
                                employeeSubInt4Str = string.Empty;
                            }
                            employeeSubStr4Str = employeeData.SubStr4;
                            employeeSubInt5Str = employeeData.SubInt5.ToString();
                            if (employeeSubInt5Str == "-1")
                            {
                                employeeSubInt5Str = string.Empty;
                            }
                            employeeSubStr5Str = employeeData.SubStr5;

							break;
						}
                    }
                    #endregion

                    #region Hours
                    string hoursStr = payDetailData.HoursDbl.ToString("##0.##");

					string cropStr = "";

					for (int j = 0; j < cropsData.CropsDataArl.Count; j++)
					{
						MainMenu.CropData cropData = (MainMenu.CropData)cropsData.CropsDataArl[j];

						if (cropData.KeyInt == payDetailData.CropKeyInt)
						{
							cropStr = cropData.CodeStr;
                            //For Excel doc
                            cropNameStr = cropData.DescriptionStr;
                            cropActiveStr = cropData.ActiveBln.ToString();
                           
                            cropSubInt1Str = cropData.SubInt1.ToString();
                            if (cropSubInt1Str == "-1")
                            {
                                cropSubInt1Str = string.Empty;
                            }
                            cropSubStr1Str = cropData.SubStr1;
                            cropSubInt2Str = cropData.SubInt1.ToString();
                            if (cropSubInt2Str == "-1")
                            {
                                cropSubInt2Str = string.Empty;
                            } 
                            cropSubStr2Str = cropData.SubStr2;
                            cropSubInt3Str = cropData.SubInt3.ToString();
                            if (cropSubInt3Str == "-1")
                            {
                                cropSubInt3Str = string.Empty;
                            }
                            cropSubStr3Str = cropData.SubStr3;
                            cropSubInt4Str = cropData.SubInt4.ToString();
                            if (cropSubInt4Str == "-1")
                            {
                                cropSubInt4Str = string.Empty;
                            }
                            cropSubStr4Str = cropData.SubStr4;
                            cropSubInt5Str = cropData.SubInt5.ToString();
                            if (cropSubInt5Str == "-1")
                            {
                                cropSubInt5Str = string.Empty;
                            }
                            cropSubStr5Str = cropData.SubStr5;

							break;
						}
                    }
#endregion

                    #region Jobs
                    string jobStr = "";

					for (int j = 0; j < jobsData.JobsDataArl.Count; j++)
					{
						MainMenu.JobData jobData = (MainMenu.JobData)jobsData.JobsDataArl[j];

						if (jobData.KeyInt == payDetailData.JobKeyInt)
						{
							jobStr = jobData.CodeStr;

                            //For Excel doc
                            jobNameStr = jobData.DescriptionStr;
                            jobActiveStr = jobData.ActiveBln.ToString();
                            jobSubInt1Str = jobData.SubInt1.ToString();
                            if (jobSubInt1Str == "-1")
                            {
                                jobSubInt1Str = string.Empty;
                            }
                            jobSubStr1Str = jobData.SubStr1;
                            jobSubInt2Str = jobData.SubInt1.ToString();
                            if (jobSubInt2Str == "-1")
                            {
                                jobSubInt2Str = string.Empty;
                            }
                            jobSubStr2Str = jobData.SubStr2;
                            jobSubInt3Str = jobData.SubInt3.ToString();
                            if (jobSubInt3Str == "-1")
                            {
                                jobSubInt3Str = string.Empty;
                            }
                            jobSubStr3Str = jobData.SubStr3;
                            jobSubInt4Str = jobData.SubInt4.ToString();
                            if (jobSubInt4Str == "-1")
                            {
                                jobSubInt4Str = string.Empty;
                            }
                            jobSubStr4Str = jobData.SubStr4;
                            jobSubInt5Str = jobData.SubInt5.ToString();
                            if (jobSubInt5Str == "-1")
                            {
                                jobSubInt5Str = string.Empty;
                            }
                            jobSubStr5Str = jobData.SubStr5;

							break;
						}
                    }
#endregion

                    #region Fields
                    string fieldStr = "";

					for (int j = 0; j < fieldsData.FieldsDataArl.Count; j++)
					{
						MainMenu.FieldData fieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[j];

						if (fieldData.KeyInt == payDetailData.FieldKeyInt)
						{
							fieldStr = fieldData.CodeStr;

                            //For Excel doc
                            fieldNameStr = fieldData.DescriptionStr;
                            fieldActiveStr = fieldData.ActiveBln.ToString();
                            fieldSubInt1Str = fieldData.SubInt1.ToString();
                            if (fieldSubInt1Str == "-1")
                            {
                                fieldSubInt1Str = string.Empty;
                            }
                            fieldSubStr1Str = fieldData.SubStr1;
                            fieldSubInt2Str = fieldData.SubInt1.ToString();
                            if (fieldSubInt2Str == "-1")
                            {
                                fieldSubInt2Str = string.Empty;
                            }
                            fieldSubStr2Str = fieldData.SubStr2;
                            fieldSubInt3Str = fieldData.SubInt3.ToString();
                            if (fieldSubInt3Str == "-1")
                            {
                                fieldSubInt3Str = string.Empty;
                            }
                            fieldSubStr3Str = fieldData.SubStr3;
                            fieldSubInt4Str = fieldData.SubInt4.ToString();
                            if (fieldSubInt4Str == "-1")
                            {
                                fieldSubInt4Str = string.Empty;
                            }
                            fieldSubStr4Str = fieldData.SubStr4;
                            fieldSubInt5Str = fieldData.SubInt5.ToString();
                            if (fieldSubInt5Str == "-1")
                            {
                                fieldSubInt5Str = string.Empty;
                            }
                            fieldSubStr5Str = fieldData.SubStr5;
                            break;
                        }
                    #endregion

                    }

                    string payUnitStr = payDetailData.PayUnitDbl.ToString("###,###,##0.##");
                    string yieldUnitStr = payDetailData.YieldUnitDbl.ToString("###,###,##0.##");
                    string priceStr = payDetailData.PriceDbl.ToString("$###,###,##0.00");
                    string totalDollarsStr = payDetailData.TotalDollarsStr;

					totalHoursDbl += payDetailData.HoursDbl;
					totalPayUnitDbl += payDetailData.PayUnitDbl;
					totalYieldUnitDbl += payDetailData.YieldUnitDbl;
					totalPayDbl += payDetailData.PriceDbl * payDetailData.PayUnitDbl;
                    if (payDetailData.TypeStr == "M")
                    {
                        totalMinPayDbl += payDetailData.PriceDbl;
                    }
                    if (payDetailData.TypeStr == "O")
                    {
                        totalOverPayDbl += payDetailData.PriceDbl;                       
                    }
                    if (totalOverPayDbl > 0)
                    {
                        overRegularTimeDbl = totalOverPayDbl * 3;
                    }

                    //Line to Write To File
                    #region Line to write to file
                    lineToWriteToFile = dateStr + "," +
                                        typeStr + "," +
                                        employeeStr + "," +
                                        hoursStr + "," +
                                        yieldUnitStr + "," +
                                        cropStr + "," +
                                        jobStr + "," +
                                        fieldStr + "," +
                                        payUnitStr + "," +
                                        priceStr + "," +
                                        totalDollarsStr + "," +
                                        startTimeStr + "," +
                                        endTimeStr + "," +
                                        reportDateStr + "," +
                                        employeeNameStr + "," +
                                        employeeAddress1Str + "," +
                                        employeeAddress2Str + "," +
                                        employeeCityStr + "," +
                                        employeeStateStr + "," +
                                        employeeZipStr + "," +
                                        employeeSSNStr + "," +
                                        employeeHourlyWageStr + "," +
                                        employeeOverTimeByEmployeeStr + "," +
                                        employeeActiveStr + "," +
                                        employeeGroupStr + "," +
                                        employeeSubInt1Str + "," +
                                        employeeSubStr1Str + "," +
                                        employeeSubInt2Str + "," +
                                        employeeSubInt3Str + "," +
                                        employeeSubStr3Str + "," +
                                        employeeSubInt4Str + "," +
                                        employeeSubStr4Str + "," +
                                        employeeSubInt5Str + "," +
                                        employeeSubStr5Str + "," +
                                        cropNameStr + "," +
                                        cropActiveStr + "," +
                                        cropSubInt1Str + "," +
                                        cropSubStr1Str + "," +
                                        cropSubInt2Str + "," +
                                        cropSubStr2Str + "," +
                                        cropSubInt3Str + "," +
                                        cropSubStr3Str + "," +
                                        cropSubInt4Str + "," +
                                        cropSubStr4Str + "," +
                                        cropSubInt5Str + "," +
                                        cropSubStr5Str + "," +
                                        jobNameStr + "," +
                                        jobActiveStr + "," +
                                        jobSubInt1Str + "," +
                                        jobSubStr1Str + "," +
                                        jobSubInt2Str + "," +
                                        jobSubStr2Str + "," +
                                        jobSubInt3Str + "," +
                                        jobSubStr3Str + "," +
                                        jobSubInt4Str + "," +
                                        jobSubStr4Str + "," +
                                        jobSubInt5Str + "," +
                                        jobSubStr5Str + "," +
                                        fieldNameStr + "," +
                                        fieldActiveStr + "," +
                                        fieldSubInt1Str + "," +
                                        fieldSubStr1Str + "," +
                                        fieldSubInt2Str + "," +
                                        fieldSubStr2Str + "," +
                                        fieldSubInt3Str + "," +
                                        fieldSubStr3Str + "," +
                                        fieldSubInt4Str + "," +
                                        fieldSubStr4Str + "," +
                                        fieldSubInt5Str + "," +
                                        fieldSubStr5Str;
                                        
                    fileToWriteTo.WriteLine(lineToWriteToFile);
                #endregion
            }
          
            #region Close file streams
            if (fileToWriteTo != null)
            {
                fileToWriteTo.Close();
            }
           #endregion
        }
        private void FillEmployeeDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Employees);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Employees, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        #region Public Helper
        public MainMenu.PayReportDataByEmployee FindOrAddPayReportDataByEmployee(ArrayList list, string codeStr, string nameStr)
        {
            MainMenu.PayReportDataByEmployee tempPayReportDataByEmployee = new MainMenu.PayReportDataByEmployee(codeStr, nameStr);
            int tempPayReportDataByEmployeeIdx = list.BinarySearch(tempPayReportDataByEmployee);

            if (tempPayReportDataByEmployeeIdx < 0)
                list.Insert(Math.Abs(tempPayReportDataByEmployeeIdx) - 1, tempPayReportDataByEmployee);
            else
                tempPayReportDataByEmployee = (MainMenu.PayReportDataByEmployee)list[tempPayReportDataByEmployeeIdx];

            return tempPayReportDataByEmployee;
        }
        private string FindDescription(string codeIn)
        {

            for (int i = 0; i < this.employeesData.EmployeesDataArl.Count; i++)
            {
                MainMenu.EmployeeData employee = (MainMenu.EmployeeData)this.employeesData.EmployeesDataArl[i];

                if (employee.KeyInt.ToString() == codeIn)
                {
                    return employee.NameStr;
                }
            }

            return "";
        }
        private string FindCode(string codeIn)
        {
            for (int i = 0; i < this.employeesData.EmployeesDataArl.Count; i++)
            {
                MainMenu.EmployeeData employee = (MainMenu.EmployeeData)this.employeesData.EmployeesDataArl[i];

                if (employee.KeyInt.ToString() == codeIn)
                {
                    return employee.CodeStr;
                }
            }

            return "";
        }
        #endregion

    }
}
