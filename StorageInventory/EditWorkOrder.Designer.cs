﻿namespace StorageInventory
{
    partial class EditWorkOrder 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitBtn = new System.Windows.Forms.Button();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.startDateLbl = new System.Windows.Forms.Label();
            this.cropLbl = new System.Windows.Forms.Label();
            this.jobLbl = new System.Windows.Forms.Label();
            this.fieldLbl = new System.Windows.Forms.Label();
            this.cropCbx = new System.Windows.Forms.ComboBox();
            this.jobCbx = new System.Windows.Forms.ComboBox();
            this.fieldCbx = new System.Windows.Forms.ComboBox();
            this.savedPayDetailsLst = new System.Windows.Forms.ListBox();
            this.fillScreenBtn = new System.Windows.Forms.Button();
            this.saveEditedBtn = new System.Windows.Forms.Button();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.employeeCbx = new System.Windows.Forms.ComboBox();
            this.employeeLbl = new System.Windows.Forms.Label();
            this.editEmployeeCbx = new System.Windows.Forms.ComboBox();
            this.editEmployeeLbl = new System.Windows.Forms.Label();
            this.editDatePicker = new System.Windows.Forms.DateTimePicker();
            this.editFieldCbx = new System.Windows.Forms.ComboBox();
            this.editJobCbx = new System.Windows.Forms.ComboBox();
            this.editCropCbx = new System.Windows.Forms.ComboBox();
            this.editYieldUnitTxt = new System.Windows.Forms.TextBox();
            this.editYieldUnitLbl = new System.Windows.Forms.Label();
            this.editPayUnitTxt = new System.Windows.Forms.TextBox();
            this.editPayUnitLbl = new System.Windows.Forms.Label();
            this.editFieldLbl = new System.Windows.Forms.Label();
            this.editJobLbl = new System.Windows.Forms.Label();
            this.editCropLbl = new System.Windows.Forms.Label();
            this.editDateLbl = new System.Windows.Forms.Label();
            this.deleteEditedBtn = new System.Windows.Forms.Button();
            this.savedPayDetailsHeaderLbl = new System.Windows.Forms.Label();
            this.keyLst = new System.Windows.Forms.ListBox();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.endDatePicker = new System.Windows.Forms.DateTimePicker();
            this.endDateLabel = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.EndEditBtn = new System.Windows.Forms.Button();
            this.sortbyDateChx = new System.Windows.Forms.CheckBox();
            this.hashTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.KeyCentralPbx = new System.Windows.Forms.PictureBox();
            this.editBoxLabelTxt = new System.Windows.Forms.TextBox();
            this.editBoxLabelLbl = new System.Windows.Forms.Label();
            this.editReferenceTxt = new System.Windows.Forms.TextBox();
            this.editReferenceLbl = new System.Windows.Forms.Label();
            this.editOtherTxt = new System.Windows.Forms.TextBox();
            this.editOtherLbl = new System.Windows.Forms.Label();
            this.growerLbl = new System.Windows.Forms.Label();
            this.growerCbx = new System.Windows.Forms.ComboBox();
            this.editGrowerCbx = new System.Windows.Forms.ComboBox();
            this.editGrowerblockLbl = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KeyCentralPbx)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(408, 472);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(92, 34);
            this.exitBtn.TabIndex = 41;
            this.exitBtn.Text = "E&xit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(725, 4);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(151, 20);
            this.subtitleLbl.TabIndex = 44;
            this.subtitleLbl.Text = "Edit Inventory Detail";
            // 
            // startDateLbl
            // 
            this.startDateLbl.AutoSize = true;
            this.startDateLbl.Location = new System.Drawing.Point(28, 46);
            this.startDateLbl.Name = "startDateLbl";
            this.startDateLbl.Size = new System.Drawing.Size(55, 13);
            this.startDateLbl.TabIndex = 45;
            this.startDateLbl.Text = "Start Date";
            this.startDateLbl.Visible = false;
            // 
            // cropLbl
            // 
            this.cropLbl.AutoSize = true;
            this.cropLbl.Location = new System.Drawing.Point(316, 46);
            this.cropLbl.Name = "cropLbl";
            this.cropLbl.Size = new System.Drawing.Size(29, 13);
            this.cropLbl.TabIndex = 49;
            this.cropLbl.Text = "Crop";
            // 
            // jobLbl
            // 
            this.jobLbl.AutoSize = true;
            this.jobLbl.Location = new System.Drawing.Point(401, 46);
            this.jobLbl.Name = "jobLbl";
            this.jobLbl.Size = new System.Drawing.Size(52, 13);
            this.jobLbl.TabIndex = 50;
            this.jobLbl.Text = "Box Type";
            // 
            // fieldLbl
            // 
            this.fieldLbl.AutoSize = true;
            this.fieldLbl.Location = new System.Drawing.Point(481, 46);
            this.fieldLbl.Name = "fieldLbl";
            this.fieldLbl.Size = new System.Drawing.Size(39, 13);
            this.fieldLbl.TabIndex = 51;
            this.fieldLbl.Text = "Where";
            // 
            // cropCbx
            // 
            this.cropCbx.FormattingEnabled = true;
            this.cropCbx.Location = new System.Drawing.Point(292, 61);
            this.cropCbx.Name = "cropCbx";
            this.cropCbx.Size = new System.Drawing.Size(77, 21);
            this.cropCbx.Sorted = true;
            this.cropCbx.TabIndex = 4;
            this.cropCbx.DropDown += new System.EventHandler(this.cropCbx_DropDown);
            this.cropCbx.SelectedIndexChanged += new System.EventHandler(this.cropCbx_SelectedIndexChanged);
            this.cropCbx.DropDownClosed += new System.EventHandler(this.cropCbx_DropDownClosed);
            this.cropCbx.Enter += new System.EventHandler(this.cropCbx_Enter);
            this.cropCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cropCbx_KeyDown);
            this.cropCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // jobCbx
            // 
            this.jobCbx.FormattingEnabled = true;
            this.jobCbx.Location = new System.Drawing.Point(375, 61);
            this.jobCbx.Name = "jobCbx";
            this.jobCbx.Size = new System.Drawing.Size(77, 21);
            this.jobCbx.Sorted = true;
            this.jobCbx.TabIndex = 5;
            this.jobCbx.DropDown += new System.EventHandler(this.jobCbx_DropDown);
            this.jobCbx.SelectedIndexChanged += new System.EventHandler(this.jobCbx_SelectedIndexChanged);
            this.jobCbx.DropDownClosed += new System.EventHandler(this.jobCbx_DropDownClosed);
            this.jobCbx.Enter += new System.EventHandler(this.jobCbx_Enter);
            this.jobCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.jobCbx_KeyDown);
            this.jobCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // fieldCbx
            // 
            this.fieldCbx.FormattingEnabled = true;
            this.fieldCbx.Location = new System.Drawing.Point(457, 61);
            this.fieldCbx.Name = "fieldCbx";
            this.fieldCbx.Size = new System.Drawing.Size(77, 21);
            this.fieldCbx.Sorted = true;
            this.fieldCbx.TabIndex = 6;
            this.fieldCbx.DropDown += new System.EventHandler(this.fieldCbx_DropDown);
            this.fieldCbx.SelectedIndexChanged += new System.EventHandler(this.fieldCbx_SelectedIndexChanged);
            this.fieldCbx.DropDownClosed += new System.EventHandler(this.fieldCbx_DropDownClosed);
            this.fieldCbx.Enter += new System.EventHandler(this.fieldCbx_Enter);
            this.fieldCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fieldCbx_KeyDown);
            this.fieldCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // savedPayDetailsLst
            // 
            this.savedPayDetailsLst.Font = new System.Drawing.Font("Courier New", 9F);
            this.savedPayDetailsLst.FormattingEnabled = true;
            this.savedPayDetailsLst.ItemHeight = 15;
            this.savedPayDetailsLst.Location = new System.Drawing.Point(9, 152);
            this.savedPayDetailsLst.Name = "savedPayDetailsLst";
            this.savedPayDetailsLst.Size = new System.Drawing.Size(973, 274);
            this.savedPayDetailsLst.TabIndex = 27;
            this.savedPayDetailsLst.SelectedIndexChanged += new System.EventHandler(this.savedPayDetailsLst_SelectedIndexChanged);
            // 
            // fillScreenBtn
            // 
            this.fillScreenBtn.Location = new System.Drawing.Point(770, 9);
            this.fillScreenBtn.Name = "fillScreenBtn";
            this.fillScreenBtn.Size = new System.Drawing.Size(120, 34);
            this.fillScreenBtn.TabIndex = 9;
            this.fillScreenBtn.Text = "Fill Screen";
            this.fillScreenBtn.UseVisualStyleBackColor = true;
            this.fillScreenBtn.Click += new System.EventHandler(this.fillScreenBtn_Click);
            this.fillScreenBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.saveBtn_KeyDown);
            // 
            // saveEditedBtn
            // 
            this.saveEditedBtn.Enabled = false;
            this.saveEditedBtn.Location = new System.Drawing.Point(642, 471);
            this.saveEditedBtn.Name = "saveEditedBtn";
            this.saveEditedBtn.Size = new System.Drawing.Size(125, 34);
            this.saveEditedBtn.TabIndex = 39;
            this.saveEditedBtn.Text = "Save &Edited Detail";
            this.saveEditedBtn.UseVisualStyleBackColor = true;
            this.saveEditedBtn.Click += new System.EventHandler(this.saveEditedBtn_Click);
            this.saveEditedBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editBtn_KeyDown);
            // 
            // startDatePicker
            // 
            this.startDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDatePicker.Location = new System.Drawing.Point(7, 61);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(97, 20);
            this.startDatePicker.TabIndex = 0;
            this.startDatePicker.Visible = false;
            this.startDatePicker.Enter += new System.EventHandler(this.datePicker_Enter);
            this.startDatePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.datePicker_KeyDown);
            // 
            // employeeCbx
            // 
            this.employeeCbx.FormattingEnabled = true;
            this.employeeCbx.Location = new System.Drawing.Point(211, 61);
            this.employeeCbx.Name = "employeeCbx";
            this.employeeCbx.Size = new System.Drawing.Size(77, 21);
            this.employeeCbx.Sorted = true;
            this.employeeCbx.TabIndex = 1;
            this.employeeCbx.DropDown += new System.EventHandler(this.employeeCbx_DropDown);
            this.employeeCbx.SelectedIndexChanged += new System.EventHandler(this.employeeCbx_SelectedIndexChanged);
            this.employeeCbx.DropDownClosed += new System.EventHandler(this.employeeCbx_DropDownClosed);
            this.employeeCbx.Enter += new System.EventHandler(this.employeeCbx_Enter);
            this.employeeCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.employeeCbx_KeyDown);
            // 
            // employeeLbl
            // 
            this.employeeLbl.AutoSize = true;
            this.employeeLbl.Location = new System.Drawing.Point(223, 46);
            this.employeeLbl.Name = "employeeLbl";
            this.employeeLbl.Size = new System.Drawing.Size(51, 13);
            this.employeeLbl.TabIndex = 46;
            this.employeeLbl.Text = "Customer";
            // 
            // editEmployeeCbx
            // 
            this.editEmployeeCbx.Enabled = false;
            this.editEmployeeCbx.FormattingEnabled = true;
            this.editEmployeeCbx.Location = new System.Drawing.Point(104, 446);
            this.editEmployeeCbx.Name = "editEmployeeCbx";
            this.editEmployeeCbx.Size = new System.Drawing.Size(77, 21);
            this.editEmployeeCbx.Sorted = true;
            this.editEmployeeCbx.TabIndex = 31;
            this.editEmployeeCbx.SelectedIndexChanged += new System.EventHandler(this.editEmployeeCbx_SelectedIndexChanged);
            this.editEmployeeCbx.DropDownClosed += new System.EventHandler(this.editEmployeeCbx_DropDownClosed);
            this.editEmployeeCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editEmployeeCbx_KeyDown);
            // 
            // editEmployeeLbl
            // 
            this.editEmployeeLbl.AutoSize = true;
            this.editEmployeeLbl.Enabled = false;
            this.editEmployeeLbl.Location = new System.Drawing.Point(117, 430);
            this.editEmployeeLbl.Name = "editEmployeeLbl";
            this.editEmployeeLbl.Size = new System.Drawing.Size(51, 13);
            this.editEmployeeLbl.TabIndex = 62;
            this.editEmployeeLbl.Text = "Customer";
            // 
            // editDatePicker
            // 
            this.editDatePicker.Enabled = false;
            this.editDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.editDatePicker.Location = new System.Drawing.Point(4, 446);
            this.editDatePicker.Name = "editDatePicker";
            this.editDatePicker.Size = new System.Drawing.Size(97, 20);
            this.editDatePicker.TabIndex = 28;
            this.editDatePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editDatePicker_KeyDown);
            // 
            // editFieldCbx
            // 
            this.editFieldCbx.Enabled = false;
            this.editFieldCbx.FormattingEnabled = true;
            this.editFieldCbx.Location = new System.Drawing.Point(345, 446);
            this.editFieldCbx.Name = "editFieldCbx";
            this.editFieldCbx.Size = new System.Drawing.Size(77, 21);
            this.editFieldCbx.Sorted = true;
            this.editFieldCbx.TabIndex = 36;
            this.editFieldCbx.SelectedIndexChanged += new System.EventHandler(this.editFieldCbx_SelectedIndexChanged);
            this.editFieldCbx.DropDownClosed += new System.EventHandler(this.editFieldCbx_DropDownClosed);
            this.editFieldCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editFieldCbx_KeyDown);
            // 
            // editJobCbx
            // 
            this.editJobCbx.Enabled = false;
            this.editJobCbx.FormattingEnabled = true;
            this.editJobCbx.Location = new System.Drawing.Point(265, 446);
            this.editJobCbx.Name = "editJobCbx";
            this.editJobCbx.Size = new System.Drawing.Size(77, 21);
            this.editJobCbx.Sorted = true;
            this.editJobCbx.TabIndex = 35;
            this.editJobCbx.SelectedIndexChanged += new System.EventHandler(this.editJobCbx_SelectedIndexChanged);
            this.editJobCbx.DropDownClosed += new System.EventHandler(this.editJobCbx_DropDownClosed);
            this.editJobCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editJobCbx_KeyDown);
            // 
            // editCropCbx
            // 
            this.editCropCbx.Enabled = false;
            this.editCropCbx.FormattingEnabled = true;
            this.editCropCbx.Location = new System.Drawing.Point(185, 446);
            this.editCropCbx.Name = "editCropCbx";
            this.editCropCbx.Size = new System.Drawing.Size(77, 21);
            this.editCropCbx.Sorted = true;
            this.editCropCbx.TabIndex = 34;
            this.editCropCbx.SelectedIndexChanged += new System.EventHandler(this.editCropCbx_SelectedIndexChanged);
            this.editCropCbx.DropDownClosed += new System.EventHandler(this.editCropCbx_DropDownClosed);
            this.editCropCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editCropCbx_KeyDown);
            // 
            // editYieldUnitTxt
            // 
            this.editYieldUnitTxt.Enabled = false;
            this.editYieldUnitTxt.Location = new System.Drawing.Point(905, 446);
            this.editYieldUnitTxt.Name = "editYieldUnitTxt";
            this.editYieldUnitTxt.Size = new System.Drawing.Size(77, 20);
            this.editYieldUnitTxt.TabIndex = 38;
            this.editYieldUnitTxt.Text = "0";
            this.editYieldUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editYieldUnitTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editPriceTxt_KeyDown);
            this.editYieldUnitTxt.Leave += new System.EventHandler(this.editPriceTxt_Leave);
            // 
            // editYieldUnitLbl
            // 
            this.editYieldUnitLbl.AutoSize = true;
            this.editYieldUnitLbl.Enabled = false;
            this.editYieldUnitLbl.Location = new System.Drawing.Point(913, 430);
            this.editYieldUnitLbl.Name = "editYieldUnitLbl";
            this.editYieldUnitLbl.Size = new System.Drawing.Size(63, 13);
            this.editYieldUnitLbl.TabIndex = 69;
            this.editYieldUnitLbl.Text = "Amount Out";
            // 
            // editPayUnitTxt
            // 
            this.editPayUnitTxt.Enabled = false;
            this.editPayUnitTxt.Location = new System.Drawing.Point(823, 446);
            this.editPayUnitTxt.Name = "editPayUnitTxt";
            this.editPayUnitTxt.Size = new System.Drawing.Size(77, 20);
            this.editPayUnitTxt.TabIndex = 37;
            this.editPayUnitTxt.Text = "0";
            this.editPayUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editPayUnitTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editPayUnitTxt_KeyDown);
            this.editPayUnitTxt.Leave += new System.EventHandler(this.editPayUnitTxt_Leave);
            // 
            // editPayUnitLbl
            // 
            this.editPayUnitLbl.AutoSize = true;
            this.editPayUnitLbl.Enabled = false;
            this.editPayUnitLbl.Location = new System.Drawing.Point(835, 430);
            this.editPayUnitLbl.Name = "editPayUnitLbl";
            this.editPayUnitLbl.Size = new System.Drawing.Size(55, 13);
            this.editPayUnitLbl.TabIndex = 68;
            this.editPayUnitLbl.Text = "Amount In";
            // 
            // editFieldLbl
            // 
            this.editFieldLbl.AutoSize = true;
            this.editFieldLbl.Enabled = false;
            this.editFieldLbl.Location = new System.Drawing.Point(369, 430);
            this.editFieldLbl.Name = "editFieldLbl";
            this.editFieldLbl.Size = new System.Drawing.Size(39, 13);
            this.editFieldLbl.TabIndex = 67;
            this.editFieldLbl.Text = "Where";
            // 
            // editJobLbl
            // 
            this.editJobLbl.AutoSize = true;
            this.editJobLbl.Enabled = false;
            this.editJobLbl.Location = new System.Drawing.Point(278, 430);
            this.editJobLbl.Name = "editJobLbl";
            this.editJobLbl.Size = new System.Drawing.Size(52, 13);
            this.editJobLbl.TabIndex = 66;
            this.editJobLbl.Text = "Box Type";
            // 
            // editCropLbl
            // 
            this.editCropLbl.AutoSize = true;
            this.editCropLbl.Enabled = false;
            this.editCropLbl.Location = new System.Drawing.Point(209, 430);
            this.editCropLbl.Name = "editCropLbl";
            this.editCropLbl.Size = new System.Drawing.Size(29, 13);
            this.editCropLbl.TabIndex = 65;
            this.editCropLbl.Text = "Crop";
            // 
            // editDateLbl
            // 
            this.editDateLbl.AutoSize = true;
            this.editDateLbl.Enabled = false;
            this.editDateLbl.Location = new System.Drawing.Point(38, 430);
            this.editDateLbl.Name = "editDateLbl";
            this.editDateLbl.Size = new System.Drawing.Size(30, 13);
            this.editDateLbl.TabIndex = 59;
            this.editDateLbl.Text = "Date";
            // 
            // deleteEditedBtn
            // 
            this.deleteEditedBtn.Enabled = false;
            this.deleteEditedBtn.Location = new System.Drawing.Point(775, 472);
            this.deleteEditedBtn.Name = "deleteEditedBtn";
            this.deleteEditedBtn.Size = new System.Drawing.Size(125, 34);
            this.deleteEditedBtn.TabIndex = 40;
            this.deleteEditedBtn.Text = "&Delete Detail";
            this.deleteEditedBtn.UseVisualStyleBackColor = true;
            this.deleteEditedBtn.Click += new System.EventHandler(this.deleteEditedBtn_Click);
            this.deleteEditedBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deleteEditedBtn_KeyDown);
            // 
            // savedPayDetailsHeaderLbl
            // 
            this.savedPayDetailsHeaderLbl.AutoSize = true;
            this.savedPayDetailsHeaderLbl.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedPayDetailsHeaderLbl.Location = new System.Drawing.Point(10, 131);
            this.savedPayDetailsHeaderLbl.Name = "savedPayDetailsHeaderLbl";
            this.savedPayDetailsHeaderLbl.Size = new System.Drawing.Size(966, 15);
            this.savedPayDetailsHeaderLbl.TabIndex = 58;
            this.savedPayDetailsHeaderLbl.Text = "|   Date   | Customer                    |  Crop  |Box Type| Where | Grower Block" +
    " | Box Label | Reference | Other | Amount In|Amount Out|";
            // 
            // keyLst
            // 
            this.keyLst.FormattingEnabled = true;
            this.keyLst.Location = new System.Drawing.Point(131, 19);
            this.keyLst.Name = "keyLst";
            this.keyLst.Size = new System.Drawing.Size(120, 17);
            this.keyLst.TabIndex = 42;
            this.keyLst.Visible = false;
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 9);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 103;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // endDatePicker
            // 
            this.endDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDatePicker.Location = new System.Drawing.Point(108, 61);
            this.endDatePicker.Name = "endDatePicker";
            this.endDatePicker.Size = new System.Drawing.Size(97, 20);
            this.endDatePicker.TabIndex = 104;
            this.endDatePicker.Visible = false;
            // 
            // endDateLabel
            // 
            this.endDateLabel.AutoSize = true;
            this.endDateLabel.Location = new System.Drawing.Point(130, 46);
            this.endDateLabel.Name = "endDateLabel";
            this.endDateLabel.Size = new System.Drawing.Size(52, 13);
            this.endDateLabel.TabIndex = 105;
            this.endDateLabel.Text = "End Date";
            this.endDateLabel.Visible = false;
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(626, 9);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(120, 34);
            this.clearBtn.TabIndex = 106;
            this.clearBtn.Text = "Clear Selections";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // EndEditBtn
            // 
            this.EndEditBtn.Location = new System.Drawing.Point(126, 472);
            this.EndEditBtn.Name = "EndEditBtn";
            this.EndEditBtn.Size = new System.Drawing.Size(92, 34);
            this.EndEditBtn.TabIndex = 107;
            this.EndEditBtn.Text = "End Edit";
            this.EndEditBtn.UseVisualStyleBackColor = true;
            this.EndEditBtn.Click += new System.EventHandler(this.EndEditBtn_Click);
            // 
            // sortbyDateChx
            // 
            this.sortbyDateChx.AutoSize = true;
            this.sortbyDateChx.Location = new System.Drawing.Point(10, 20);
            this.sortbyDateChx.Name = "sortbyDateChx";
            this.sortbyDateChx.Size = new System.Drawing.Size(85, 17);
            this.sortbyDateChx.TabIndex = 108;
            this.sortbyDateChx.Text = "Sort by Date";
            this.sortbyDateChx.UseVisualStyleBackColor = true;
            this.sortbyDateChx.CheckedChanged += new System.EventHandler(this.sortbyDateChx_CheckedChanged);
            // 
            // hashTxt
            // 
            this.hashTxt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.hashTxt.Location = new System.Drawing.Point(714, 61);
            this.hashTxt.Name = "hashTxt";
            this.hashTxt.Size = new System.Drawing.Size(77, 20);
            this.hashTxt.TabIndex = 109;
            this.hashTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.hashTxt.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(714, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 110;
            this.label1.Text = "Enter \"paid\" to edit paid lines.";
            this.label1.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.growerLbl);
            this.groupBox1.Controls.Add(this.growerCbx);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.employeeLbl);
            this.groupBox1.Controls.Add(this.hashTxt);
            this.groupBox1.Controls.Add(this.startDateLbl);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.sortbyDateChx);
            this.groupBox1.Controls.Add(this.cropLbl);
            this.groupBox1.Controls.Add(this.clearBtn);
            this.groupBox1.Controls.Add(this.jobLbl);
            this.groupBox1.Controls.Add(this.endDatePicker);
            this.groupBox1.Controls.Add(this.fieldLbl);
            this.groupBox1.Controls.Add(this.endDateLabel);
            this.groupBox1.Controls.Add(this.cropCbx);
            this.groupBox1.Controls.Add(this.jobCbx);
            this.groupBox1.Controls.Add(this.keyLst);
            this.groupBox1.Controls.Add(this.fieldCbx);
            this.groupBox1.Controls.Add(this.fillScreenBtn);
            this.groupBox1.Controls.Add(this.startDatePicker);
            this.groupBox1.Controls.Add(this.employeeCbx);
            this.groupBox1.Location = new System.Drawing.Point(6, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(897, 92);
            this.groupBox1.TabIndex = 111;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edit Filters";
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(794, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 28);
            this.label2.TabIndex = 111;
            this.label2.Text = "This can bring a lot of data and creates log.";
            this.label2.Visible = false;
            // 
            // KeyCentralPbx
            // 
            this.KeyCentralPbx.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___200x40_pix_;
            this.KeyCentralPbx.Location = new System.Drawing.Point(281, -22);
            this.KeyCentralPbx.Name = "KeyCentralPbx";
            this.KeyCentralPbx.Size = new System.Drawing.Size(317, 94);
            this.KeyCentralPbx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.KeyCentralPbx.TabIndex = 113;
            this.KeyCentralPbx.TabStop = false;
            // 
            // editBoxLabelTxt
            // 
            this.editBoxLabelTxt.Enabled = false;
            this.editBoxLabelTxt.Location = new System.Drawing.Point(510, 446);
            this.editBoxLabelTxt.Name = "editBoxLabelTxt";
            this.editBoxLabelTxt.Size = new System.Drawing.Size(90, 20);
            this.editBoxLabelTxt.TabIndex = 114;
            // 
            // editBoxLabelLbl
            // 
            this.editBoxLabelLbl.AutoSize = true;
            this.editBoxLabelLbl.Enabled = false;
            this.editBoxLabelLbl.Location = new System.Drawing.Point(526, 430);
            this.editBoxLabelLbl.Name = "editBoxLabelLbl";
            this.editBoxLabelLbl.Size = new System.Drawing.Size(54, 13);
            this.editBoxLabelLbl.TabIndex = 115;
            this.editBoxLabelLbl.Text = "Box Label";
            // 
            // editReferenceTxt
            // 
            this.editReferenceTxt.Enabled = false;
            this.editReferenceTxt.Location = new System.Drawing.Point(606, 446);
            this.editReferenceTxt.Name = "editReferenceTxt";
            this.editReferenceTxt.Size = new System.Drawing.Size(90, 20);
            this.editReferenceTxt.TabIndex = 116;
            // 
            // editReferenceLbl
            // 
            this.editReferenceLbl.AutoSize = true;
            this.editReferenceLbl.Enabled = false;
            this.editReferenceLbl.Location = new System.Drawing.Point(622, 430);
            this.editReferenceLbl.Name = "editReferenceLbl";
            this.editReferenceLbl.Size = new System.Drawing.Size(57, 13);
            this.editReferenceLbl.TabIndex = 117;
            this.editReferenceLbl.Text = "Reference";
            // 
            // editOtherTxt
            // 
            this.editOtherTxt.Enabled = false;
            this.editOtherTxt.Location = new System.Drawing.Point(704, 446);
            this.editOtherTxt.Name = "editOtherTxt";
            this.editOtherTxt.Size = new System.Drawing.Size(90, 20);
            this.editOtherTxt.TabIndex = 118;
            // 
            // editOtherLbl
            // 
            this.editOtherLbl.AutoSize = true;
            this.editOtherLbl.Enabled = false;
            this.editOtherLbl.Location = new System.Drawing.Point(735, 430);
            this.editOtherLbl.Name = "editOtherLbl";
            this.editOtherLbl.Size = new System.Drawing.Size(33, 13);
            this.editOtherLbl.TabIndex = 119;
            this.editOtherLbl.Text = "Other";
            // 
            // growerLbl
            // 
            this.growerLbl.AutoSize = true;
            this.growerLbl.Location = new System.Drawing.Point(545, 46);
            this.growerLbl.Name = "growerLbl";
            this.growerLbl.Size = new System.Drawing.Size(71, 13);
            this.growerLbl.TabIndex = 113;
            this.growerLbl.Text = "Grower Block";
            // 
            // growerCbx
            // 
            this.growerCbx.FormattingEnabled = true;
            this.growerCbx.Location = new System.Drawing.Point(540, 61);
            this.growerCbx.Name = "growerCbx";
            this.growerCbx.Size = new System.Drawing.Size(77, 21);
            this.growerCbx.Sorted = true;
            this.growerCbx.TabIndex = 112;
            // 
            // editGrowerCbx
            // 
            this.editGrowerCbx.Enabled = false;
            this.editGrowerCbx.FormattingEnabled = true;
            this.editGrowerCbx.Location = new System.Drawing.Point(428, 445);
            this.editGrowerCbx.Name = "editGrowerCbx";
            this.editGrowerCbx.Size = new System.Drawing.Size(77, 21);
            this.editGrowerCbx.Sorted = true;
            this.editGrowerCbx.TabIndex = 120;
            // 
            // editGrowerblockLbl
            // 
            this.editGrowerblockLbl.AutoSize = true;
            this.editGrowerblockLbl.Enabled = false;
            this.editGrowerblockLbl.Location = new System.Drawing.Point(434, 429);
            this.editGrowerblockLbl.Name = "editGrowerblockLbl";
            this.editGrowerblockLbl.Size = new System.Drawing.Size(68, 13);
            this.editGrowerblockLbl.TabIndex = 121;
            this.editGrowerblockLbl.Text = "GrowerBlock";
            // 
            // EditWorkOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 509);
            this.Controls.Add(this.editGrowerCbx);
            this.Controls.Add(this.editGrowerblockLbl);
            this.Controls.Add(this.editOtherLbl);
            this.Controls.Add(this.editOtherTxt);
            this.Controls.Add(this.editReferenceLbl);
            this.Controls.Add(this.editReferenceTxt);
            this.Controls.Add(this.editBoxLabelLbl);
            this.Controls.Add(this.editBoxLabelTxt);
            this.Controls.Add(this.KeyCentralPbx);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.EndEditBtn);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.savedPayDetailsHeaderLbl);
            this.Controls.Add(this.deleteEditedBtn);
            this.Controls.Add(this.editEmployeeCbx);
            this.Controls.Add(this.editEmployeeLbl);
            this.Controls.Add(this.editDatePicker);
            this.Controls.Add(this.editFieldCbx);
            this.Controls.Add(this.editJobCbx);
            this.Controls.Add(this.editCropCbx);
            this.Controls.Add(this.editYieldUnitTxt);
            this.Controls.Add(this.editYieldUnitLbl);
            this.Controls.Add(this.editPayUnitTxt);
            this.Controls.Add(this.editPayUnitLbl);
            this.Controls.Add(this.editFieldLbl);
            this.Controls.Add(this.editJobLbl);
            this.Controls.Add(this.editCropLbl);
            this.Controls.Add(this.editDateLbl);
            this.Controls.Add(this.saveEditedBtn);
            this.Controls.Add(this.savedPayDetailsLst);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "EditWorkOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Storage Inventory - Edit Inventory Detail";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KeyCentralPbx)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Label startDateLbl;
        private System.Windows.Forms.Label cropLbl;
        private System.Windows.Forms.Label jobLbl;
        private System.Windows.Forms.Label fieldLbl;
        private System.Windows.Forms.ComboBox cropCbx;
        private System.Windows.Forms.ComboBox jobCbx;
        private System.Windows.Forms.ComboBox fieldCbx;
        private System.Windows.Forms.ListBox savedPayDetailsLst;
        private System.Windows.Forms.Button fillScreenBtn;
        private System.Windows.Forms.Button saveEditedBtn;
        private System.Windows.Forms.DateTimePicker startDatePicker;
		private System.Windows.Forms.ComboBox employeeCbx;
        private System.Windows.Forms.Label employeeLbl;
        private System.Windows.Forms.ComboBox editEmployeeCbx;
        private System.Windows.Forms.Label editEmployeeLbl;
        private System.Windows.Forms.DateTimePicker editDatePicker;
        private System.Windows.Forms.ComboBox editFieldCbx;
        private System.Windows.Forms.ComboBox editJobCbx;
        private System.Windows.Forms.ComboBox editCropCbx;
        private System.Windows.Forms.TextBox editYieldUnitTxt;
        private System.Windows.Forms.Label editYieldUnitLbl;
        private System.Windows.Forms.TextBox editPayUnitTxt;
        private System.Windows.Forms.Label editPayUnitLbl;
        private System.Windows.Forms.Label editFieldLbl;
        private System.Windows.Forms.Label editJobLbl;
        private System.Windows.Forms.Label editCropLbl;
        private System.Windows.Forms.Label editDateLbl;
        private System.Windows.Forms.Button deleteEditedBtn;
        private System.Windows.Forms.Label savedPayDetailsHeaderLbl;
        private System.Windows.Forms.ListBox keyLst;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.DateTimePicker endDatePicker;
        private System.Windows.Forms.Label endDateLabel;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Button EndEditBtn;
        private System.Windows.Forms.CheckBox sortbyDateChx;
        private System.Windows.Forms.TextBox hashTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox KeyCentralPbx;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox editBoxLabelTxt;
        private System.Windows.Forms.Label editBoxLabelLbl;
        private System.Windows.Forms.TextBox editReferenceTxt;
        private System.Windows.Forms.Label editReferenceLbl;
        private System.Windows.Forms.TextBox editOtherTxt;
        private System.Windows.Forms.Label editOtherLbl;
        private System.Windows.Forms.Label growerLbl;
        private System.Windows.Forms.ComboBox growerCbx;
        private System.Windows.Forms.ComboBox editGrowerCbx;
        private System.Windows.Forms.Label editGrowerblockLbl;
    }
}