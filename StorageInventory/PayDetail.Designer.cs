﻿namespace StorageInventory
{
    partial class PayDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitBtn = new System.Windows.Forms.Button();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.dateLbl = new System.Windows.Forms.Label();
            this.cropLbl = new System.Windows.Forms.Label();
            this.jobLbl = new System.Windows.Forms.Label();
            this.fieldLbl = new System.Windows.Forms.Label();
            this.payUnitTxt = new System.Windows.Forms.TextBox();
            this.payUnitLbl = new System.Windows.Forms.Label();
            this.cropCbx = new System.Windows.Forms.ComboBox();
            this.jobCbx = new System.Windows.Forms.ComboBox();
            this.fieldCbx = new System.Windows.Forms.ComboBox();
            this.savedPayDetailsLst = new System.Windows.Forms.ListBox();
            this.saveBtn = new System.Windows.Forms.Button();
            this.savedPayDetailsLbl = new System.Windows.Forms.Label();
            this.saveEditedBtn = new System.Windows.Forms.Button();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.employeeCbx = new System.Windows.Forms.ComboBox();
            this.employeeLbl = new System.Windows.Forms.Label();
            this.editEmployeeCbx = new System.Windows.Forms.ComboBox();
            this.editEmployeeLbl = new System.Windows.Forms.Label();
            this.editDatePicker = new System.Windows.Forms.DateTimePicker();
            this.editFieldCbx = new System.Windows.Forms.ComboBox();
            this.editJobCbx = new System.Windows.Forms.ComboBox();
            this.editCropCbx = new System.Windows.Forms.ComboBox();
            this.editPayUnitTxt = new System.Windows.Forms.TextBox();
            this.editPayUnitLbl = new System.Windows.Forms.Label();
            this.editFieldLbl = new System.Windows.Forms.Label();
            this.editJobLbl = new System.Windows.Forms.Label();
            this.editCropLbl = new System.Windows.Forms.Label();
            this.editDateLbl = new System.Windows.Forms.Label();
            this.deleteEditedBtn = new System.Windows.Forms.Button();
            this.savedPayDetailsHeaderLbl = new System.Windows.Forms.Label();
            this.keyLst = new System.Windows.Forms.ListBox();
            this.yieldUnitTxt = new System.Windows.Forms.TextBox();
            this.yieldUnitLbl = new System.Windows.Forms.Label();
            this.editYieldUnitTxt = new System.Windows.Forms.TextBox();
            this.editYieldUnitLbl = new System.Windows.Forms.Label();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.editBoxLabelTxt = new System.Windows.Forms.TextBox();
            this.editBoxLabelLbl = new System.Windows.Forms.Label();
            this.editReferenceLbl = new System.Windows.Forms.Label();
            this.editReferenceTxt = new System.Windows.Forms.TextBox();
            this.editOtherLbl = new System.Windows.Forms.Label();
            this.editOtherTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OtherTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.referenceTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.boxLabelTxt = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.editGrowerCbx = new System.Windows.Forms.ComboBox();
            this.editGrowerblockLbl = new System.Windows.Forms.Label();
            this.growerCbx = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(408, 449);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(92, 34);
            this.exitBtn.TabIndex = 100;
            this.exitBtn.Text = "E&xit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(395, 47);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(119, 20);
            this.subtitleLbl.TabIndex = 44;
            this.subtitleLbl.Text = "Inventory Detail";
            // 
            // dateLbl
            // 
            this.dateLbl.AutoSize = true;
            this.dateLbl.Location = new System.Drawing.Point(37, 69);
            this.dateLbl.Name = "dateLbl";
            this.dateLbl.Size = new System.Drawing.Size(30, 13);
            this.dateLbl.TabIndex = 45;
            this.dateLbl.Text = "Date";
            // 
            // cropLbl
            // 
            this.cropLbl.AutoSize = true;
            this.cropLbl.Location = new System.Drawing.Point(274, 68);
            this.cropLbl.Name = "cropLbl";
            this.cropLbl.Size = new System.Drawing.Size(29, 13);
            this.cropLbl.TabIndex = 49;
            this.cropLbl.Text = "Crop";
            // 
            // jobLbl
            // 
            this.jobLbl.AutoSize = true;
            this.jobLbl.Location = new System.Drawing.Point(344, 69);
            this.jobLbl.Name = "jobLbl";
            this.jobLbl.Size = new System.Drawing.Size(52, 13);
            this.jobLbl.TabIndex = 50;
            this.jobLbl.Text = "Box Type";
            // 
            // fieldLbl
            // 
            this.fieldLbl.AutoSize = true;
            this.fieldLbl.Location = new System.Drawing.Point(433, 68);
            this.fieldLbl.Name = "fieldLbl";
            this.fieldLbl.Size = new System.Drawing.Size(39, 13);
            this.fieldLbl.TabIndex = 51;
            this.fieldLbl.Text = "Where";
            // 
            // payUnitTxt
            // 
            this.payUnitTxt.Location = new System.Drawing.Point(880, 83);
            this.payUnitTxt.Name = "payUnitTxt";
            this.payUnitTxt.Size = new System.Drawing.Size(60, 20);
            this.payUnitTxt.TabIndex = 10;
            this.payUnitTxt.Text = "0";
            this.payUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.payUnitTxt.Enter += new System.EventHandler(this.payUnitTxt_Enter);
            this.payUnitTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.payUnitTxt_KeyDown);
            this.payUnitTxt.Leave += new System.EventHandler(this.payUnitTxt_Leave);
            // 
            // payUnitLbl
            // 
            this.payUnitLbl.AutoSize = true;
            this.payUnitLbl.Location = new System.Drawing.Point(883, 69);
            this.payUnitLbl.Name = "payUnitLbl";
            this.payUnitLbl.Size = new System.Drawing.Size(55, 13);
            this.payUnitLbl.TabIndex = 52;
            this.payUnitLbl.Text = "Amount In";
            // 
            // cropCbx
            // 
            this.cropCbx.FormattingEnabled = true;
            this.cropCbx.Location = new System.Drawing.Point(250, 83);
            this.cropCbx.Name = "cropCbx";
            this.cropCbx.Size = new System.Drawing.Size(77, 21);
            this.cropCbx.Sorted = true;
            this.cropCbx.TabIndex = 4;
            this.cropCbx.DropDown += new System.EventHandler(this.cropCbx_DropDown);
            this.cropCbx.SelectedIndexChanged += new System.EventHandler(this.cropCbx_SelectedIndexChanged);
            this.cropCbx.DropDownClosed += new System.EventHandler(this.cropCbx_DropDownClosed);
            this.cropCbx.Enter += new System.EventHandler(this.cropCbx_Enter);
            this.cropCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cropCbx_KeyDown);
            this.cropCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // jobCbx
            // 
            this.jobCbx.FormattingEnabled = true;
            this.jobCbx.Location = new System.Drawing.Point(332, 83);
            this.jobCbx.Name = "jobCbx";
            this.jobCbx.Size = new System.Drawing.Size(77, 21);
            this.jobCbx.Sorted = true;
            this.jobCbx.TabIndex = 5;
            this.jobCbx.DropDown += new System.EventHandler(this.jobCbx_DropDown);
            this.jobCbx.SelectedIndexChanged += new System.EventHandler(this.jobCbx_SelectedIndexChanged);
            this.jobCbx.DropDownClosed += new System.EventHandler(this.jobCbx_DropDownClosed);
            this.jobCbx.Enter += new System.EventHandler(this.jobCbx_Enter);
            this.jobCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.jobCbx_KeyDown);
            this.jobCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // fieldCbx
            // 
            this.fieldCbx.FormattingEnabled = true;
            this.fieldCbx.Location = new System.Drawing.Point(414, 83);
            this.fieldCbx.Name = "fieldCbx";
            this.fieldCbx.Size = new System.Drawing.Size(77, 21);
            this.fieldCbx.Sorted = true;
            this.fieldCbx.TabIndex = 6;
            this.fieldCbx.DropDown += new System.EventHandler(this.fieldCbx_DropDown);
            this.fieldCbx.SelectedIndexChanged += new System.EventHandler(this.fieldCbx_SelectedIndexChanged);
            this.fieldCbx.DropDownClosed += new System.EventHandler(this.fieldCbx_DropDownClosed);
            this.fieldCbx.Enter += new System.EventHandler(this.fieldCbx_Enter);
            this.fieldCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fieldCbx_KeyDown);
            this.fieldCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // savedPayDetailsLst
            // 
            this.savedPayDetailsLst.Font = new System.Drawing.Font("Courier New", 9F);
            this.savedPayDetailsLst.FormattingEnabled = true;
            this.savedPayDetailsLst.ItemHeight = 15;
            this.savedPayDetailsLst.Location = new System.Drawing.Point(9, 170);
            this.savedPayDetailsLst.Name = "savedPayDetailsLst";
            this.savedPayDetailsLst.Size = new System.Drawing.Size(993, 229);
            this.savedPayDetailsLst.TabIndex = 27;
            this.savedPayDetailsLst.SelectedIndexChanged += new System.EventHandler(this.savedPayDetailsLst_SelectedIndexChanged);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(624, 112);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(152, 34);
            this.saveBtn.TabIndex = 12;
            this.saveBtn.Text = "&Save New Inventory Detail";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            this.saveBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.saveBtn_KeyDown);
            // 
            // savedPayDetailsLbl
            // 
            this.savedPayDetailsLbl.AutoSize = true;
            this.savedPayDetailsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedPayDetailsLbl.Location = new System.Drawing.Point(366, 119);
            this.savedPayDetailsLbl.Name = "savedPayDetailsLbl";
            this.savedPayDetailsLbl.Size = new System.Drawing.Size(176, 20);
            this.savedPayDetailsLbl.TabIndex = 57;
            this.savedPayDetailsLbl.Text = "Saved Inventory Details";
            // 
            // saveEditedBtn
            // 
            this.saveEditedBtn.Enabled = false;
            this.saveEditedBtn.Location = new System.Drawing.Point(590, 448);
            this.saveEditedBtn.Name = "saveEditedBtn";
            this.saveEditedBtn.Size = new System.Drawing.Size(158, 34);
            this.saveEditedBtn.TabIndex = 50;
            this.saveEditedBtn.Text = "Save &Edited Inventory Detail";
            this.saveEditedBtn.UseVisualStyleBackColor = true;
            this.saveEditedBtn.Click += new System.EventHandler(this.saveEditedBtn_Click);
            this.saveEditedBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editBtn_KeyDown);
            // 
            // datePicker
            // 
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(4, 83);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(97, 20);
            this.datePicker.TabIndex = 0;
            this.datePicker.Enter += new System.EventHandler(this.datePicker_Enter);
            this.datePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.datePicker_KeyDown);
            // 
            // employeeCbx
            // 
            this.employeeCbx.FormattingEnabled = true;
            this.employeeCbx.Location = new System.Drawing.Point(106, 83);
            this.employeeCbx.Name = "employeeCbx";
            this.employeeCbx.Size = new System.Drawing.Size(139, 21);
            this.employeeCbx.Sorted = true;
            this.employeeCbx.TabIndex = 1;
            this.employeeCbx.DropDown += new System.EventHandler(this.employeeCbx_DropDown);
            this.employeeCbx.SelectedIndexChanged += new System.EventHandler(this.employeeCbx_SelectedIndexChanged);
            this.employeeCbx.DropDownClosed += new System.EventHandler(this.employeeCbx_DropDownClosed);
            this.employeeCbx.Enter += new System.EventHandler(this.employeeCbx_Enter);
            this.employeeCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.employeeCbx_KeyDown);
            // 
            // employeeLbl
            // 
            this.employeeLbl.AutoSize = true;
            this.employeeLbl.Location = new System.Drawing.Point(150, 70);
            this.employeeLbl.Name = "employeeLbl";
            this.employeeLbl.Size = new System.Drawing.Size(51, 13);
            this.employeeLbl.TabIndex = 46;
            this.employeeLbl.Text = "Customer";
            // 
            // editEmployeeCbx
            // 
            this.editEmployeeCbx.Enabled = false;
            this.editEmployeeCbx.FormattingEnabled = true;
            this.editEmployeeCbx.Location = new System.Drawing.Point(106, 422);
            this.editEmployeeCbx.Name = "editEmployeeCbx";
            this.editEmployeeCbx.Size = new System.Drawing.Size(139, 21);
            this.editEmployeeCbx.Sorted = true;
            this.editEmployeeCbx.TabIndex = 32;
            this.editEmployeeCbx.SelectedIndexChanged += new System.EventHandler(this.editEmployeeCbx_SelectedIndexChanged);
            this.editEmployeeCbx.DropDownClosed += new System.EventHandler(this.editEmployeeCbx_DropDownClosed);
            this.editEmployeeCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editEmployeeCbx_KeyDown);
            // 
            // editEmployeeLbl
            // 
            this.editEmployeeLbl.AutoSize = true;
            this.editEmployeeLbl.Enabled = false;
            this.editEmployeeLbl.Location = new System.Drawing.Point(150, 406);
            this.editEmployeeLbl.Name = "editEmployeeLbl";
            this.editEmployeeLbl.Size = new System.Drawing.Size(51, 13);
            this.editEmployeeLbl.TabIndex = 62;
            this.editEmployeeLbl.Text = "Customer";
            // 
            // editDatePicker
            // 
            this.editDatePicker.Enabled = false;
            this.editDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.editDatePicker.Location = new System.Drawing.Point(4, 423);
            this.editDatePicker.Name = "editDatePicker";
            this.editDatePicker.Size = new System.Drawing.Size(97, 20);
            this.editDatePicker.TabIndex = 31;
            this.editDatePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editDatePicker_KeyDown);
            // 
            // editFieldCbx
            // 
            this.editFieldCbx.Enabled = false;
            this.editFieldCbx.FormattingEnabled = true;
            this.editFieldCbx.Location = new System.Drawing.Point(414, 421);
            this.editFieldCbx.Name = "editFieldCbx";
            this.editFieldCbx.Size = new System.Drawing.Size(77, 21);
            this.editFieldCbx.Sorted = true;
            this.editFieldCbx.TabIndex = 36;
            this.editFieldCbx.SelectedIndexChanged += new System.EventHandler(this.editFieldCbx_SelectedIndexChanged);
            this.editFieldCbx.DropDownClosed += new System.EventHandler(this.editFieldCbx_DropDownClosed);
            this.editFieldCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editFieldCbx_KeyDown);
            // 
            // editJobCbx
            // 
            this.editJobCbx.Enabled = false;
            this.editJobCbx.FormattingEnabled = true;
            this.editJobCbx.Location = new System.Drawing.Point(332, 421);
            this.editJobCbx.Name = "editJobCbx";
            this.editJobCbx.Size = new System.Drawing.Size(77, 21);
            this.editJobCbx.Sorted = true;
            this.editJobCbx.TabIndex = 35;
            this.editJobCbx.SelectedIndexChanged += new System.EventHandler(this.editJobCbx_SelectedIndexChanged);
            this.editJobCbx.DropDownClosed += new System.EventHandler(this.editJobCbx_DropDownClosed);
            this.editJobCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editJobCbx_KeyDown);
            // 
            // editCropCbx
            // 
            this.editCropCbx.Enabled = false;
            this.editCropCbx.FormattingEnabled = true;
            this.editCropCbx.Location = new System.Drawing.Point(250, 421);
            this.editCropCbx.Name = "editCropCbx";
            this.editCropCbx.Size = new System.Drawing.Size(77, 21);
            this.editCropCbx.Sorted = true;
            this.editCropCbx.TabIndex = 34;
            this.editCropCbx.SelectedIndexChanged += new System.EventHandler(this.editCropCbx_SelectedIndexChanged);
            this.editCropCbx.DropDownClosed += new System.EventHandler(this.editCropCbx_DropDownClosed);
            this.editCropCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editCropCbx_KeyDown);
            // 
            // editPayUnitTxt
            // 
            this.editPayUnitTxt.Enabled = false;
            this.editPayUnitTxt.Location = new System.Drawing.Point(877, 421);
            this.editPayUnitTxt.Name = "editPayUnitTxt";
            this.editPayUnitTxt.Size = new System.Drawing.Size(60, 20);
            this.editPayUnitTxt.TabIndex = 44;
            this.editPayUnitTxt.Text = "0";
            this.editPayUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editPayUnitTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.boxLabelTxt_KeyDown);
            // 
            // editPayUnitLbl
            // 
            this.editPayUnitLbl.AutoSize = true;
            this.editPayUnitLbl.Enabled = false;
            this.editPayUnitLbl.Location = new System.Drawing.Point(880, 405);
            this.editPayUnitLbl.Name = "editPayUnitLbl";
            this.editPayUnitLbl.Size = new System.Drawing.Size(55, 13);
            this.editPayUnitLbl.TabIndex = 68;
            this.editPayUnitLbl.Text = "Amount In";
            // 
            // editFieldLbl
            // 
            this.editFieldLbl.AutoSize = true;
            this.editFieldLbl.Enabled = false;
            this.editFieldLbl.Location = new System.Drawing.Point(433, 405);
            this.editFieldLbl.Name = "editFieldLbl";
            this.editFieldLbl.Size = new System.Drawing.Size(39, 13);
            this.editFieldLbl.TabIndex = 67;
            this.editFieldLbl.Text = "Where";
            // 
            // editJobLbl
            // 
            this.editJobLbl.AutoSize = true;
            this.editJobLbl.Enabled = false;
            this.editJobLbl.Location = new System.Drawing.Point(344, 406);
            this.editJobLbl.Name = "editJobLbl";
            this.editJobLbl.Size = new System.Drawing.Size(52, 13);
            this.editJobLbl.TabIndex = 66;
            this.editJobLbl.Text = "Box Type";
            // 
            // editCropLbl
            // 
            this.editCropLbl.AutoSize = true;
            this.editCropLbl.Enabled = false;
            this.editCropLbl.Location = new System.Drawing.Point(274, 405);
            this.editCropLbl.Name = "editCropLbl";
            this.editCropLbl.Size = new System.Drawing.Size(29, 13);
            this.editCropLbl.TabIndex = 65;
            this.editCropLbl.Text = "Crop";
            // 
            // editDateLbl
            // 
            this.editDateLbl.AutoSize = true;
            this.editDateLbl.Enabled = false;
            this.editDateLbl.Location = new System.Drawing.Point(37, 407);
            this.editDateLbl.Name = "editDateLbl";
            this.editDateLbl.Size = new System.Drawing.Size(30, 13);
            this.editDateLbl.TabIndex = 59;
            this.editDateLbl.Text = "Date";
            // 
            // deleteEditedBtn
            // 
            this.deleteEditedBtn.Enabled = false;
            this.deleteEditedBtn.Location = new System.Drawing.Point(759, 449);
            this.deleteEditedBtn.Name = "deleteEditedBtn";
            this.deleteEditedBtn.Size = new System.Drawing.Size(141, 34);
            this.deleteEditedBtn.TabIndex = 52;
            this.deleteEditedBtn.Text = "&Delete Inventory Detail";
            this.deleteEditedBtn.UseVisualStyleBackColor = true;
            this.deleteEditedBtn.Click += new System.EventHandler(this.deleteEditedBtn_Click);
            this.deleteEditedBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deleteEditedBtn_KeyDown);
            // 
            // savedPayDetailsHeaderLbl
            // 
            this.savedPayDetailsHeaderLbl.AutoSize = true;
            this.savedPayDetailsHeaderLbl.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedPayDetailsHeaderLbl.Location = new System.Drawing.Point(13, 152);
            this.savedPayDetailsHeaderLbl.Name = "savedPayDetailsHeaderLbl";
            this.savedPayDetailsHeaderLbl.Size = new System.Drawing.Size(959, 15);
            this.savedPayDetailsHeaderLbl.TabIndex = 58;
            this.savedPayDetailsHeaderLbl.Text = "|   Date   | Customer |                  |  Crop  |Box Type| Where |Grower Block " +
    "| Box Label | Reference | Other | Amount In|Amount Out|";
            // 
            // keyLst
            // 
            this.keyLst.FormattingEnabled = true;
            this.keyLst.Location = new System.Drawing.Point(137, 42);
            this.keyLst.Name = "keyLst";
            this.keyLst.Size = new System.Drawing.Size(120, 17);
            this.keyLst.TabIndex = 42;
            this.keyLst.Visible = false;
            // 
            // yieldUnitTxt
            // 
            this.yieldUnitTxt.Location = new System.Drawing.Point(945, 83);
            this.yieldUnitTxt.Name = "yieldUnitTxt";
            this.yieldUnitTxt.Size = new System.Drawing.Size(60, 20);
            this.yieldUnitTxt.TabIndex = 11;
            this.yieldUnitTxt.Text = "0";
            this.yieldUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.yieldUnitTxt.Enter += new System.EventHandler(this.yieldUnitTxt_Enter);
            this.yieldUnitTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.yieldUnitTxt_KeyDown);
            this.yieldUnitTxt.Leave += new System.EventHandler(this.yieldUnitTxt_Leave);
            // 
            // yieldUnitLbl
            // 
            this.yieldUnitLbl.AutoSize = true;
            this.yieldUnitLbl.Location = new System.Drawing.Point(944, 70);
            this.yieldUnitLbl.Name = "yieldUnitLbl";
            this.yieldUnitLbl.Size = new System.Drawing.Size(63, 13);
            this.yieldUnitLbl.TabIndex = 48;
            this.yieldUnitLbl.Text = "Amount Out";
            // 
            // editYieldUnitTxt
            // 
            this.editYieldUnitTxt.Enabled = false;
            this.editYieldUnitTxt.Location = new System.Drawing.Point(942, 421);
            this.editYieldUnitTxt.Name = "editYieldUnitTxt";
            this.editYieldUnitTxt.Size = new System.Drawing.Size(60, 20);
            this.editYieldUnitTxt.TabIndex = 46;
            this.editYieldUnitTxt.Text = "0";
            this.editYieldUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editYieldUnitTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editYieldUnitTxt_KeyDown);
            this.editYieldUnitTxt.Leave += new System.EventHandler(this.editYieldUnitTxt_Leave);
            // 
            // editYieldUnitLbl
            // 
            this.editYieldUnitLbl.AutoSize = true;
            this.editYieldUnitLbl.Enabled = false;
            this.editYieldUnitLbl.Location = new System.Drawing.Point(941, 405);
            this.editYieldUnitLbl.Name = "editYieldUnitLbl";
            this.editYieldUnitLbl.Size = new System.Drawing.Size(63, 13);
            this.editYieldUnitLbl.TabIndex = 64;
            this.editYieldUnitLbl.Text = "Amount Out";
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 9);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 103;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // editBoxLabelTxt
            // 
            this.editBoxLabelTxt.Enabled = false;
            this.editBoxLabelTxt.Location = new System.Drawing.Point(592, 421);
            this.editBoxLabelTxt.Name = "editBoxLabelTxt";
            this.editBoxLabelTxt.Size = new System.Drawing.Size(90, 20);
            this.editBoxLabelTxt.TabIndex = 38;
            this.editBoxLabelTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.boxLabelTxt_KeyDown);
            // 
            // editBoxLabelLbl
            // 
            this.editBoxLabelLbl.AutoSize = true;
            this.editBoxLabelLbl.Enabled = false;
            this.editBoxLabelLbl.Location = new System.Drawing.Point(610, 405);
            this.editBoxLabelLbl.Name = "editBoxLabelLbl";
            this.editBoxLabelLbl.Size = new System.Drawing.Size(54, 13);
            this.editBoxLabelLbl.TabIndex = 107;
            this.editBoxLabelLbl.Text = "Box Label";
            // 
            // editReferenceLbl
            // 
            this.editReferenceLbl.AutoSize = true;
            this.editReferenceLbl.Enabled = false;
            this.editReferenceLbl.Location = new System.Drawing.Point(704, 405);
            this.editReferenceLbl.Name = "editReferenceLbl";
            this.editReferenceLbl.Size = new System.Drawing.Size(57, 13);
            this.editReferenceLbl.TabIndex = 109;
            this.editReferenceLbl.Text = "Reference";
            // 
            // editReferenceTxt
            // 
            this.editReferenceTxt.Enabled = false;
            this.editReferenceTxt.Location = new System.Drawing.Point(687, 421);
            this.editReferenceTxt.Name = "editReferenceTxt";
            this.editReferenceTxt.Size = new System.Drawing.Size(90, 20);
            this.editReferenceTxt.TabIndex = 40;
            this.editReferenceTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.boxLabelTxt_KeyDown);
            // 
            // editOtherLbl
            // 
            this.editOtherLbl.AutoSize = true;
            this.editOtherLbl.Enabled = false;
            this.editOtherLbl.Location = new System.Drawing.Point(811, 405);
            this.editOtherLbl.Name = "editOtherLbl";
            this.editOtherLbl.Size = new System.Drawing.Size(33, 13);
            this.editOtherLbl.TabIndex = 111;
            this.editOtherLbl.Text = "Other";
            // 
            // editOtherTxt
            // 
            this.editOtherTxt.Enabled = false;
            this.editOtherTxt.Location = new System.Drawing.Point(782, 421);
            this.editOtherTxt.Name = "editOtherTxt";
            this.editOtherTxt.Size = new System.Drawing.Size(90, 20);
            this.editOtherTxt.TabIndex = 42;
            this.editOtherTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.boxLabelTxt_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(814, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 117;
            this.label1.Text = "Other";
            // 
            // OtherTxt
            // 
            this.OtherTxt.Location = new System.Drawing.Point(785, 83);
            this.OtherTxt.Name = "OtherTxt";
            this.OtherTxt.Size = new System.Drawing.Size(90, 20);
            this.OtherTxt.TabIndex = 9;
            this.OtherTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.boxLabelTxt_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(707, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 115;
            this.label2.Text = "Reference";
            // 
            // referenceTxt
            // 
            this.referenceTxt.Location = new System.Drawing.Point(690, 83);
            this.referenceTxt.Name = "referenceTxt";
            this.referenceTxt.Size = new System.Drawing.Size(90, 20);
            this.referenceTxt.TabIndex = 8;
            this.referenceTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.boxLabelTxt_KeyDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(613, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 113;
            this.label3.Text = "Box Label";
            // 
            // boxLabelTxt
            // 
            this.boxLabelTxt.Location = new System.Drawing.Point(595, 83);
            this.boxLabelTxt.Name = "boxLabelTxt";
            this.boxLabelTxt.Size = new System.Drawing.Size(90, 20);
            this.boxLabelTxt.TabIndex = 7;
            this.boxLabelTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.boxLabelTxt_KeyDown);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___100x20_pix_;
            this.pictureBox1.Location = new System.Drawing.Point(347, -12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(214, 58);
            this.pictureBox1.TabIndex = 118;
            this.pictureBox1.TabStop = false;
            // 
            // editGrowerCbx
            // 
            this.editGrowerCbx.Enabled = false;
            this.editGrowerCbx.FormattingEnabled = true;
            this.editGrowerCbx.Location = new System.Drawing.Point(497, 421);
            this.editGrowerCbx.Name = "editGrowerCbx";
            this.editGrowerCbx.Size = new System.Drawing.Size(91, 21);
            this.editGrowerCbx.Sorted = true;
            this.editGrowerCbx.TabIndex = 119;
            // 
            // editGrowerblockLbl
            // 
            this.editGrowerblockLbl.AutoSize = true;
            this.editGrowerblockLbl.Enabled = false;
            this.editGrowerblockLbl.Location = new System.Drawing.Point(507, 405);
            this.editGrowerblockLbl.Name = "editGrowerblockLbl";
            this.editGrowerblockLbl.Size = new System.Drawing.Size(71, 13);
            this.editGrowerblockLbl.TabIndex = 120;
            this.editGrowerblockLbl.Text = "Grower Block";
            // 
            // growerCbx
            // 
            this.growerCbx.FormattingEnabled = true;
            this.growerCbx.Location = new System.Drawing.Point(495, 82);
            this.growerCbx.Name = "growerCbx";
            this.growerCbx.Size = new System.Drawing.Size(95, 21);
            this.growerCbx.Sorted = true;
            this.growerCbx.TabIndex = 121;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(506, 67);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 122;
            this.label5.Text = "Grower Block";
            // 
            // PayDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1011, 487);
            this.Controls.Add(this.growerCbx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.editGrowerCbx);
            this.Controls.Add(this.editGrowerblockLbl);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OtherTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.referenceTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.boxLabelTxt);
            this.Controls.Add(this.editOtherLbl);
            this.Controls.Add(this.editOtherTxt);
            this.Controls.Add(this.editReferenceLbl);
            this.Controls.Add(this.editReferenceTxt);
            this.Controls.Add(this.editBoxLabelLbl);
            this.Controls.Add(this.editBoxLabelTxt);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.editYieldUnitTxt);
            this.Controls.Add(this.editYieldUnitLbl);
            this.Controls.Add(this.yieldUnitTxt);
            this.Controls.Add(this.yieldUnitLbl);
            this.Controls.Add(this.keyLst);
            this.Controls.Add(this.savedPayDetailsHeaderLbl);
            this.Controls.Add(this.deleteEditedBtn);
            this.Controls.Add(this.editEmployeeCbx);
            this.Controls.Add(this.editEmployeeLbl);
            this.Controls.Add(this.editDatePicker);
            this.Controls.Add(this.editFieldCbx);
            this.Controls.Add(this.editJobCbx);
            this.Controls.Add(this.editCropCbx);
            this.Controls.Add(this.editPayUnitTxt);
            this.Controls.Add(this.editPayUnitLbl);
            this.Controls.Add(this.editFieldLbl);
            this.Controls.Add(this.editJobLbl);
            this.Controls.Add(this.editCropLbl);
            this.Controls.Add(this.editDateLbl);
            this.Controls.Add(this.employeeCbx);
            this.Controls.Add(this.employeeLbl);
            this.Controls.Add(this.datePicker);
            this.Controls.Add(this.saveEditedBtn);
            this.Controls.Add(this.savedPayDetailsLbl);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.fieldCbx);
            this.Controls.Add(this.jobCbx);
            this.Controls.Add(this.cropCbx);
            this.Controls.Add(this.payUnitTxt);
            this.Controls.Add(this.payUnitLbl);
            this.Controls.Add(this.fieldLbl);
            this.Controls.Add(this.jobLbl);
            this.Controls.Add(this.cropLbl);
            this.Controls.Add(this.dateLbl);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.savedPayDetailsLst);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PayDetail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Storage Inventory - Inventory Detail";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Label dateLbl;
        private System.Windows.Forms.Label cropLbl;
        private System.Windows.Forms.Label jobLbl;
        private System.Windows.Forms.Label fieldLbl;
        private System.Windows.Forms.TextBox payUnitTxt;
        private System.Windows.Forms.Label payUnitLbl;
        private System.Windows.Forms.ComboBox cropCbx;
        private System.Windows.Forms.ComboBox jobCbx;
        private System.Windows.Forms.ComboBox fieldCbx;
        private System.Windows.Forms.ListBox savedPayDetailsLst;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label savedPayDetailsLbl;
        private System.Windows.Forms.Button saveEditedBtn;
        private System.Windows.Forms.DateTimePicker datePicker;
		private System.Windows.Forms.ComboBox employeeCbx;
        private System.Windows.Forms.Label employeeLbl;
        private System.Windows.Forms.ComboBox editEmployeeCbx;
        private System.Windows.Forms.Label editEmployeeLbl;
        private System.Windows.Forms.DateTimePicker editDatePicker;
        private System.Windows.Forms.ComboBox editFieldCbx;
        private System.Windows.Forms.ComboBox editJobCbx;
        private System.Windows.Forms.ComboBox editCropCbx;
        private System.Windows.Forms.TextBox editPayUnitTxt;
        private System.Windows.Forms.Label editPayUnitLbl;
        private System.Windows.Forms.Label editFieldLbl;
        private System.Windows.Forms.Label editJobLbl;
        private System.Windows.Forms.Label editCropLbl;
        private System.Windows.Forms.Label editDateLbl;
        private System.Windows.Forms.Button deleteEditedBtn;
        private System.Windows.Forms.Label savedPayDetailsHeaderLbl;
        private System.Windows.Forms.ListBox keyLst;
		private System.Windows.Forms.TextBox yieldUnitTxt;
		private System.Windows.Forms.Label yieldUnitLbl;
		private System.Windows.Forms.TextBox editYieldUnitTxt;
        private System.Windows.Forms.Label editYieldUnitLbl;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.TextBox editBoxLabelTxt;
        private System.Windows.Forms.Label editBoxLabelLbl;
        private System.Windows.Forms.Label editReferenceLbl;
        private System.Windows.Forms.TextBox editReferenceTxt;
        private System.Windows.Forms.Label editOtherLbl;
        private System.Windows.Forms.TextBox editOtherTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox OtherTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox referenceTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox boxLabelTxt;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox editGrowerCbx;
        private System.Windows.Forms.Label editGrowerblockLbl;
        private System.Windows.Forms.ComboBox growerCbx;
        private System.Windows.Forms.Label label5;
    }
}