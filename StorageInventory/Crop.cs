﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Diagnostics;


namespace StorageInventory
{
	public partial class Crop : Form
	{
		#region Vars
		//Set the global variables for delimiter character.
		char delimiter = '~';        
		//Set the path of the program & save files.
		private string xmlFileName = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
        //test 3-24-15 9:37
		//Create the cropsData object.
		MainMenu.CropsData cropsData = new MainMenu.CropsData();
       //from Michele PC
		private ArrayList ActiveArl;
		private ArrayList InactiveArl;
		#endregion

		#region Constructor
		public Crop()
		{
			InitializeComponent();
			cropsData = new MainMenu.CropsData();
			FillSavedCropsLst(true);

            //Make form non-resizable.
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;

			//Fill the crop data from XML.
			FillCropDataFromXML();
            CompanyNameData.Text = MainMenu.CompanyStr;
		}
		#endregion

		#region Form Functions.
		#region KeyDown Functions (For Enter-As-Tab Functionality)
		private void newCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void newDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveNewBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveBtn_Click(null, null);
		}
		private void editCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveEditedBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveEditedBtn_Click(null, null);
		}
		#endregion

		#region Input Validation Functions (Checking for proper data type inputs).

		private void newCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(newCodeTxt, delimiter))
			{
				//Check for a unique crop code.
				foreach (MainMenu.CropData tempCropData in cropsData.CropsDataArl)
				{
					if (tempCropData.CodeStr == this.newCodeTxt.Text.Trim())
					{
						MessageBox.Show("This crop code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

						//Focus on the line the user needs to change.
						this.newCodeTxt.Focus();
						this.newCodeTxt.Text = string.Empty;

						break;
					}
				}
			}
		}

		private void editCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(editCodeTxt, delimiter))
			{
				//Check for a unique crop code.
				for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
				{
					MainMenu.CropData tempCropData = (MainMenu.CropData)cropsData.CropsDataArl[i];

					if (tempCropData.CodeStr == this.editCodeTxt.Text.Trim() && tempCropData.KeyInt.ToString() != this.keyTxt.Text)
					{
						MessageBox.Show("This crop code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

						//Focus on the line the user needs to change.
						this.editCodeTxt.Focus();
						this.editCodeTxt.SelectAll();

						break;
					}
				}
			}
		}
		#endregion

		#region List Box Functions
		private void savedCropsLst_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (savedCropsLst.SelectedIndex != -1)
			{
				//Clear out the new entry spaces.
				this.newCodeTxt.Text = string.Empty;
				this.newDescriptionTxt.Text = string.Empty;

				if (activeRdo.Checked)
				{
					MainMenu.CropData cropData = (MainMenu.CropData)ActiveArl[savedCropsLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = cropData.CodeStr;
					editDescriptionTxt.Text = cropData.DescriptionStr;
					keyTxt.Text = cropData.KeyInt.ToString();
				}
				else
				{
					MainMenu.CropData cropData = (MainMenu.CropData)InactiveArl[savedCropsLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = cropData.CodeStr;
					editDescriptionTxt.Text = cropData.DescriptionStr;
					keyTxt.Text = cropData.KeyInt.ToString();
				}

				//Extrapolate the selected code & description.
				//string selectedEntry = savedCropsLst.SelectedItem.ToString();
				//string[] entryPieces = selectedEntry.Split(delimiter);
				//string selectedCode = entryPieces[0].Trim();
				//string selectedDescription = entryPieces[1].Trim();

				//Fill the Edit entry boxes.
				//editCodeTxt.Text = selectedCode;
				//editDescriptionTxt.Text = selectedDescription;

				//Enable the entry boxes & button.
				editCodeTxt.Enabled = true;
				editCodeLbl.Enabled = true;
				editDescriptionTxt.Enabled = true;
				editDescriptionLbl.Enabled = true;
				saveEditedBtn.Enabled = true;
				enableDisableBtn.Enabled = true;
			}
			else
			{
				//Clear the Edit entry boxes.
				editCodeTxt.Text = string.Empty;
				editDescriptionTxt.Text = string.Empty;
				keyTxt.Text = string.Empty;

				//Disable the entry boxes & button.
				editCodeTxt.Enabled = false;
				editCodeLbl.Enabled = false;
				editDescriptionTxt.Enabled = false;
				editDescriptionLbl.Enabled = false;
				saveEditedBtn.Enabled = false;
				enableDisableBtn.Enabled = false;
			}
		}
		private void activeRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (activeRdo.Checked)
			{
				enableDisableBtn.Text = "&Disable";

				FillSavedCropsLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "En&able";

				FillSavedCropsLst(activeRdo.Checked);
			}
		}
		private void inactiveRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (inactiveRdo.Checked)
			{
				enableDisableBtn.Text = "En&able";

				FillSavedCropsLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "&Disable";

				FillSavedCropsLst(activeRdo.Checked);
			}
		}
		#endregion

		private void exitBtn_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		#endregion

		#region New Crop Functions.
		private void saveBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.newCodeTxt, delimiter))
			{
				if (!InputStringContainsChar(this.newDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
					FillCropDataFromXML();

					//Add the new Crop Data into the list.
					MainMenu.CropData cropData = MainMenu.FindOrAddCropData(cropsData, this.newCodeTxt.Text.Trim(), this.newDescriptionTxt.Text.Trim(), cropsData.NextKeyInt);
					cropData.ActiveBln = activeRdo.Checked;

					//Save the new data to XML.
					SaveCropDataToXML();

					//Refill the GUI list.
					FillSavedCropsLst(activeRdo.Checked);

					//Clear the freshly added new code & description.
					newCodeTxt.Clear();
					newDescriptionTxt.Clear();

					//Clear the edit entry spaces.
					savedCropsLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void newCodeTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedCropsLst.SelectedIndex = -1;
		}
		private void newDescriptionTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedCropsLst.SelectedIndex = -1;
		}
		#endregion

		#region Edit Crop Functions.
		private void saveEditedBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//  character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.editCodeTxt, delimiter) && this.keyTxt.Text != "")
			{
				if (!InputStringContainsChar(this.editDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
					FillCropDataFromXML();

					//Save the edited crop.
					MainMenu.SaveEditedCropData(cropsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), activeRdo.Checked);

					//Save the new data to XML.
					SaveCropDataToXML();

					//Refill the GUI list.
					FillSavedCropsLst(activeRdo.Checked);

					//Clear the edit entry spaces.
					savedCropsLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void enableDisableBtn_Click(object sender, EventArgs e)
		{
			if (this.keyTxt.Text != "")
			{
				//Fill the list from XML.
				FillCropDataFromXML();

				//Enable/Disable out the selected crop.
				if (activeRdo.Checked)
                {
                    MainMenu.SaveEditedCropData(cropsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), false);
                }
                else
                {
                    MainMenu.SaveEditedCropData(cropsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), true);
                }
				//Save the new data to XML.
				SaveCropDataToXML();

				//Refill the GUI list.
				FillSavedCropsLst(activeRdo.Checked);

				//Clear the edit entry spaces.
				savedCropsLst.SelectedIndex = -1;

				//Put focus back on 'NewCode' entry box.
				newCodeTxt.Focus();
			}
		}
		#endregion

		#region Helper Functions.
		private void FillSavedCropsLst(bool activeBln)
		{
			//Clear out the Saved Crops List.
			savedCropsLst.ClearSelected();
			savedCropsLst.Items.Clear();
			savedCropsLst.SelectedIndex = -1;

			ActiveArl = new ArrayList();
			InactiveArl = new ArrayList();

			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				MainMenu.CropsData cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
                cropsData.CropsDataArl.Sort();
				for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
				{
					MainMenu.CropData cropData = (MainMenu.CropData)cropsData.CropsDataArl[i];

					if (cropData.ActiveBln == activeBln)
					{
						savedCropsLst.Items.Add(cropData.CodeStr + " ~ " + cropData.DescriptionStr);
					}

					if (cropData.ActiveBln)
					{
						ActiveArl.Add(cropData);
					}
					else
					{
						InactiveArl.Add(cropData);
					}
				}
			}
		}
		private void FillCropDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void SaveCropDataToXML()
		{
			//Serialize (convert an object instance to an XML document):
			XmlSerializer xmlSerializer = new XmlSerializer(cropsData.GetType());
			// Create an XmlTextWriter using a FileStream.
			Stream fileStream2 = new FileStream(xmlFileName, FileMode.Create);
			XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
			// Serialize using the XmlTextWriter.
			xmlSerializer.Serialize(xmlWriter, cropsData);
			xmlWriter.Flush();
			xmlWriter.Close();
		}
		private bool InputStringContainsChar(TextBox textBoxToCheck, char charToCheckFor)
		{
			if (textBoxToCheck.Text.Contains(charToCheckFor))
			{
				MessageBox.Show("Input cannot contain '" + charToCheckFor + "' characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				textBoxToCheck.Focus();
				textBoxToCheck.SelectAll();

				return true;
			}
			else
				return false;
		}
		#endregion

        private void makeListBtn_Click(object sender, EventArgs e)
        {
            #region Make PDF
            string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Crop Data\";
            //string fileNameStr = "Pay_Data_Report_" + timeStampStr + ".pdf";
            string fileNameStr = "Crop_Data.pdf";
            CropReport cropReport = new CropReport();
            cropReport.Main(saveFolderStr, fileNameStr, cropsData);
            #endregion
            #region Open PDF
            Process.Start(saveFolderStr + fileNameStr);
            #endregion
        }
	}
}
