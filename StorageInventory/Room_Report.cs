using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;




namespace StorageInventory
{
	public class Room_Report
	{
		#region Vars
		private Document document;
		private PdfWriter PDFWriter;
        private double lineCountPerPageDbl = 55;
        private double lineCountDbl = 0;
        #endregion

		#region Constructor
		public Room_Report()
		{

		}
		#endregion

		#region Display Code
        public void Main(string saveFolderLocationStr, string fileNameStr, string commentStr, MainMenu.EmployeesData employeesData, MainMenu.CropsData cropsData, MainMenu.FieldsData fieldsData, ArrayList RoomDataArl)
		{
			try
			{
				//if temp folder doesn't already exist on client's computer, create the folder
				DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

				if (!directoryInfo.Exists)
				{
					directoryInfo.Create();
				}

				document = new Document(PageSize.LETTER, 18, 18, 18, 18);

				// creation of the different writers
				PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
				PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

				document.Open();

				#region Footer
				HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
				footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
				footer.Alignment = Element.ALIGN_CENTER;
				document.Footer = footer;
				#endregion

				#region Header
				PdfPTable pdfPTable = HeaderPdfPTable(commentStr);
				#endregion

				#region Details
				PdfPCell detailCell = new PdfPCell();

                #region Detail Vars
                string lastCustomerStr = "";
                string lastRoomStr = "";
                double customerTotalInDbl = 0;
                double customerTotalOutDbl = 0;
                double roomTotalInDbl = 0;
                double roomTotalOutDbl = 0;
                double totalInDbl = 0;
				double totalOutDbl = 0;
                #endregion

                #region Setting & Printing Detail Vars
                for (int i = 0; i < RoomDataArl.Count; i++)
				{
                    MainMenu.RoomItem roomItem = (MainMenu.RoomItem)RoomDataArl[i];
                    if(lastCustomerStr == "")
                    {
                        lastCustomerStr = roomItem.customerName;
                    }
                    if (lastCustomerStr != roomItem.customerName)
                    {
                        #region Check Line Count
                        if (lineCountDbl >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable(commentStr);
                        }
                        #endregion
                        #region SubTotal Customer
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total " + lastCustomerStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.Colspan = 3;
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(customerTotalInDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(customerTotalOutDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase((customerTotalInDbl - customerTotalOutDbl).ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        lastCustomerStr = roomItem.customerName;
                        customerTotalInDbl = 0;
                        customerTotalOutDbl = 0;
                        #endregion
                    }
                    if(lastRoomStr == "")
                    {
                        lastRoomStr = roomItem.roomName;
                    }
                    if (lastRoomStr != roomItem.roomName)
                    {
                        #region Check Line Count
                        if (lineCountDbl >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable(commentStr);
                        }
                        #endregion
                        #region SubTotal Room
                        detailCell = new PdfPCell(new Phrase("Total " + lastRoomStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.Colspan = 4;
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(roomTotalInDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(roomTotalOutDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase((roomTotalInDbl - roomTotalOutDbl).ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = 1f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        lastRoomStr = roomItem.roomName;
                        roomTotalInDbl = 0;
                        roomTotalOutDbl = 0;
                        #endregion
                    }
                    #region Setting Detail Vars
                    string roomNameStr = roomItem.roomName;
                    if(roomNameStr.Length > 30)
                    {
                        roomNameStr = roomNameStr.Substring(0, 30);
                    }
                    string customerNameStr = roomItem.customerName;
                    if (customerNameStr.Length > 35)
                    {
                        customerNameStr = customerNameStr.Substring(0, 35);
                    }
                    string cropNameStr = roomItem.cropName;
                    if(cropNameStr.Length > 22)
                    {
                        cropNameStr = cropNameStr.Substring(0, 22);
                    }
                    string inStr = roomItem.InDbl.ToString("###,###,##0");
                    string outStr = roomItem.OutDbl.ToString("###,###,##0");
                    string leftStr = (roomItem.InDbl - roomItem.OutDbl).ToString("###,###,##0");
                    customerTotalInDbl += roomItem.InDbl;
                    customerTotalOutDbl += roomItem.OutDbl;
                    roomTotalInDbl += roomItem.InDbl;
                    roomTotalOutDbl += roomItem.OutDbl;
                    totalInDbl += roomItem.InDbl;
                    totalOutDbl += roomItem.OutDbl;
                    #endregion
                    #region Check Line Count
                    if (lineCountDbl >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable(commentStr);
                    }
                    #endregion
                    #region Print Details
                    detailCell = new PdfPCell(new Phrase(roomNameStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
					detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
					detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(roomItem.customerName, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(roomItem.cropName, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(roomItem.growerBlockName, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(inStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK; 
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(outStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(leftStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = Color.BLACK; 
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    lineCountDbl += 1;
                #endregion
                }
                #endregion
                
                #region Grand Totals
                #region Check Line Count
                        if (lineCountDbl >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable(commentStr);
                        }
                        #endregion
                #region SubTotal Customer
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("Total " + lastCustomerStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.Colspan = 3;
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(customerTotalInDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(customerTotalOutDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase((customerTotalInDbl - customerTotalOutDbl).ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    
                #region Check Line Count
                        if (lineCountDbl >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable(commentStr);
                        }
                        #endregion
                #region SubTotal Room
                        detailCell = new PdfPCell(new Phrase("Total " + lastRoomStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.Colspan = 4;
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(roomTotalInDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(roomTotalOutDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase((roomTotalInDbl - roomTotalOutDbl).ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD, Color.BLUE)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthTop = 1f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthLeft = 1f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = 1f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthBottom = 1f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                #region Check Line Count
                        if (lineCountDbl + 6 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable(commentStr);
                        }
                        #endregion
                #region Grand Total
                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.Colspan = 4;
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = 1f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthBottom = 1f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(totalInDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = 1f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthBottom = 1f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(totalOutDbl.ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = 1f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthBottom = 1f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase((totalInDbl - totalOutDbl).ToString("###,###,##0"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = 1f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthRight = 1f;
                    detailCell.BorderColorRight = Color.BLACK;
                    detailCell.BorderWidthBottom = 1f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    #endregion
                #endregion          

                document.Add(pdfPTable);
			}
			catch (Exception ex)
			{
				//Exception was thrown.
			}
			finally
			{
				if (document != null && document.IsOpen())
				{
					// we close the document
					document.Close();
				}
			}
		}
       
       
        private PdfPTable HeaderPdfPTable(string commentStr)
		{
			#region Header
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);
            
            paragraph = new Paragraph();
			paragraph.Alignment = Element.ALIGN_CENTER;
			paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
			paragraph.Add("Room Report");
			paragraph.SpacingAfter = 6f;
			paragraph.SpacingBefore = 0f;
			paragraph.Leading = 10f;
			document.Add(paragraph);

			paragraph = new Paragraph();
			paragraph.Alignment = Element.ALIGN_CENTER;
			paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
			paragraph.Add(commentStr);
			paragraph.SpacingAfter = 6f;
			paragraph.SpacingBefore = 0f;
			paragraph.Leading = 10f;
			document.Add(paragraph);

			PdfPTable table = new PdfPTable(7);
			table.HorizontalAlignment = Element.ALIGN_LEFT;

            float whereWidth = 130f;
			float customerWidth = 130f; 
			float cropWidth = 110f;
            float growerBlockWidth = 110f;
            float inWidth = 30f;
			float outWidth = 30f;
            float leftWidth = 30f;
			float tableWidth = 0;

            tableWidth = whereWidth + customerWidth + cropWidth + growerBlockWidth + inWidth + outWidth + leftWidth;

            table.SetWidths(new float[] { whereWidth, customerWidth, cropWidth, growerBlockWidth, inWidth, outWidth, leftWidth});
			table.TotalWidth = tableWidth;
			table.SplitRows = true;
			table.SplitLate = false;
			table.LockedWidth = true;
			table.DefaultCell.Padding = 0;
			table.DefaultCell.BorderWidth = 1f;
			table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
			table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            Color tintColor = new Color(220, 230, 241);

			PdfPCell detailCell = new PdfPCell(new Phrase("Where", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.BLACK;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.BLACK;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Crop", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.BLACK;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);


            detailCell = new PdfPCell(new Phrase("Grower Block", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            detailCell.NoWrap = true;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.BLACK;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("In", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.BLACK;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Out", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.BLACK;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Left", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            detailCell.NoWrap = true;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.BLACK;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);
            lineCountDbl += 3;
			#endregion

			return table;
        }
                #endregion
        #endregion
    }
}
