﻿namespace StorageInventory
{
    partial class MultipleRoomMove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitBtn = new System.Windows.Forms.Button();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.startDateLbl = new System.Windows.Forms.Label();
            this.cropLbl = new System.Windows.Forms.Label();
            this.jobLbl = new System.Windows.Forms.Label();
            this.fieldLbl = new System.Windows.Forms.Label();
            this.cropCbx = new System.Windows.Forms.ComboBox();
            this.jobCbx = new System.Windows.Forms.ComboBox();
            this.fieldCbx = new System.Windows.Forms.ComboBox();
            this.savedPayDetailsLst = new System.Windows.Forms.ListBox();
            this.fillScreenBtn = new System.Windows.Forms.Button();
            this.saveEditedBtn = new System.Windows.Forms.Button();
            this.startDatePicker = new System.Windows.Forms.DateTimePicker();
            this.employeeCbx = new System.Windows.Forms.ComboBox();
            this.employeeLbl = new System.Windows.Forms.Label();
            this.editFieldCbx = new System.Windows.Forms.ComboBox();
            this.editFieldLbl = new System.Windows.Forms.Label();
            this.savedPayDetailsHeaderLbl = new System.Windows.Forms.Label();
            this.keyLst = new System.Windows.Forms.ListBox();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.endDatePicker = new System.Windows.Forms.DateTimePicker();
            this.endDateLabel = new System.Windows.Forms.Label();
            this.clearBtn = new System.Windows.Forms.Button();
            this.EndEditBtn = new System.Windows.Forms.Button();
            this.sortbyDateChx = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.growerLbl = new System.Windows.Forms.Label();
            this.growerCbx = new System.Windows.Forms.ComboBox();
            this.KeyCentralPbx = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KeyCentralPbx)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(408, 472);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(92, 34);
            this.exitBtn.TabIndex = 41;
            this.exitBtn.Text = "E&xit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(725, 4);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(152, 20);
            this.subtitleLbl.TabIndex = 44;
            this.subtitleLbl.Text = "Multiple Room Move";
            // 
            // startDateLbl
            // 
            this.startDateLbl.AutoSize = true;
            this.startDateLbl.Location = new System.Drawing.Point(28, 46);
            this.startDateLbl.Name = "startDateLbl";
            this.startDateLbl.Size = new System.Drawing.Size(55, 13);
            this.startDateLbl.TabIndex = 45;
            this.startDateLbl.Text = "Start Date";
            this.startDateLbl.Visible = false;
            // 
            // cropLbl
            // 
            this.cropLbl.AutoSize = true;
            this.cropLbl.Location = new System.Drawing.Point(316, 46);
            this.cropLbl.Name = "cropLbl";
            this.cropLbl.Size = new System.Drawing.Size(29, 13);
            this.cropLbl.TabIndex = 49;
            this.cropLbl.Text = "Crop";
            // 
            // jobLbl
            // 
            this.jobLbl.AutoSize = true;
            this.jobLbl.Location = new System.Drawing.Point(401, 46);
            this.jobLbl.Name = "jobLbl";
            this.jobLbl.Size = new System.Drawing.Size(52, 13);
            this.jobLbl.TabIndex = 50;
            this.jobLbl.Text = "Box Type";
            // 
            // fieldLbl
            // 
            this.fieldLbl.AutoSize = true;
            this.fieldLbl.Location = new System.Drawing.Point(481, 46);
            this.fieldLbl.Name = "fieldLbl";
            this.fieldLbl.Size = new System.Drawing.Size(39, 13);
            this.fieldLbl.TabIndex = 51;
            this.fieldLbl.Text = "Where";
            // 
            // cropCbx
            // 
            this.cropCbx.FormattingEnabled = true;
            this.cropCbx.Location = new System.Drawing.Point(292, 61);
            this.cropCbx.Name = "cropCbx";
            this.cropCbx.Size = new System.Drawing.Size(77, 21);
            this.cropCbx.Sorted = true;
            this.cropCbx.TabIndex = 4;
            this.cropCbx.DropDown += new System.EventHandler(this.cropCbx_DropDown);
            this.cropCbx.SelectedIndexChanged += new System.EventHandler(this.cropCbx_SelectedIndexChanged);
            this.cropCbx.DropDownClosed += new System.EventHandler(this.cropCbx_DropDownClosed);
            this.cropCbx.Enter += new System.EventHandler(this.cropCbx_Enter);
            this.cropCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cropCbx_KeyDown);
            this.cropCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // jobCbx
            // 
            this.jobCbx.FormattingEnabled = true;
            this.jobCbx.Location = new System.Drawing.Point(375, 61);
            this.jobCbx.Name = "jobCbx";
            this.jobCbx.Size = new System.Drawing.Size(77, 21);
            this.jobCbx.Sorted = true;
            this.jobCbx.TabIndex = 5;
            this.jobCbx.DropDown += new System.EventHandler(this.jobCbx_DropDown);
            this.jobCbx.SelectedIndexChanged += new System.EventHandler(this.jobCbx_SelectedIndexChanged);
            this.jobCbx.DropDownClosed += new System.EventHandler(this.jobCbx_DropDownClosed);
            this.jobCbx.Enter += new System.EventHandler(this.jobCbx_Enter);
            this.jobCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.jobCbx_KeyDown);
            this.jobCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // fieldCbx
            // 
            this.fieldCbx.FormattingEnabled = true;
            this.fieldCbx.Location = new System.Drawing.Point(457, 61);
            this.fieldCbx.Name = "fieldCbx";
            this.fieldCbx.Size = new System.Drawing.Size(77, 21);
            this.fieldCbx.Sorted = true;
            this.fieldCbx.TabIndex = 6;
            this.fieldCbx.DropDown += new System.EventHandler(this.fieldCbx_DropDown);
            this.fieldCbx.SelectedIndexChanged += new System.EventHandler(this.fieldCbx_SelectedIndexChanged);
            this.fieldCbx.DropDownClosed += new System.EventHandler(this.fieldCbx_DropDownClosed);
            this.fieldCbx.Enter += new System.EventHandler(this.fieldCbx_Enter);
            this.fieldCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.fieldCbx_KeyDown);
            this.fieldCbx.Leave += new System.EventHandler(this.comboBox_Leave);
            // 
            // savedPayDetailsLst
            // 
            this.savedPayDetailsLst.Font = new System.Drawing.Font("Courier New", 9F);
            this.savedPayDetailsLst.FormattingEnabled = true;
            this.savedPayDetailsLst.ItemHeight = 15;
            this.savedPayDetailsLst.Location = new System.Drawing.Point(9, 152);
            this.savedPayDetailsLst.Name = "savedPayDetailsLst";
            this.savedPayDetailsLst.Size = new System.Drawing.Size(973, 274);
            this.savedPayDetailsLst.TabIndex = 27;
            this.savedPayDetailsLst.SelectedIndexChanged += new System.EventHandler(this.savedPayDetailsLst_SelectedIndexChanged);
            // 
            // fillScreenBtn
            // 
            this.fillScreenBtn.Location = new System.Drawing.Point(770, 9);
            this.fillScreenBtn.Name = "fillScreenBtn";
            this.fillScreenBtn.Size = new System.Drawing.Size(120, 34);
            this.fillScreenBtn.TabIndex = 9;
            this.fillScreenBtn.Text = "Fill Screen";
            this.fillScreenBtn.UseVisualStyleBackColor = true;
            this.fillScreenBtn.Click += new System.EventHandler(this.fillScreenBtn_Click);
            this.fillScreenBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.saveBtn_KeyDown);
            // 
            // saveEditedBtn
            // 
            this.saveEditedBtn.Enabled = false;
            this.saveEditedBtn.Location = new System.Drawing.Point(642, 471);
            this.saveEditedBtn.Name = "saveEditedBtn";
            this.saveEditedBtn.Size = new System.Drawing.Size(125, 34);
            this.saveEditedBtn.TabIndex = 39;
            this.saveEditedBtn.Text = "Save &Edited Detail";
            this.saveEditedBtn.UseVisualStyleBackColor = true;
            this.saveEditedBtn.Click += new System.EventHandler(this.saveEditedBtn_Click);
            this.saveEditedBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editBtn_KeyDown);
            // 
            // startDatePicker
            // 
            this.startDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.startDatePicker.Location = new System.Drawing.Point(7, 61);
            this.startDatePicker.Name = "startDatePicker";
            this.startDatePicker.Size = new System.Drawing.Size(97, 20);
            this.startDatePicker.TabIndex = 0;
            this.startDatePicker.Visible = false;
            this.startDatePicker.Enter += new System.EventHandler(this.datePicker_Enter);
            this.startDatePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.datePicker_KeyDown);
            // 
            // employeeCbx
            // 
            this.employeeCbx.FormattingEnabled = true;
            this.employeeCbx.Location = new System.Drawing.Point(211, 61);
            this.employeeCbx.Name = "employeeCbx";
            this.employeeCbx.Size = new System.Drawing.Size(77, 21);
            this.employeeCbx.Sorted = true;
            this.employeeCbx.TabIndex = 1;
            this.employeeCbx.DropDown += new System.EventHandler(this.employeeCbx_DropDown);
            this.employeeCbx.SelectedIndexChanged += new System.EventHandler(this.employeeCbx_SelectedIndexChanged);
            this.employeeCbx.DropDownClosed += new System.EventHandler(this.employeeCbx_DropDownClosed);
            this.employeeCbx.Enter += new System.EventHandler(this.employeeCbx_Enter);
            this.employeeCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.employeeCbx_KeyDown);
            // 
            // employeeLbl
            // 
            this.employeeLbl.AutoSize = true;
            this.employeeLbl.Location = new System.Drawing.Point(223, 46);
            this.employeeLbl.Name = "employeeLbl";
            this.employeeLbl.Size = new System.Drawing.Size(51, 13);
            this.employeeLbl.TabIndex = 46;
            this.employeeLbl.Text = "Customer";
            // 
            // editFieldCbx
            // 
            this.editFieldCbx.FormattingEnabled = true;
            this.editFieldCbx.Location = new System.Drawing.Point(345, 446);
            this.editFieldCbx.Name = "editFieldCbx";
            this.editFieldCbx.Size = new System.Drawing.Size(77, 21);
            this.editFieldCbx.Sorted = true;
            this.editFieldCbx.TabIndex = 36;
            this.editFieldCbx.SelectedIndexChanged += new System.EventHandler(this.editFieldCbx_SelectedIndexChanged);
            this.editFieldCbx.DropDownClosed += new System.EventHandler(this.editFieldCbx_DropDownClosed);
            this.editFieldCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editFieldCbx_KeyDown);
            // 
            // editFieldLbl
            // 
            this.editFieldLbl.AutoSize = true;
            this.editFieldLbl.Location = new System.Drawing.Point(369, 430);
            this.editFieldLbl.Name = "editFieldLbl";
            this.editFieldLbl.Size = new System.Drawing.Size(39, 13);
            this.editFieldLbl.TabIndex = 67;
            this.editFieldLbl.Text = "Where";
            // 
            // savedPayDetailsHeaderLbl
            // 
            this.savedPayDetailsHeaderLbl.AutoSize = true;
            this.savedPayDetailsHeaderLbl.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedPayDetailsHeaderLbl.Location = new System.Drawing.Point(10, 131);
            this.savedPayDetailsHeaderLbl.Name = "savedPayDetailsHeaderLbl";
            this.savedPayDetailsHeaderLbl.Size = new System.Drawing.Size(966, 15);
            this.savedPayDetailsHeaderLbl.TabIndex = 58;
            this.savedPayDetailsHeaderLbl.Text = "|   Date   | Customer                    |  Crop  |Box Type| Where | Grower Block" +
    " | Box Label | Reference | Other | Amount In|Amount Out|";
            // 
            // keyLst
            // 
            this.keyLst.FormattingEnabled = true;
            this.keyLst.Location = new System.Drawing.Point(131, 19);
            this.keyLst.Name = "keyLst";
            this.keyLst.Size = new System.Drawing.Size(120, 17);
            this.keyLst.TabIndex = 42;
            this.keyLst.Visible = false;
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 9);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 103;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // endDatePicker
            // 
            this.endDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.endDatePicker.Location = new System.Drawing.Point(108, 61);
            this.endDatePicker.Name = "endDatePicker";
            this.endDatePicker.Size = new System.Drawing.Size(97, 20);
            this.endDatePicker.TabIndex = 104;
            this.endDatePicker.Visible = false;
            // 
            // endDateLabel
            // 
            this.endDateLabel.AutoSize = true;
            this.endDateLabel.Location = new System.Drawing.Point(130, 46);
            this.endDateLabel.Name = "endDateLabel";
            this.endDateLabel.Size = new System.Drawing.Size(52, 13);
            this.endDateLabel.TabIndex = 105;
            this.endDateLabel.Text = "End Date";
            this.endDateLabel.Visible = false;
            // 
            // clearBtn
            // 
            this.clearBtn.Location = new System.Drawing.Point(626, 9);
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(120, 34);
            this.clearBtn.TabIndex = 106;
            this.clearBtn.Text = "Clear Selections";
            this.clearBtn.UseVisualStyleBackColor = true;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // EndEditBtn
            // 
            this.EndEditBtn.Location = new System.Drawing.Point(126, 472);
            this.EndEditBtn.Name = "EndEditBtn";
            this.EndEditBtn.Size = new System.Drawing.Size(92, 34);
            this.EndEditBtn.TabIndex = 107;
            this.EndEditBtn.Text = "End Edit";
            this.EndEditBtn.UseVisualStyleBackColor = true;
            this.EndEditBtn.Click += new System.EventHandler(this.EndEditBtn_Click);
            // 
            // sortbyDateChx
            // 
            this.sortbyDateChx.AutoSize = true;
            this.sortbyDateChx.Location = new System.Drawing.Point(10, 20);
            this.sortbyDateChx.Name = "sortbyDateChx";
            this.sortbyDateChx.Size = new System.Drawing.Size(85, 17);
            this.sortbyDateChx.TabIndex = 108;
            this.sortbyDateChx.Text = "Sort by Date";
            this.sortbyDateChx.UseVisualStyleBackColor = true;
            this.sortbyDateChx.CheckedChanged += new System.EventHandler(this.sortbyDateChx_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.groupBox1.Controls.Add(this.growerLbl);
            this.groupBox1.Controls.Add(this.growerCbx);
            this.groupBox1.Controls.Add(this.employeeLbl);
            this.groupBox1.Controls.Add(this.startDateLbl);
            this.groupBox1.Controls.Add(this.sortbyDateChx);
            this.groupBox1.Controls.Add(this.cropLbl);
            this.groupBox1.Controls.Add(this.clearBtn);
            this.groupBox1.Controls.Add(this.jobLbl);
            this.groupBox1.Controls.Add(this.endDatePicker);
            this.groupBox1.Controls.Add(this.fieldLbl);
            this.groupBox1.Controls.Add(this.endDateLabel);
            this.groupBox1.Controls.Add(this.cropCbx);
            this.groupBox1.Controls.Add(this.jobCbx);
            this.groupBox1.Controls.Add(this.keyLst);
            this.groupBox1.Controls.Add(this.fieldCbx);
            this.groupBox1.Controls.Add(this.fillScreenBtn);
            this.groupBox1.Controls.Add(this.startDatePicker);
            this.groupBox1.Controls.Add(this.employeeCbx);
            this.groupBox1.Location = new System.Drawing.Point(6, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(897, 92);
            this.groupBox1.TabIndex = 111;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Edit Filters";
            // 
            // growerLbl
            // 
            this.growerLbl.AutoSize = true;
            this.growerLbl.Location = new System.Drawing.Point(545, 46);
            this.growerLbl.Name = "growerLbl";
            this.growerLbl.Size = new System.Drawing.Size(71, 13);
            this.growerLbl.TabIndex = 113;
            this.growerLbl.Text = "Grower Block";
            // 
            // growerCbx
            // 
            this.growerCbx.FormattingEnabled = true;
            this.growerCbx.Location = new System.Drawing.Point(540, 61);
            this.growerCbx.Name = "growerCbx";
            this.growerCbx.Size = new System.Drawing.Size(77, 21);
            this.growerCbx.Sorted = true;
            this.growerCbx.TabIndex = 112;
            // 
            // KeyCentralPbx
            // 
            this.KeyCentralPbx.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___200x40_pix_;
            this.KeyCentralPbx.Location = new System.Drawing.Point(281, -22);
            this.KeyCentralPbx.Name = "KeyCentralPbx";
            this.KeyCentralPbx.Size = new System.Drawing.Size(317, 94);
            this.KeyCentralPbx.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.KeyCentralPbx.TabIndex = 113;
            this.KeyCentralPbx.TabStop = false;
            // 
            // MultipleRoomMove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 509);
            this.Controls.Add(this.KeyCentralPbx);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.EndEditBtn);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.savedPayDetailsHeaderLbl);
            this.Controls.Add(this.editFieldCbx);
            this.Controls.Add(this.editFieldLbl);
            this.Controls.Add(this.saveEditedBtn);
            this.Controls.Add(this.savedPayDetailsLst);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MultipleRoomMove";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Storage Inventory - Edit Inventory Detail";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.KeyCentralPbx)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Label startDateLbl;
        private System.Windows.Forms.Label cropLbl;
        private System.Windows.Forms.Label jobLbl;
        private System.Windows.Forms.Label fieldLbl;
        private System.Windows.Forms.ComboBox cropCbx;
        private System.Windows.Forms.ComboBox jobCbx;
        private System.Windows.Forms.ComboBox fieldCbx;
        private System.Windows.Forms.ListBox savedPayDetailsLst;
        private System.Windows.Forms.Button fillScreenBtn;
        private System.Windows.Forms.Button saveEditedBtn;
        private System.Windows.Forms.DateTimePicker startDatePicker;
		private System.Windows.Forms.ComboBox employeeCbx;
        private System.Windows.Forms.Label employeeLbl;
        private System.Windows.Forms.ComboBox editFieldCbx;
        private System.Windows.Forms.Label editFieldLbl;
        private System.Windows.Forms.Label savedPayDetailsHeaderLbl;
        private System.Windows.Forms.ListBox keyLst;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.DateTimePicker endDatePicker;
        private System.Windows.Forms.Label endDateLabel;
        private System.Windows.Forms.Button clearBtn;
        private System.Windows.Forms.Button EndEditBtn;
        private System.Windows.Forms.CheckBox sortbyDateChx;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox KeyCentralPbx;
        private System.Windows.Forms.Label growerLbl;
        private System.Windows.Forms.ComboBox growerCbx;
    }
}