﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Management;
using SoftekBarcodeMakerLib2;
using System.Drawing.Printing;


namespace StorageInventory
{
    public partial class ShippingOrder : Form
    {

        #region Vars

        private MainMenu.CropsData cropsData;
        private MainMenu.JobsData jobsData;
        private MainMenu.FieldsData fieldsData;
        private MainMenu.EmployeesData employeesData;
        private MainMenu.PayDetailsData payDetailsData;
        private MainMenu.PayrollOptionsData payrollOptionsData;
        private MainMenu.Grower_Blocks_Data growerBlocksData;
        private ArrayList receiverArl;
        private ArrayList EmployeesArl;
        private ArrayList CropsArl;
        private ArrayList FieldsArl;
        private ArrayList JobsArl;
        private ArrayList GrowerBlockArl;
        private Printer printer;
        private string xmlFileName_Crops;
        private string xmlFileName_Jobs;
        private string xmlFileName_Fields;
        private string xmlFileName_Employees;
        private string xmlFileName_payrollOptionsData;
        private string xmlFileName_PayDetails;
        private string xmlFileName_Grower_Block;
        private int tagCountInt;
        private string timeStampStr;
        private string employeeLabelStr;
        private string cropLabelStr;
        private string jobLabelStr;
        private string fieldLabelStr;
        private string boxLabelLabelStr;
        private string growerBlockLabelStr;
        private Int32 countLabelInt;


        //Program option variables.
        private char delimiter;
        private string delimiterWithSpaces;
       
        #endregion


        public ShippingOrder()
        {
//Initialize the program option variables
			delimiter = '~';
			delimiterWithSpaces = " " + delimiter.ToString() + " ";
			

			//Default-Initialize
			InitializeComponent();

			//Initialize object variables.
			cropsData = new MainMenu.CropsData();
			jobsData = new MainMenu.JobsData();
			fieldsData = new MainMenu.FieldsData();
            employeesData = new MainMenu.EmployeesData();
            payrollOptionsData = new MainMenu.PayrollOptionsData();
            payDetailsData = new MainMenu.PayDetailsData();
            growerBlocksData = new MainMenu.Grower_Blocks_Data();
			//Set the XML file names
			xmlFileName_Crops = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
			xmlFileName_Jobs = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
			xmlFileName_Fields = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
            xmlFileName_Employees = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
            xmlFileName_payrollOptionsData = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayrollOptionsData.xml";
            xmlFileName_PayDetails = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayDetailData.xml";
            xmlFileName_Grower_Block = Path.GetDirectoryName(Application.ExecutablePath) + @"\Grower_Block_Data.xml";
            //Load all of our data from XML
            FillCropDataFromXML();
			FillFieldDataFromXML();
			FillJobDataFromXML();
            FillEmployeeDataFromXML();
            FillPayrollOptionsDataFromXML();
            FillPayDetailDataFromXML();
            FillGrowerBlockDataFromXML();
            //Set the dropdown boxes to auto-complete
            cropCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            crop2Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            crop3Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            crop4Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            crop5Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            crop6Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            crop7Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            crop8Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

			cropCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            crop2Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            crop3Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            crop4Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            crop5Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            crop6Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            crop7Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            crop8Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;

			jobCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            job2Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            job3Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            job4Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            job5Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            job6Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            job7Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            job8Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

			jobCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            job2Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            job3Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            job4Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            job5Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            job6Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            job7Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            job8Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;

			fieldCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            field2Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            field3Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            field4Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            field5Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            field6Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            field7Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            field8Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

			fieldCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            field2Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            field3Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            field4Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            field5Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            field6Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            field7Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            field8Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;

            growerCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            grower2Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            grower3Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            grower4Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            grower5Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            grower6Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            grower7Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            grower8Cbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;

            growerCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            grower2Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            grower3Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            grower4Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            grower5Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            grower6Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            grower7Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            grower8Cbx.AutoCompleteSource = AutoCompleteSource.ListItems;

            employeeCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            employeeCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
			//Fill the dropdown lists
			FillCropsDropdown();
			FillJobsDropdown();
			FillFieldsDropdown();
            FillEmployeesDropdown();
            FillGrowerBlocksDropdown();
            OurReferenceTxt.Text = DateTime.Now.ToString().Replace("/", "-").Replace(":", "-");
			
            CompanyNameData.Text = MainMenu.CompanyStr;
		}

        #region Helper Methods
        //Get Data
        private void FillCropDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Crops);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Crops, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
       
        private void FillJobDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Jobs);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Jobs, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillFieldDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Fields);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Fields, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillEmployeeDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Employees);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Employees, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillPayrollOptionsDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_payrollOptionsData);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayrollOptionsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_payrollOptionsData, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                payrollOptionsData = (MainMenu.PayrollOptionsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillPayDetailDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_PayDetails);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayDetailsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_PayDetails, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                payDetailsData = (MainMenu.PayDetailsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillGrowerBlockDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Grower_Block);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.Grower_Blocks_Data));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Grower_Block, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                growerBlocksData = (MainMenu.Grower_Blocks_Data)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }

        //Fill DropDown Functions

        private void FillCropsDropdown()
        {
            //Clear the existing list
            cropCbx.Items.Clear();
            crop2Cbx.Items.Clear();
            crop3Cbx.Items.Clear();
            crop4Cbx.Items.Clear();
            crop5Cbx.Items.Clear();
            crop6Cbx.Items.Clear();
            crop7Cbx.Items.Clear();
            crop8Cbx.Items.Clear();

            cropCbx.SelectedIndex = -1;
            crop2Cbx.SelectedIndex = -1;
            crop3Cbx.SelectedIndex = -1;
            crop4Cbx.SelectedIndex = -1;
            crop5Cbx.SelectedIndex = -1;
            crop6Cbx.SelectedIndex = -1;
            crop7Cbx.SelectedIndex = -1;
            crop8Cbx.SelectedIndex = -1;

            CropsArl = new ArrayList();

            //Fill the crop data object
            FillCropDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
            {
                MainMenu.CropData crop = (MainMenu.CropData)cropsData.CropsDataArl[i];

                if (crop.ActiveBln)
                {
                    cropCbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    crop2Cbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    crop3Cbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    crop4Cbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    crop5Cbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    crop6Cbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    crop7Cbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    crop8Cbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
                    CropsArl.Add(crop);
                }
            }
        }
        private void FillJobsDropdown()
        {
            //Clear the existing list
            jobCbx.Items.Clear();
            job2Cbx.Items.Clear();
            job3Cbx.Items.Clear();
            job4Cbx.Items.Clear();
            job5Cbx.Items.Clear();
            job6Cbx.Items.Clear();
            job7Cbx.Items.Clear();
            job8Cbx.Items.Clear();

            jobCbx.SelectedIndex = -1;
            job2Cbx.SelectedIndex = -1;
            job3Cbx.SelectedIndex = -1;
            job4Cbx.SelectedIndex = -1;
            job5Cbx.SelectedIndex = -1;
            job6Cbx.SelectedIndex = -1;
            job7Cbx.SelectedIndex = -1;
            job8Cbx.SelectedIndex = -1;

            JobsArl = new ArrayList();

            //Fill the job data object
            FillJobDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
            {
                MainMenu.JobData job = (MainMenu.JobData)jobsData.JobsDataArl[i];

                if (job.ActiveBln)
                {
                    jobCbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    job2Cbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    job3Cbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    job4Cbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    job5Cbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    job6Cbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    job7Cbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    job8Cbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
                    JobsArl.Add(job);
                }
            }
        }
        private void FillFieldsDropdown()
        {
            //Clear the existing list
            fieldCbx.Items.Clear();
            field2Cbx.Items.Clear();
            field3Cbx.Items.Clear();
            field4Cbx.Items.Clear();
            field5Cbx.Items.Clear();
            field6Cbx.Items.Clear();
            field7Cbx.Items.Clear();
            field8Cbx.Items.Clear();

            fieldCbx.SelectedIndex = -1;
            field2Cbx.SelectedIndex = -1;
            field3Cbx.SelectedIndex = -1;
            field4Cbx.SelectedIndex = -1;
            field5Cbx.SelectedIndex = -1;
            field6Cbx.SelectedIndex = -1;
            field7Cbx.SelectedIndex = -1;
            field8Cbx.SelectedIndex = -1;

            FieldsArl = new ArrayList();

            //Fill the field data object
            FillFieldDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
            {
                MainMenu.FieldData field = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

                if (field.ActiveBln)
                {
                    fieldCbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    field2Cbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    field3Cbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    field4Cbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    field5Cbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    field6Cbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    field7Cbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    field8Cbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
                    FieldsArl.Add(field);
                }
            }
        }
        private void FillEmployeesDropdown()
        {
            //Clear the existing list
            employeeCbx.Items.Clear();
            employeeCbx.SelectedIndex = -1;

            //editEmployeeCbx.Items.Clear();
            //editEmployeeCbx.SelectedIndex = -1;

            EmployeesArl = new ArrayList();

            //Fill the employee data object
            FillEmployeeDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
            {
                MainMenu.EmployeeData employee = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                if (employee.ActiveBln)
                {

                    employeeCbx.Items.Add(employee.CodeStr + delimiterWithSpaces + employee.NameStr);
                   
                    EmployeesArl.Add(employee);
                }
            }
        }
        private void FillGrowerBlocksDropdown()
        {
            //Clear the existing list
            growerCbx.Items.Clear();
            grower2Cbx.Items.Clear();
            grower3Cbx.Items.Clear();
            grower4Cbx.Items.Clear();
            grower5Cbx.Items.Clear();
            grower6Cbx.Items.Clear();
            grower7Cbx.Items.Clear();
            grower8Cbx.Items.Clear();

            growerCbx.SelectedIndex = -1;
            grower2Cbx.SelectedIndex = -1;
            grower3Cbx.SelectedIndex = -1;
            grower4Cbx.SelectedIndex = -1;
            grower5Cbx.SelectedIndex = -1;
            grower6Cbx.SelectedIndex = -1;
            grower7Cbx.SelectedIndex = -1;
            grower8Cbx.SelectedIndex = -1;

            GrowerBlockArl = new ArrayList();

            //Fill the crop data object
            FillGrowerBlockDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < growerBlocksData.GrowerBlocksDataArl.Count; i++)
            {
                MainMenu.GrowerBlockData grower = (MainMenu.GrowerBlockData)growerBlocksData.GrowerBlocksDataArl[i];

                if (grower.ActiveBln)
                {
                    growerCbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    grower2Cbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    grower3Cbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    grower4Cbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    grower5Cbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    grower6Cbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    grower7Cbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    grower8Cbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    GrowerBlockArl.Add(grower);
                }
            }
        }
        //Adjust ComboBox dropdown list width
        private static void SetComboScrollWidth(object sender)
        {
            //CREDIT - this code was found at: http://rajeshkm.blogspot.com/2006/11/adjust-combobox-drop-down-list-width-c.html
            //      THANKS Rajesh!
            try
            {
                ComboBox senderComboBox = (ComboBox)sender;
                int width = senderComboBox.Width;
                Graphics g = senderComboBox.CreateGraphics();
                Font font = senderComboBox.Font;

                //checks if a scrollbar will be displayed.
                //If yes, then get its width to adjust the size of the drop down list.
                int vertScrollBarWidth = (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems) ? SystemInformation.VerticalScrollBarWidth : 0;

                //Loop through list items and check size of each items.
                //set the width of the drop down list to the width of the largest item.

                int newWidth;

                foreach (string s in ((ComboBox)sender).Items)
                {
                    if (s != null)
                    {
                        newWidth = (int)g.MeasureString(s.Trim(), font).Width + vertScrollBarWidth;

                        if (width < newWidth)
                        {
                            width = newWidth;
                        }
                    }
                }

                senderComboBox.DropDownWidth = width;
            }
            catch (Exception objException)
            {
                //Catch objException
            }
        }

        //Validation Function(s)
        private void ValidateQuantity(TextBox textBoxToCheck, string textBoxName)
        {
            try
            {
                decimal temp = decimal.Parse(textBoxToCheck.Text);

                if (textBoxToCheck.Text != "" && textBoxToCheck.Text.IndexOf(",") == -1)
                {
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
                else if (textBoxToCheck.Text.IndexOf(",") != -1)
                {
                    textBoxToCheck.Text.Replace(",", "");
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show(this, "Invalid " + textBoxName + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxToCheck.Focus();
                textBoxToCheck.SelectAll();
            }
        }

        //Formating Function(s)
        private static string DisplayFormat(decimal value)
        {
            if (value == 0)
                return value.ToString();
            else
                return decimal.Round(value, 2).ToString("#,###,##0.##");
        }
        
       
        private void EnableAllTextFields()
        {
            
            cropCbx.Enabled = true;
            jobCbx.Enabled = true;
            fieldCbx.Enabled = true;
            priceTxt.Enabled = true;
        }
        private void ResetScreen()
        {
             cropCbx.SelectedIndex = -1;
             jobCbx.SelectedIndex = -1;
             fieldCbx.SelectedIndex = -1;
             priceTxt.Text = "0";
        }
        public static string ParseTildaString(string stringToParse, int field)
        {
            string[] stringSplit = stringToParse.Split('~');

            if (stringSplit.Length <= field)
                return "";
            else
                return stringSplit[field];
        }
        #endregion

        private void printReportBtn_Click(object sender, EventArgs e)
        {
            // save data
            // print tags call print labels
            // create and print report
            timeStampStr = DateTime.Today.ToShortDateString() + "-" + DateTime.Now.ToLongTimeString();
            receiverArl = new ArrayList();
            MainMenu.ReceiverSend tempReceiverSend = new MainMenu.ReceiverSend();
            if (priceTxt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = cropCbx.SelectedItem.ToString();
                jobLabelStr = jobCbx.SelectedItem.ToString();
                fieldLabelStr = fieldCbx.SelectedItem.ToString();
                growerBlockLabelStr = growerCbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabelTxt.Text.ToString();
                countLabelInt = Convert.ToInt32(priceTxt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }
            if (price2Txt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = crop2Cbx.SelectedItem.ToString();
                jobLabelStr = job2Cbx.SelectedItem.ToString();
                fieldLabelStr = field2Cbx.SelectedItem.ToString();
                growerBlockLabelStr = grower2Cbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabel2Txt.Text.ToString();
                countLabelInt = Convert.ToInt32(price2Txt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }
            if (price3Txt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = crop3Cbx.SelectedItem.ToString();
                jobLabelStr = job3Cbx.SelectedItem.ToString();
                fieldLabelStr = field3Cbx.SelectedItem.ToString();
                growerBlockLabelStr = grower3Cbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabel3Txt.Text.ToString();
                countLabelInt = Convert.ToInt32(price3Txt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }
            if (price4Txt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = crop4Cbx.SelectedItem.ToString();
                jobLabelStr = job4Cbx.SelectedItem.ToString();
                fieldLabelStr = field4Cbx.SelectedItem.ToString();
                growerBlockLabelStr = grower4Cbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabel4Txt.Text.ToString();
                countLabelInt = Convert.ToInt32(price4Txt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }
            if (price5Txt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = crop5Cbx.SelectedItem.ToString();
                jobLabelStr = job5Cbx.SelectedItem.ToString();
                fieldLabelStr = field5Cbx.SelectedItem.ToString();
                growerBlockLabelStr = grower5Cbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabel5Txt.Text.ToString();
                countLabelInt = Convert.ToInt32(price5Txt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }
            if (price6Txt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = crop6Cbx.SelectedItem.ToString();
                jobLabelStr = job6Cbx.SelectedItem.ToString();
                fieldLabelStr = field6Cbx.SelectedItem.ToString();
                growerBlockLabelStr = grower6Cbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabel6Txt.Text.ToString();
                countLabelInt = Convert.ToInt32(price6Txt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }
            if (price7Txt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = crop7Cbx.SelectedItem.ToString();
                jobLabelStr = job7Cbx.SelectedItem.ToString();
                fieldLabelStr = field7Cbx.SelectedItem.ToString();
                growerBlockLabelStr = grower7Cbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabel7Txt.Text.ToString();
                countLabelInt = Convert.ToInt32(price7Txt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }
            if (price8Txt.Text != "")
            {
                employeeLabelStr = employeeCbx.SelectedItem.ToString();
                cropLabelStr = crop8Cbx.SelectedItem.ToString();
                jobLabelStr = job8Cbx.SelectedItem.ToString();
                fieldLabelStr = field8Cbx.SelectedItem.ToString();
                growerBlockLabelStr = grower8Cbx.SelectedItem.ToString();
                boxLabelLabelStr = boxLabel8Txt.Text.ToString();
                countLabelInt = Convert.ToInt32(price8Txt.Text);
                tempReceiverSend = new MainMenu.ReceiverSend();
                tempReceiverSend.cropLabelStr = cropLabelStr;
                tempReceiverSend.jobLabelStr = jobLabelStr;
                tempReceiverSend.fieldLabelStr = fieldLabelStr;
                tempReceiverSend.growerLabelStr = growerBlockLabelStr;
                tempReceiverSend.boxLabelLabelStr = boxLabelLabelStr;
                tempReceiverSend.countLabelInt = countLabelInt;
                receiverArl.Add(tempReceiverSend);
                saveDataLine();
            }

            #region Make PDF
            
            string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Shipper\";
            string fileNameStr = "Shipper" + OurReferenceTxt.Text.ToString() + " " + DateTime.Now.ToString().Replace("/", "-").Replace(":", "-") + ".pdf";
            ShippingOrder_Report workReport = new ShippingOrder_Report();

            workReport.Main(saveFolderStr, fileNameStr, receiverArl, employeeLabelStr, datePicker.Text.ToString(), OurReferenceTxt.Text.ToString(), CustReferenceTxt.Text.ToString(), cust1Lbl.Text.ToString(), cust2Lbl.Text.ToString(), cust3Lbl.Text.ToString(), cust4Lbl.Text.ToString());
            #endregion
            #region Open PDF
            Process.Start(saveFolderStr + fileNameStr);
            #endregion
            
        }

        //private void printLabels(string employeeLabelStr, string cropLabelStr, string jobLabelStr, string fieldLabelStr, string boxLabelLabelStr, Int32 countLabelInt)
        //private void printLabels()
        //{
            
        //    tagCountInt = countLabelInt;
        //    for (int i = 0; i < tagCountInt; i++)
        //    {
        //        printer = new Printer();
        //        printer.PrintPage += new PrintPageEventHandler(PrintBarcode);
        //        printer.PrintDirectToPrinter(payrollOptionsData.LabelPrinterStr.Trim());

        //    }
        //}

        private void PrintBarcode(object sender, PrintPageEventArgs e)
        {

            #region Make Label


            //website for inches to pixel conversion: http://www.classical-webdesigns.co.uk/resources/pixelinchconvert.html

            //4 in. x 2 in. = 400 px X 200 px
            //e.Graphics.DrawRectangle(new Pen(Brushes.Black), 0, 0, totalWidth, totalHeight);

            //float totalWidth = e.PageSettings.PaperSize.Width;
            //float totalHeight = e.PageSettings.PaperSize.Height;
            //float totalWidth = 400; //4 inches
            //float totalHeight = 200;//2 inches

            //Company name
            int currentX = 10;
            int currentY = 5;
            int lineHeight = 15;
            e.Graphics.DrawString(ParseTildaString(employeeLabelStr, 1).Trim(), new Font("Arial", 20), Brushes.Black, new Point(currentX, currentY));

            //Company code
            currentX = 10;
            currentY = 30;
            e.Graphics.DrawString(ParseTildaString(employeeLabelStr, 0).Trim(), new Font("Arial", 80), Brushes.Black, new Point(currentX, currentY));

            //Variety Code
            currentX = 10;
            currentY = 130;
            e.Graphics.DrawString(ParseTildaString(cropLabelStr, 0).Trim(), new Font("Arial", 100), Brushes.Black, new Point(currentX, currentY));

            //Variety Description
            currentX = 10;
            currentY = 280;
            e.Graphics.DrawString(ParseTildaString(cropLabelStr, 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            //box Label
            currentX = 10;
            currentY = 350;
            e.Graphics.DrawString(boxLabelLabelStr, new Font("Arial", 50), Brushes.Black, new Point(currentX, currentY));
            //Box Type Description
            currentX = 10;
            currentY = 430;
            e.Graphics.DrawString(ParseTildaString(jobLabelStr, 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            //Where Description
            currentX = 10;
            currentY = 500;
            e.Graphics.DrawString(ParseTildaString(fieldLabelStr, 1).Trim(), new Font("Arial", 40), Brushes.Black, new Point(currentX, currentY));
            //Where Time Stamp
            currentX = 10;
            currentY = 560;
            e.Graphics.DrawString(timeStampStr, new Font("Arial", 20), Brushes.Black, new Point(currentX, currentY));

            #endregion

        }

        private void priceTxt_TextChanged(object sender, EventArgs e)
        {
             
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void employeeCbx_SelectedIndexChanged(object sender, EventArgs e)
        {
            
             for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
            {
                MainMenu.EmployeeData employee = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                if (employee.CodeStr + delimiterWithSpaces + employee.NameStr == employeeCbx.SelectedItem.ToString())
                {
                    cust1Lbl.Text = employee.NameStr;
                     cust2Lbl.Text = employee.Address1Str;
                     if (employee.Address2Str != "")
                     {
                     cust3Lbl.Text = employee.Address2Str;
                     cust4Lbl.Text = employee.CityStr + " " + employee.StateStr + " " + employee.ZipStr;
                     }
                     else
                     {
                     cust3Lbl.Text = employee.CityStr + " " + employee.StateStr + " " + employee.ZipStr;
                     cust4Lbl.Text = "";
                     }
                    
                }
            }
        }

        private void saveDataLine()
        {
           

            MainMenu.EmployeeData employeeData = new MainMenu.EmployeeData();
            //for loop on EmployeeArl and match Code to first part of employeeCbx.SelectedItem

            for (int i = 0; i < EmployeesArl.Count; i++)
            {
                MainMenu.EmployeeData tempData = (MainMenu.EmployeeData)EmployeesArl[i];

                if (tempData.CodeStr == ParseTildaString(employeeLabelStr, 0).Trim())
                {
                    employeeData = tempData;
                    break;
                }
            }
            //for loop on CropArl and match Code to first part of cropCbx.SelectedItem
            MainMenu.CropData cropData = new MainMenu.CropData();
            for (int i = 0; i < CropsArl.Count; i++)
            {
                MainMenu.CropData tempCropData = (MainMenu.CropData)CropsArl[i];

                if (tempCropData.CodeStr == ParseTildaString(cropLabelStr, 0).Trim())
                {
                    cropData = tempCropData;
                    break;
                }
            }
            //for loop on JobArl and match Code to first part of JobCbx.SelectedItem
            MainMenu.JobData jobData = new MainMenu.JobData();
            for (int i = 0; i < JobsArl.Count; i++)
            {
                MainMenu.JobData tempJobData = (MainMenu.JobData)JobsArl[i];

                if (tempJobData.CodeStr == ParseTildaString(jobLabelStr, 0).Trim())
                {
                    jobData = tempJobData;
                    break;
                }
            }
            //for loop on FieldArl and match Code to first part of FieldCbx.SelectedItem
            MainMenu.FieldData fieldData = new MainMenu.FieldData();
            for (int i = 0; i < FieldsArl.Count; i++)
            {
                MainMenu.FieldData tempFieldData = (MainMenu.FieldData)FieldsArl[i];

                if (tempFieldData.CodeStr == ParseTildaString(fieldLabelStr, 0).Trim())
                {
                    fieldData = tempFieldData;
                    break;
                }
            }
            //for loop onGrowerBlockArl and match Code to first part of FieldCbx.SelectedItem
            MainMenu.GrowerBlockData growerBlockData = new MainMenu.GrowerBlockData();
            for (int i = 0; i < GrowerBlockArl.Count; i++)
            {
                MainMenu.GrowerBlockData tempGrowerBlockdData = (MainMenu.GrowerBlockData)GrowerBlockArl[i];

                if (tempGrowerBlockdData.CodeStr == ParseTildaString(growerBlockLabelStr, 0).Trim())
                {
                    growerBlockData = tempGrowerBlockdData;
                    break;
                }
            }
            //Save the data from the user entry field to a PayDetail object
            // MainMenu.PayDetailData payDetail = MainMenu.FindOrAddPayDetailData(payDetailsData.PayDetailsDataArl, Convert.ToDateTime(datePicker.Text), Convert.ToDateTime(startTimePicker.Text), Convert.ToDateTime(endTimePicker.Text), employeeData.KeyInt, cropData.KeyInt, jobData.KeyInt, fieldData.KeyInt, "P");
            MainMenu.PayDetailData payDetail = new MainMenu.PayDetailData();
            payDetail.DateDtm = Convert.ToDateTime(datePicker.Text);
            payDetail.StartTimeDtm = Convert.ToDateTime(System.DateTime.Now);
            payDetail.EndTimeDtm = Convert.ToDateTime(System.DateTime.Now);
            payDetail.EmployeeKeyInt = employeeData.KeyInt;
            payDetail.CropKeyInt = cropData.KeyInt;
            payDetail.JobKeyInt = jobData.KeyInt;
            payDetail.FieldKeyInt = fieldData.KeyInt;
            payDetail.Extra1Int = growerBlockData.KeyInt;
            payDetail.TypeStr = "P";
            payDetail.ActiveBln = true;
            payDetail.KeyInt = payDetailsData.PayDetailsDataArl.Count;
            //payDetail.HoursDbl = Convert.ToDouble(hoursTxt.Text);
            payDetail.PayUnitDbl = 0;
            payDetail.YieldUnitDbl = Convert.ToDouble(countLabelInt);
            //payDetail.PriceDbl = Convert.ToDouble(priceTxt.Text);
            payDetail.UserNameStr = boxLabelLabelStr;
            payDetail.Extra1Str = OurReferenceTxt.Text;
            payDetail.Extra2Str = CustReferenceTxt.Text;

            //adds line always keep the most recent object up top
            payDetailsData.PayDetailsDataArl.Insert(0, payDetail);

            //Save the Pay Details To XML
            SavePayDetailDataToXML();


        }

        private void SavePayDetailDataToXML()
        {
            //Serialize (convert an object instance to an XML document):
            XmlSerializer xmlSerializer = new XmlSerializer(payDetailsData.GetType());
            // Create an XmlTextWriter using a FileStream.
            Stream fileStream2 = new FileStream(xmlFileName_PayDetails, FileMode.Create);
            XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
            // Serialize using the XmlTextWriter.
            xmlSerializer.Serialize(xmlWriter, payDetailsData);
            xmlWriter.Flush();
            xmlWriter.Close();
        }

        

        
    }
}
