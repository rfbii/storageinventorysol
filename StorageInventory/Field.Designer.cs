﻿namespace StorageInventory
{
    partial class Field
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exitBtn = new System.Windows.Forms.Button();
            this.newCodeTxt = new System.Windows.Forms.TextBox();
            this.newCodeLbl = new System.Windows.Forms.Label();
            this.newDescriptionLbl = new System.Windows.Forms.Label();
            this.newDescriptionTxt = new System.Windows.Forms.TextBox();
            this.savedFieldsLst = new System.Windows.Forms.ListBox();
            this.saveNewBtn = new System.Windows.Forms.Button();
            this.savedFieldsLbl = new System.Windows.Forms.Label();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.saveEditedBtn = new System.Windows.Forms.Button();
            this.editDescriptionLbl = new System.Windows.Forms.Label();
            this.editDescriptionTxt = new System.Windows.Forms.TextBox();
            this.editCodeLbl = new System.Windows.Forms.Label();
            this.editCodeTxt = new System.Windows.Forms.TextBox();
            this.enableDisableBtn = new System.Windows.Forms.Button();
            this.keyTxt = new System.Windows.Forms.TextBox();
            this.activeRdo = new System.Windows.Forms.RadioButton();
            this.inactiveRdo = new System.Windows.Forms.RadioButton();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.fieldListBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(538, 291);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(92, 34);
            this.exitBtn.TabIndex = 8;
            this.exitBtn.Text = "E&xit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // newCodeTxt
            // 
            this.newCodeTxt.AcceptsTab = true;
            this.newCodeTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newCodeTxt.Location = new System.Drawing.Point(332, 82);
            this.newCodeTxt.MaxLength = 16;
            this.newCodeTxt.Name = "newCodeTxt";
            this.newCodeTxt.Size = new System.Drawing.Size(183, 24);
            this.newCodeTxt.TabIndex = 0;
            this.newCodeTxt.Enter += new System.EventHandler(this.newCodeTxt_Enter);
            this.newCodeTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.newCodeTxt_KeyDown);
            this.newCodeTxt.Leave += new System.EventHandler(this.newCodeTxt_Leave);
            // 
            // newCodeLbl
            // 
            this.newCodeLbl.AutoSize = true;
            this.newCodeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newCodeLbl.Location = new System.Drawing.Point(395, 62);
            this.newCodeLbl.Name = "newCodeLbl";
            this.newCodeLbl.Size = new System.Drawing.Size(78, 18);
            this.newCodeLbl.TabIndex = 13;
            this.newCodeLbl.Text = "New Code";
            // 
            // newDescriptionLbl
            // 
            this.newDescriptionLbl.AutoSize = true;
            this.newDescriptionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newDescriptionLbl.Location = new System.Drawing.Point(670, 62);
            this.newDescriptionLbl.Name = "newDescriptionLbl";
            this.newDescriptionLbl.Size = new System.Drawing.Size(117, 18);
            this.newDescriptionLbl.TabIndex = 14;
            this.newDescriptionLbl.Text = "New Description";
            // 
            // newDescriptionTxt
            // 
            this.newDescriptionTxt.AcceptsReturn = true;
            this.newDescriptionTxt.AcceptsTab = true;
            this.newDescriptionTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.newDescriptionTxt.Location = new System.Drawing.Point(529, 82);
            this.newDescriptionTxt.MaxLength = 32;
            this.newDescriptionTxt.Name = "newDescriptionTxt";
            this.newDescriptionTxt.Size = new System.Drawing.Size(366, 24);
            this.newDescriptionTxt.TabIndex = 1;
            this.newDescriptionTxt.Enter += new System.EventHandler(this.newDescriptionTxt_Enter);
            this.newDescriptionTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.newDescriptionTxt_KeyDown);
            // 
            // savedFieldsLst
            // 
            this.savedFieldsLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedFieldsLst.FormattingEnabled = true;
            this.savedFieldsLst.ItemHeight = 18;
            this.savedFieldsLst.Location = new System.Drawing.Point(12, 84);
            this.savedFieldsLst.Name = "savedFieldsLst";
            this.savedFieldsLst.Size = new System.Drawing.Size(306, 238);
            this.savedFieldsLst.TabIndex = 9;
            this.savedFieldsLst.SelectedIndexChanged += new System.EventHandler(this.savedFieldsLst_SelectedIndexChanged);
            // 
            // saveNewBtn
            // 
            this.saveNewBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveNewBtn.Location = new System.Drawing.Point(792, 111);
            this.saveNewBtn.Name = "saveNewBtn";
            this.saveNewBtn.Size = new System.Drawing.Size(103, 30);
            this.saveNewBtn.TabIndex = 2;
            this.saveNewBtn.Text = "Save &New Field";
            this.saveNewBtn.UseVisualStyleBackColor = true;
            this.saveNewBtn.Click += new System.EventHandler(this.saveBtn_Click);
            this.saveNewBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.saveNewBtn_KeyDown);
            // 
            // savedFieldsLbl
            // 
            this.savedFieldsLbl.AutoSize = true;
            this.savedFieldsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedFieldsLbl.Location = new System.Drawing.Point(12, 62);
            this.savedFieldsLbl.Name = "savedFieldsLbl";
            this.savedFieldsLbl.Size = new System.Drawing.Size(115, 18);
            this.savedFieldsLbl.TabIndex = 12;
            this.savedFieldsLbl.Text = "Saved Where(s)";
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(671, 8);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(56, 20);
            this.subtitleLbl.TabIndex = 11;
            this.subtitleLbl.Text = "Where";
            // 
            // saveEditedBtn
            // 
            this.saveEditedBtn.Enabled = false;
            this.saveEditedBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveEditedBtn.Location = new System.Drawing.Point(675, 237);
            this.saveEditedBtn.Name = "saveEditedBtn";
            this.saveEditedBtn.Size = new System.Drawing.Size(103, 30);
            this.saveEditedBtn.TabIndex = 5;
            this.saveEditedBtn.Text = "Save &Edited Field";
            this.saveEditedBtn.UseVisualStyleBackColor = true;
            this.saveEditedBtn.Click += new System.EventHandler(this.saveEditedBtn_Click);
            this.saveEditedBtn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.saveEditedBtn_KeyDown);
            // 
            // editDescriptionLbl
            // 
            this.editDescriptionLbl.AutoSize = true;
            this.editDescriptionLbl.Enabled = false;
            this.editDescriptionLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editDescriptionLbl.Location = new System.Drawing.Point(672, 187);
            this.editDescriptionLbl.Name = "editDescriptionLbl";
            this.editDescriptionLbl.Size = new System.Drawing.Size(112, 18);
            this.editDescriptionLbl.TabIndex = 16;
            this.editDescriptionLbl.Text = "Edit Description";
            // 
            // editDescriptionTxt
            // 
            this.editDescriptionTxt.AcceptsReturn = true;
            this.editDescriptionTxt.AcceptsTab = true;
            this.editDescriptionTxt.Enabled = false;
            this.editDescriptionTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editDescriptionTxt.Location = new System.Drawing.Point(529, 207);
            this.editDescriptionTxt.MaxLength = 32;
            this.editDescriptionTxt.Name = "editDescriptionTxt";
            this.editDescriptionTxt.Size = new System.Drawing.Size(366, 24);
            this.editDescriptionTxt.TabIndex = 4;
            this.editDescriptionTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editDescriptionTxt_KeyDown);
            // 
            // editCodeLbl
            // 
            this.editCodeLbl.AutoSize = true;
            this.editCodeLbl.Enabled = false;
            this.editCodeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editCodeLbl.Location = new System.Drawing.Point(397, 187);
            this.editCodeLbl.Name = "editCodeLbl";
            this.editCodeLbl.Size = new System.Drawing.Size(73, 18);
            this.editCodeLbl.TabIndex = 15;
            this.editCodeLbl.Text = "Edit Code";
            // 
            // editCodeTxt
            // 
            this.editCodeTxt.AcceptsReturn = true;
            this.editCodeTxt.AcceptsTab = true;
            this.editCodeTxt.Enabled = false;
            this.editCodeTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editCodeTxt.Location = new System.Drawing.Point(332, 207);
            this.editCodeTxt.MaxLength = 16;
            this.editCodeTxt.Name = "editCodeTxt";
            this.editCodeTxt.Size = new System.Drawing.Size(183, 24);
            this.editCodeTxt.TabIndex = 3;
            this.editCodeTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editCodeTxt_KeyDown);
            this.editCodeTxt.Leave += new System.EventHandler(this.editCodeTxt_Leave);
            // 
            // enableDisableBtn
            // 
            this.enableDisableBtn.Enabled = false;
            this.enableDisableBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.enableDisableBtn.Location = new System.Drawing.Point(792, 237);
            this.enableDisableBtn.Name = "enableDisableBtn";
            this.enableDisableBtn.Size = new System.Drawing.Size(103, 30);
            this.enableDisableBtn.TabIndex = 6;
            this.enableDisableBtn.Text = "&Disable";
            this.enableDisableBtn.UseVisualStyleBackColor = true;
            this.enableDisableBtn.Click += new System.EventHandler(this.enableDisableBtn_Click);
            // 
            // keyTxt
            // 
            this.keyTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.keyTxt.Location = new System.Drawing.Point(332, 233);
            this.keyTxt.Name = "keyTxt";
            this.keyTxt.Size = new System.Drawing.Size(64, 24);
            this.keyTxt.TabIndex = 17;
            this.keyTxt.Visible = false;
            // 
            // activeRdo
            // 
            this.activeRdo.AutoSize = true;
            this.activeRdo.Checked = true;
            this.activeRdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.activeRdo.Location = new System.Drawing.Point(192, 60);
            this.activeRdo.Name = "activeRdo";
            this.activeRdo.Size = new System.Drawing.Size(65, 22);
            this.activeRdo.TabIndex = 18;
            this.activeRdo.TabStop = true;
            this.activeRdo.Text = "Active";
            this.activeRdo.UseVisualStyleBackColor = true;
            this.activeRdo.CheckedChanged += new System.EventHandler(this.activeRdo_CheckedChanged);
            // 
            // inactiveRdo
            // 
            this.inactiveRdo.AutoSize = true;
            this.inactiveRdo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inactiveRdo.Location = new System.Drawing.Point(255, 60);
            this.inactiveRdo.Name = "inactiveRdo";
            this.inactiveRdo.Size = new System.Drawing.Size(75, 22);
            this.inactiveRdo.TabIndex = 19;
            this.inactiveRdo.Text = "Inactive";
            this.inactiveRdo.UseVisualStyleBackColor = true;
            this.inactiveRdo.CheckedChanged += new System.EventHandler(this.inactiveRdo_CheckedChanged);
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(15, 13);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 20;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // fieldListBtn
            // 
            this.fieldListBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldListBtn.Location = new System.Drawing.Point(365, 291);
            this.fieldListBtn.Name = "fieldListBtn";
            this.fieldListBtn.Size = new System.Drawing.Size(92, 34);
            this.fieldListBtn.TabIndex = 21;
            this.fieldListBtn.Text = "Where List";
            this.fieldListBtn.UseVisualStyleBackColor = true;
            this.fieldListBtn.Click += new System.EventHandler(this.fieldListBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___100x20_pix_;
            this.pictureBox1.Location = new System.Drawing.Point(348, -12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(213, 68);
            this.pictureBox1.TabIndex = 22;
            this.pictureBox1.TabStop = false;
            // 
            // Field
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(909, 337);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.fieldListBtn);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.inactiveRdo);
            this.Controls.Add(this.activeRdo);
            this.Controls.Add(this.keyTxt);
            this.Controls.Add(this.enableDisableBtn);
            this.Controls.Add(this.saveEditedBtn);
            this.Controls.Add(this.editDescriptionLbl);
            this.Controls.Add(this.editDescriptionTxt);
            this.Controls.Add(this.editCodeLbl);
            this.Controls.Add(this.editCodeTxt);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.savedFieldsLbl);
            this.Controls.Add(this.saveNewBtn);
            this.Controls.Add(this.savedFieldsLst);
            this.Controls.Add(this.newDescriptionLbl);
            this.Controls.Add(this.newDescriptionTxt);
            this.Controls.Add(this.newCodeLbl);
            this.Controls.Add(this.newCodeTxt);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Field";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Storage Inventory - Where";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.TextBox newCodeTxt;
        private System.Windows.Forms.Label newCodeLbl;
        private System.Windows.Forms.Label newDescriptionLbl;
        private System.Windows.Forms.TextBox newDescriptionTxt;
        private System.Windows.Forms.ListBox savedFieldsLst;
        private System.Windows.Forms.Button saveNewBtn;
        private System.Windows.Forms.Label savedFieldsLbl;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Button saveEditedBtn;
        private System.Windows.Forms.Label editDescriptionLbl;
        private System.Windows.Forms.TextBox editDescriptionTxt;
        private System.Windows.Forms.Label editCodeLbl;
        private System.Windows.Forms.TextBox editCodeTxt;
        private System.Windows.Forms.Button enableDisableBtn;
        private System.Windows.Forms.TextBox keyTxt;
        private System.Windows.Forms.RadioButton activeRdo;
        private System.Windows.Forms.RadioButton inactiveRdo;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.Button fieldListBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}