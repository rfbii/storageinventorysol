using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;

namespace PayrollFieldDataDesktop
{
    public class PayDataByEmployee_Report
    {
        #region Vars
        private Document document;
        private PdfWriter PDFWriter;
        private ArrayList payReportTotalsByEmployeeArl;
        private double lineCountPerPageDbl = 55;
        private double lineCountDbl = 0;
        #endregion

        #region Constructor
        public PayDataByEmployee_Report()
        {
           
        }
        #endregion

        #region Display Code
        public void Main(string saveFolderLocationStr, string fileNameStr, string commentStr, ArrayList payReportTotalsByEmployeeArl)
        {
            try
            {
                //if temp folder doesn't already exist on client's computer, create the folder
                DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                document = new Document(PageSize.LETTER, 18, 18, 18, 18);

                // creation of the different writers
                PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                document.Open();

                #region Footer
                HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                footer.Alignment = Element.ALIGN_CENTER;
                document.Footer = footer;
                #endregion

                #region Header
                PdfPTable pdfPTable = HeaderPdfPTable(commentStr);
                #endregion

                #region Details
                PdfPCell detailCell = new PdfPCell();

                #region Detail Vars
                string employeeStr = string.Empty;
                string employeeNameStr = string.Empty;
                double payDbl = 0;
                double hoursDbl = 0;
                double payUnitDbl = 0;
                double yieldUnitDbl = 0;
                double totalTotalPayDbl = 0;
                double totalHoursDbl = 0;
                double totalPayUnitDbl = 0;
                double totalYieldUnitDbl = 0;
               #endregion

                #region Setting & Printing Detail Vars
                for (int i = 0; i < payReportTotalsByEmployeeArl.Count; i++)
                {
                    MainMenu.PayReportDataByEmployee tempPayReportData = (MainMenu.PayReportDataByEmployee)payReportTotalsByEmployeeArl[i];
                    #region Vars

                    #region reset vars
                    payDbl = 0;
                    hoursDbl = 0;
                    payUnitDbl = 0;
                    yieldUnitDbl = 0;
                    #endregion

                    employeeStr = tempPayReportData.codeStr;
                    employeeNameStr = tempPayReportData.nameStr;
                    payDbl += tempPayReportData.totalPayDbl;
                    hoursDbl = tempPayReportData.totalHoursDbl;
                    payUnitDbl = tempPayReportData.totalPayUnitsDbl;
                    yieldUnitDbl = tempPayReportData.totalYieldUnitsDbl;
                    #endregion

                    if (lineCountDbl >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable(commentStr);
                    }
                    #region Print Details
                    detailCell = new PdfPCell(new Phrase(employeeStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(employeeNameStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(payDbl.ToString("$###,###,##0.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    if (i == payReportTotalsByEmployeeArl.Count - 1)
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                    }
                    else
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    }
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(hoursDbl.ToString("##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    if (i == payReportTotalsByEmployeeArl.Count - 1)
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                    }
                    else
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    }
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(payUnitDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    if (i == payReportTotalsByEmployeeArl.Count - 1)
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                    }
                    else
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    }
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(yieldUnitDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    if (i == payReportTotalsByEmployeeArl.Count - 1)
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                    }
                    else
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    }
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    
                    //Sum up totals for grand total at bottom
                    totalTotalPayDbl += payDbl;
                    totalHoursDbl += hoursDbl;
                    totalPayUnitDbl += payUnitDbl;
                    totalYieldUnitDbl += yieldUnitDbl;
                }


                    #endregion
                #endregion
                if (lineCountDbl + 2 >= lineCountPerPageDbl)
                {
                    lineCountDbl = 0;

                    document.Add(pdfPTable);
                    document.NewPage();
                    pdfPTable = HeaderPdfPTable(commentStr);
                }

                #region Grand Total
                detailCell = new PdfPCell(new Phrase("Grand Totals:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                detailCell.BorderWidth = 0f;
                detailCell.Colspan = 2;
                detailCell.BorderWidthLeft = 0f;
                detailCell.BorderColorLeft = Color.BLACK;
                detailCell.BorderWidthBottom = 0f;
                detailCell.BorderColorBottom = Color.BLACK;
                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfPTable.AddCell(detailCell);

                //Grand Total Pay
                detailCell = new PdfPCell(new Phrase(totalTotalPayDbl.ToString("$###,###,##0.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.BLACK;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.BLACK;
                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfPTable.AddCell(detailCell);

                //Grand Total Hours
                detailCell = new PdfPCell(new Phrase(totalHoursDbl.ToString("##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.BLACK;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.BLACK;
                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfPTable.AddCell(detailCell);

                //Grand Total Pay Units
                detailCell = new PdfPCell(new Phrase(totalPayUnitDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.BLACK;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.BLACK;
                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfPTable.AddCell(detailCell);

                //Grand Total Yield Units
                detailCell = new PdfPCell(new Phrase(totalYieldUnitDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.BLACK;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.BLACK;
                detailCell.BorderWidthRight = .5f;
                detailCell.BorderColorRight = Color.BLACK;
                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                pdfPTable.AddCell(detailCell);
                #endregion

                document.Add(pdfPTable);
            }
            catch (Exception ex)
            {
                //Exception was thrown.
            }
            finally
            {
                if (document != null && document.IsOpen())
                {
                    // we close the document
                    document.Close();
                }
            }
        }

        #region Public Helpers
        private PdfPTable HeaderPdfPTable(string commentStr)
        {
            #region Header
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
            paragraph.Add("Pay Data By Employee Report");
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL); 
            paragraph.Add(commentStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            PdfPTable table = new PdfPTable(6);
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            float codeWidth = 88f; 
            float employeeWidth = 88f;
            float totalPayWidth = 88f;
            float totalHoursWidth = 88f;
            float totalPayUnitsWidth = 88f;
            float totalYieldUnitsWidth = 88f;

            float tableWidth = 0;

            tableWidth = codeWidth + employeeWidth + totalPayWidth + totalHoursWidth + totalPayUnitsWidth + totalYieldUnitsWidth;

            table.SetWidths(new float[] { codeWidth, employeeWidth, totalPayWidth, totalHoursWidth, totalPayUnitsWidth, totalYieldUnitsWidth });
            table.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table.SplitRows = true;
            table.SplitLate = false;
            table.LockedWidth = true;
            table.DefaultCell.Padding = 0;
            table.DefaultCell.BorderWidth = 1f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            Color tintColor = new Color(220, 230, 241);

            PdfPCell detailCell = new PdfPCell(new Phrase("Code", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Employee", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total Pay", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total Hours", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total Pay Units", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total Yield Units", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            lineCountDbl += 4;
            #endregion

            return table;
        }
        
        #endregion
                #endregion

        
        
        #endregion
    }
}
