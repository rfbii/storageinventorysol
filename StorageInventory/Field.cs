﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Diagnostics;

namespace StorageInventory
{
	public partial class Field : Form
	{
		#region Vars
		//Set the global variables for delimiter character.
		char delimiter = '~';

		//Set the path of the program & save files.
		private string xmlFileName = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";

		//Create the fieldsData object.
		MainMenu.FieldsData fieldsData = new MainMenu.FieldsData();

		private ArrayList ActiveArl;
		private ArrayList InactiveArl;
		#endregion

		#region Constructor
		public Field()
		{
			InitializeComponent();
			fieldsData = new MainMenu.FieldsData();
			FillSavedFieldsLst(true);

			//Make form non-resizable.
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;

			//Fill the field data from XML.
			FillFieldDataFromXML();
            CompanyNameData.Text = MainMenu.CompanyStr;
		}
		#endregion

		#region Form Functions.
		#region KeyDown Functions (For Enter-As-Tab Functionality)
		private void newCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void newDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveNewBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveBtn_Click(null, null);
		}
		private void editCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveEditedBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveEditedBtn_Click(null, null);
		}
		#endregion

		#region Input Validation Functions (Checking for proper data type inputs).

		private void newCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(newCodeTxt, delimiter))
			{
				//Check for a unique field code.
				foreach (MainMenu.FieldData tempFieldData in fieldsData.FieldsDataArl)
				{
                    if (tempFieldData.CodeStr == this.newCodeTxt.Text.Trim())
                    {
                        MessageBox.Show("This field code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //Focus on the line the user needs to change.
                        this.newCodeTxt.Focus();
                        this.newCodeTxt.Text = string.Empty;

                        break;
                    }
				}
			}

		}

		private void editCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(editCodeTxt, delimiter))
			{
				//Check for a unique field code.
				for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
				{
					MainMenu.FieldData tempFieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];


                    if (tempFieldData.CodeStr == this.editCodeTxt.Text.Trim() && tempFieldData.KeyInt.ToString() != this.keyTxt.Text)
                    {
                        MessageBox.Show("This field code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //Focus on the line the user needs to change.
                        this.editCodeTxt.Focus();
                        this.editCodeTxt.SelectAll();

                        break;
                    }
                   
				}
			}
		}
		#endregion

		#region List Box Functions
		private void savedFieldsLst_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (savedFieldsLst.SelectedIndex != -1)
			{
				//Clear out the new entry spaces.
				this.newCodeTxt.Text = string.Empty;
				this.newDescriptionTxt.Text = string.Empty;

				if (activeRdo.Checked)
				{
					MainMenu.FieldData fieldData = (MainMenu.FieldData)ActiveArl[savedFieldsLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = fieldData.CodeStr;
					editDescriptionTxt.Text = fieldData.DescriptionStr;
					keyTxt.Text = fieldData.KeyInt.ToString();
				}
				else
				{
					MainMenu.FieldData fieldData = (MainMenu.FieldData)InactiveArl[savedFieldsLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = fieldData.CodeStr;
					editDescriptionTxt.Text = fieldData.DescriptionStr;
					keyTxt.Text = fieldData.KeyInt.ToString();
				}

				//Extrapolate the selected code & description.
				//string selectedEntry = savedFieldsLst.SelectedItem.ToString();
				//string[] entryPieces = selectedEntry.Split(delimiter);
				//string selectedCode = entryPieces[0].Trim();
				//string selectedDescription = entryPieces[1].Trim();

				//Fill the Edit entry boxes.
				//editCodeTxt.Text = selectedCode;
				//editDescriptionTxt.Text = selectedDescription;

				//Enable the entry boxes & button.
				editCodeTxt.Enabled = true;
				editCodeLbl.Enabled = true;
				editDescriptionTxt.Enabled = true;
				editDescriptionLbl.Enabled = true;
				saveEditedBtn.Enabled = true;
				enableDisableBtn.Enabled = true;
			}
			else
			{
				//Clear the Edit entry boxes.
				editCodeTxt.Text = string.Empty;
				editDescriptionTxt.Text = string.Empty;
				keyTxt.Text = string.Empty;

				//Disable the entry boxes & button.
				editCodeTxt.Enabled = false;
				editCodeLbl.Enabled = false;
				editDescriptionTxt.Enabled = false;
				editDescriptionLbl.Enabled = false;
				saveEditedBtn.Enabled = false;
				enableDisableBtn.Enabled = false;
			}
		}
		private void activeRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (activeRdo.Checked)
			{
				enableDisableBtn.Text = "&Disable";

				FillSavedFieldsLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "En&able";

				FillSavedFieldsLst(activeRdo.Checked);
			}
		}
		private void inactiveRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (inactiveRdo.Checked)
			{
				enableDisableBtn.Text = "En&able";

				FillSavedFieldsLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "&Disable";

				FillSavedFieldsLst(activeRdo.Checked);
			}
		}
		#endregion

		private void exitBtn_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		#endregion

		#region New Field Functions.
		private void saveBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//  character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.newCodeTxt, delimiter))
			{
				if (!InputStringContainsChar(this.newDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
					FillFieldDataFromXML();

					//Add the new Field Data into the list.
					MainMenu.FieldData fieldData = MainMenu.FindOrAddFieldData(fieldsData, this.newCodeTxt.Text.Trim(), this.newDescriptionTxt.Text.Trim(), fieldsData.NextKeyInt);
					fieldData.ActiveBln = activeRdo.Checked;

					//Save the new data to XML.
					SaveFieldDataToXML();

					//Refill the GUI list.
					FillSavedFieldsLst(activeRdo.Checked);

					//Clear the freshly added new code & description.
					newCodeTxt.Clear();
					newDescriptionTxt.Clear();

					//Clear the edit entry spaces.
					savedFieldsLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void newCodeTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedFieldsLst.SelectedIndex = -1;
		}
		private void newDescriptionTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedFieldsLst.SelectedIndex = -1;
		}
		#endregion

		#region Edit Field Functions.
		private void saveEditedBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//  character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.editCodeTxt, delimiter) && this.keyTxt.Text != "")
			{
				if (!InputStringContainsChar(this.editDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
					FillFieldDataFromXML();

					//Save the edited field.
					MainMenu.SaveEditedFieldData(fieldsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), activeRdo.Checked);

					//Save the new data to XML.
					SaveFieldDataToXML();

					//Refill the GUI list.
					FillSavedFieldsLst(activeRdo.Checked);

					//Clear the edit entry spaces.
					savedFieldsLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void enableDisableBtn_Click(object sender, EventArgs e)
		{
			if (this.keyTxt.Text != "")
			{
				//Fill the list from XML.
				FillFieldDataFromXML();

				//Enable/Disable out the selected field.
				 if (activeRdo.Checked)
                {
                    MainMenu.SaveEditedFieldData(fieldsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), false);
                }
                else
                {
                    MainMenu.SaveEditedFieldData(fieldsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), true);
                }

				SaveFieldDataToXML();

				//Refill the GUI list.
				FillSavedFieldsLst(activeRdo.Checked);

				//Clear the edit entry spaces.
				savedFieldsLst.SelectedIndex = -1;

				//Put focus back on 'NewCode' entry box.
				newCodeTxt.Focus();
			}
		}
		#endregion

		#region Helper Functions.
		private void FillSavedFieldsLst(bool activeBln)
		{
			//Clear out the Saved Fields List.
			savedFieldsLst.ClearSelected();
			savedFieldsLst.Items.Clear();
			savedFieldsLst.SelectedIndex = -1;

			ActiveArl = new ArrayList();
			InactiveArl = new ArrayList();

			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				MainMenu.FieldsData fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
                fieldsData.FieldsDataArl.Sort();
                for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
                {
                    MainMenu.FieldData fieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

                    if (fieldData.ActiveBln == activeBln)
                    {
                        savedFieldsLst.Items.Add(fieldData.CodeStr + " ~ " + fieldData.DescriptionStr);
                    }

                    if (fieldData.ActiveBln)
                    {
                        ActiveArl.Add(fieldData);
                    }
                    else
                    {
                        InactiveArl.Add(fieldData);
                    }
                }
			}
		}
		private void FillFieldDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void SaveFieldDataToXML()
		{
			//Serialize (convert an object instance to an XML document):
			XmlSerializer xmlSerializer = new XmlSerializer(fieldsData.GetType());
			// Create an XmlTextWriter using a FileStream.
			Stream fileStream2 = new FileStream(xmlFileName, FileMode.Create);
			XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
			// Serialize using the XmlTextWriter.
			xmlSerializer.Serialize(xmlWriter, fieldsData);
			xmlWriter.Flush();
			xmlWriter.Close();
		}
		private bool InputStringContainsChar(TextBox textBoxToCheck, char charToCheckFor)
		{
			if (textBoxToCheck.Text.Contains(charToCheckFor))
			{
				MessageBox.Show("Input cannot contain '" + charToCheckFor + "' characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				textBoxToCheck.Focus();
				textBoxToCheck.SelectAll();

				return true;
			}
			else
				return false;
		}
		#endregion

        private void fieldListBtn_Click(object sender, EventArgs e)
        {
            #region Make PDF
            string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Where Data\";
            string fileNameStr = "Where_Data.pdf";
            FieldReport fieldReport = new FieldReport();
            fieldReport.Main(saveFolderStr, fileNameStr, fieldsData);
            #endregion
            #region Open PDF
            Process.Start(saveFolderStr + fileNameStr);
            #endregion
        }
	}
}
