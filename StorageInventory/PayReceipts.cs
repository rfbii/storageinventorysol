﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace PayrollFieldDataDesktop
{
    public partial class PayReceipts : Form
    {
        #region Vars
        private MainMenu.EmployeesData employeesData;
        private Document document;
		private PdfWriter PDFWriter;
        private ArrayList payReportEmployeesArl;
        private ArrayList payReportTotalsArl;
        private ArrayList payReportNameTotalsArl;
        //private ArrayList EmployeesArl;
        private string xmlFileName_Employees;
        private double lineCountPerPageDbl = 55;
        private double lineCountDbl = 0;
		#endregion

		#region Constructor
		public PayReceipts()
		{
            employeesData = new MainMenu.EmployeesData();
            xmlFileName_Employees = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
            FillEmployeeDataFromXML();

		}
		#endregion

        #region Display Code
        public void Main(string saveFolderLocationStr, string fileNameStr, MainMenu.EmployeesData employeesData, MainMenu.CropsData cropsData, MainMenu.JobsData jobsData, MainMenu.FieldsData fieldsData, ArrayList payDetailsDataArl, Int32 reportNumberInt)
        {
            payReportEmployeesArl = new ArrayList();
            payReportTotalsArl = new ArrayList();
            payReportNameTotalsArl = new ArrayList();
            
            string payThroughDateStr = "";

            for (int i = 0; i < payDetailsDataArl.Count; i++)
            {
                MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsDataArl[i];
                if (payDetail.SummaryNumberInt == reportNumberInt)
                {
                    ValueIsInArrayList(payReportEmployeesArl, payDetail.EmployeeKeyInt.ToString());
                    payThroughDateStr = payDetail.ReportDateDtm.ToString();
                }
            }


            //Fill payReportEmployeesArl
            for (int i = 0; i < payReportEmployeesArl.Count; i++)
            {
                string tempEmployee = payReportEmployeesArl[i].ToString();

                for (int j = 0; j < payDetailsDataArl.Count; j++)
                {
                    MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsDataArl[j];

                    if (payDetail.EmployeeKeyInt.ToString() == tempEmployee && payDetail.SummaryNumberInt == reportNumberInt)
                    {
                        //get employee info
                        string selectedEmployeeCodeStr = FindCode("employee", tempEmployee);
                        string selectedEmployeeNameStr = FindDescription("employee", tempEmployee);
                        TotalPayByEmployee tempTotalPayByEmployee = FindOrAddTotalPayByEmployee(payReportTotalsArl, selectedEmployeeCodeStr, selectedEmployeeNameStr);
                        tempTotalPayByEmployee.totalPayDbl += (payDetail.PayUnitDbl * payDetail.PriceDbl);
                    }
                }
            }

            //Fill payReportEmployeesNameArl
            for (int i = 0; i < payReportEmployeesArl.Count; i++)
            {
                string tempEmployee = payReportEmployeesArl[i].ToString();

                for (int j = 0; j < payDetailsDataArl.Count; j++)
                {
                    MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsDataArl[j];

                    if (payDetail.EmployeeKeyInt.ToString() == tempEmployee && payDetail.SummaryNumberInt == reportNumberInt)
                    {
                        //get employee info
                        string selectedEmployeeNameStr = FindDescription("employee", tempEmployee);
                        TotalPayByEmployeeName tempTotalPayByEmployeeName = FindOrAddTotalPayByEmployeeName(payReportNameTotalsArl, selectedEmployeeNameStr);
                        tempTotalPayByEmployeeName.totalPayDbl += (payDetail.PayUnitDbl * payDetail.PriceDbl);
                        tempTotalPayByEmployeeName.employeeKey = Convert.ToInt32(tempEmployee);
                    }
                }
            }

            //Loop through payReportEmployeesArl and make a ARL for each employee -- Name, Code, TotalPay for this report. - CBO for these 3 items.
            //Insert this page before Pay Receipts with Total pay, Employee, Code WITH GRAND TOTAL at bottom of page.  BREAK Page before starting pay receipts on fresh page.
            try
            {
                //if temp folder doesn't already exist on client's computer, create the folder
                DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                document = new Document(PageSize.LETTER, 18, 18, 18, 18);

                // creation of the different writers
                PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                document.Open();
                PdfPCell detailCell = new PdfPCell();
                #region Cover Page
                
                #region Footer
                HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                footer.Alignment = Element.ALIGN_CENTER;
                document.Footer = footer;
                #endregion

            PdfPTable pdfPTable = HeaderCoverPdfPTable(payThroughDateStr);
            double grandTotalDollarsDbl = 0.0f;

            for (int i = 0; i < payReportTotalsArl.Count; i++)
            {
                TotalPayByEmployee tempTotalPayByEmployee = (TotalPayByEmployee)payReportTotalsArl[i];

                detailCell = new PdfPCell(new Phrase(tempTotalPayByEmployee.codeStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                pdfPTable.AddCell(detailCell);

                detailCell = new PdfPCell(new Phrase(tempTotalPayByEmployee.nameStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                pdfPTable.AddCell(detailCell);

                detailCell = new PdfPCell(new Phrase(tempTotalPayByEmployee.totalPayDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                detailCell.BorderWidthRight = .5f;
                detailCell.BorderColorRight = Color.LIGHT_GRAY;
                pdfPTable.AddCell(detailCell);

                grandTotalDollarsDbl += tempTotalPayByEmployee.totalPayDbl;
                lineCountDbl += 1;
               
                if (lineCountDbl + 2 >= lineCountPerPageDbl)
                {
                    lineCountDbl = 0;

                    document.Add(pdfPTable);
                    document.NewPage();
                    pdfPTable = HeaderCoverPdfPTable(payThroughDateStr);
                }
            }

            detailCell = new PdfPCell(new Phrase("Grand Total:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.Colspan = 2;
            detailCell.BorderWidth = 0f;
            detailCell.BorderWidthLeft = 0.5f;
            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
            detailCell.BorderWidthBottom = 0.5f;
            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
            pdfPTable.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase(grandTotalDollarsDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderWidth = 0f;
            detailCell.BorderWidthLeft = 0.5f;
            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
            detailCell.BorderWidthBottom = 0.5f;
            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
            detailCell.BorderWidthRight = 0.5f;
            detailCell.BorderColorRight = Color.LIGHT_GRAY;
            pdfPTable.AddCell(detailCell);

            document.ResetPageCount();
            document.ResetFooter();
            document.Add(pdfPTable);
            document.NewPage();
                #endregion

                #region Cover Page - By Employee Name

            #region Footer
            footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
            footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
            footer.Alignment = Element.ALIGN_CENTER;
            document.Footer = footer;
            #endregion

            pdfPTable = HeaderCoverNamePdfPTable(payThroughDateStr);
            grandTotalDollarsDbl = 0.0f;
            lineCountDbl = 0;
            for (int i = 0; i < payReportNameTotalsArl.Count; i++)
            {
                TotalPayByEmployeeName tempTotalPayByEmployeeName = (TotalPayByEmployeeName)payReportNameTotalsArl[i];

                detailCell = new PdfPCell(new Phrase(tempTotalPayByEmployeeName.nameStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                pdfPTable.AddCell(detailCell);

                detailCell = new PdfPCell(new Phrase(tempTotalPayByEmployeeName.totalPayDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                detailCell.BorderWidth = 0f;
                detailCell.BorderWidthLeft = .5f;
                detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                detailCell.BorderWidthBottom = .5f;
                detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                detailCell.BorderWidthRight = .5f;
                detailCell.BorderColorRight = Color.LIGHT_GRAY;
                pdfPTable.AddCell(detailCell);

                grandTotalDollarsDbl += tempTotalPayByEmployeeName.totalPayDbl;
                lineCountDbl += 1;

                if (lineCountDbl + 2 >= lineCountPerPageDbl)
                {
                    lineCountDbl = 0;

                    document.Add(pdfPTable);
                    document.NewPage();
                    pdfPTable = HeaderCoverNamePdfPTable(payThroughDateStr);
                }
            }

            detailCell = new PdfPCell(new Phrase("Grand Total:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderWidth = 0f;
            detailCell.BorderWidthLeft = 0.5f;
            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
            detailCell.BorderWidthBottom = 0.5f;
            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
            pdfPTable.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase(grandTotalDollarsDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderWidth = 0f;
            detailCell.BorderWidthLeft = 0.5f;
            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
            detailCell.BorderWidthBottom = 0.5f;
            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
            detailCell.BorderWidthRight = 0.5f;
            detailCell.BorderColorRight = Color.LIGHT_GRAY;
            pdfPTable.AddCell(detailCell);

            document.ResetPageCount();
            document.ResetFooter();
            document.Add(pdfPTable);
            document.NewPage();
            #endregion

                #region Main Report Details
            for (int h = 0; h < payReportNameTotalsArl.Count; h++)
                {
                    TotalPayByEmployeeName tempEmployee = (TotalPayByEmployeeName)payReportNameTotalsArl[h];
                    #region Footer
                    footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
                    footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                    footer.Alignment = Element.ALIGN_CENTER;
                    document.Footer = footer;
                    #endregion

                    #region Header
                    document.NewPage();
                    pdfPTable = HeaderPdfPTable(employeesData, tempEmployee.employeeKey, payThroughDateStr);
                    #endregion

                    #region Detail Vars
                    double totalHoursDbl = 0;
                    double totalPayUnitDbl = 0;
                    double totalYieldUnitDbl = 0;
                    double totalPayDbl = 0;
                    double totalMinPayDbl = 0;
                    double totalOverPayDbl = 0;
                    double overRegularTimeDbl = 0;
                    lineCountDbl = 0;
                    #endregion

                    #region Setting & Printing Detail Vars
                    for (int i = 0; i < payDetailsDataArl.Count; i++)
                    {
                        MainMenu.PayDetailData payDetailData = (MainMenu.PayDetailData)payDetailsDataArl[i];


                        if (payDetailData.EmployeeKeyInt == tempEmployee.employeeKey && payDetailData.SummaryNumberInt == reportNumberInt)
                        {
                            string dateStr = "";

                            if (payDetailData.DateDtm != DateTime.MinValue)
                            {
                                dateStr = payDetailData.DateDtm.ToShortDateString();
                            }

                            string typeStr = payDetailData.TypeStr.ToString();

                            string employeeStr = "";

                            for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
                            {
                                MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

                                if (employeeData.KeyInt == payDetailData.EmployeeKeyInt)
                                {
                                    employeeStr = employeeData.CodeStr;

                                    break;
                                }
                            }

                            string hoursStr = payDetailData.HoursDbl.ToString("##0.##");

                            string cropStr = "";

                            for (int j = 0; j < cropsData.CropsDataArl.Count; j++)
                            {
                                MainMenu.CropData cropData = (MainMenu.CropData)cropsData.CropsDataArl[j];

                                if (cropData.KeyInt == payDetailData.CropKeyInt)
                                {
                                    cropStr = cropData.CodeStr;

                                    break;
                                }
                            }

                            string jobStr = "";

                            for (int j = 0; j < jobsData.JobsDataArl.Count; j++)
                            {
                                MainMenu.JobData jobData = (MainMenu.JobData)jobsData.JobsDataArl[j];

                                if (jobData.KeyInt == payDetailData.JobKeyInt)
                                {
                                    jobStr = jobData.CodeStr;

                                    break;
                                }
                            }

                            string fieldStr = "";

                            for (int j = 0; j < fieldsData.FieldsDataArl.Count; j++)
                            {
                                MainMenu.FieldData fieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[j];

                                if (fieldData.KeyInt == payDetailData.FieldKeyInt)
                                {
                                    fieldStr = fieldData.CodeStr;

                                    break;
                                }
                            }

                            string payUnitStr = payDetailData.PayUnitDbl.ToString("###,###,##0.##");
                            string yieldUnitStr = payDetailData.YieldUnitDbl.ToString("###,###,##0.##");
                            string priceStr = payDetailData.PriceDbl.ToString("$###,###,##0.00");
                            string totalDollarsStr = payDetailData.TotalDollarsStr;

                            totalHoursDbl += payDetailData.HoursDbl;
                            totalPayUnitDbl += payDetailData.PayUnitDbl;
                            totalYieldUnitDbl += payDetailData.YieldUnitDbl;
                            totalPayDbl += (payDetailData.PriceDbl * payDetailData.PayUnitDbl);
                            if (payDetailData.TypeStr == "M")
                            {
                                totalMinPayDbl += payDetailData.PriceDbl;
                            }
                            if (payDetailData.TypeStr == "O")
                            {
                                totalOverPayDbl += payDetailData.PriceDbl;
                            }
                            if (totalOverPayDbl > 0)
                            {
                                overRegularTimeDbl = totalOverPayDbl * 3;
                            }

                            if (lineCountDbl >= lineCountPerPageDbl)
                            {
                                lineCountDbl = 0;

                                document.Add(pdfPTable);
                                document.NewPage();
                                pdfPTable = HeaderPdfPTable(employeesData, tempEmployee.employeeKey, payThroughDateStr);

                            }

                            #region Print Details
                            detailCell = new PdfPCell(new Phrase(dateStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(typeStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(hoursStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(yieldUnitStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(jobStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(fieldStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(payUnitStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(priceStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(totalDollarsStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                            detailCell.BorderWidth = 0f;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                            detailCell.BorderWidthRight = .5f;
                            detailCell.BorderColorRight = Color.LIGHT_GRAY;
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                            lineCountDbl += 1;
                            #endregion
                        }
                    }
                    #endregion

                    #region Total Dollars
                    if (lineCountDbl + 11 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable(employeesData, Convert.ToInt32(payReportEmployeesArl[h].ToString()), payThroughDateStr);
                    }
                        detailCell = new PdfPCell(new Phrase("Total Dollars:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 9;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(totalPayDbl.ToString("$###,###,##0.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 2;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthTop = .5f;
                        detailCell.BorderColorTop = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                    #endregion

                    #region Total Hours
                        detailCell = new PdfPCell(new Phrase("Total Hours:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 9;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(totalHoursDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 2;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        #endregion

                    #region Total Yield
                        detailCell = new PdfPCell(new Phrase("Total Yield Units:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 9;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(totalYieldUnitDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 2;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        #endregion

                    #region Total Pay Units
                        detailCell = new PdfPCell(new Phrase("Total Pay Units:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 9;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(totalPayUnitDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 2;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.BLACK;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = Color.BLACK;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                        detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        #endregion

                    #region Signature & Overtime
                        if (totalOverPayDbl == 0)
                        {
                            detailCell = new PdfPCell(new Phrase("Signature:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 7;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.BLACK;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                        }
                        else if (totalOverPayDbl != 0)
                        {
                            detailCell = new PdfPCell(new Phrase("Signature:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 7;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.BLACK;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            #region Total Overtime
                            detailCell = new PdfPCell(new Phrase("Total Overtime Add On:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 2;
                            detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(totalOverPayDbl.ToString("$###,###,##0.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 2;
                            detailCell.BorderWidthLeft = .5f;
                            detailCell.BorderColorLeft = Color.BLACK;
                            detailCell.BorderWidthRight = .5f;
                            detailCell.BorderColorRight = Color.BLACK;
                            detailCell.BorderWidthBottom = .5f;
                            detailCell.BorderColorBottom = Color.BLACK;
                            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                            #endregion
                        }
                        #endregion
                    
                    #region Employee Information & RFBII Company Information

                        Int32 employeeInt = tempEmployee.employeeKey;

                    string employeeNameStr = "";
                    string employeeAddress1Str = "";
                    string employeeAddress2Str = "";
                    string employeeAddress3Str = "";
                    string employeeSSNStr = "";

                    for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
                        {
                            MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

                            if (employeeData.KeyInt == employeeInt)
                            {
                                employeeNameStr = employeeData.NameStr;
                                employeeAddress1Str = employeeData.Address1Str;
                                employeeAddress2Str = employeeData.Address2Str;
                                employeeAddress3Str = employeeData.CityStr + ", " + employeeData.StateStr + "  " + employeeData.ZipStr;  
                                employeeSSNStr = employeeData.SSNStr;
                                break;
                            }
                        }

                        //Blank
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 11;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        //Employee / Company
                        detailCell = new PdfPCell(new Phrase("Employee:  ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 1;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(employeeNameStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 6;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(MainMenu.CompanyStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 4;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        //Employee Address / CompanyAddress
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 1;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(employeeAddress1Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 6;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(MainMenu.CompanyAddressStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 4;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        //Employee Address2 / Company Address2 both contain data
                        if (employeeAddress2Str != String.Empty && MainMenu.CompanyAddress2Str != string.Empty)
                        {
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeAddress2Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyAddress2Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Employee Address3 / Company Address3
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Employee SSN / Company EIN
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeSSNStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyEINStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Company Phone
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 7;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyPhoneStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                        }
                        else if (employeeAddress2Str == String.Empty && MainMenu.CompanyAddress2Str != String.Empty)
                        {
                            //Employee Address3 / Company Address3
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyAddress2Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Employee SSN / Company EIN
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeSSNStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Blank / Company EIN
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 7;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyEINStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Company Phone
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 7;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyPhoneStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                        }
                        else if (employeeAddress2Str != String.Empty && MainMenu.CompanyAddress2Str == String.Empty)
                        {
                            //Employee Address2 / Company Address3
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeAddress2Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Employee Address3 / Company EIN
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyEINStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Employee SSN / Company Phone
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeSSNStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyPhoneStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                        }
                        else
                        {
                            //Employee Address3 / Company Address3
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyAddress3Str, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Employee SSN / Company EIN
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 1;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(employeeSSNStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 6;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyEINStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            //Company Phone
                            detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 7;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);

                            detailCell = new PdfPCell(new Phrase(MainMenu.CompanyPhoneStr, FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD)));
                            detailCell.BorderWidth = 0f;
                            detailCell.Colspan = 4;
                            detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                            pdfPTable.AddCell(detailCell);
                        }
                
                        #endregion

                    document.ResetPageCount();
                    document.ResetFooter();
                    document.Add(pdfPTable);
                    document.NewPage();
                }
            #endregion
            }
            catch (Exception ex)
            {
                string error = ex.Message.ToString();
            }
            finally
            {
                if (document != null && document.IsOpen())
                {
                    // we close the document
                    document.Close();
                }
            }
        }


        private PdfPTable HeaderPdfPTable(MainMenu.EmployeesData employeesData, Int32 employeeInt, string dateStr)
        {
            //string employeeStr = "";

            //for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
            //{
            //    MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

            //    if (employeeData.KeyInt == employeeInt)
            //    {
            //        employeeStr = employeeData.NameStr;
            //        break;
            //    }
            //}

            #region Header
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
            paragraph.Add("Pay Receipt");
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
            paragraph.Add("Pay Through Date: " + Convert.ToDateTime(dateStr).ToShortDateString());
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            PdfPTable table = new PdfPTable(11);
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            float dateWidth = 50f;
            float codeWidth = 25f;
            float employeeWidth = 50f;
            float hoursWidth = 30f;
            float yieldUnitWidth = 45f;
            float cropWidth = 50f;
            float jobWidth = 50f;
            float fieldWidth = 50f;
            float payUnitWidth = 45f;
            float priceWidth = 55f;
            float totalWidth = 57f;

            float tableWidth = 0;

            tableWidth = dateWidth + codeWidth + employeeWidth + hoursWidth + yieldUnitWidth + cropWidth + jobWidth + fieldWidth + payUnitWidth + priceWidth + totalWidth;

            table.SetWidths(new float[] { dateWidth, codeWidth, employeeWidth, hoursWidth, yieldUnitWidth, cropWidth, jobWidth, fieldWidth, payUnitWidth, priceWidth, totalWidth });
            table.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table.SplitRows = true;
            table.SplitLate = false;
            table.LockedWidth = true;
            table.DefaultCell.Padding = 0;
            //table.WidthPercentage = 100; // percentage
            table.DefaultCell.BorderWidth = 1f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            Color tintColor = new Color(220, 230, 241);

            PdfPCell detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Code", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Employee", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Hours", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Yield Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Crop", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Job", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Field", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Pay Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Price", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            //table.HeaderRows = 4; // this is the end of the table header
            lineCountDbl += 4;
            #endregion

            return table;
        }
        private PdfPTable HeaderCoverPdfPTable(string dateStr)
        {
            #region Header
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
            paragraph.Add("Pay Receipt Summary");
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
            paragraph.Add("Pay Through Date: " + Convert.ToDateTime(dateStr).ToShortDateString());
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            PdfPTable table = new PdfPTable(3);
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            float employeeNameWidth = 180f;
            float employeeCodeWidth = 180f;
            float employeeTotalWidth = 180f;
           
            float tableWidth = 0;

            tableWidth = employeeNameWidth + employeeCodeWidth + employeeTotalWidth;

            table.SetWidths(new float[] { employeeNameWidth, employeeCodeWidth, employeeTotalWidth });
            table.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table.SplitRows = true;
            table.SplitLate = false;
            table.LockedWidth = true;
            table.DefaultCell.Padding = 0;
            //table.WidthPercentage = 100; // percentage
            table.DefaultCell.BorderWidth = 1f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            Color tintColor = new Color(220, 230, 241);

            PdfPCell detailCell = new PdfPCell(new Phrase("Code", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Employee Name", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            lineCountDbl += 4;
            #endregion

            return table;
        }
        private PdfPTable HeaderCoverNamePdfPTable(string dateStr)
        {
            #region Header
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
            paragraph.Add("Pay Receipt Summary By Name");
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
            paragraph.Add("Pay Through Date: " + Convert.ToDateTime(dateStr).ToShortDateString());
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            PdfPTable table = new PdfPTable(2);
            table.HorizontalAlignment = Element.ALIGN_CENTER;

            float employeeNameWidth = 180f;
            float employeeTotalWidth = 180f;

            float tableWidth = 0;

            tableWidth = employeeNameWidth + employeeTotalWidth;

            table.SetWidths(new float[] { employeeNameWidth, employeeTotalWidth });
            table.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table.SplitRows = true;
            table.SplitLate = false;
            table.LockedWidth = true;
            table.DefaultCell.Padding = 0;
            //table.WidthPercentage = 100; // percentage
            table.DefaultCell.BorderWidth = 1f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            Color tintColor = new Color(220, 230, 241);

            PdfPCell detailCell = new PdfPCell(new Phrase("Employee Name", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            lineCountDbl += 4;
            #endregion

            return table;
        }
        private TotalPayByEmployee FindOrAddTotalPayByEmployee(ArrayList list, string codeStr, string nameStr)
        {
            TotalPayByEmployee tempTotalPayByEmployee = new TotalPayByEmployee(codeStr, nameStr);
            int tempTotalPayByEmployeeIdx = list.BinarySearch(tempTotalPayByEmployee);

            if (tempTotalPayByEmployeeIdx < 0)
                list.Insert(Math.Abs(tempTotalPayByEmployeeIdx) - 1, tempTotalPayByEmployee);
            else
                tempTotalPayByEmployee = (TotalPayByEmployee)list[tempTotalPayByEmployeeIdx];

            return tempTotalPayByEmployee;
        }
        private TotalPayByEmployeeName FindOrAddTotalPayByEmployeeName(ArrayList list, string nmaeStr)
        {
            TotalPayByEmployeeName tempTotalPayByEmployeeName = new TotalPayByEmployeeName(nmaeStr);
            int tempTotalPayByEmployeeNameIdx = list.BinarySearch(tempTotalPayByEmployeeName);

            if (tempTotalPayByEmployeeNameIdx < 0)
                list.Insert(Math.Abs(tempTotalPayByEmployeeNameIdx) - 1, tempTotalPayByEmployeeName);
            else
                tempTotalPayByEmployeeName = (TotalPayByEmployeeName)list[tempTotalPayByEmployeeNameIdx];

            return tempTotalPayByEmployeeName;
        }
        private void ValueIsInArrayList(ArrayList list, string valueToFindStr)
        {
            int tempItemIdx = list.BinarySearch(valueToFindStr);

            if (tempItemIdx < 0)
            {
                list.Insert(Math.Abs(tempItemIdx) - 1, valueToFindStr);
            }
        }
        private void FillEmployeeDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Employees);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Employees, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private string FindDescription(string dataTypeStr, string codeIn)
        {
            if (dataTypeStr.ToLower() == "employee")
            {
                for (int i = 0; i < this.employeesData.EmployeesDataArl.Count; i++)
                {
                    MainMenu.EmployeeData employee = (MainMenu.EmployeeData)this.employeesData.EmployeesDataArl[i];

                    if (employee.KeyInt.ToString() == codeIn)
                    {
                        return employee.NameStr;
                    }
                }
            }
            
            return "";
        }
        private string FindCode(string dataTypeStr, string codeIn)
        {
            if (dataTypeStr.ToLower() == "employee")
            {
                for (int i = 0; i < this.employeesData.EmployeesDataArl.Count; i++)
                {
                    MainMenu.EmployeeData employee = (MainMenu.EmployeeData)this.employeesData.EmployeesDataArl[i];

                    if (employee.KeyInt.ToString() == codeIn)
                    {
                        return employee.CodeStr;
                    }
                }
            }

            return "";
        }
        public class TotalPayByEmployee : IComparable
        {
            #region Vars
            public string codeStr;
            public string nameStr;
            public double totalPayDbl;
            #endregion

            public TotalPayByEmployee()
            {
                codeStr = string.Empty;
                nameStr = string.Empty;
                totalPayDbl = 0.0f;
            }
            public TotalPayByEmployee(string CodeStr, string NameStr)
            {
                codeStr = CodeStr;
                nameStr = NameStr;
                totalPayDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                TotalPayByEmployee Y = (TotalPayByEmployee)obj;

                //Compare codeStr
                tempCompare = this.codeStr.CompareTo(Y.codeStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare nameStr
                tempCompare = this.nameStr.CompareTo(Y.nameStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }

        public class TotalPayByEmployeeName : IComparable
        {
            #region Vars
            public string nameStr;
            public int employeeKey;
            public double totalPayDbl;
            #endregion

            public TotalPayByEmployeeName()
            {
                nameStr = string.Empty;
                employeeKey = -1;
                totalPayDbl = 0.0f;
            }
            public TotalPayByEmployeeName(string NameStr)
            {
                nameStr = NameStr;
                employeeKey = -1;
                totalPayDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                TotalPayByEmployeeName Y = (TotalPayByEmployeeName)obj;

                //Compare nameStr
                tempCompare = this.nameStr.CompareTo(Y.nameStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        #endregion
    }
}
