using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;




namespace StorageInventory
{
	public class PayData_Report
	{
		#region Vars
		private Document document;
		private PdfWriter PDFWriter;
        private double lineCountPerPageDbl = 55;
        private double lineCountDbl = 0;
        #endregion

		#region Constructor
		public PayData_Report()
		{

		}
		#endregion

		#region Display Code
		public void Main(string saveFolderLocationStr, string fileNameStr, string commentStr, MainMenu.EmployeesData employeesData, MainMenu.CropsData cropsData, MainMenu.JobsData jobsData, MainMenu.FieldsData fieldsData, MainMenu.Grower_Blocks_Data growerblocksData, ArrayList SelectedPayDetailsDataArl)
		{
			try
			{
				//if temp folder doesn't already exist on client's computer, create the folder
				DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

				if (!directoryInfo.Exists)
				{
					directoryInfo.Create();
				}

				document = new Document(PageSize.LETTER, 18, 18, 18, 18);

				// creation of the different writers
				PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
				PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

				document.Open();

				#region Footer
				HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
				footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
				footer.Alignment = Element.ALIGN_CENTER;
				document.Footer = footer;
				#endregion

				#region Header
				PdfPTable pdfPTable = HeaderPdfPTable(commentStr);
				#endregion

				#region Details
				PdfPCell detailCell = new PdfPCell();

                #region Detail Vars
                double totalInDbl = 0;
				double totalOutDbl = 0;
                #endregion

                #region Setting & Printing Detail Vars
                for (int i = 0; i < SelectedPayDetailsDataArl.Count; i++)
				{
					MainMenu.PayDetailData payDetailData = (MainMenu.PayDetailData)SelectedPayDetailsDataArl[i];
                    #region Setting Detail Vars
                    string dateStr = "";

					if (payDetailData.DateDtm != DateTime.MinValue)
					{
						dateStr = payDetailData.DateDtm.ToShortDateString();
					}
                    
                    string customerStr = "";

                    for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
                    {
                        MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

                        if (employeeData.KeyInt == payDetailData.EmployeeKeyInt)
                        {
                            customerStr = employeeData.CodeStr.PadRight(17).Substring(0, 17);  // cut to 17
                            break;
                        }
                    }

                    string cropStr = "";

					for (int j = 0; j < cropsData.CropsDataArl.Count; j++)
					{
						MainMenu.CropData cropData = (MainMenu.CropData)cropsData.CropsDataArl[j];

						if (cropData.KeyInt == payDetailData.CropKeyInt)
						{
                            cropStr = cropData.CodeStr.PadRight(11).Substring(0, 11);  // cut to 11
                            break;
						}
					}
                    
                    string boxLabelStr = payDetailData.UserNameStr.PadRight(11).Substring(0, 11);  // cut to 11
                    
                    string whereStr = "";

					for (int j = 0; j < fieldsData.FieldsDataArl.Count; j++)
					{
						MainMenu.FieldData fieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[j];

						if (fieldData.KeyInt == payDetailData.FieldKeyInt)
						{
                            whereStr = fieldData.CodeStr.PadRight(11).Substring(0, 11);  // cut to 11
							break;
						}
					}
                    string growerBlockStr = "";

                    for (int j = 0; j < growerblocksData.GrowerBlocksDataArl.Count; j++)
                    {
                        MainMenu.GrowerBlockData growerBlockData = (MainMenu.GrowerBlockData)growerblocksData.GrowerBlocksDataArl[j];

                        if (growerBlockData.KeyInt == payDetailData.Extra1Int)
                        {
                            growerBlockStr = growerBlockData.CodeStr.PadRight(11).Substring(0, 11);  // cut to 11
                            break;
                        }
                    }

                    string referenceStr = payDetailData.Extra1Str.PadRight(23).Substring(0, 23);  // cut to 23
                    string otherStr = payDetailData.Extra2Str.PadRight(10).Substring(0, 10);  // cut to 10
                    string inStr = payDetailData.PayUnitDbl.ToString("###,###,##0.##");
                    string outStr = payDetailData.YieldUnitDbl.ToString("###,###,##0.##");

					totalInDbl += payDetailData.PayUnitDbl;
					totalOutDbl += payDetailData.YieldUnitDbl;

                    if (lineCountDbl >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable(commentStr);
                    }
                #endregion

                    #region Print Details
                    detailCell = new PdfPCell(new Phrase(dateStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
					detailCell.BorderWidthLeft = .5f;
					detailCell.BorderColorLeft = Color.LIGHT_GRAY;
					detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(customerStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(boxLabelStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(whereStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(growerBlockStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(referenceStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(otherStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.LIGHT_GRAY;
					detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(inStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    if (i == SelectedPayDetailsDataArl.Count - 1)
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                    }
                    else
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    }
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
					detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
					detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(outStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
					detailCell.BorderWidth = 0f;
                    if (i == SelectedPayDetailsDataArl.Count - 1)
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.BLACK;
                    }
                    else
                    {
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                    }
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
					pdfPTable.AddCell(detailCell);

                    lineCountDbl += 1;
                #endregion
                }
                #endregion

                if (lineCountDbl + 6 >= lineCountPerPageDbl)
                {
                    lineCountDbl = 0;

                    document.Add(pdfPTable);
                    document.NewPage();
                    pdfPTable = HeaderPdfPTable(commentStr);
                }

                #region Grand Totals
                if (SelectedPayDetailsDataArl.Count != 0)
                {
                    #region Grand Total In
                    detailCell = new PdfPCell(new Phrase("Total In:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 8;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(totalInDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 2;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = Color.BLACK;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    #endregion

                    #region Grand Total Out
                    detailCell = new PdfPCell(new Phrase("Total Out:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 8;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(totalOutDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 2;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = Color.BLACK;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    #endregion

                    #region Grand Total Difference
                    detailCell = new PdfPCell(new Phrase("Difference:", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 8;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    double differenceDbl = 0;
                    differenceDbl = (totalInDbl - totalOutDbl);
                    detailCell = new PdfPCell(new Phrase(differenceDbl.ToString("###,###,##0.##"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 2;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = Color.BLACK;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = Color.BLACK;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = Color.BLACK;
                    detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    #endregion
                }
                #endregion          

                document.Add(pdfPTable);
			}
			catch (Exception ex)
			{
				//Exception was thrown.
			}
			finally
			{
				if (document != null && document.IsOpen())
				{
					// we close the document
					document.Close();
				}
			}
		}
       
       
        private PdfPTable HeaderPdfPTable(string commentStr)
		{
			#region Header
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);
            
            paragraph = new Paragraph();
			paragraph.Alignment = Element.ALIGN_CENTER;
			paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
			paragraph.Add("Storage Inventory Report");
			paragraph.SpacingAfter = 6f;
			paragraph.SpacingBefore = 0f;
			paragraph.Leading = 10f;
			document.Add(paragraph);

			paragraph = new Paragraph();
			paragraph.Alignment = Element.ALIGN_CENTER;
			paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
			paragraph.Add(commentStr);
			paragraph.SpacingAfter = 6f;
			paragraph.SpacingBefore = 0f;
			paragraph.Leading = 10f;
			document.Add(paragraph);

			PdfPTable table = new PdfPTable(10);
			table.HorizontalAlignment = Element.ALIGN_LEFT;
            
			float dateWidth = 50f;
			float customerWidth = 60f; 
			float cropWidth = 57f;
            float boxLabelWidth = 57f;
            float whereWidth = 57f;
            float growerBlockWidth = 57f;
            float referenceWidth = 115f;
            float otherWidth = 55f;
			float inWidth = 30f;
			float outWidth = 30f;

			float tableWidth = 0;

            tableWidth = dateWidth + customerWidth + cropWidth + boxLabelWidth + whereWidth + growerBlockWidth + referenceWidth + otherWidth + inWidth + outWidth;

            table.SetWidths(new float[] { dateWidth, customerWidth, cropWidth, boxLabelWidth, whereWidth, growerBlockWidth, referenceWidth, otherWidth, inWidth, outWidth });
			table.TotalWidth = tableWidth;
			table.SplitRows = true;
			table.SplitLate = false;
			table.LockedWidth = true;
			table.DefaultCell.Padding = 0;
			table.DefaultCell.BorderWidth = 1f;
			table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
			table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            Color tintColor = new Color(220, 230, 241);

			PdfPCell detailCell = new PdfPCell(new Phrase("Date", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Customer", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Crop", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Box Label", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Where", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);


            detailCell = new PdfPCell(new Phrase("Grower Block", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            detailCell.NoWrap = true;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Reference", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Other", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("In", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

			detailCell = new PdfPCell(new Phrase("Out", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
			detailCell.NoWrap = true;
			detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
			detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
			table.DefaultCell.Padding = 0;
			table.AddCell(detailCell);

            lineCountDbl += 3;
			#endregion

			return table;
        }
                #endregion
        #endregion
    }
}
