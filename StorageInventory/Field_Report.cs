﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using SoftekBarcodeMakerLib2;


namespace StorageInventory
{
    public partial class FieldReport : Form
    {
        #region Vars
        private Document document;
        private PdfWriter PDFWriter;
        private double lineCountPerPageDbl = 6; // was 55 per jr also changed counting of linecountdbl
        private double lineCountDbl = 0;
        #endregion

        #region Constructor
        public FieldReport()
        {


        }
        #endregion

        #region Display Code
        public void Main(string saveFolderLocationStr, string fileNameStr, MainMenu.FieldsData FieldsData)
        {

            //Loop through payReportEmployeesArl and make a ARL for each employee -- Name, Code, TotalPay for this report. - CBO for these 3 items.
            //Insert this page before Pay Receipts with Total pay, Employee, Code WITH GRAND TOTAL at bottom of page.  BREAK Page before starting pay receipts on fresh page.
            try
            {
                //if temp folder doesn't already exist on client's computer, create the folder
                DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                document = new Document(PageSize.LETTER, 18, 18, 18, 18);

                // creation of the different writers
                PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
                PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                document.Open();
                PdfPCell detailCell = new PdfPCell();
                #region Cover Page

                #region Footer
                HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                footer.Alignment = Element.ALIGN_CENTER;
                document.Footer = footer;
                #endregion

                PdfPTable pdfPTable = HeaderCoverPdfPTable();
                FieldsData.FieldsDataArl.Sort();
                for (int i = 0; i < FieldsData.FieldsDataArl.Count; i++)
                {
                    MainMenu.FieldData tempField = (MainMenu.FieldData)FieldsData.FieldsDataArl[i];
                    if (tempField.ActiveBln)
                    {
                        #region Make Barcode
                        System.Drawing.Bitmap bitmap;
                        //int width = 146;
                        //int height = 45;

                        //bitmap = new Bitmap(width, height);

                        BarcodeMaker barcodeMaker = new BarcodeMaker();
                        //barcodeMaker.barcode = gtinNumberStr; //barcode: string to encode
                        //barcodeMaker.message = gtinNumberStr;
                        barcodeMaker.barcode = tempField.CodeStr; //barcode: string to encode
                        barcodeMaker.message = tempField.CodeStr;

                        //Note:
                        //Code 25 barcodes must be numeric and contain an even number of characters.
                        //Code 39 barcodes must start and end with a * character.
                        //Code 128 barcodes in symbol set c must be numeric and contain an even number of characters.
                        barcodeMaker.Type = 2;//Type: 0 = Code 39, 1 = Code 25, 2 = Code 128, Anything else = Code 39

                        barcodeMaker.ModuleSize = 0;
                        //barcodeMaker.Height = 100;//that's the default height //Height: height of bitmap
                        //barcodeMaker.Width = 400;//that's the default width //Width: width of bitmap
                        //barcodeMaker.Height = 50;
                        //barcodeMaker.Width = 200;

                        barcodeMaker.Code128SymSet = "b"; //Code128SymSet: Code 128 symbol set, "a", "b" or "c"

                        //If you are creating Code 128 barcodes then it is recommended that you set the ModuleSize to a value such as 2, 3 (or more)
                        //and let the component calculate the width of the bitmap for you. This ensures that the relative width of bars are constant.
                        //The default value for ModuleSize is 0, which means that the component will scale the barcode size
                        //defined by the Width property.
                        // barcodeMaker.ModuleSize = 0;

                        bitmap = barcodeMaker.MakeBarcode();
                        #endregion
                        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(bitmap, System.Drawing.Imaging.ImageFormat.Bmp);
                        image.ScaleToFit(200, 50);
                        image.Alignment = iTextSharp.text.Image.ALIGN_CENTER;
                        PdfPCell cell = new PdfPCell(image);
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell.VerticalAlignment = PdfCell.ALIGN_MIDDLE;
                        cell.BorderWidth = 0f;
                        cell.MinimumHeight = 100; // set minimum cell height by jr 
                        cell.BorderWidthLeft = .5f;
                        cell.BorderColorLeft = Color.LIGHT_GRAY;
                        cell.BorderWidthBottom = .5f;
                        cell.BorderColorBottom = Color.LIGHT_GRAY;
                        cell.BorderWidthRight = .5f;
                        cell.BorderColorRight = Color.LIGHT_GRAY;
                        pdfPTable.AddCell(cell);


                        //detailCell = new PdfPCell(new Phrase(barcodeMaker.MakeBarcode(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        //detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        //detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        //detailCell.BorderWidth = 0f;
                        //detailCell.BorderWidthLeft = .5f;
                        //detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                        //detailCell.BorderWidthBottom = .5f;
                        //detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                        //pdfPTable.AddCell(detailCell);
                        detailCell = new PdfPCell(new Phrase("I", FontFactory.GetFont("Arial", 60, iTextSharp.text.Color.WHITE)));
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempField.CodeStr, FontFactory.GetFont("Arial", 20, iTextSharp.text.Font.NORMAL)));
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .0f;
                        detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempField.DescriptionStr, FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.NORMAL)));
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = Color.LIGHT_GRAY;
                        pdfPTable.AddCell(detailCell);

                        lineCountDbl += 1;

                        if (lineCountDbl >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderCoverPdfPTable();
                        }
                    }
                }


                document.Add(pdfPTable);

                #endregion




            }
            catch (Exception ex)
            {
                string error = ex.Message.ToString();
            }
            finally
            {
                if (document != null && document.IsOpen())
                {
                    // we close the document
                    document.Close();
                }
            }
        }


        private PdfPTable HeaderCoverPdfPTable()
        {
            #region Header
            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
            paragraph.Add("Where List Report");
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_RIGHT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL);
            paragraph.Add(System.DateTime.Now.ToString());
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
            paragraph.Add("");
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            PdfPTable table = new PdfPTable(4);
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            float FieldBarCodeWidth = 250f;
            float spaceWidth = 5f;
            float FieldCodeWidth = 145f;
            float FieldDescrWidth = 145f;

            float tableWidth = 0;

            tableWidth = FieldBarCodeWidth + spaceWidth + FieldCodeWidth + FieldDescrWidth;

            table.SetWidths(new float[] { FieldBarCodeWidth, spaceWidth, FieldCodeWidth, FieldDescrWidth });
            table.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table.SplitRows = true;
            table.SplitLate = false;
            table.LockedWidth = true;
            table.DefaultCell.Padding = 0;
            //table.WidthPercentage = 100; // percentage
            table.DefaultCell.BorderWidth = 1f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            Color tintColor = new Color(220, 230, 241);

            PdfPCell detailCell = new PdfPCell(new Phrase("Bar Code", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Where Code", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            detailCell.Colspan = 2;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Description", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = true;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            lineCountDbl += 0;
            #endregion

            return table;
        }
        private void ValueIsInArrayList(ArrayList list, string valueToFindStr)
        {
            int tempItemIdx = list.BinarySearch(valueToFindStr);

            if (tempItemIdx < 0)
            {
                list.Insert(Math.Abs(tempItemIdx) - 1, valueToFindStr);
            }
        }



        #endregion
    }
}
