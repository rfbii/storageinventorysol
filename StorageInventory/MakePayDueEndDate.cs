﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PayrollFieldDataDesktop
{
    public partial class MakePayDueEndDate : Form
    {
        public MakePayDueEndDate()
        {
            InitializeComponent();
            PayThruDate.Text = System.DateTime.Today.ToShortDateString();
            CompanyNameData.Text = MainMenu.CompanyStr;
        }

        private void labelSelectEndDate_Click(object sender, EventArgs e)
        {

        } 

       

        private void PayThruDate_ValueChanged(object sender, EventArgs e)
        {
           
        }

        private void makePayDataReportBtn_Click(object sender, EventArgs e)
        {
            MakePayDue_Criteria makePayDueCriteria = new MakePayDue_Criteria(Convert.ToDateTime(PayThruDate.Text.ToString()));
            this.Hide();
            //makePayDueCriteria.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();
            this.Close();
        }
    }
}
