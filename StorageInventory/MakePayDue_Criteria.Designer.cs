﻿namespace PayrollFieldDataDesktop
{
	partial class MakePayDue_Criteria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MakePayDue_Criteria));
            this.exitBtn = new System.Windows.Forms.Button();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.titleLbl = new System.Windows.Forms.Label();
            this.savedPayDetailsLst = new System.Windows.Forms.ListBox();
            this.saveEditedBtn = new System.Windows.Forms.Button();
            this.deleteEditedBtn = new System.Windows.Forms.Button();
            this.savedPayDetailsHeaderLbl = new System.Windows.Forms.Label();
            this.keyLst = new System.Windows.Forms.ListBox();
            this.editYieldUnitTxt = new System.Windows.Forms.TextBox();
            this.editYieldUnitLbl = new System.Windows.Forms.Label();
            this.editEmployeeCbx = new System.Windows.Forms.ComboBox();
            this.editEmployeeLbl = new System.Windows.Forms.Label();
            this.editEndTimePicker = new System.Windows.Forms.DateTimePicker();
            this.editStartTimePicker = new System.Windows.Forms.DateTimePicker();
            this.editDatePicker = new System.Windows.Forms.DateTimePicker();
            this.editFieldCbx = new System.Windows.Forms.ComboBox();
            this.editJobCbx = new System.Windows.Forms.ComboBox();
            this.editCropCbx = new System.Windows.Forms.ComboBox();
            this.editPriceTxt = new System.Windows.Forms.TextBox();
            this.editPriceLbl = new System.Windows.Forms.Label();
            this.editPayUnitTxt = new System.Windows.Forms.TextBox();
            this.editPayUnitLbl = new System.Windows.Forms.Label();
            this.editFieldLbl = new System.Windows.Forms.Label();
            this.editJobLbl = new System.Windows.Forms.Label();
            this.editCropLbl = new System.Windows.Forms.Label();
            this.editHoursTxt = new System.Windows.Forms.TextBox();
            this.editHoursLbl = new System.Windows.Forms.Label();
            this.editEndTimeLbl = new System.Windows.Forms.Label();
            this.editStartTimeLbl = new System.Windows.Forms.Label();
            this.editDateLbl = new System.Windows.Forms.Label();
            this.labelTotalHours = new System.Windows.Forms.Label();
            this.labelTotalPayUnits = new System.Windows.Forms.Label();
            this.labelTotalDollars = new System.Windows.Forms.Label();
            this.labelDollarsPerHour = new System.Windows.Forms.Label();
            this.buttonPreviousEmployee = new System.Windows.Forms.Button();
            this.buttonNextEmployee = new System.Windows.Forms.Button();
            this.AllTotalHoursData = new System.Windows.Forms.Label();
            this.AllTotalPayUnitsData = new System.Windows.Forms.Label();
            this.AllTotalDollarsData = new System.Windows.Forms.Label();
            this.AllDollarsPerHourCalc = new System.Windows.Forms.Label();
            this.EmpTotalHoursData = new System.Windows.Forms.Label();
            this.EmpTotalPayUnitsData = new System.Windows.Forms.Label();
            this.EmpTotalDollarsData = new System.Windows.Forms.Label();
            this.EmpDollarsPerHourCalc = new System.Windows.Forms.Label();
            this.labelAllEmployees = new System.Windows.Forms.Label();
            this.labelThisEmployeeLbl = new System.Windows.Forms.Label();
            this.EmployeeNameData = new System.Windows.Forms.Label();
            this.DollarsToMakeMinCalc = new System.Windows.Forms.Label();
            this.labelDollarsToMakeMinimum = new System.Windows.Forms.Label();
            this.buttonProcessMinimumWage = new System.Windows.Forms.Button();
            this.buttonProcessOvertime = new System.Windows.Forms.Button();
            this.labelPayThroughDate = new System.Windows.Forms.Label();
            this.PayThroughDate = new System.Windows.Forms.Label();
            this.buttonProcessPayrollMakeReport = new System.Windows.Forms.Button();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.warning2Pnl = new System.Windows.Forms.Panel();
            this.warn2CancelBtn = new System.Windows.Forms.Button();
            this.warn2ProceedBtn = new System.Windows.Forms.Button();
            this.warningLbl = new System.Windows.Forms.Label();
            this.warning1Pnl = new System.Windows.Forms.Panel();
            this.warn1proceedBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.warn1CancelBtn = new System.Windows.Forms.Button();
            this.warning2Pnl.SuspendLayout();
            this.warning1Pnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(805, 518);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(92, 37);
            this.exitBtn.TabIndex = 14;
            this.exitBtn.Text = "E&xit";
            this.exitBtn.UseVisualStyleBackColor = true;
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(414, 37);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(78, 20);
            this.subtitleLbl.TabIndex = 17;
            this.subtitleLbl.Text = "Make Pay";
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLbl.Location = new System.Drawing.Point(278, 9);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(352, 24);
            this.titleLbl.TabIndex = 16;
            this.titleLbl.Text = "KeyCentral® Easy Payroll Data Collection";
            // 
            // savedPayDetailsLst
            // 
            this.savedPayDetailsLst.Font = new System.Drawing.Font("Courier New", 9F);
            this.savedPayDetailsLst.FormattingEnabled = true;
            this.savedPayDetailsLst.ItemHeight = 15;
            this.savedPayDetailsLst.Location = new System.Drawing.Point(13, 98);
            this.savedPayDetailsLst.Name = "savedPayDetailsLst";
            this.savedPayDetailsLst.Size = new System.Drawing.Size(891, 289);
            this.savedPayDetailsLst.Sorted = true;
            this.savedPayDetailsLst.TabIndex = 0;
            this.savedPayDetailsLst.SelectedIndexChanged += new System.EventHandler(this.savedPayDetailsLst_SelectedIndexChanged);
            // 
            // saveEditedBtn
            // 
            this.saveEditedBtn.Enabled = false;
            this.saveEditedBtn.Location = new System.Drawing.Point(646, 432);
            this.saveEditedBtn.Name = "saveEditedBtn";
            this.saveEditedBtn.Size = new System.Drawing.Size(125, 34);
            this.saveEditedBtn.TabIndex = 12;
            this.saveEditedBtn.Text = "Save &Edited Pay Detail";
            this.saveEditedBtn.UseVisualStyleBackColor = true;
            this.saveEditedBtn.Click += new System.EventHandler(this.saveEditedBtn_Click);
            // 
            // deleteEditedBtn
            // 
            this.deleteEditedBtn.Enabled = false;
            this.deleteEditedBtn.Location = new System.Drawing.Point(777, 432);
            this.deleteEditedBtn.Name = "deleteEditedBtn";
            this.deleteEditedBtn.Size = new System.Drawing.Size(125, 34);
            this.deleteEditedBtn.TabIndex = 13;
            this.deleteEditedBtn.Text = "&Delete Pay Detail";
            this.deleteEditedBtn.UseVisualStyleBackColor = true;
            this.deleteEditedBtn.Click += new System.EventHandler(this.deleteEditedBtn_Click);
            // 
            // savedPayDetailsHeaderLbl
            // 
            this.savedPayDetailsHeaderLbl.AutoSize = true;
            this.savedPayDetailsHeaderLbl.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedPayDetailsHeaderLbl.Location = new System.Drawing.Point(10, 62);
            this.savedPayDetailsHeaderLbl.Name = "savedPayDetailsHeaderLbl";
            this.savedPayDetailsHeaderLbl.Size = new System.Drawing.Size(791, 30);
            this.savedPayDetailsHeaderLbl.TabIndex = 18;
            this.savedPayDetailsHeaderLbl.Text = resources.GetString("savedPayDetailsHeaderLbl.Text");
            // 
            // keyLst
            // 
            this.keyLst.FormattingEnabled = true;
            this.keyLst.Location = new System.Drawing.Point(12, 36);
            this.keyLst.Name = "keyLst";
            this.keyLst.Size = new System.Drawing.Size(120, 17);
            this.keyLst.Sorted = true;
            this.keyLst.TabIndex = 15;
            this.keyLst.Visible = false;
            this.keyLst.SelectedIndexChanged += new System.EventHandler(this.keyLst_SelectedIndexChanged);
            // 
            // editYieldUnitTxt
            // 
            this.editYieldUnitTxt.Enabled = false;
            this.editYieldUnitTxt.Location = new System.Drawing.Point(423, 406);
            this.editYieldUnitTxt.Name = "editYieldUnitTxt";
            this.editYieldUnitTxt.Size = new System.Drawing.Size(77, 20);
            this.editYieldUnitTxt.TabIndex = 6;
            this.editYieldUnitTxt.Text = "0";
            this.editYieldUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // editYieldUnitLbl
            // 
            this.editYieldUnitLbl.AutoSize = true;
            this.editYieldUnitLbl.Enabled = false;
            this.editYieldUnitLbl.Location = new System.Drawing.Point(434, 390);
            this.editYieldUnitLbl.Name = "editYieldUnitLbl";
            this.editYieldUnitLbl.Size = new System.Drawing.Size(52, 13);
            this.editYieldUnitLbl.TabIndex = 24;
            this.editYieldUnitLbl.Text = "Yield Unit";
            // 
            // editEmployeeCbx
            // 
            this.editEmployeeCbx.Enabled = false;
            this.editEmployeeCbx.FormattingEnabled = true;
            this.editEmployeeCbx.Location = new System.Drawing.Point(263, 406);
            this.editEmployeeCbx.Name = "editEmployeeCbx";
            this.editEmployeeCbx.Size = new System.Drawing.Size(77, 21);
            this.editEmployeeCbx.TabIndex = 4;
            this.editEmployeeCbx.SelectedIndexChanged += new System.EventHandler(this.editEmployeeCbx_SelectedIndexChanged);
            this.editEmployeeCbx.DropDownClosed += new System.EventHandler(this.editEmployeeCbx_DropDownClosed);
            this.editEmployeeCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editEmployeeCbx_KeyDown);
            // 
            // editEmployeeLbl
            // 
            this.editEmployeeLbl.AutoSize = true;
            this.editEmployeeLbl.Enabled = false;
            this.editEmployeeLbl.Location = new System.Drawing.Point(276, 390);
            this.editEmployeeLbl.Name = "editEmployeeLbl";
            this.editEmployeeLbl.Size = new System.Drawing.Size(53, 13);
            this.editEmployeeLbl.TabIndex = 22;
            this.editEmployeeLbl.Text = "Employee";
            // 
            // editEndTimePicker
            // 
            this.editEndTimePicker.CustomFormat = "hh:mm tt";
            this.editEndTimePicker.Enabled = false;
            this.editEndTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.editEndTimePicker.Location = new System.Drawing.Point(184, 407);
            this.editEndTimePicker.Name = "editEndTimePicker";
            this.editEndTimePicker.ShowUpDown = true;
            this.editEndTimePicker.Size = new System.Drawing.Size(77, 20);
            this.editEndTimePicker.TabIndex = 3;
            this.editEndTimePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editEndTimePicker_KeyDown);
            // 
            // editStartTimePicker
            // 
            this.editStartTimePicker.CustomFormat = "hh:mm tt";
            this.editStartTimePicker.Enabled = false;
            this.editStartTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.editStartTimePicker.Location = new System.Drawing.Point(105, 407);
            this.editStartTimePicker.Name = "editStartTimePicker";
            this.editStartTimePicker.ShowUpDown = true;
            this.editStartTimePicker.Size = new System.Drawing.Size(77, 20);
            this.editStartTimePicker.TabIndex = 2;
            this.editStartTimePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editStartTimePicker_KeyDown);
            // 
            // editDatePicker
            // 
            this.editDatePicker.Enabled = false;
            this.editDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.editDatePicker.Location = new System.Drawing.Point(6, 407);
            this.editDatePicker.Name = "editDatePicker";
            this.editDatePicker.Size = new System.Drawing.Size(97, 20);
            this.editDatePicker.TabIndex = 1;
            this.editDatePicker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editDatePicker_KeyDown);
            // 
            // editFieldCbx
            // 
            this.editFieldCbx.Enabled = false;
            this.editFieldCbx.FormattingEnabled = true;
            this.editFieldCbx.Location = new System.Drawing.Point(663, 406);
            this.editFieldCbx.Name = "editFieldCbx";
            this.editFieldCbx.Size = new System.Drawing.Size(77, 21);
            this.editFieldCbx.TabIndex = 9;
            this.editFieldCbx.SelectedIndexChanged += new System.EventHandler(this.editFieldCbx_SelectedIndexChanged);
            this.editFieldCbx.DropDownClosed += new System.EventHandler(this.editFieldCbx_DropDownClosed);
            this.editFieldCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editFieldCbx_KeyDown);
            // 
            // editJobCbx
            // 
            this.editJobCbx.Enabled = false;
            this.editJobCbx.FormattingEnabled = true;
            this.editJobCbx.Location = new System.Drawing.Point(583, 406);
            this.editJobCbx.Name = "editJobCbx";
            this.editJobCbx.Size = new System.Drawing.Size(77, 21);
            this.editJobCbx.TabIndex = 8;
            this.editJobCbx.SelectedIndexChanged += new System.EventHandler(this.editJobCbx_SelectedIndexChanged);
            this.editJobCbx.DropDownClosed += new System.EventHandler(this.editJobCbx_DropDownClosed);
            this.editJobCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editJobCbx_KeyDown);
            // 
            // editCropCbx
            // 
            this.editCropCbx.Enabled = false;
            this.editCropCbx.FormattingEnabled = true;
            this.editCropCbx.Location = new System.Drawing.Point(503, 406);
            this.editCropCbx.Name = "editCropCbx";
            this.editCropCbx.Size = new System.Drawing.Size(77, 21);
            this.editCropCbx.TabIndex = 7;
            this.editCropCbx.SelectedIndexChanged += new System.EventHandler(this.editCropCbx_SelectedIndexChanged);
            this.editCropCbx.DropDownClosed += new System.EventHandler(this.editCropCbx_DropDownClosed);
            this.editCropCbx.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editCropCbx_KeyDown);
            // 
            // editPriceTxt
            // 
            this.editPriceTxt.Enabled = false;
            this.editPriceTxt.Location = new System.Drawing.Point(825, 407);
            this.editPriceTxt.Name = "editPriceTxt";
            this.editPriceTxt.Size = new System.Drawing.Size(77, 20);
            this.editPriceTxt.TabIndex = 11;
            this.editPriceTxt.Text = "0";
            this.editPriceTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editPriceTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editPriceTxt_KeyDown);
            this.editPriceTxt.Leave += new System.EventHandler(this.editPriceTxt_Leave);
            // 
            // editPriceLbl
            // 
            this.editPriceLbl.AutoSize = true;
            this.editPriceLbl.Enabled = false;
            this.editPriceLbl.Location = new System.Drawing.Point(849, 390);
            this.editPriceLbl.Name = "editPriceLbl";
            this.editPriceLbl.Size = new System.Drawing.Size(31, 13);
            this.editPriceLbl.TabIndex = 28;
            this.editPriceLbl.Text = "Price";
            // 
            // editPayUnitTxt
            // 
            this.editPayUnitTxt.Enabled = false;
            this.editPayUnitTxt.Location = new System.Drawing.Point(743, 406);
            this.editPayUnitTxt.Name = "editPayUnitTxt";
            this.editPayUnitTxt.Size = new System.Drawing.Size(77, 20);
            this.editPayUnitTxt.TabIndex = 10;
            this.editPayUnitTxt.Text = "0";
            this.editPayUnitTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editPayUnitTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editPayUnitTxt_KeyDown);
            this.editPayUnitTxt.Leave += new System.EventHandler(this.editPayUnitTxt_Leave);
            // 
            // editPayUnitLbl
            // 
            this.editPayUnitLbl.AutoSize = true;
            this.editPayUnitLbl.Enabled = false;
            this.editPayUnitLbl.Location = new System.Drawing.Point(758, 390);
            this.editPayUnitLbl.Name = "editPayUnitLbl";
            this.editPayUnitLbl.Size = new System.Drawing.Size(47, 13);
            this.editPayUnitLbl.TabIndex = 27;
            this.editPayUnitLbl.Text = "Pay Unit";
            // 
            // editFieldLbl
            // 
            this.editFieldLbl.AutoSize = true;
            this.editFieldLbl.Enabled = false;
            this.editFieldLbl.Location = new System.Drawing.Point(687, 390);
            this.editFieldLbl.Name = "editFieldLbl";
            this.editFieldLbl.Size = new System.Drawing.Size(29, 13);
            this.editFieldLbl.TabIndex = 26;
            this.editFieldLbl.Text = "Field";
            // 
            // editJobLbl
            // 
            this.editJobLbl.AutoSize = true;
            this.editJobLbl.Enabled = false;
            this.editJobLbl.Location = new System.Drawing.Point(609, 391);
            this.editJobLbl.Name = "editJobLbl";
            this.editJobLbl.Size = new System.Drawing.Size(24, 13);
            this.editJobLbl.TabIndex = 29;
            this.editJobLbl.Text = "Job";
            // 
            // editCropLbl
            // 
            this.editCropLbl.AutoSize = true;
            this.editCropLbl.Enabled = false;
            this.editCropLbl.Location = new System.Drawing.Point(527, 390);
            this.editCropLbl.Name = "editCropLbl";
            this.editCropLbl.Size = new System.Drawing.Size(29, 13);
            this.editCropLbl.TabIndex = 25;
            this.editCropLbl.Text = "Crop";
            // 
            // editHoursTxt
            // 
            this.editHoursTxt.Enabled = false;
            this.editHoursTxt.Location = new System.Drawing.Point(343, 406);
            this.editHoursTxt.Name = "editHoursTxt";
            this.editHoursTxt.Size = new System.Drawing.Size(77, 20);
            this.editHoursTxt.TabIndex = 5;
            this.editHoursTxt.Text = "0";
            this.editHoursTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.editHoursTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.editHoursTxt_KeyDown);
            this.editHoursTxt.Leave += new System.EventHandler(this.editHoursTxt_Leave);
            // 
            // editHoursLbl
            // 
            this.editHoursLbl.AutoSize = true;
            this.editHoursLbl.Enabled = false;
            this.editHoursLbl.Location = new System.Drawing.Point(364, 390);
            this.editHoursLbl.Name = "editHoursLbl";
            this.editHoursLbl.Size = new System.Drawing.Size(35, 13);
            this.editHoursLbl.TabIndex = 23;
            this.editHoursLbl.Text = "Hours";
            // 
            // editEndTimeLbl
            // 
            this.editEndTimeLbl.AutoSize = true;
            this.editEndTimeLbl.Enabled = false;
            this.editEndTimeLbl.Location = new System.Drawing.Point(196, 390);
            this.editEndTimeLbl.Name = "editEndTimeLbl";
            this.editEndTimeLbl.Size = new System.Drawing.Size(52, 13);
            this.editEndTimeLbl.TabIndex = 21;
            this.editEndTimeLbl.Text = "End Time";
            // 
            // editStartTimeLbl
            // 
            this.editStartTimeLbl.AutoSize = true;
            this.editStartTimeLbl.Enabled = false;
            this.editStartTimeLbl.Location = new System.Drawing.Point(117, 391);
            this.editStartTimeLbl.Name = "editStartTimeLbl";
            this.editStartTimeLbl.Size = new System.Drawing.Size(55, 13);
            this.editStartTimeLbl.TabIndex = 20;
            this.editStartTimeLbl.Text = "Start Time";
            // 
            // editDateLbl
            // 
            this.editDateLbl.AutoSize = true;
            this.editDateLbl.Enabled = false;
            this.editDateLbl.Location = new System.Drawing.Point(40, 391);
            this.editDateLbl.Name = "editDateLbl";
            this.editDateLbl.Size = new System.Drawing.Size(30, 13);
            this.editDateLbl.TabIndex = 19;
            this.editDateLbl.Text = "Date";
            // 
            // labelTotalHours
            // 
            this.labelTotalHours.Location = new System.Drawing.Point(130, 472);
            this.labelTotalHours.Name = "labelTotalHours";
            this.labelTotalHours.Size = new System.Drawing.Size(95, 14);
            this.labelTotalHours.TabIndex = 30;
            this.labelTotalHours.Text = "Total Hours:";
            this.labelTotalHours.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTotalPayUnits
            // 
            this.labelTotalPayUnits.Location = new System.Drawing.Point(130, 495);
            this.labelTotalPayUnits.Name = "labelTotalPayUnits";
            this.labelTotalPayUnits.Size = new System.Drawing.Size(95, 14);
            this.labelTotalPayUnits.TabIndex = 32;
            this.labelTotalPayUnits.Text = "Total Pay Units:";
            this.labelTotalPayUnits.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTotalPayUnits.Click += new System.EventHandler(this.label3_Click);
            // 
            // labelTotalDollars
            // 
            this.labelTotalDollars.Location = new System.Drawing.Point(130, 518);
            this.labelTotalDollars.Name = "labelTotalDollars";
            this.labelTotalDollars.Size = new System.Drawing.Size(95, 14);
            this.labelTotalDollars.TabIndex = 33;
            this.labelTotalDollars.Text = "Total Dollars:";
            this.labelTotalDollars.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDollarsPerHour
            // 
            this.labelDollarsPerHour.Location = new System.Drawing.Point(130, 541);
            this.labelDollarsPerHour.Name = "labelDollarsPerHour";
            this.labelDollarsPerHour.Size = new System.Drawing.Size(95, 14);
            this.labelDollarsPerHour.TabIndex = 34;
            this.labelDollarsPerHour.Text = "Dollars/Hour:";
            this.labelDollarsPerHour.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonPreviousEmployee
            // 
            this.buttonPreviousEmployee.AutoSize = true;
            this.buttonPreviousEmployee.Location = new System.Drawing.Point(397, 432);
            this.buttonPreviousEmployee.Name = "buttonPreviousEmployee";
            this.buttonPreviousEmployee.Size = new System.Drawing.Size(128, 34);
            this.buttonPreviousEmployee.TabIndex = 35;
            this.buttonPreviousEmployee.Text = "<<< Previous Employee";
            this.buttonPreviousEmployee.UseVisualStyleBackColor = true;
            this.buttonPreviousEmployee.Click += new System.EventHandler(this.buttonPreviousEmployee_Click);
            // 
            // buttonNextEmployee
            // 
            this.buttonNextEmployee.AutoSize = true;
            this.buttonNextEmployee.Location = new System.Drawing.Point(531, 432);
            this.buttonNextEmployee.Name = "buttonNextEmployee";
            this.buttonNextEmployee.Size = new System.Drawing.Size(109, 34);
            this.buttonNextEmployee.TabIndex = 36;
            this.buttonNextEmployee.Text = "Next Employee >>>";
            this.buttonNextEmployee.UseVisualStyleBackColor = true;
            this.buttonNextEmployee.Click += new System.EventHandler(this.buttonNextEmployee_Click);
            // 
            // AllTotalHoursData
            // 
            this.AllTotalHoursData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AllTotalHoursData.Location = new System.Drawing.Point(32, 472);
            this.AllTotalHoursData.Name = "AllTotalHoursData";
            this.AllTotalHoursData.Size = new System.Drawing.Size(100, 14);
            this.AllTotalHoursData.TabIndex = 37;
            this.AllTotalHoursData.Text = "All Hours";
            this.AllTotalHoursData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AllTotalPayUnitsData
            // 
            this.AllTotalPayUnitsData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AllTotalPayUnitsData.Location = new System.Drawing.Point(32, 495);
            this.AllTotalPayUnitsData.Name = "AllTotalPayUnitsData";
            this.AllTotalPayUnitsData.Size = new System.Drawing.Size(100, 14);
            this.AllTotalPayUnitsData.TabIndex = 38;
            this.AllTotalPayUnitsData.Text = "All Pay Units";
            this.AllTotalPayUnitsData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AllTotalDollarsData
            // 
            this.AllTotalDollarsData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AllTotalDollarsData.Location = new System.Drawing.Point(32, 518);
            this.AllTotalDollarsData.Name = "AllTotalDollarsData";
            this.AllTotalDollarsData.Size = new System.Drawing.Size(100, 14);
            this.AllTotalDollarsData.TabIndex = 39;
            this.AllTotalDollarsData.Text = "All Dollars";
            this.AllTotalDollarsData.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AllDollarsPerHourCalc
            // 
            this.AllDollarsPerHourCalc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.AllDollarsPerHourCalc.Location = new System.Drawing.Point(32, 541);
            this.AllDollarsPerHourCalc.Name = "AllDollarsPerHourCalc";
            this.AllDollarsPerHourCalc.Size = new System.Drawing.Size(100, 14);
            this.AllDollarsPerHourCalc.TabIndex = 40;
            this.AllDollarsPerHourCalc.Text = "All Dollars/Hour";
            this.AllDollarsPerHourCalc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // EmpTotalHoursData
            // 
            this.EmpTotalHoursData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmpTotalHoursData.Location = new System.Drawing.Point(225, 472);
            this.EmpTotalHoursData.Name = "EmpTotalHoursData";
            this.EmpTotalHoursData.Size = new System.Drawing.Size(100, 14);
            this.EmpTotalHoursData.TabIndex = 41;
            this.EmpTotalHoursData.Text = "Emp Hours";
            this.EmpTotalHoursData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EmpTotalPayUnitsData
            // 
            this.EmpTotalPayUnitsData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmpTotalPayUnitsData.Location = new System.Drawing.Point(225, 495);
            this.EmpTotalPayUnitsData.Name = "EmpTotalPayUnitsData";
            this.EmpTotalPayUnitsData.Size = new System.Drawing.Size(100, 14);
            this.EmpTotalPayUnitsData.TabIndex = 42;
            this.EmpTotalPayUnitsData.Text = "Emp Pay Units";
            this.EmpTotalPayUnitsData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EmpTotalDollarsData
            // 
            this.EmpTotalDollarsData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmpTotalDollarsData.Location = new System.Drawing.Point(225, 518);
            this.EmpTotalDollarsData.Name = "EmpTotalDollarsData";
            this.EmpTotalDollarsData.Size = new System.Drawing.Size(100, 14);
            this.EmpTotalDollarsData.TabIndex = 43;
            this.EmpTotalDollarsData.Text = "Emp Dollars";
            this.EmpTotalDollarsData.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EmpDollarsPerHourCalc
            // 
            this.EmpDollarsPerHourCalc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmpDollarsPerHourCalc.Location = new System.Drawing.Point(225, 541);
            this.EmpDollarsPerHourCalc.Name = "EmpDollarsPerHourCalc";
            this.EmpDollarsPerHourCalc.Size = new System.Drawing.Size(100, 14);
            this.EmpDollarsPerHourCalc.TabIndex = 44;
            this.EmpDollarsPerHourCalc.Text = "Emp Dollars/Hour";
            this.EmpDollarsPerHourCalc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelAllEmployees
            // 
            this.labelAllEmployees.Location = new System.Drawing.Point(32, 452);
            this.labelAllEmployees.Name = "labelAllEmployees";
            this.labelAllEmployees.Size = new System.Drawing.Size(100, 14);
            this.labelAllEmployees.TabIndex = 45;
            this.labelAllEmployees.Text = "All Employees:";
            this.labelAllEmployees.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelThisEmployeeLbl
            // 
            this.labelThisEmployeeLbl.Location = new System.Drawing.Point(225, 432);
            this.labelThisEmployeeLbl.Name = "labelThisEmployeeLbl";
            this.labelThisEmployeeLbl.Size = new System.Drawing.Size(100, 14);
            this.labelThisEmployeeLbl.TabIndex = 46;
            this.labelThisEmployeeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EmployeeNameData
            // 
            this.EmployeeNameData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.EmployeeNameData.Location = new System.Drawing.Point(172, 452);
            this.EmployeeNameData.Name = "EmployeeNameData";
            this.EmployeeNameData.Size = new System.Drawing.Size(207, 14);
            this.EmployeeNameData.TabIndex = 47;
            this.EmployeeNameData.Text = "Employee Name";
            this.EmployeeNameData.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DollarsToMakeMinCalc
            // 
            this.DollarsToMakeMinCalc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.DollarsToMakeMinCalc.Location = new System.Drawing.Point(340, 495);
            this.DollarsToMakeMinCalc.Name = "DollarsToMakeMinCalc";
            this.DollarsToMakeMinCalc.Size = new System.Drawing.Size(110, 14);
            this.DollarsToMakeMinCalc.TabIndex = 48;
            this.DollarsToMakeMinCalc.Text = "DollarsToMakeMin";
            this.DollarsToMakeMinCalc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelDollarsToMakeMinimum
            // 
            this.labelDollarsToMakeMinimum.Location = new System.Drawing.Point(340, 472);
            this.labelDollarsToMakeMinimum.Name = "labelDollarsToMakeMinimum";
            this.labelDollarsToMakeMinimum.Size = new System.Drawing.Size(110, 14);
            this.labelDollarsToMakeMinimum.TabIndex = 49;
            this.labelDollarsToMakeMinimum.Text = "Dollars to Make Min.";
            this.labelDollarsToMakeMinimum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonProcessMinimumWage
            // 
            this.buttonProcessMinimumWage.AutoSize = true;
            this.buttonProcessMinimumWage.Enabled = false;
            this.buttonProcessMinimumWage.Location = new System.Drawing.Point(456, 475);
            this.buttonProcessMinimumWage.Name = "buttonProcessMinimumWage";
            this.buttonProcessMinimumWage.Size = new System.Drawing.Size(189, 34);
            this.buttonProcessMinimumWage.TabIndex = 50;
            this.buttonProcessMinimumWage.Text = "Process Minimum Wage Add-on >>>";
            this.buttonProcessMinimumWage.UseVisualStyleBackColor = true;
            this.buttonProcessMinimumWage.Click += new System.EventHandler(this.buttonProcessMinimumWage_Click);
            // 
            // buttonProcessOvertime
            // 
            this.buttonProcessOvertime.AutoSize = true;
            this.buttonProcessOvertime.Location = new System.Drawing.Point(663, 475);
            this.buttonProcessOvertime.Name = "buttonProcessOvertime";
            this.buttonProcessOvertime.Size = new System.Drawing.Size(121, 34);
            this.buttonProcessOvertime.TabIndex = 51;
            this.buttonProcessOvertime.Text = "Process Overtime >>>";
            this.buttonProcessOvertime.UseVisualStyleBackColor = true;
            // 
            // labelPayThroughDate
            // 
            this.labelPayThroughDate.Location = new System.Drawing.Point(6, 432);
            this.labelPayThroughDate.Name = "labelPayThroughDate";
            this.labelPayThroughDate.Size = new System.Drawing.Size(100, 14);
            this.labelPayThroughDate.TabIndex = 52;
            this.labelPayThroughDate.Text = "Pay Through Date:";
            this.labelPayThroughDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PayThroughDate
            // 
            this.PayThroughDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PayThroughDate.Location = new System.Drawing.Point(105, 432);
            this.PayThroughDate.Name = "PayThroughDate";
            this.PayThroughDate.Size = new System.Drawing.Size(77, 14);
            this.PayThroughDate.TabIndex = 53;
            this.PayThroughDate.Text = "Pay thru Date";
            // 
            // buttonProcessPayrollMakeReport
            // 
            this.buttonProcessPayrollMakeReport.AutoSize = true;
            this.buttonProcessPayrollMakeReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonProcessPayrollMakeReport.Location = new System.Drawing.Point(518, 521);
            this.buttonProcessPayrollMakeReport.Name = "buttonProcessPayrollMakeReport";
            this.buttonProcessPayrollMakeReport.Size = new System.Drawing.Size(237, 34);
            this.buttonProcessPayrollMakeReport.TabIndex = 54;
            this.buttonProcessPayrollMakeReport.Text = "Process Payroll and Create Receipts >>>";
            this.buttonProcessPayrollMakeReport.UseVisualStyleBackColor = true;
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 9);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 55;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // warning2Pnl
            // 
            this.warning2Pnl.Controls.Add(this.warn2CancelBtn);
            this.warning2Pnl.Controls.Add(this.warn2ProceedBtn);
            this.warning2Pnl.Controls.Add(this.warningLbl);
            this.warning2Pnl.Location = new System.Drawing.Point(45, 27);
            this.warning2Pnl.Name = "warning2Pnl";
            this.warning2Pnl.Size = new System.Drawing.Size(402, 227);
            this.warning2Pnl.TabIndex = 600;
            this.warning2Pnl.Visible = false;
            // 
            // warn2CancelBtn
            // 
            this.warn2CancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warn2CancelBtn.Location = new System.Drawing.Point(218, 115);
            this.warn2CancelBtn.Name = "warn2CancelBtn";
            this.warn2CancelBtn.Size = new System.Drawing.Size(89, 28);
            this.warn2CancelBtn.TabIndex = 603;
            this.warn2CancelBtn.Text = "Cancel";
            this.warn2CancelBtn.UseVisualStyleBackColor = true;
            this.warn2CancelBtn.Click += new System.EventHandler(this.warn2CancelBtn_Click);
            // 
            // warn2ProceedBtn
            // 
            this.warn2ProceedBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warn2ProceedBtn.Location = new System.Drawing.Point(113, 115);
            this.warn2ProceedBtn.Name = "warn2ProceedBtn";
            this.warn2ProceedBtn.Size = new System.Drawing.Size(89, 28);
            this.warn2ProceedBtn.TabIndex = 602;
            this.warn2ProceedBtn.Text = "Proceed";
            this.warn2ProceedBtn.UseVisualStyleBackColor = true;
            this.warn2ProceedBtn.Click += new System.EventHandler(this.warn2ProceedBtn_Click);
            // 
            // warningLbl
            // 
            this.warningLbl.AutoSize = true;
            this.warningLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warningLbl.ForeColor = System.Drawing.Color.Red;
            this.warningLbl.Location = new System.Drawing.Point(127, 73);
            this.warningLbl.Name = "warningLbl";
            this.warningLbl.Size = new System.Drawing.Size(172, 29);
            this.warningLbl.TabIndex = 601;
            this.warningLbl.Text = "Are you sure?";
            // 
            // warning1Pnl
            // 
            this.warning1Pnl.Controls.Add(this.warning2Pnl);
            this.warning1Pnl.Controls.Add(this.warn1proceedBtn);
            this.warning1Pnl.Controls.Add(this.label1);
            this.warning1Pnl.Controls.Add(this.warn1CancelBtn);
            this.warning1Pnl.Location = new System.Drawing.Point(437, 321);
            this.warning1Pnl.Name = "warning1Pnl";
            this.warning1Pnl.Size = new System.Drawing.Size(476, 245);
            this.warning1Pnl.TabIndex = 500;
            this.warning1Pnl.Visible = false;
            // 
            // warn1proceedBtn
            // 
            this.warn1proceedBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warn1proceedBtn.Location = new System.Drawing.Point(341, 178);
            this.warn1proceedBtn.Name = "warn1proceedBtn";
            this.warn1proceedBtn.Size = new System.Drawing.Size(89, 28);
            this.warn1proceedBtn.TabIndex = 502;
            this.warn1proceedBtn.Text = "Proceed";
            this.warn1proceedBtn.UseVisualStyleBackColor = true;
            this.warn1proceedBtn.Click += new System.EventHandler(this.warn1proceedBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(266, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(350, 26);
            this.label1.TabIndex = 501;
            this.label1.Text = "Not all employees made minimum.";
            // 
            // warn1CancelBtn
            // 
            this.warn1CancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.warn1CancelBtn.Location = new System.Drawing.Point(446, 178);
            this.warn1CancelBtn.Name = "warn1CancelBtn";
            this.warn1CancelBtn.Size = new System.Drawing.Size(89, 28);
            this.warn1CancelBtn.TabIndex = 503;
            this.warn1CancelBtn.Text = "Cancel";
            this.warn1CancelBtn.UseVisualStyleBackColor = true;
            this.warn1CancelBtn.Click += new System.EventHandler(this.warn1CancelBtn_Click);
            // 
            // MakePayDue_Criteria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(914, 558);
            this.Controls.Add(this.savedPayDetailsLst);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.buttonProcessPayrollMakeReport);
            this.Controls.Add(this.PayThroughDate);
            this.Controls.Add(this.labelPayThroughDate);
            this.Controls.Add(this.buttonProcessOvertime);
            this.Controls.Add(this.buttonProcessMinimumWage);
            this.Controls.Add(this.labelDollarsToMakeMinimum);
            this.Controls.Add(this.DollarsToMakeMinCalc);
            this.Controls.Add(this.EmployeeNameData);
            this.Controls.Add(this.labelThisEmployeeLbl);
            this.Controls.Add(this.labelAllEmployees);
            this.Controls.Add(this.EmpDollarsPerHourCalc);
            this.Controls.Add(this.EmpTotalDollarsData);
            this.Controls.Add(this.EmpTotalPayUnitsData);
            this.Controls.Add(this.EmpTotalHoursData);
            this.Controls.Add(this.AllDollarsPerHourCalc);
            this.Controls.Add(this.AllTotalDollarsData);
            this.Controls.Add(this.AllTotalPayUnitsData);
            this.Controls.Add(this.AllTotalHoursData);
            this.Controls.Add(this.buttonNextEmployee);
            this.Controls.Add(this.buttonPreviousEmployee);
            this.Controls.Add(this.labelDollarsPerHour);
            this.Controls.Add(this.labelTotalDollars);
            this.Controls.Add(this.labelTotalPayUnits);
            this.Controls.Add(this.labelTotalHours);
            this.Controls.Add(this.editYieldUnitTxt);
            this.Controls.Add(this.editYieldUnitLbl);
            this.Controls.Add(this.editEmployeeCbx);
            this.Controls.Add(this.editEmployeeLbl);
            this.Controls.Add(this.editEndTimePicker);
            this.Controls.Add(this.editStartTimePicker);
            this.Controls.Add(this.editDatePicker);
            this.Controls.Add(this.editFieldCbx);
            this.Controls.Add(this.editJobCbx);
            this.Controls.Add(this.editCropCbx);
            this.Controls.Add(this.editPriceTxt);
            this.Controls.Add(this.editPriceLbl);
            this.Controls.Add(this.editPayUnitTxt);
            this.Controls.Add(this.editPayUnitLbl);
            this.Controls.Add(this.editFieldLbl);
            this.Controls.Add(this.editJobLbl);
            this.Controls.Add(this.editCropLbl);
            this.Controls.Add(this.editHoursTxt);
            this.Controls.Add(this.editHoursLbl);
            this.Controls.Add(this.editEndTimeLbl);
            this.Controls.Add(this.editStartTimeLbl);
            this.Controls.Add(this.editDateLbl);
            this.Controls.Add(this.keyLst);
            this.Controls.Add(this.savedPayDetailsHeaderLbl);
            this.Controls.Add(this.deleteEditedBtn);
            this.Controls.Add(this.saveEditedBtn);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.titleLbl);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.warning1Pnl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MakePayDue_Criteria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Easy Payroll Data Collection - Make Pay";
            this.warning2Pnl.ResumeLayout(false);
            this.warning2Pnl.PerformLayout();
            this.warning1Pnl.ResumeLayout(false);
            this.warning1Pnl.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label subtitleLbl;
		private System.Windows.Forms.Label titleLbl;
		private System.Windows.Forms.ListBox savedPayDetailsLst;
		private System.Windows.Forms.Button saveEditedBtn;
		private System.Windows.Forms.Button deleteEditedBtn;
        private System.Windows.Forms.Label savedPayDetailsHeaderLbl;
		private System.Windows.Forms.ListBox keyLst;
		private System.Windows.Forms.TextBox editYieldUnitTxt;
		private System.Windows.Forms.Label editYieldUnitLbl;
		private System.Windows.Forms.ComboBox editEmployeeCbx;
		private System.Windows.Forms.Label editEmployeeLbl;
		private System.Windows.Forms.DateTimePicker editEndTimePicker;
		private System.Windows.Forms.DateTimePicker editStartTimePicker;
		private System.Windows.Forms.DateTimePicker editDatePicker;
		private System.Windows.Forms.ComboBox editFieldCbx;
		private System.Windows.Forms.ComboBox editJobCbx;
		private System.Windows.Forms.ComboBox editCropCbx;
		private System.Windows.Forms.TextBox editPriceTxt;
		private System.Windows.Forms.Label editPriceLbl;
		private System.Windows.Forms.TextBox editPayUnitTxt;
		private System.Windows.Forms.Label editPayUnitLbl;
		private System.Windows.Forms.Label editFieldLbl;
		private System.Windows.Forms.Label editJobLbl;
		private System.Windows.Forms.Label editCropLbl;
		private System.Windows.Forms.TextBox editHoursTxt;
		private System.Windows.Forms.Label editHoursLbl;
		private System.Windows.Forms.Label editEndTimeLbl;
		private System.Windows.Forms.Label editStartTimeLbl;
		private System.Windows.Forms.Label editDateLbl;
        private System.Windows.Forms.Label labelTotalHours;
        private System.Windows.Forms.Label labelTotalPayUnits;
        private System.Windows.Forms.Label labelTotalDollars;
        private System.Windows.Forms.Label labelDollarsPerHour;
        private System.Windows.Forms.Button buttonPreviousEmployee;
        private System.Windows.Forms.Button buttonNextEmployee;
        private System.Windows.Forms.Label AllTotalHoursData;
        private System.Windows.Forms.Label AllTotalPayUnitsData;
        private System.Windows.Forms.Label AllTotalDollarsData;
        private System.Windows.Forms.Label AllDollarsPerHourCalc;
        private System.Windows.Forms.Label EmpTotalHoursData;
        private System.Windows.Forms.Label EmpTotalPayUnitsData;
        private System.Windows.Forms.Label EmpTotalDollarsData;
        private System.Windows.Forms.Label EmpDollarsPerHourCalc;
        private System.Windows.Forms.Label labelAllEmployees;
        private System.Windows.Forms.Label labelThisEmployeeLbl;
        private System.Windows.Forms.Label EmployeeNameData;
        private System.Windows.Forms.Label DollarsToMakeMinCalc;
        private System.Windows.Forms.Label labelDollarsToMakeMinimum;
        private System.Windows.Forms.Button buttonProcessMinimumWage;
        private System.Windows.Forms.Button buttonProcessOvertime;
        private System.Windows.Forms.Label labelPayThroughDate;
        private System.Windows.Forms.Label PayThroughDate;
        private System.Windows.Forms.Button buttonProcessPayrollMakeReport;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.Panel warning2Pnl;
        private System.Windows.Forms.Button warn2CancelBtn;
        private System.Windows.Forms.Button warn2ProceedBtn;
        private System.Windows.Forms.Label warningLbl;
        private System.Windows.Forms.Panel warning1Pnl;
        private System.Windows.Forms.Button warn1CancelBtn;
        private System.Windows.Forms.Button warn1proceedBtn;
        private System.Windows.Forms.Label label1;
    }
}