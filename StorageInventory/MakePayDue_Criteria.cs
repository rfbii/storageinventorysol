﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;

namespace PayrollFieldDataDesktop
{
	public partial class MakePayDue_Criteria : Form
	{
		#region Vars

		private MainMenu.CropsData cropsData;
		private MainMenu.EmployeesData employeesData;
		private MainMenu.JobsData jobsData;
		private MainMenu.FieldsData fieldsData;
		private MainMenu.PayDetailsData payDetailsData;
        private MainMenu.PayrollOptionsData payrollOptionsData;
        private DateTime PayThruDate;
        private ArrayList payReportEmployeesArl;
        private ArrayList payReportLinesArl;
        private Int32 currentEmployeeInt;

		private ArrayList CropsArl;
		private ArrayList EmployeesArl;
		private ArrayList FieldsArl;
		private ArrayList JobsArl;

		private string xmlFileName_Crops;
		private string xmlFileName_Employees;
		private string xmlFileName_Jobs;
		private string xmlFileName_Fields;
		private string xmlFileName_PayDetails;
        private string xmlFileName_payrollOptionsData;

		//Program option variables.
		private char delimiter;
		private string delimiterWithSpaces;
		private int tabIndexToEndInt;
		private int savedEntriesCharsPerColumn;
        private bool ProceedWarningBln = false;
		#endregion

		#region Constructor
		public MakePayDue_Criteria(DateTime paythrudate)
		{
			//Initialize the program option variables.
			delimiter = '~';
			delimiterWithSpaces = " " + delimiter.ToString() + " ";
			tabIndexToEndInt = 120;
			savedEntriesCharsPerColumn = 10;
			//Default-Initialize.
			InitializeComponent();
			//Setup our Saved Entries Header.
			//savedPayDetailsHeaderLbl.Text = MakeHeaderColumns(new List<string> { "Date", "Start Time", "End Time", "Employee", "Hours", "Yield Unit", "Crop", "Job", "Field", "Pay Unit", "Price", "Total" });

			//Make form non-resizable.
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;

			//Initialize object variables.
			cropsData = new MainMenu.CropsData();
			employeesData = new MainMenu.EmployeesData();
			cropsData = new MainMenu.CropsData();
			jobsData = new MainMenu.JobsData();
			fieldsData = new MainMenu.FieldsData();
			payDetailsData = new MainMenu.PayDetailsData();
            payrollOptionsData = new MainMenu.PayrollOptionsData();
            PayThroughDate.Text = paythrudate.ToShortDateString();
            PayThruDate = paythrudate;
            CompanyNameData.Text = MainMenu.CompanyStr;
			//Set the XML file names.
			xmlFileName_Crops = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
			xmlFileName_Employees = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
			xmlFileName_Jobs = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
			xmlFileName_Fields = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
			xmlFileName_PayDetails = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayDetailData.xml";
            xmlFileName_payrollOptionsData = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayrollOptionsData.xml";

			//Load all of our data from XML.
			FillCropDataFromXML();
			FillEmployeeDataFromXML();
			FillFieldDataFromXML();
			FillJobDataFromXML();
            FillPayrollOptionsDataFromXML();
			//FillPayDetailDataFromXML();

			//Fill the dropdown lists.
            //FillEmployeesDropdown();
            //FillCropsDropdown();
            //FillJobsDropdown();
            //FillFieldsDropdown();

			//Fill the saved pay details list.
			FillSavedPayDetails();
            buttonProcessPayrollMakeReport_Click();
		}
		#endregion

		#region Form Functions
		#region Button Click Functions
		private void exitBtn_Click()
		{
           this.Close();
		}
		#endregion

		#region KeyDown Functions (For Enter-As-Tab Functionality)
		private void editDatePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editStartTimePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editEndTimePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editEmployeeCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editHoursTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editCropCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editJobCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editFieldCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editPayUnitTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editYieldUnitTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editPriceTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		
		#endregion

		#region DropDown Functions
		private void editEmployeeCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (editEmployeeCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = editEmployeeCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////editEmployeeCbx.SelectedText = code;
				//editEmployeeCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
			}
		}
		private void editCropCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (editCropCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = editCropCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////editCropCbx.SelectedText = code;
				//editCropCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
			}
		}
		private void editJobCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (editJobCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = editJobCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////editJobCbx.SelectedText = code;
				//editJobCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
			}
		}
		private void editFieldCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (editFieldCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = editFieldCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////editFieldCbx.SelectedText = code;
				//editFieldCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
			}
		}

		private void editEmployeeCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void editCropCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void editJobCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void editFieldCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		#endregion

		#region Input Validation Functions (Checking for proper data type inputs).
		private void editHoursTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Hours");
			}
		}
		private void editPayUnitTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Pay Unit");
			}
		}
		private void editYieldUnitTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Yield Unit");
			}
		}
		private void editPriceTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Price");
			}
		}
		#endregion

		private void savedPayDetailsLst_SelectedIndexChanged(object sender, EventArgs e)
		{
			//IF - They have selected an entry on the list.
			if (savedPayDetailsLst.SelectedIndex != -1 && GetKey() != -1)
			{
                //Reset dropdown boxes
                editCropCbx.SelectedIndex = -1;
                editEmployeeCbx.SelectedIndex = -1;
                editFieldCbx.SelectedIndex = -1;
                editJobCbx.SelectedIndex = -1;
				//Extrapolate the variables from the selected line in the list.
                MainMenu.PayDetailData payDetail = new MainMenu.PayDetailData();
                for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
                {

                    payDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                    if (GetKey() == payDetail.KeyInt)
                    {
                        break;
                    }
                }
     			string selectedDate = payDetail.DateDtm.ToShortDateString();
                string selectedStartTime = payDetail.StartTimeDtm.ToShortTimeString();
                string selectedEndTime = payDetail.EndTimeDtm.ToShortTimeString();
                #region Set Employee
                string selectedEmployeeCode = "";
                for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
                {
                    MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

                    if (employeeData.KeyInt == payDetail.EmployeeKeyInt)
                    {
                        selectedEmployeeCode = employeeData.CodeStr;
                        break;
                    }
                }
                string selectedEmployeeDescription = FindDescription("employee", selectedEmployeeCode);
                #endregion
                string selectedHours = payDetail.HoursDbl.ToString();
				string selectedYieldUnit = payDetail.YieldUnitDbl.ToString();
                #region Set Crop
                string selectedCropCode = "";
                for (int j = 0; j < cropsData.CropsDataArl.Count; j++)
                {
                    MainMenu.CropData cropData = (MainMenu.CropData)cropsData.CropsDataArl[j];

                    if (cropData.KeyInt == payDetail.CropKeyInt)
                    {
                        selectedCropCode = cropData.CodeStr;

                        break;
                    }
                }
				string selectedCropDescription = FindDescription("crop", selectedCropCode);
                #endregion
                #region Set Job
                string selectedJobCode = "";
                for (int j = 0; j < jobsData.JobsDataArl.Count; j++)
                {
                    MainMenu.JobData jobData = (MainMenu.JobData)jobsData.JobsDataArl[j];

                    if (jobData.KeyInt == payDetail.JobKeyInt)
                    {
                        selectedJobCode = jobData.CodeStr;

                        break;
                    }
                }
				string selectedJobDescription = FindDescription("job", selectedJobCode);
                #endregion
                #region Set Field
                string selectedFieldCode = "";
                for (int j = 0; j < fieldsData.FieldsDataArl.Count; j++)
                {
                    MainMenu.FieldData fieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[j];

                    if (fieldData.KeyInt == payDetail.FieldKeyInt)
                    {
                        selectedFieldCode = fieldData.CodeStr;

                        break;
                    }
                }
				string selectedFieldDescription = FindDescription("field", selectedFieldCode);
                #endregion
                string selectedPayUnit = payDetail.PayUnitDbl.ToString();
				string selectedPrice = payDetail.PriceDbl.ToString();
				string selectedSummaryNumber = payDetail.SummaryNumberInt.ToString();
               

				//Enable the entry boxes & button.
				editDatePicker.Enabled = true;
				editDateLbl.Enabled = true;
				editStartTimePicker.Enabled = true;
				editStartTimeLbl.Enabled = true;
				editEndTimePicker.Enabled = true;
				editEndTimeLbl.Enabled = true;
				editEmployeeCbx.Enabled = true;
				editEmployeeLbl.Enabled = true;
				editHoursTxt.Enabled = true;
				editHoursLbl.Enabled = true;
				editCropCbx.Enabled = true;
				editCropLbl.Enabled = true;
				editJobCbx.Enabled = true;
				editJobLbl.Enabled = true;
				editFieldCbx.Enabled = true;
				editFieldLbl.Enabled = true;
				editPayUnitTxt.Enabled = true;
				editPayUnitLbl.Enabled = true;
				editYieldUnitTxt.Enabled = true;
				editYieldUnitLbl.Enabled = true;
				editPriceTxt.Enabled = true;
				editPriceLbl.Enabled = true;
				saveEditedBtn.Enabled = true;
				deleteEditedBtn.Enabled = true;

				//Fill the Edit entry boxes.
				editDatePicker.Text = selectedDate;
				editStartTimePicker.Text = selectedStartTime;
				editEndTimePicker.Text = selectedEndTime;

				for (int i = 0; i < editEmployeeCbx.Items.Count; i++)
				{
					if (editEmployeeCbx.Items[i].ToString() == selectedEmployeeCode + delimiterWithSpaces + selectedEmployeeDescription)
					{
						editEmployeeCbx.SelectedIndex = i;
						break;
					}
				}

				editHoursTxt.Text = selectedHours;

				for (int i = 0; i < editCropCbx.Items.Count; i++)
				{
					if (editCropCbx.Items[i].ToString() == selectedCropCode + delimiterWithSpaces + selectedCropDescription)
					{
						editCropCbx.SelectedIndex = i;
						break;
					}
				}

				for (int i = 0; i < editJobCbx.Items.Count; i++)
				{
					if (editJobCbx.Items[i].ToString() == selectedJobCode + delimiterWithSpaces + selectedJobDescription)
					{
						editJobCbx.SelectedIndex = i;
						break;
					}
				}

				for (int i = 0; i < editFieldCbx.Items.Count; i++)
				{
					if (editFieldCbx.Items[i].ToString() == selectedFieldCode + delimiterWithSpaces + selectedFieldDescription)
					{
						editFieldCbx.SelectedIndex = i;
						break;
					}
				}

				editPayUnitTxt.Text = selectedPayUnit;
				editYieldUnitTxt.Text = selectedYieldUnit;
				editPriceTxt.Text = selectedPrice;
			}
			else//<-- They have NOT selected an entry on the list.
			{
				editDatePicker.Text = DateTime.Today.ToShortDateString();
				editStartTimePicker.Text = DateTime.Now.ToShortTimeString();
				editEndTimePicker.Text = DateTime.Now.ToShortTimeString();

				editEmployeeCbx.Text = string.Empty;
				editEmployeeCbx.SelectedItem = string.Empty;
				editEmployeeCbx.SelectedIndex = -1;
				editHoursTxt.Text = "0";
				editCropCbx.Text = string.Empty;
				editCropCbx.SelectedItem = string.Empty;
				editCropCbx.SelectedIndex = -1;
				editJobCbx.Text = string.Empty;
				editJobCbx.SelectedItem = string.Empty;
				editJobCbx.SelectedIndex = -1;
				editFieldCbx.Text = string.Empty;
				editFieldCbx.SelectedItem = string.Empty;
				editFieldCbx.SelectedIndex = -1;
				editPayUnitTxt.Text = "0";
				editYieldUnitTxt.Text = "0";
				editPriceTxt.Text = "0";

				//Enable the entry boxes & buttons
				editDatePicker.Enabled = false;
				editDateLbl.Enabled = false;
				editStartTimePicker.Enabled = false;
				editStartTimeLbl.Enabled = false;
				editEndTimePicker.Enabled = false;
				editEndTimeLbl.Enabled = false;
				editEmployeeCbx.Enabled = false;
				editEmployeeLbl.Enabled = false;
				editHoursTxt.Enabled = false;
				editHoursLbl.Enabled = false;
				editCropCbx.Enabled = false;
				editCropLbl.Enabled = false;
				editJobCbx.Enabled = false;
				editJobLbl.Enabled = false;
				editFieldCbx.Enabled = false;
				editFieldLbl.Enabled = false;
				editPayUnitTxt.Enabled = false;
				editPayUnitLbl.Enabled = false;
				editYieldUnitTxt.Enabled = false;
				editYieldUnitLbl.Enabled = false;
				editPriceTxt.Enabled = false;
				editPriceLbl.Enabled = false;
				saveEditedBtn.Enabled = false;
				deleteEditedBtn.Enabled = false;
			}
		}
		#endregion

		#region Edit Pay Detail Methods
		private void saveEditedBtn_Click(object sender, EventArgs e)
		{
            //Extrapolate the variables from the selected line in the list.
                MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[GetKey()];
                if (payDetail.TypeStr != "M" && payDetail.TypeStr != "O")
                {
                    //Check For Selected Drop Downs.
                    if (editEmployeeCbx.SelectedIndex == -1 || editEmployeeCbx.Text.Trim() == "")
                    {
                        MessageBox.Show("Please select an employee", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        editEmployeeCbx.Focus();
                    }
                    if (editCropCbx.SelectedIndex == -1 || editCropCbx.Text.Trim() == "")
                    {
                        MessageBox.Show("Please select a crop", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        editCropCbx.Focus();
                    }
                    if (editJobCbx.SelectedIndex == -1 || editJobCbx.Text.Trim() == "")
                    {
                        MessageBox.Show("Please select a job", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        editJobCbx.Focus();
                    }
                    if (editFieldCbx.SelectedIndex == -1 || editFieldCbx.Text.Trim() == "")
                    {
                        MessageBox.Show("Please select a field", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        editFieldCbx.Focus();
                    }
                }
			
				//TODO: Add a line # to the table (XML File) that will serve as a key.

				MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)EmployeesArl[editEmployeeCbx.SelectedIndex];
                MainMenu.CropData cropData = new MainMenu.CropData();
                if (editCropCbx.SelectedIndex == -1)
                {
                   cropData.KeyInt = -1;
                }
                else
                {
                   cropData = (MainMenu.CropData)CropsArl[editCropCbx.SelectedIndex];
                } 
                MainMenu.JobData jobData = new MainMenu.JobData();
                if (editJobCbx.SelectedIndex == -1)
                {
                    jobData.KeyInt = -1;
                }
                else
                {
                    jobData = (MainMenu.JobData)JobsArl[editJobCbx.SelectedIndex];
                }
                MainMenu.FieldData fieldData = new MainMenu.FieldData();
                if (editFieldCbx.SelectedIndex == -1)
                {
                   fieldData.KeyInt = -1;
                }
                else
                {
                    fieldData = (MainMenu.FieldData)FieldsArl[editFieldCbx.SelectedIndex];
                }

				//Save the edited entry.
				MainMenu.SaveEditedPayDetailData(GetKey(), payDetailsData.PayDetailsDataArl, Convert.ToDateTime(editDatePicker.Text), Convert.ToDateTime(editStartTimePicker.Text), Convert.ToDateTime(editEndTimePicker.Text), employeeData.KeyInt, cropData.KeyInt, jobData.KeyInt, fieldData.KeyInt, editHoursTxt.Text, editPayUnitTxt.Text, editYieldUnitTxt.Text, editPriceTxt.Text, payDetail.TypeStr);

				//Save the Pay Details To XML.
				SavePayDetailDataToXML();
                
                FillScreen(currentEmployeeInt);
			
		}
		private void deleteEditedBtn_Click(object sender, EventArgs e)
		{
			//Fill the list from XML.
			FillPayDetailDataFromXML();

            //Delete out the selected Pay Detail.
			MainMenu.DeletePayDetailData(GetKey(), payDetailsData.PayDetailsDataArl);
            for (int h = 0; h < payReportLinesArl.Count; h++)
            {
                if (GetKey() == Convert.ToInt32(payReportLinesArl[h]))
                {
                    payReportLinesArl.RemoveAt(h);
                    break;
                }
            }


			//Save the new data to XML.
			SavePayDetailDataToXML();
            FillScreen(currentEmployeeInt);
		}
		#endregion

		#region Helper Methods
		//Get Data
		private void FillCropDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Crops);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Crops, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillEmployeeDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Employees);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Employees, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillJobDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Jobs);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Jobs, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillFieldDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Fields);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Fields, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillPayDetailDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_PayDetails);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayDetailsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_PayDetails, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				payDetailsData = (MainMenu.PayDetailsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
        private void FillPayrollOptionsDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_payrollOptionsData);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayrollOptionsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_payrollOptionsData, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                payrollOptionsData = (MainMenu.PayrollOptionsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
		private int GetKey()
		{
			int key = -1;
			int.TryParse(keyLst.Items[savedPayDetailsLst.SelectedIndex].ToString(), out key);
			return key;
		}

		//Fill DropDown Functions
		private void FillEmployeesDropdown()
		{
			//Clear the existing list.
			this.editEmployeeCbx.Items.Clear();
			this.editEmployeeCbx.SelectedIndex = -1;

			EmployeesArl = new ArrayList();

			//Fill the employee data object.
			FillEmployeeDataFromXML();

			//Fill the GUI dropdown list from the data object.
			for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
			{
				MainMenu.EmployeeData employee = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

				if (employee.ActiveBln)
				{
					editEmployeeCbx.Items.Add(employee.CodeStr + delimiterWithSpaces + employee.NameStr);
					EmployeesArl.Add(employee);
				}
			}
		}
		private void FillCropsDropdown()
		{
			//Clear the existing list.
			this.editCropCbx.Items.Clear();
			this.editCropCbx.SelectedIndex = -1;

			CropsArl = new ArrayList();

			//Fill the crop data object.
			FillCropDataFromXML();

			//Fill the GUI dropdown list from the data object.
			for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
			{
				MainMenu.CropData crop = (MainMenu.CropData)cropsData.CropsDataArl[i];

				if (crop.ActiveBln)
				{
					editCropCbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
					CropsArl.Add(crop);
				}
			}
		}
		private void FillJobsDropdown()
		{
			//Clear the existing list.
			this.editJobCbx.Items.Clear();
			this.editJobCbx.SelectedIndex = -1;

			JobsArl = new ArrayList();

			//Fill the job data object.
			FillJobDataFromXML();

			//Fill the GUI dropdown list from the data object.
			for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
			{
				MainMenu.JobData job = (MainMenu.JobData)jobsData.JobsDataArl[i];

				if (job.ActiveBln)
				{
					editJobCbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
					JobsArl.Add(job);
				}
			}
		}
		private void FillFieldsDropdown()
		{
			//Clear the existing list.
			this.editFieldCbx.Items.Clear();
			this.editFieldCbx.SelectedIndex = -1;

			FieldsArl = new ArrayList();

			//Fill the field data object.
			FillFieldDataFromXML();

			//Fill the GUI dropdown list from the data object.
			for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
			{
				MainMenu.FieldData field = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

				if (field.ActiveBln)
				{
					editFieldCbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
					FieldsArl.Add(field);
				}
			}
		}

		//Fill ListBox Functions
        private void FillScreen(Int32 employee)
        {
            //Clear the existing list.
            this.savedPayDetailsLst.ClearSelected();
            this.savedPayDetailsLst.Items.Clear();
            this.savedPayDetailsLst.SelectedIndex = -1;

            this.keyLst.Items.Clear();
            double employeeTotalHoursDbl = 0;
            double employeeTotalPayUnitDbl = 0;
            double employeeTotalYieldUnitDbl = 0;
            double employeeTotalPayDbl = 0;
            double totalHoursDbl = 0;
            double totalPayUnitDbl = 0;
            double totalYieldUnitDbl = 0;
            double totalPayDbl = 0;
            for (int h = 0; h < payReportLinesArl.Count; h++)
            {
                for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
                {

                    MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                    if (payDetail.KeyInt == Convert.ToInt32(payReportLinesArl[h]))
                    {
                        totalHoursDbl += payDetail.HoursDbl;
                        totalPayUnitDbl += payDetail.PayUnitDbl;
                        totalYieldUnitDbl += payDetail.YieldUnitDbl;
                        totalPayDbl += payDetail.PriceDbl * payDetail.PayUnitDbl;
                        if (Convert.ToInt32(payReportEmployeesArl[employee]) == payDetail.EmployeeKeyInt)
                        {
                            employeeTotalHoursDbl += payDetail.HoursDbl;
                            employeeTotalPayUnitDbl += payDetail.PayUnitDbl;
                            employeeTotalYieldUnitDbl += payDetail.YieldUnitDbl;
                            employeeTotalPayDbl += payDetail.PriceDbl * payDetail.PayUnitDbl;
                            string EmployeeStr = "";
                            string CropStr = "";
                            string JobStr = "";
                            string FieldStr = "";

                            for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
                            {
                                MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

                                if (employeeData.KeyInt == payDetail.EmployeeKeyInt)
                                {
                                    EmployeeStr = employeeData.CodeStr;
                                    EmployeeNameData.Text = EmployeeStr;
                                    labelThisEmployeeLbl.Text = employeeData.NameStr;
                                    break;
                                }
                            }

                            for (int j = 0; j < cropsData.CropsDataArl.Count; j++)
                            {
                                MainMenu.CropData cropData = (MainMenu.CropData)cropsData.CropsDataArl[j];

                                if (cropData.KeyInt == payDetail.CropKeyInt)
                                {
                                    CropStr = cropData.CodeStr;

                                    break;
                                }
                            }

                            for (int j = 0; j < jobsData.JobsDataArl.Count; j++)
                            {
                                MainMenu.JobData jobData = (MainMenu.JobData)jobsData.JobsDataArl[j];

                                if (jobData.KeyInt == payDetail.JobKeyInt)
                                {
                                    JobStr = jobData.CodeStr;

                                    break;
                                }
                            }

                            for (int j = 0; j < fieldsData.FieldsDataArl.Count; j++)
                            {
                                MainMenu.FieldData fieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[j];

                                if (fieldData.KeyInt == payDetail.FieldKeyInt)
                                {
                                    FieldStr = fieldData.CodeStr;

                                    break;
                                }
                            }
                           
                            // Sort ListBox Items
                            int tempItemInt = 0;
                            for (int j = 0; j < keyLst.Items.Count; j++)
                            {
                                if (Convert.ToInt32(keyLst.Items[j].ToString()) < payDetail.KeyInt)
                                {
                                    tempItemInt = j + 1;
                                }
                            }
                            //Store in the right place in ListBox
                            this.savedPayDetailsLst.Items.Insert(tempItemInt, MakeColumns(new List<string> { payDetail.DateDtm.ToShortDateString(), payDetail.TypeStr, EmployeeStr, payDetail.HoursDbl.ToString(), payDetail.YieldUnitDbl.ToString(), CropStr, JobStr, FieldStr, payDetail.PayUnitDbl.ToString(), payDetail.PriceDbl.ToString("$###,###,##0.00"), payDetail.TotalDollarsStr }));

                            //Store the key in same Index
                            this.keyLst.Items.Insert(tempItemInt, payDetail.KeyInt);
                        }
                    }
                }
            }
            EmpTotalHoursData.Text = employeeTotalHoursDbl.ToString("###,###,##0.##");
            EmpTotalPayUnitsData.Text = employeeTotalPayUnitDbl.ToString("###,###,##0.##");
            EmpTotalDollarsData.Text = employeeTotalPayDbl.ToString("$###,###,##0.00");
            EmpDollarsPerHourCalc.Text = (employeeTotalPayDbl / employeeTotalHoursDbl).ToString("$###,###,##0.00");
            AllDollarsPerHourCalc.Text = (totalPayDbl / totalHoursDbl).ToString("$###,###,##0.00");
            AllTotalHoursData.Text = totalHoursDbl.ToString("###,###,##0.##");
            AllTotalPayUnitsData.Text = totalPayUnitDbl.ToString("###,###,##0.##");
            AllTotalDollarsData.Text = totalPayDbl.ToString("$###,###,##0.00");
            if (payrollOptionsData.MinimumWageDbl > (employeeTotalPayDbl / employeeTotalHoursDbl))
            {
                DollarsToMakeMinCalc.Text = ((payrollOptionsData.MinimumWageDbl * employeeTotalHoursDbl) - employeeTotalPayDbl).ToString("$###,###,##0.00");
                buttonProcessMinimumWage.Enabled = true;
                buttonProcessMinimumWage.Visible = true;
                labelDollarsToMakeMinimum.Visible = true;
                DollarsToMakeMinCalc.Visible = true;
                ProceedWarningBln = true;
            }
            else
            {
                DollarsToMakeMinCalc.Text = "";
                buttonProcessMinimumWage.Enabled = false;
                buttonProcessMinimumWage.Visible = false;
                labelDollarsToMakeMinimum.Visible = false;
                DollarsToMakeMinCalc.Visible = false;
            }
            if (payrollOptionsData.PayOvertimeAllBln || payrollOptionsData.OvertimePerEmployeeBln)
            {
                buttonProcessOvertime.Enabled = true;
                buttonProcessOvertime.Visible = true;
            }
            else
            {
                buttonProcessOvertime.Enabled = false;
                buttonProcessOvertime.Visible = false;
            }
            if (currentEmployeeInt == 0)
            {
                buttonPreviousEmployee.Enabled = false;
            }
            else
            {
                buttonPreviousEmployee.Enabled = true;
            }
            if (currentEmployeeInt == payReportEmployeesArl.Count)
            {
                buttonNextEmployee.Enabled = false;
            }
            else
            {
                buttonNextEmployee.Enabled = true;
            }
        }
		private void FillSavedPayDetails()
		{
			//Fill the pay detail data object.
			FillPayDetailDataFromXML();
            payReportLinesArl = new ArrayList();
            payReportEmployeesArl = new ArrayList();
            
            //Make list on Employees that need to be paid
            for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
            {
                MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                //Need to check and make sure line is not paid before we add detail to ArraryList.
                if (payDetail.ReportDateDtm.ToShortDateString() == "1/1/0001" && payDetail.ActiveBln)
                {
                    ValueIsInArrayList(payReportEmployeesArl, payDetail.EmployeeKeyInt.ToString());
                    payReportLinesArl.Add(payDetail.KeyInt.ToString());
                }
            }
            if (payReportEmployeesArl.Count > 0)
            {
                currentEmployeeInt = 0;
                FillScreen(currentEmployeeInt);
            }
            else
            {
                this.savedPayDetailsLst.ClearSelected();
                this.savedPayDetailsLst.Items.Clear();
                this.savedPayDetailsLst.SelectedIndex = -1;
                EmpTotalHoursData.Text = "";
                EmpTotalPayUnitsData.Text = "";
                EmpTotalDollarsData.Text = "";
                EmpDollarsPerHourCalc.Text = "";
                AllDollarsPerHourCalc.Text = "";
                AllTotalHoursData.Text = "";
                AllTotalPayUnitsData.Text = "";
                AllTotalDollarsData.Text = "";
                DollarsToMakeMinCalc.Text = "";
                EmployeeNameData.Text = "";
                buttonProcessMinimumWage.Enabled = false;
                buttonProcessMinimumWage.Visible = false;
                labelDollarsToMakeMinimum.Visible = false;
                DollarsToMakeMinCalc.Visible = false;
                buttonProcessOvertime.Enabled = false;
                buttonProcessOvertime.Visible = false;
                buttonPreviousEmployee.Enabled = false;
                buttonNextEmployee.Enabled = false;
                buttonProcessPayrollMakeReport.Enabled = false;
            }
        }
        private void ValueIsInArrayList(ArrayList list, string valueToFindStr)
        {
            int tempItemIdx = list.BinarySearch(valueToFindStr);

            if (tempItemIdx < 0)
            {
                list.Insert(Math.Abs(tempItemIdx) - 1, valueToFindStr);
            }
        }
		//Save data to XML functions
		private void SavePayDetailDataToXML()
		{
			//Serialize (convert an object instance to an XML document):
			XmlSerializer xmlSerializer = new XmlSerializer(payDetailsData.GetType());
			// Create an XmlTextWriter using a FileStream.
			Stream fileStream2 = new FileStream(xmlFileName_PayDetails, FileMode.Create);
			XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
			// Serialize using the XmlTextWriter.
			xmlSerializer.Serialize(xmlWriter, payDetailsData);
			xmlWriter.Flush();
			xmlWriter.Close();
		}

		//Adjust ComboBox dropdown list width
		private static void SetComboScrollWidth(object sender)
		{
			//CREDIT - this code was found at: http://rajeshkm.blogspot.com/2006/11/adjust-combobox-drop-down-list-width-c.html
			//      THANKS Rajesh!
			try
			{
				ComboBox senderComboBox = (ComboBox)sender;
				int width = senderComboBox.Width;
				Graphics g = senderComboBox.CreateGraphics();
				Font font = senderComboBox.Font;

				//checks if a scrollbar will be displayed.
				//If yes, then get its width to adjust the size of the drop down list.
				int vertScrollBarWidth = (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems) ? SystemInformation.VerticalScrollBarWidth : 0;

				//Loop through list items and check size of each items.
				//set the width of the drop down list to the width of the largest item.

				int newWidth;

				foreach (string s in ((ComboBox)sender).Items)
				{
					if (s != null)
					{
						newWidth = (int)g.MeasureString(s.Trim(), font).Width + vertScrollBarWidth;

						if (width < newWidth)
						{
							width = newWidth;
						}
					}
				}

				senderComboBox.DropDownWidth = width;
			}
			catch (Exception objException)
			{
				//Catch objException
			}
		}

		//Validation Function(s)
		private void ValidateQuantity(TextBox textBoxToCheck, string textBoxName)
		{
			try
			{
				decimal temp = decimal.Parse(textBoxToCheck.Text);

				if (textBoxToCheck.Text != "" && textBoxToCheck.Text.IndexOf(",") == -1)
				{
					textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
				}
				else if (textBoxToCheck.Text.IndexOf(",") != -1)
				{
					textBoxToCheck.Text.Replace(",", "");
					textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
				}
			}
			catch (System.FormatException ex)
			{
				MessageBox.Show(this, "Invalid " + textBoxName + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				textBoxToCheck.Focus();
				textBoxToCheck.SelectAll();
			}
		}

		//Formating Function(s)
		private static string DisplayFormat(decimal value)
		{
			if (value == 0)
				return value.ToString();
			else
				return decimal.Round(value, 2).ToString("#,###,##0.##");
		}
		private string FindDescription(string dataTypeStr, string codeIn)
		{
			if (dataTypeStr.ToLower() == "crop")
			{
				for (int i = 0; i < this.cropsData.CropsDataArl.Count; i++)
				{
					MainMenu.CropData crop = (MainMenu.CropData)this.cropsData.CropsDataArl[i];

					if (crop.CodeStr == codeIn)
					{
						return crop.DescriptionStr;
					}
				}
			}
			else if (dataTypeStr.ToLower() == "employee")
			{
				for (int i = 0; i < this.employeesData.EmployeesDataArl.Count; i++)
				{
					MainMenu.EmployeeData employee = (MainMenu.EmployeeData)this.employeesData.EmployeesDataArl[i];

					if (employee.CodeStr == codeIn)
					{
						return employee.NameStr;
					}
				}
			}
			else if (dataTypeStr.ToLower() == "job")
			{
				for (int i = 0; i < this.jobsData.JobsDataArl.Count; i++)
				{
					MainMenu.JobData job = (MainMenu.JobData)this.jobsData.JobsDataArl[i];

					if (job.CodeStr == codeIn)
					{
						return job.DescriptionStr;
					}
				}
			}
			else if (dataTypeStr.ToLower() == "field")
			{
				for (int i = 0; i < this.fieldsData.FieldsDataArl.Count; i++)
				{
					MainMenu.FieldData field = (MainMenu.FieldData)this.fieldsData.FieldsDataArl[i];

					if (field.CodeStr == codeIn)
					{
						return field.DescriptionStr;
					}
				}
			}

			return "";
		}
		private string MakeHeaderColumns(List<string> stringsLst)
		{
			string outputStr = "|";

			foreach (string str in stringsLst)
			{
				int spaceToFill = savedEntriesCharsPerColumn - str.Length;

				if (spaceToFill % 2 == 0/*Is Even*/)
				{
					for (int i = 0; i < (spaceToFill / 2); i++)
					{
						outputStr += " ";
					}

					outputStr += str;

					for (int i = 0; i < (spaceToFill / 2); i++)
					{
						outputStr += " ";
					}

					outputStr += "|";
				}
				else
				{
					for (int i = 0; i < (((spaceToFill - 1) / 2) + 1); i++)
					{
						outputStr += " ";
					}

					outputStr += str;

					for (int i = 0; i < ((spaceToFill - 1) / 2); i++)
					{
						outputStr += " ";
					}

					outputStr += "|";
				}
			}

			return outputStr;
		}
		private string MakeColumns(List<string> stringsLst)
		{
			string outputStr = "   ";

			for (int i = 0; i < stringsLst.Count; i++)
			{
				string str = stringsLst[i];

				switch (i)
				{
					case 0://Date
						/*Left Align*/
						outputStr += str.PadRight(10, ' ');
						outputStr += "|";
						break;

					case 1://Code
						/*Left Align*/
						outputStr += str.PadRight(7, ' ');
						outputStr += "|";
						break;

					case 2://Employee
						/*Left Align*/
						outputStr += str.PadRight(10, ' ');
						outputStr += "|";
						break;

					case 3://Hours
						/*Right Align*/
						outputStr += str.PadLeft(7, ' ');
						outputStr += "|";
						break;

					case 4://Yield Unit
						/*Right Align*/
						outputStr += str.PadLeft(7, ' ');
						outputStr += "|";
						break;

					case 5://Crop
						/*Left Align*/
						outputStr += str.PadRight(10, ' ');
						outputStr += "|";
						break;

					case 6://Job
						/*Left Align*/
						outputStr += str.PadRight(10, ' ');
						outputStr += "|";
						break;

					case 7://Field
						/*Left Align*/
						outputStr += str.PadRight(10, ' ');
						outputStr += "|";
						break;

					case 8://Pay Unit
						/*Right Align*/
						outputStr += str.PadLeft(7, ' ');
						outputStr += "|";
						break;

					case 9://Price
						/*Right Align*/
						outputStr += str.PadLeft(10, ' ');
						outputStr += "|";
						break;

					case 10://Total
						/*Right Align*/
						outputStr += str.PadLeft(10, ' ');
						outputStr += "|";
						break;
				}
			}

			return outputStr;
		}
		#endregion

        private void keyLst_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void buttonNextEmployee_Click(object sender, EventArgs e)
        {
            if (payReportEmployeesArl.Count -1 > currentEmployeeInt)
            {
                currentEmployeeInt = currentEmployeeInt + 1;
                FillScreen(currentEmployeeInt);
            }
        }

        private void buttonPreviousEmployee_Click(object sender, EventArgs e)
        {

            if (currentEmployeeInt >= 1)
            {
                currentEmployeeInt = currentEmployeeInt - 1;
                FillScreen(currentEmployeeInt);
            }
        }

        private void buttonProcessMinimumWage_Click(object sender, EventArgs e)
        {
            //Save the data from the user entry field to a PayDetail object
           // MainMenu.PayDetailData payDetail = MainMenu.FindOrAddPayDetailData(payDetailsData.PayDetailsDataArl, Convert.ToDateTime(PayThroughDate.Text), System.DateTime.MinValue, System.DateTime.MinValue, Convert.ToInt32(payReportEmployeesArl[currentEmployeeInt]), -1, -1, -1, "M");
            MainMenu.PayDetailData payDetail = new MainMenu.PayDetailData();
            payDetail.DateDtm = Convert.ToDateTime(PayThroughDate.Text);
            payDetail.StartTimeDtm = Convert.ToDateTime(System.DateTime.MinValue);
            payDetail.EndTimeDtm = Convert.ToDateTime(System.DateTime.MinValue);
            payDetail.EmployeeKeyInt =  Convert.ToInt32(payReportEmployeesArl[currentEmployeeInt]);
            payDetail.CropKeyInt = -1;
            payDetail.JobKeyInt = -1;
            payDetail.FieldKeyInt = -1;
            payDetail.TypeStr = "M";
            payDetail.ActiveBln = true;
            payDetail.KeyInt = payDetailsData.PayDetailsDataArl.Count;
            //Add hours, pay unit, & price to payDetail
            payDetail.HoursDbl += 0;
            payDetail.PayUnitDbl = 1;
            payDetail.YieldUnitDbl = 0;
            payDetail.PriceDbl += Convert.ToDouble(DollarsToMakeMinCalc.Text.Substring(1,DollarsToMakeMinCalc.Text.Length -1));

            payDetailsData.PayDetailsDataArl.Add(payDetail);
            //always add new line to Arraylist of lines
            payReportLinesArl.Add(payDetail.KeyInt);

            //Save the Pay Details To XML
            SavePayDetailDataToXML();
            ProceedWarningBln = false;
            FillScreen(currentEmployeeInt);
        }

        private void buttonProcessPayrollMakeReport_Click()
        {
               for (int h = 0; h < payReportLinesArl.Count; h++)
                {
                    for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
                    {

                        MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                        if (payDetail.SummaryNumberInt == 0 && payDetail.ActiveBln)
                        {
                            
                        }
                    }
                }
                #region Make PDF
                string timeStampStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".").Replace(" ", "_");
                string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Pay Data\";
                //string fileNameStr = "Pay_Data_Report_" + timeStampStr + ".pdf";
                //string fileNameStr = "Make_Pay_" + tempNumberInt + timeStampStr + ".pdf";
                string fileNameStr = "Make_Pay_Due_" + timeStampStr + ".pdf";

                PayDueReceipts payDueReceipts = new PayDueReceipts();
                payDueReceipts.Main(saveFolderStr, fileNameStr, employeesData, cropsData, jobsData, fieldsData, payDetailsData.PayDetailsDataArl, PayThruDate.ToString());
                //payReceipts.Main(saveFolderStr, fileNameStr, employeesData, cropsData, jobsData, fieldsData, payDetailsData.PayDetailsDataArl, tempNumberInt);
                #endregion
                #region Open PDF
                Process.Start(saveFolderStr + fileNameStr);
                #endregion
                ////Clear Screen.
                //this.savedPayDetailsLst.ClearSelected();
                //this.savedPayDetailsLst.Items.Clear();
                //this.savedPayDetailsLst.SelectedIndex = -1;
                //EmpTotalHoursData.Text = "";
                //EmpTotalPayUnitsData.Text = "";
                //EmpTotalDollarsData.Text = "";
                //EmpDollarsPerHourCalc.Text = "";
                //AllDollarsPerHourCalc.Text = "";
                //AllTotalHoursData.Text = "";
                //AllTotalPayUnitsData.Text = "";
                //AllTotalDollarsData.Text = "";
                //DollarsToMakeMinCalc.Text = "";
                //EmployeeNameData.Text = "";
                //labelThisEmployeeLbl.Text = "";
                //buttonProcessMinimumWage.Enabled = false;
                //buttonProcessMinimumWage.Visible = false;
                //labelDollarsToMakeMinimum.Visible = false;
                //DollarsToMakeMinCalc.Visible = false;
                //buttonProcessOvertime.Enabled = false;
                //buttonProcessOvertime.Visible = false;
                //buttonPreviousEmployee.Enabled = false;
                //buttonNextEmployee.Enabled = false;
                //buttonProcessPayrollMakeReport.Enabled = false;
                //FillSavedPayDetails();
            //}
                exitBtn_Click();
        }

        private void warn1proceedBtn_Click(object sender, EventArgs e)
        {
            warning2Pnl.Visible = true;
            warn2CancelBtn.Focus();

        }

        private void warn1CancelBtn_Click(object sender, EventArgs e)
        {
            warning1Pnl.Visible = false;
        }

        private void warn2ProceedBtn_Click(object sender, EventArgs e)
        {
            warning1Pnl.Visible = false;
            warning2Pnl.Visible = false;
            savedPayDetailsLst.Enabled = true;
            //need to find biggest report nunber in the data then Add 1 and use for this report
            Int32 tempNumberInt = 0;
            for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
            {
                MainMenu.PayDetailData tempDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                //make sure line is Active
                if (tempDetail.ActiveBln)
                {
                    if (tempDetail.SummaryNumberInt > tempNumberInt)
                    {
                        tempNumberInt = tempDetail.SummaryNumberInt;
                    }
                }
            }

            //add one to last Report number
            tempNumberInt = tempNumberInt + 1;

            //add paydate and Report Number to the all records in sort arraylist.
            for (int h = 0; h < payReportLinesArl.Count; h++)
            {
                for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
                {

                    MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                    if (payDetail.KeyInt == Convert.ToInt32(payReportLinesArl[h]))
                    {
                        payDetail.SummaryNumberInt = tempNumberInt;
                        payDetail.ReportDateDtm = PayThruDate;
                    }
                }
            }
            SavePayDetailDataToXML();
            #region Make PDF
            string timeStampStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".").Replace(" ", "_");
            string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Pay Data\";
            //string fileNameStr = "Pay_Data_Report_" + timeStampStr + ".pdf";
            string fileNameStr = "Make_Pay_" + tempNumberInt + timeStampStr + ".pdf";

            PayReceipts payReceipts = new PayReceipts();
            payReceipts.Main(saveFolderStr, fileNameStr, employeesData, cropsData, jobsData, fieldsData, payDetailsData.PayDetailsDataArl, tempNumberInt);
            #endregion
            #region Open PDF
            Process.Start(saveFolderStr + fileNameStr);
            #endregion
            //Clear Screen.
            this.savedPayDetailsLst.ClearSelected();
            this.savedPayDetailsLst.Items.Clear();
            this.savedPayDetailsLst.SelectedIndex = -1;
            EmpTotalHoursData.Text = "";
            EmpTotalPayUnitsData.Text = "";
            EmpTotalDollarsData.Text = "";
            EmpDollarsPerHourCalc.Text = "";
            AllDollarsPerHourCalc.Text = "";
            AllTotalHoursData.Text = "";
            AllTotalPayUnitsData.Text = "";
            AllTotalDollarsData.Text = "";
            DollarsToMakeMinCalc.Text = "";
            EmployeeNameData.Text = "";
            labelThisEmployeeLbl.Text = "";
            buttonProcessMinimumWage.Enabled = false;
            buttonProcessMinimumWage.Visible = false;
            labelDollarsToMakeMinimum.Visible = false;
            DollarsToMakeMinCalc.Visible = false;
            buttonProcessOvertime.Enabled = false;
            buttonProcessOvertime.Visible = false;
            buttonPreviousEmployee.Enabled = false;
            buttonNextEmployee.Enabled = false;
            buttonProcessPayrollMakeReport.Enabled = false;
            FillSavedPayDetails();
        }

        private void warn2CancelBtn_Click(object sender, EventArgs e)
        {
            warning1Pnl.Visible = false;
            warning2Pnl.Visible = false;
        }

        

        

        

       
	}
}
