﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Diagnostics;


namespace StorageInventory
{
	public partial class Grower_Block : Form
	{
		#region Vars
		//Set the global variables for delimiter character.
		char delimiter = '~';        
		//Set the path of the program & save files.
		private string xmlFileName = Path.GetDirectoryName(Application.ExecutablePath) + @"\Grower_Block_Data.xml";
        //test 3-24-15 9:37
		//Create the cropsData object.
		MainMenu.Grower_Blocks_Data growerBlocksData = new MainMenu.Grower_Blocks_Data();
       //from Michele PC
		private ArrayList ActiveArl;
		private ArrayList InactiveArl;
		#endregion

		#region Constructor
		public Grower_Block()
		{
			InitializeComponent();
            growerBlocksData = new MainMenu.Grower_Blocks_Data();
            FillSavedGrowerBlockLst(true);

            //Make form non-resizable.
            //this.MinimumSize = this.MaximumSize = this.Size;
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;

            //Fill the crop data from XML.
            FillGrowerBlockDataFromXML();
            CompanyNameData.Text = MainMenu.CompanyStr;
		}
		#endregion

		#region Form Functions.
		#region KeyDown Functions (For Enter-As-Tab Functionality)
		private void newCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void newDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveNewBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveBtn_Click(null, null);
		}
		private void editCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveEditedBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveEditedBtn_Click(null, null);
		}
		#endregion

		#region Input Validation Functions (Checking for proper data type inputs).

		private void newCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(newCodeTxt, delimiter))
			{
				//Check for a unique crop code.
				foreach (MainMenu.GrowerBlockData tempCropData in growerBlocksData.GrowerBlocksDataArl)
				{
					if (tempCropData.CodeStr == this.newCodeTxt.Text.Trim())
					{
						MessageBox.Show("This Grower Block code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

						//Focus on the line the user needs to change.
						this.newCodeTxt.Focus();
						this.newCodeTxt.Text = string.Empty;

						break;
					}
				}
			}
		}

		private void editCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(editCodeTxt, delimiter))
			{
				//Check for a unique crop code.
				for (int i = 0; i < growerBlocksData.GrowerBlocksDataArl.Count; i++)
				{
					MainMenu.GrowerBlockData tempGrowerData = (MainMenu.GrowerBlockData)growerBlocksData.GrowerBlocksDataArl[i];

					if (tempGrowerData.CodeStr == this.editCodeTxt.Text.Trim() && tempGrowerData.KeyInt.ToString() != this.keyTxt.Text)
					{
						MessageBox.Show("This Grower Block code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

						//Focus on the line the user needs to change.
						this.editCodeTxt.Focus();
						this.editCodeTxt.SelectAll();

						break;
					}
				}
			}
		}
		#endregion

		#region List Box Functions
		private void savedCropsLst_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (savedGrowerBlocksLst.SelectedIndex != -1)
			{
				//Clear out the new entry spaces.
				this.newCodeTxt.Text = string.Empty;
				this.newDescriptionTxt.Text = string.Empty;

				if (activeRdo.Checked)
				{
					MainMenu.GrowerBlockData growerData = (MainMenu.GrowerBlockData)ActiveArl[savedGrowerBlocksLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = growerData.CodeStr;
					editDescriptionTxt.Text = growerData.DescriptionStr;
					keyTxt.Text = growerData.KeyInt.ToString();
				}
				else
				{
					MainMenu.GrowerBlockData growerData = (MainMenu.GrowerBlockData)InactiveArl[savedGrowerBlocksLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = growerData.CodeStr;
					editDescriptionTxt.Text = growerData.DescriptionStr;
					keyTxt.Text = growerData.KeyInt.ToString();
				}

				//Extrapolate the selected code & description.
				//string selectedEntry = savedCropsLst.SelectedItem.ToString();
				//string[] entryPieces = selectedEntry.Split(delimiter);
				//string selectedCode = entryPieces[0].Trim();
				//string selectedDescription = entryPieces[1].Trim();

				//Fill the Edit entry boxes.
				//editCodeTxt.Text = selectedCode;
				//editDescriptionTxt.Text = selectedDescription;

				//Enable the entry boxes & button.
				editCodeTxt.Enabled = true;
				editCodeLbl.Enabled = true;
				editDescriptionTxt.Enabled = true;
				editDescriptionLbl.Enabled = true;
				saveEditedBtn.Enabled = true;
				enableDisableBtn.Enabled = true;
			}
			else
			{
				//Clear the Edit entry boxes.
				editCodeTxt.Text = string.Empty;
				editDescriptionTxt.Text = string.Empty;
				keyTxt.Text = string.Empty;

				//Disable the entry boxes & button.
				editCodeTxt.Enabled = false;
				editCodeLbl.Enabled = false;
				editDescriptionTxt.Enabled = false;
				editDescriptionLbl.Enabled = false;
				saveEditedBtn.Enabled = false;
				enableDisableBtn.Enabled = false;
			}
		}
		private void activeRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (activeRdo.Checked)
			{
				enableDisableBtn.Text = "&Disable";
                FillSavedGrowerBlockLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "En&able";

                FillSavedGrowerBlockLst(activeRdo.Checked);
			}
		}
		private void inactiveRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (inactiveRdo.Checked)
			{
				enableDisableBtn.Text = "En&able";

                FillSavedGrowerBlockLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "&Disable";

                FillSavedGrowerBlockLst(activeRdo.Checked);
			}
		}
		#endregion

		private void exitBtn_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		#endregion

		#region New Crop Functions.
		private void saveBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.newCodeTxt, delimiter))
			{
				if (!InputStringContainsChar(this.newDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
                    FillGrowerBlockDataFromXML();
					//Add the new Crop Data into the list.
					MainMenu.GrowerBlockData growerData = MainMenu.FindOrAddGrowerBlockData(growerBlocksData, this.newCodeTxt.Text.Trim(), this.newDescriptionTxt.Text.Trim(), growerBlocksData.NextKeyInt);
                    growerData.ActiveBln = activeRdo.Checked;

                    //Save the new data to XML.
                    SaveGrowerBlockDataToXML();

					//Refill the GUI list.
					FillSavedGrowerBlockLst(activeRdo.Checked);

					//Clear the freshly added new code & description.
					newCodeTxt.Clear();
					newDescriptionTxt.Clear();

					//Clear the edit entry spaces.
					savedGrowerBlocksLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void newCodeTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedGrowerBlocksLst.SelectedIndex = -1;
		}
		private void newDescriptionTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedGrowerBlocksLst.SelectedIndex = -1;
		}
		#endregion

		#region Edit Grower Block Functions.
		private void saveEditedBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//  character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.editCodeTxt, delimiter) && this.keyTxt.Text != "")
			{
				if (!InputStringContainsChar(this.editDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
					FillGrowerBlockDataFromXML();

					//Save the edited crop.
					MainMenu.SaveEditedGrowerBlockData(growerBlocksData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), activeRdo.Checked);

                    //Save the new data to XML.
                    SaveGrowerBlockDataToXML();

					//Refill the GUI list.
					FillSavedGrowerBlockLst(activeRdo.Checked);

					//Clear the edit entry spaces.
					savedGrowerBlocksLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void enableDisableBtn_Click(object sender, EventArgs e)
		{
			if (this.keyTxt.Text != "")
			{
				//Fill the list from XML.
				FillGrowerBlockDataFromXML();

				//Enable/Disable out the selected crop.
				if (activeRdo.Checked)
                {
                    MainMenu.SaveEditedGrowerBlockData(growerBlocksData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), false);
                }
                else
                {
                    MainMenu.SaveEditedGrowerBlockData(growerBlocksData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), true);
                }
                //Save the new data to XML.
                SaveGrowerBlockDataToXML();

				//Refill the GUI list.
				FillSavedGrowerBlockLst(activeRdo.Checked);

				//Clear the edit entry spaces.
				savedGrowerBlocksLst.SelectedIndex = -1;

				//Put focus back on 'NewCode' entry box.
				newCodeTxt.Focus();
			}
		}
		#endregion

		#region Helper Functions.
		private void FillSavedGrowerBlockLst(bool activeBln)
		{
			//Clear out the Saved Crops List.
			savedGrowerBlocksLst.ClearSelected();
			savedGrowerBlocksLst.Items.Clear();
			savedGrowerBlocksLst.SelectedIndex = -1;

			ActiveArl = new ArrayList();
			InactiveArl = new ArrayList();

			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.Grower_Blocks_Data));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				MainMenu.Grower_Blocks_Data growerBlocksData = (MainMenu.Grower_Blocks_Data)serializer.Deserialize(xmlReader);
				fileStream.Close();
                growerBlocksData.GrowerBlocksDataArl.Sort();
				for (int i = 0; i < growerBlocksData.GrowerBlocksDataArl.Count; i++)
				{
					MainMenu.GrowerBlockData growerData = (MainMenu.GrowerBlockData)growerBlocksData.GrowerBlocksDataArl[i];

					if (growerData.ActiveBln == activeBln)
					{
						savedGrowerBlocksLst.Items.Add(growerData.CodeStr + " ~ " + growerData.DescriptionStr);
					}

					if (growerData.ActiveBln)
					{
						ActiveArl.Add(growerData);
					}
					else
					{
						InactiveArl.Add(growerData);
					}
				}
			}
		}
		private void FillGrowerBlockDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.Grower_Blocks_Data));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				growerBlocksData = (MainMenu.Grower_Blocks_Data)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void SaveGrowerBlockDataToXML()
		{
			//Serialize (convert an object instance to an XML document):
			XmlSerializer xmlSerializer = new XmlSerializer(growerBlocksData.GetType());
			// Create an XmlTextWriter using a FileStream.
			Stream fileStream2 = new FileStream(xmlFileName, FileMode.Create);
			XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
			// Serialize using the XmlTextWriter.
			xmlSerializer.Serialize(xmlWriter, growerBlocksData);
			xmlWriter.Flush();
			xmlWriter.Close();
		}
		private bool InputStringContainsChar(TextBox textBoxToCheck, char charToCheckFor)
		{
			if (textBoxToCheck.Text.Contains(charToCheckFor))
			{
				MessageBox.Show("Input cannot contain '" + charToCheckFor + "' characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				textBoxToCheck.Focus();
				textBoxToCheck.SelectAll();

				return true;
			}
			else
				return false;
		}
		#endregion

        private void makeListBtn_Click(object sender, EventArgs e)
        {
            #region Make PDF
            string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Grower Block Data\";
            //string fileNameStr = "Pay_Data_Report_" + timeStampStr + ".pdf";
            string fileNameStr = "Grower_Block_Data.pdf";
            Grower_Block_Report growerBlockReport = new Grower_Block_Report();
            growerBlockReport.Main(saveFolderStr, fileNameStr, growerBlocksData);
            #endregion
            #region Open PDF
            Process.Start(saveFolderStr + fileNameStr);
            #endregion
        }
	}
}
