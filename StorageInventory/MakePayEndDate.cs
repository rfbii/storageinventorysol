﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PayrollFieldDataDesktop
{
    public partial class MakePayEndDate : Form
    {
        public MakePayEndDate()
        {
            InitializeComponent();
            PayThruDate.Text = System.DateTime.Today.ToShortDateString();
            CompanyNameData.Text = MainMenu.CompanyStr;
        }

        private void labelSelectEndDate_Click(object sender, EventArgs e)
        {

        } 

        private void buttonStartProcess_Click(object sender, EventArgs e)
        {
            MakePay_Criteria makepaycriteria = new MakePay_Criteria(Convert.ToDateTime(PayThruDate.Text.ToString()));
            makepaycriteria.ShowDialog();
            this.Close();
        }

        private void PayThruDate_ValueChanged(object sender, EventArgs e)
        {
           
        }
    }
}
