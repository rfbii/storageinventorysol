﻿namespace StorageInventory
{
    partial class WorkOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cropCbx = new System.Windows.Forms.ComboBox();
            this.jobCbx = new System.Windows.Forms.ComboBox();
            this.fieldCbx = new System.Windows.Forms.ComboBox();
            this.priceTxt = new System.Windows.Forms.TextBox();
            this.printReportBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.employeeCbx = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.boxLabelTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.OurReferenceTxt = new System.Windows.Forms.TextBox();
            this.CustReferenceTxt = new System.Windows.Forms.TextBox();
            this.cust1Lbl = new System.Windows.Forms.Label();
            this.cust2Lbl = new System.Windows.Forms.Label();
            this.cust3Lbl = new System.Windows.Forms.Label();
            this.cust4Lbl = new System.Windows.Forms.Label();
            this.boxLabel2Txt = new System.Windows.Forms.TextBox();
            this.price2Txt = new System.Windows.Forms.TextBox();
            this.field2Cbx = new System.Windows.Forms.ComboBox();
            this.job2Cbx = new System.Windows.Forms.ComboBox();
            this.crop2Cbx = new System.Windows.Forms.ComboBox();
            this.boxLabel3Txt = new System.Windows.Forms.TextBox();
            this.price3Txt = new System.Windows.Forms.TextBox();
            this.field3Cbx = new System.Windows.Forms.ComboBox();
            this.job3Cbx = new System.Windows.Forms.ComboBox();
            this.crop3Cbx = new System.Windows.Forms.ComboBox();
            this.boxLabel4Txt = new System.Windows.Forms.TextBox();
            this.price4Txt = new System.Windows.Forms.TextBox();
            this.field4Cbx = new System.Windows.Forms.ComboBox();
            this.job4Cbx = new System.Windows.Forms.ComboBox();
            this.crop4Cbx = new System.Windows.Forms.ComboBox();
            this.boxLabel5Txt = new System.Windows.Forms.TextBox();
            this.price5Txt = new System.Windows.Forms.TextBox();
            this.field5Cbx = new System.Windows.Forms.ComboBox();
            this.job5Cbx = new System.Windows.Forms.ComboBox();
            this.crop5Cbx = new System.Windows.Forms.ComboBox();
            this.boxLabel6Txt = new System.Windows.Forms.TextBox();
            this.price6Txt = new System.Windows.Forms.TextBox();
            this.field6Cbx = new System.Windows.Forms.ComboBox();
            this.job6Cbx = new System.Windows.Forms.ComboBox();
            this.crop6Cbx = new System.Windows.Forms.ComboBox();
            this.boxLabel7Txt = new System.Windows.Forms.TextBox();
            this.price7Txt = new System.Windows.Forms.TextBox();
            this.field7Cbx = new System.Windows.Forms.ComboBox();
            this.job7Cbx = new System.Windows.Forms.ComboBox();
            this.crop7Cbx = new System.Windows.Forms.ComboBox();
            this.boxLabel8Txt = new System.Windows.Forms.TextBox();
            this.price8Txt = new System.Windows.Forms.TextBox();
            this.field8Cbx = new System.Windows.Forms.ComboBox();
            this.job8Cbx = new System.Windows.Forms.ComboBox();
            this.crop8Cbx = new System.Windows.Forms.ComboBox();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelPrintChk = new System.Windows.Forms.CheckBox();
            this.grower8Cbx = new System.Windows.Forms.ComboBox();
            this.grower7Cbx = new System.Windows.Forms.ComboBox();
            this.grower6Cbx = new System.Windows.Forms.ComboBox();
            this.grower5Cbx = new System.Windows.Forms.ComboBox();
            this.grower4Cbx = new System.Windows.Forms.ComboBox();
            this.grower3Cbx = new System.Windows.Forms.ComboBox();
            this.grower2Cbx = new System.Windows.Forms.ComboBox();
            this.growerCbx = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(4, 9);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 106;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(760, 10);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(137, 20);
            this.subtitleLbl.TabIndex = 105;
            this.subtitleLbl.Text = "Receiving Product";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(132, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 18);
            this.label1.TabIndex = 107;
            this.label1.Text = "Crop Code:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(288, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 18);
            this.label2.TabIndex = 108;
            this.label2.Text = "Box Type Code:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(468, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 18);
            this.label3.TabIndex = 109;
            this.label3.Text = "Where Code:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 18);
            this.label4.TabIndex = 110;
            this.label4.Text = "Quantity:";
            // 
            // cropCbx
            // 
            this.cropCbx.DropDownWidth = 300;
            this.cropCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropCbx.FormattingEnabled = true;
            this.cropCbx.Location = new System.Drawing.Point(93, 178);
            this.cropCbx.Name = "cropCbx";
            this.cropCbx.Size = new System.Drawing.Size(162, 28);
            this.cropCbx.TabIndex = 11;
            // 
            // jobCbx
            // 
            this.jobCbx.DropDownWidth = 300;
            this.jobCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobCbx.FormattingEnabled = true;
            this.jobCbx.Location = new System.Drawing.Point(264, 178);
            this.jobCbx.Name = "jobCbx";
            this.jobCbx.Size = new System.Drawing.Size(162, 28);
            this.jobCbx.TabIndex = 12;
            // 
            // fieldCbx
            // 
            this.fieldCbx.DropDownWidth = 300;
            this.fieldCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldCbx.FormattingEnabled = true;
            this.fieldCbx.Location = new System.Drawing.Point(435, 178);
            this.fieldCbx.Name = "fieldCbx";
            this.fieldCbx.Size = new System.Drawing.Size(162, 28);
            this.fieldCbx.TabIndex = 13;
            // 
            // priceTxt
            // 
            this.priceTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceTxt.Location = new System.Drawing.Point(12, 179);
            this.priceTxt.Name = "priceTxt";
            this.priceTxt.Size = new System.Drawing.Size(72, 26);
            this.priceTxt.TabIndex = 10;
            this.priceTxt.TextChanged += new System.EventHandler(this.priceTxt_TextChanged);
            // 
            // printReportBtn
            // 
            this.printReportBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printReportBtn.Location = new System.Drawing.Point(293, 472);
            this.printReportBtn.Name = "printReportBtn";
            this.printReportBtn.Size = new System.Drawing.Size(277, 26);
            this.printReportBtn.TabIndex = 115;
            this.printReportBtn.Text = "Save , Print Labels and Print Report";
            this.printReportBtn.UseVisualStyleBackColor = true;
            this.printReportBtn.Click += new System.EventHandler(this.printReportBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(620, 472);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(148, 26);
            this.exitBtn.TabIndex = 116;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // employeeCbx
            // 
            this.employeeCbx.DropDownWidth = 300;
            this.employeeCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeCbx.FormattingEnabled = true;
            this.employeeCbx.Location = new System.Drawing.Point(142, 44);
            this.employeeCbx.Name = "employeeCbx";
            this.employeeCbx.Size = new System.Drawing.Size(216, 28);
            this.employeeCbx.TabIndex = 1;
            this.employeeCbx.SelectedIndexChanged += new System.EventHandler(this.employeeCbx_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(20, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 18);
            this.label5.TabIndex = 117;
            this.label5.Text = "Customer Code:";
            // 
            // boxLabelTxt
            // 
            this.boxLabelTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabelTxt.Location = new System.Drawing.Point(783, 179);
            this.boxLabelTxt.Name = "boxLabelTxt";
            this.boxLabelTxt.Size = new System.Drawing.Size(162, 26);
            this.boxLabelTxt.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(826, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 18);
            this.label6.TabIndex = 118;
            this.label6.Text = "Box Label:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(599, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(166, 18);
            this.label7.TabIndex = 121;
            this.label7.Text = "Our Reference Number:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(556, 110);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(207, 18);
            this.label8.TabIndex = 122;
            this.label8.Text = "Customer Reference Number:";
            // 
            // OurReferenceTxt
            // 
            this.OurReferenceTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OurReferenceTxt.Location = new System.Drawing.Point(771, 77);
            this.OurReferenceTxt.Name = "OurReferenceTxt";
            this.OurReferenceTxt.Size = new System.Drawing.Size(173, 26);
            this.OurReferenceTxt.TabIndex = 3;
            // 
            // CustReferenceTxt
            // 
            this.CustReferenceTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustReferenceTxt.Location = new System.Drawing.Point(771, 107);
            this.CustReferenceTxt.Name = "CustReferenceTxt";
            this.CustReferenceTxt.Size = new System.Drawing.Size(173, 26);
            this.CustReferenceTxt.TabIndex = 4;
            // 
            // cust1Lbl
            // 
            this.cust1Lbl.AutoSize = true;
            this.cust1Lbl.Location = new System.Drawing.Point(37, 77);
            this.cust1Lbl.Name = "cust1Lbl";
            this.cust1Lbl.Size = new System.Drawing.Size(0, 13);
            this.cust1Lbl.TabIndex = 125;
            // 
            // cust2Lbl
            // 
            this.cust2Lbl.AutoSize = true;
            this.cust2Lbl.Location = new System.Drawing.Point(37, 98);
            this.cust2Lbl.Name = "cust2Lbl";
            this.cust2Lbl.Size = new System.Drawing.Size(0, 13);
            this.cust2Lbl.TabIndex = 126;
            // 
            // cust3Lbl
            // 
            this.cust3Lbl.AutoSize = true;
            this.cust3Lbl.Location = new System.Drawing.Point(37, 119);
            this.cust3Lbl.Name = "cust3Lbl";
            this.cust3Lbl.Size = new System.Drawing.Size(0, 13);
            this.cust3Lbl.TabIndex = 127;
            // 
            // cust4Lbl
            // 
            this.cust4Lbl.AutoSize = true;
            this.cust4Lbl.Location = new System.Drawing.Point(37, 140);
            this.cust4Lbl.Name = "cust4Lbl";
            this.cust4Lbl.Size = new System.Drawing.Size(0, 13);
            this.cust4Lbl.TabIndex = 128;
            // 
            // boxLabel2Txt
            // 
            this.boxLabel2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabel2Txt.Location = new System.Drawing.Point(784, 213);
            this.boxLabel2Txt.Name = "boxLabel2Txt";
            this.boxLabel2Txt.Size = new System.Drawing.Size(162, 26);
            this.boxLabel2Txt.TabIndex = 133;
            // 
            // price2Txt
            // 
            this.price2Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price2Txt.Location = new System.Drawing.Point(13, 213);
            this.price2Txt.Name = "price2Txt";
            this.price2Txt.Size = new System.Drawing.Size(72, 26);
            this.price2Txt.TabIndex = 129;
            // 
            // field2Cbx
            // 
            this.field2Cbx.DropDownWidth = 300;
            this.field2Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.field2Cbx.FormattingEnabled = true;
            this.field2Cbx.Location = new System.Drawing.Point(436, 212);
            this.field2Cbx.Name = "field2Cbx";
            this.field2Cbx.Size = new System.Drawing.Size(162, 28);
            this.field2Cbx.TabIndex = 132;
            // 
            // job2Cbx
            // 
            this.job2Cbx.DropDownWidth = 300;
            this.job2Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.job2Cbx.FormattingEnabled = true;
            this.job2Cbx.Location = new System.Drawing.Point(265, 212);
            this.job2Cbx.Name = "job2Cbx";
            this.job2Cbx.Size = new System.Drawing.Size(162, 28);
            this.job2Cbx.TabIndex = 131;
            // 
            // crop2Cbx
            // 
            this.crop2Cbx.DropDownWidth = 300;
            this.crop2Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crop2Cbx.FormattingEnabled = true;
            this.crop2Cbx.Location = new System.Drawing.Point(94, 212);
            this.crop2Cbx.Name = "crop2Cbx";
            this.crop2Cbx.Size = new System.Drawing.Size(162, 28);
            this.crop2Cbx.TabIndex = 130;
            // 
            // boxLabel3Txt
            // 
            this.boxLabel3Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabel3Txt.Location = new System.Drawing.Point(784, 246);
            this.boxLabel3Txt.Name = "boxLabel3Txt";
            this.boxLabel3Txt.Size = new System.Drawing.Size(162, 26);
            this.boxLabel3Txt.TabIndex = 138;
            // 
            // price3Txt
            // 
            this.price3Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price3Txt.Location = new System.Drawing.Point(13, 246);
            this.price3Txt.Name = "price3Txt";
            this.price3Txt.Size = new System.Drawing.Size(72, 26);
            this.price3Txt.TabIndex = 134;
            // 
            // field3Cbx
            // 
            this.field3Cbx.DropDownWidth = 300;
            this.field3Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.field3Cbx.FormattingEnabled = true;
            this.field3Cbx.Location = new System.Drawing.Point(436, 245);
            this.field3Cbx.Name = "field3Cbx";
            this.field3Cbx.Size = new System.Drawing.Size(162, 28);
            this.field3Cbx.TabIndex = 137;
            // 
            // job3Cbx
            // 
            this.job3Cbx.DropDownWidth = 300;
            this.job3Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.job3Cbx.FormattingEnabled = true;
            this.job3Cbx.Location = new System.Drawing.Point(265, 245);
            this.job3Cbx.Name = "job3Cbx";
            this.job3Cbx.Size = new System.Drawing.Size(162, 28);
            this.job3Cbx.TabIndex = 136;
            // 
            // crop3Cbx
            // 
            this.crop3Cbx.DropDownWidth = 300;
            this.crop3Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crop3Cbx.FormattingEnabled = true;
            this.crop3Cbx.Location = new System.Drawing.Point(94, 245);
            this.crop3Cbx.Name = "crop3Cbx";
            this.crop3Cbx.Size = new System.Drawing.Size(162, 28);
            this.crop3Cbx.TabIndex = 135;
            // 
            // boxLabel4Txt
            // 
            this.boxLabel4Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabel4Txt.Location = new System.Drawing.Point(784, 280);
            this.boxLabel4Txt.Name = "boxLabel4Txt";
            this.boxLabel4Txt.Size = new System.Drawing.Size(162, 26);
            this.boxLabel4Txt.TabIndex = 143;
            // 
            // price4Txt
            // 
            this.price4Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price4Txt.Location = new System.Drawing.Point(13, 280);
            this.price4Txt.Name = "price4Txt";
            this.price4Txt.Size = new System.Drawing.Size(72, 26);
            this.price4Txt.TabIndex = 139;
            // 
            // field4Cbx
            // 
            this.field4Cbx.DropDownWidth = 300;
            this.field4Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.field4Cbx.FormattingEnabled = true;
            this.field4Cbx.Location = new System.Drawing.Point(436, 279);
            this.field4Cbx.Name = "field4Cbx";
            this.field4Cbx.Size = new System.Drawing.Size(162, 28);
            this.field4Cbx.TabIndex = 142;
            // 
            // job4Cbx
            // 
            this.job4Cbx.DropDownWidth = 300;
            this.job4Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.job4Cbx.FormattingEnabled = true;
            this.job4Cbx.Location = new System.Drawing.Point(265, 279);
            this.job4Cbx.Name = "job4Cbx";
            this.job4Cbx.Size = new System.Drawing.Size(162, 28);
            this.job4Cbx.TabIndex = 141;
            // 
            // crop4Cbx
            // 
            this.crop4Cbx.DropDownWidth = 300;
            this.crop4Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crop4Cbx.FormattingEnabled = true;
            this.crop4Cbx.Location = new System.Drawing.Point(94, 279);
            this.crop4Cbx.Name = "crop4Cbx";
            this.crop4Cbx.Size = new System.Drawing.Size(162, 28);
            this.crop4Cbx.TabIndex = 140;
            // 
            // boxLabel5Txt
            // 
            this.boxLabel5Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabel5Txt.Location = new System.Drawing.Point(784, 315);
            this.boxLabel5Txt.Name = "boxLabel5Txt";
            this.boxLabel5Txt.Size = new System.Drawing.Size(162, 26);
            this.boxLabel5Txt.TabIndex = 148;
            // 
            // price5Txt
            // 
            this.price5Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price5Txt.Location = new System.Drawing.Point(13, 315);
            this.price5Txt.Name = "price5Txt";
            this.price5Txt.Size = new System.Drawing.Size(72, 26);
            this.price5Txt.TabIndex = 144;
            // 
            // field5Cbx
            // 
            this.field5Cbx.DropDownWidth = 300;
            this.field5Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.field5Cbx.FormattingEnabled = true;
            this.field5Cbx.Location = new System.Drawing.Point(436, 314);
            this.field5Cbx.Name = "field5Cbx";
            this.field5Cbx.Size = new System.Drawing.Size(162, 28);
            this.field5Cbx.TabIndex = 147;
            // 
            // job5Cbx
            // 
            this.job5Cbx.DropDownWidth = 300;
            this.job5Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.job5Cbx.FormattingEnabled = true;
            this.job5Cbx.Location = new System.Drawing.Point(265, 314);
            this.job5Cbx.Name = "job5Cbx";
            this.job5Cbx.Size = new System.Drawing.Size(162, 28);
            this.job5Cbx.TabIndex = 146;
            // 
            // crop5Cbx
            // 
            this.crop5Cbx.DropDownWidth = 300;
            this.crop5Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crop5Cbx.FormattingEnabled = true;
            this.crop5Cbx.Location = new System.Drawing.Point(94, 314);
            this.crop5Cbx.Name = "crop5Cbx";
            this.crop5Cbx.Size = new System.Drawing.Size(162, 28);
            this.crop5Cbx.TabIndex = 145;
            // 
            // boxLabel6Txt
            // 
            this.boxLabel6Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabel6Txt.Location = new System.Drawing.Point(784, 350);
            this.boxLabel6Txt.Name = "boxLabel6Txt";
            this.boxLabel6Txt.Size = new System.Drawing.Size(162, 26);
            this.boxLabel6Txt.TabIndex = 153;
            // 
            // price6Txt
            // 
            this.price6Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price6Txt.Location = new System.Drawing.Point(13, 350);
            this.price6Txt.Name = "price6Txt";
            this.price6Txt.Size = new System.Drawing.Size(72, 26);
            this.price6Txt.TabIndex = 149;
            // 
            // field6Cbx
            // 
            this.field6Cbx.DropDownWidth = 300;
            this.field6Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.field6Cbx.FormattingEnabled = true;
            this.field6Cbx.Location = new System.Drawing.Point(436, 349);
            this.field6Cbx.Name = "field6Cbx";
            this.field6Cbx.Size = new System.Drawing.Size(162, 28);
            this.field6Cbx.TabIndex = 152;
            // 
            // job6Cbx
            // 
            this.job6Cbx.DropDownWidth = 300;
            this.job6Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.job6Cbx.FormattingEnabled = true;
            this.job6Cbx.Location = new System.Drawing.Point(265, 349);
            this.job6Cbx.Name = "job6Cbx";
            this.job6Cbx.Size = new System.Drawing.Size(162, 28);
            this.job6Cbx.TabIndex = 151;
            // 
            // crop6Cbx
            // 
            this.crop6Cbx.DropDownWidth = 300;
            this.crop6Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crop6Cbx.FormattingEnabled = true;
            this.crop6Cbx.Location = new System.Drawing.Point(94, 349);
            this.crop6Cbx.Name = "crop6Cbx";
            this.crop6Cbx.Size = new System.Drawing.Size(162, 28);
            this.crop6Cbx.TabIndex = 150;
            // 
            // boxLabel7Txt
            // 
            this.boxLabel7Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabel7Txt.Location = new System.Drawing.Point(784, 385);
            this.boxLabel7Txt.Name = "boxLabel7Txt";
            this.boxLabel7Txt.Size = new System.Drawing.Size(162, 26);
            this.boxLabel7Txt.TabIndex = 158;
            // 
            // price7Txt
            // 
            this.price7Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price7Txt.Location = new System.Drawing.Point(13, 385);
            this.price7Txt.Name = "price7Txt";
            this.price7Txt.Size = new System.Drawing.Size(72, 26);
            this.price7Txt.TabIndex = 154;
            // 
            // field7Cbx
            // 
            this.field7Cbx.DropDownWidth = 300;
            this.field7Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.field7Cbx.FormattingEnabled = true;
            this.field7Cbx.Location = new System.Drawing.Point(436, 384);
            this.field7Cbx.Name = "field7Cbx";
            this.field7Cbx.Size = new System.Drawing.Size(162, 28);
            this.field7Cbx.TabIndex = 157;
            // 
            // job7Cbx
            // 
            this.job7Cbx.DropDownWidth = 300;
            this.job7Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.job7Cbx.FormattingEnabled = true;
            this.job7Cbx.Location = new System.Drawing.Point(265, 384);
            this.job7Cbx.Name = "job7Cbx";
            this.job7Cbx.Size = new System.Drawing.Size(162, 28);
            this.job7Cbx.TabIndex = 156;
            // 
            // crop7Cbx
            // 
            this.crop7Cbx.DropDownWidth = 300;
            this.crop7Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crop7Cbx.FormattingEnabled = true;
            this.crop7Cbx.Location = new System.Drawing.Point(94, 384);
            this.crop7Cbx.Name = "crop7Cbx";
            this.crop7Cbx.Size = new System.Drawing.Size(162, 28);
            this.crop7Cbx.TabIndex = 155;
            // 
            // boxLabel8Txt
            // 
            this.boxLabel8Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.boxLabel8Txt.Location = new System.Drawing.Point(784, 419);
            this.boxLabel8Txt.Name = "boxLabel8Txt";
            this.boxLabel8Txt.Size = new System.Drawing.Size(162, 26);
            this.boxLabel8Txt.TabIndex = 163;
            // 
            // price8Txt
            // 
            this.price8Txt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.price8Txt.Location = new System.Drawing.Point(13, 419);
            this.price8Txt.Name = "price8Txt";
            this.price8Txt.Size = new System.Drawing.Size(72, 26);
            this.price8Txt.TabIndex = 159;
            // 
            // field8Cbx
            // 
            this.field8Cbx.DropDownWidth = 300;
            this.field8Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.field8Cbx.FormattingEnabled = true;
            this.field8Cbx.Location = new System.Drawing.Point(436, 418);
            this.field8Cbx.Name = "field8Cbx";
            this.field8Cbx.Size = new System.Drawing.Size(162, 28);
            this.field8Cbx.TabIndex = 162;
            // 
            // job8Cbx
            // 
            this.job8Cbx.DropDownWidth = 300;
            this.job8Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.job8Cbx.FormattingEnabled = true;
            this.job8Cbx.Location = new System.Drawing.Point(265, 418);
            this.job8Cbx.Name = "job8Cbx";
            this.job8Cbx.Size = new System.Drawing.Size(162, 28);
            this.job8Cbx.TabIndex = 161;
            // 
            // crop8Cbx
            // 
            this.crop8Cbx.DropDownWidth = 300;
            this.crop8Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.crop8Cbx.FormattingEnabled = true;
            this.crop8Cbx.Location = new System.Drawing.Point(94, 418);
            this.crop8Cbx.Name = "crop8Cbx";
            this.crop8Cbx.Size = new System.Drawing.Size(162, 28);
            this.crop8Cbx.TabIndex = 160;
            // 
            // datePicker
            // 
            this.datePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(771, 45);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(173, 26);
            this.datePicker.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(722, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 18);
            this.label9.TabIndex = 164;
            this.label9.Text = "Date:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___100x20_pix_;
            this.pictureBox1.Location = new System.Drawing.Point(285, -13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(213, 56);
            this.pictureBox1.TabIndex = 165;
            this.pictureBox1.TabStop = false;
            // 
            // labelPrintChk
            // 
            this.labelPrintChk.AutoSize = true;
            this.labelPrintChk.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrintChk.Location = new System.Drawing.Point(23, 473);
            this.labelPrintChk.Name = "labelPrintChk";
            this.labelPrintChk.Size = new System.Drawing.Size(225, 24);
            this.labelPrintChk.TabIndex = 166;
            this.labelPrintChk.Text = "Print Labels Click for yes";
            this.labelPrintChk.UseVisualStyleBackColor = true;
            // 
            // grower8Cbx
            // 
            this.grower8Cbx.DropDownWidth = 300;
            this.grower8Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grower8Cbx.FormattingEnabled = true;
            this.grower8Cbx.Location = new System.Drawing.Point(609, 417);
            this.grower8Cbx.Name = "grower8Cbx";
            this.grower8Cbx.Size = new System.Drawing.Size(162, 28);
            this.grower8Cbx.TabIndex = 175;
            // 
            // grower7Cbx
            // 
            this.grower7Cbx.DropDownWidth = 300;
            this.grower7Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grower7Cbx.FormattingEnabled = true;
            this.grower7Cbx.Location = new System.Drawing.Point(609, 383);
            this.grower7Cbx.Name = "grower7Cbx";
            this.grower7Cbx.Size = new System.Drawing.Size(162, 28);
            this.grower7Cbx.TabIndex = 174;
            // 
            // grower6Cbx
            // 
            this.grower6Cbx.DropDownWidth = 300;
            this.grower6Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grower6Cbx.FormattingEnabled = true;
            this.grower6Cbx.Location = new System.Drawing.Point(609, 348);
            this.grower6Cbx.Name = "grower6Cbx";
            this.grower6Cbx.Size = new System.Drawing.Size(162, 28);
            this.grower6Cbx.TabIndex = 173;
            // 
            // grower5Cbx
            // 
            this.grower5Cbx.DropDownWidth = 300;
            this.grower5Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grower5Cbx.FormattingEnabled = true;
            this.grower5Cbx.Location = new System.Drawing.Point(609, 313);
            this.grower5Cbx.Name = "grower5Cbx";
            this.grower5Cbx.Size = new System.Drawing.Size(162, 28);
            this.grower5Cbx.TabIndex = 172;
            // 
            // grower4Cbx
            // 
            this.grower4Cbx.DropDownWidth = 300;
            this.grower4Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grower4Cbx.FormattingEnabled = true;
            this.grower4Cbx.Location = new System.Drawing.Point(609, 278);
            this.grower4Cbx.Name = "grower4Cbx";
            this.grower4Cbx.Size = new System.Drawing.Size(162, 28);
            this.grower4Cbx.TabIndex = 171;
            // 
            // grower3Cbx
            // 
            this.grower3Cbx.DropDownWidth = 300;
            this.grower3Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grower3Cbx.FormattingEnabled = true;
            this.grower3Cbx.Location = new System.Drawing.Point(609, 244);
            this.grower3Cbx.Name = "grower3Cbx";
            this.grower3Cbx.Size = new System.Drawing.Size(162, 28);
            this.grower3Cbx.TabIndex = 170;
            // 
            // grower2Cbx
            // 
            this.grower2Cbx.DropDownWidth = 300;
            this.grower2Cbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grower2Cbx.FormattingEnabled = true;
            this.grower2Cbx.Location = new System.Drawing.Point(609, 211);
            this.grower2Cbx.Name = "grower2Cbx";
            this.grower2Cbx.Size = new System.Drawing.Size(162, 28);
            this.grower2Cbx.TabIndex = 169;
            // 
            // growerCbx
            // 
            this.growerCbx.DropDownWidth = 300;
            this.growerCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerCbx.FormattingEnabled = true;
            this.growerCbx.Location = new System.Drawing.Point(608, 177);
            this.growerCbx.Name = "growerCbx";
            this.growerCbx.Size = new System.Drawing.Size(162, 28);
            this.growerCbx.TabIndex = 167;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(615, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(144, 18);
            this.label10.TabIndex = 168;
            this.label10.Text = "Grower Block Code:";
            // 
            // WorkOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(956, 510);
            this.Controls.Add(this.grower8Cbx);
            this.Controls.Add(this.grower7Cbx);
            this.Controls.Add(this.grower6Cbx);
            this.Controls.Add(this.grower5Cbx);
            this.Controls.Add(this.grower4Cbx);
            this.Controls.Add(this.grower3Cbx);
            this.Controls.Add(this.grower2Cbx);
            this.Controls.Add(this.growerCbx);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.labelPrintChk);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.datePicker);
            this.Controls.Add(this.boxLabel8Txt);
            this.Controls.Add(this.price8Txt);
            this.Controls.Add(this.field8Cbx);
            this.Controls.Add(this.job8Cbx);
            this.Controls.Add(this.crop8Cbx);
            this.Controls.Add(this.boxLabel7Txt);
            this.Controls.Add(this.price7Txt);
            this.Controls.Add(this.field7Cbx);
            this.Controls.Add(this.job7Cbx);
            this.Controls.Add(this.crop7Cbx);
            this.Controls.Add(this.boxLabel6Txt);
            this.Controls.Add(this.price6Txt);
            this.Controls.Add(this.field6Cbx);
            this.Controls.Add(this.job6Cbx);
            this.Controls.Add(this.crop6Cbx);
            this.Controls.Add(this.boxLabel5Txt);
            this.Controls.Add(this.price5Txt);
            this.Controls.Add(this.field5Cbx);
            this.Controls.Add(this.job5Cbx);
            this.Controls.Add(this.crop5Cbx);
            this.Controls.Add(this.boxLabel4Txt);
            this.Controls.Add(this.price4Txt);
            this.Controls.Add(this.field4Cbx);
            this.Controls.Add(this.job4Cbx);
            this.Controls.Add(this.crop4Cbx);
            this.Controls.Add(this.boxLabel3Txt);
            this.Controls.Add(this.price3Txt);
            this.Controls.Add(this.field3Cbx);
            this.Controls.Add(this.job3Cbx);
            this.Controls.Add(this.crop3Cbx);
            this.Controls.Add(this.boxLabel2Txt);
            this.Controls.Add(this.price2Txt);
            this.Controls.Add(this.field2Cbx);
            this.Controls.Add(this.job2Cbx);
            this.Controls.Add(this.crop2Cbx);
            this.Controls.Add(this.cust4Lbl);
            this.Controls.Add(this.cust3Lbl);
            this.Controls.Add(this.cust2Lbl);
            this.Controls.Add(this.cust1Lbl);
            this.Controls.Add(this.CustReferenceTxt);
            this.Controls.Add(this.OurReferenceTxt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.boxLabelTxt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.employeeCbx);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.printReportBtn);
            this.Controls.Add(this.priceTxt);
            this.Controls.Add(this.fieldCbx);
            this.Controls.Add(this.jobCbx);
            this.Controls.Add(this.cropCbx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.subtitleLbl);
            this.Name = "WorkOrder";
            this.Text = "Receiving Product";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cropCbx;
        private System.Windows.Forms.ComboBox jobCbx;
        private System.Windows.Forms.ComboBox fieldCbx;
        private System.Windows.Forms.TextBox priceTxt;
        private System.Windows.Forms.Button printReportBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.ComboBox employeeCbx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox boxLabelTxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox OurReferenceTxt;
        private System.Windows.Forms.TextBox CustReferenceTxt;
        private System.Windows.Forms.Label cust1Lbl;
        private System.Windows.Forms.Label cust2Lbl;
        private System.Windows.Forms.Label cust3Lbl;
        private System.Windows.Forms.Label cust4Lbl;
        private System.Windows.Forms.TextBox boxLabel2Txt;
        private System.Windows.Forms.TextBox price2Txt;
        private System.Windows.Forms.ComboBox field2Cbx;
        private System.Windows.Forms.ComboBox job2Cbx;
        private System.Windows.Forms.ComboBox crop2Cbx;
        private System.Windows.Forms.TextBox boxLabel3Txt;
        private System.Windows.Forms.TextBox price3Txt;
        private System.Windows.Forms.ComboBox field3Cbx;
        private System.Windows.Forms.ComboBox job3Cbx;
        private System.Windows.Forms.ComboBox crop3Cbx;
        private System.Windows.Forms.TextBox boxLabel4Txt;
        private System.Windows.Forms.TextBox price4Txt;
        private System.Windows.Forms.ComboBox field4Cbx;
        private System.Windows.Forms.ComboBox job4Cbx;
        private System.Windows.Forms.ComboBox crop4Cbx;
        private System.Windows.Forms.TextBox boxLabel5Txt;
        private System.Windows.Forms.TextBox price5Txt;
        private System.Windows.Forms.ComboBox field5Cbx;
        private System.Windows.Forms.ComboBox job5Cbx;
        private System.Windows.Forms.ComboBox crop5Cbx;
        private System.Windows.Forms.TextBox boxLabel6Txt;
        private System.Windows.Forms.TextBox price6Txt;
        private System.Windows.Forms.ComboBox field6Cbx;
        private System.Windows.Forms.ComboBox job6Cbx;
        private System.Windows.Forms.ComboBox crop6Cbx;
        private System.Windows.Forms.TextBox boxLabel7Txt;
        private System.Windows.Forms.TextBox price7Txt;
        private System.Windows.Forms.ComboBox field7Cbx;
        private System.Windows.Forms.ComboBox job7Cbx;
        private System.Windows.Forms.ComboBox crop7Cbx;
        private System.Windows.Forms.TextBox boxLabel8Txt;
        private System.Windows.Forms.TextBox price8Txt;
        private System.Windows.Forms.ComboBox field8Cbx;
        private System.Windows.Forms.ComboBox job8Cbx;
        private System.Windows.Forms.ComboBox crop8Cbx;
        private System.Windows.Forms.DateTimePicker datePicker;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox labelPrintChk;
        private System.Windows.Forms.ComboBox grower8Cbx;
        private System.Windows.Forms.ComboBox grower7Cbx;
        private System.Windows.Forms.ComboBox grower6Cbx;
        private System.Windows.Forms.ComboBox grower5Cbx;
        private System.Windows.Forms.ComboBox grower4Cbx;
        private System.Windows.Forms.ComboBox grower3Cbx;
        private System.Windows.Forms.ComboBox grower2Cbx;
        private System.Windows.Forms.ComboBox growerCbx;
        private System.Windows.Forms.Label label10;
    }
}