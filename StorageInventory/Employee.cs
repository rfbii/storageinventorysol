﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Diagnostics;

namespace StorageInventory
{
    public partial class Employee : Form
    {
        #region Vars
        //Set the global variables for delimiter character.
        char delimiter = '~';
        bool editBln = false;
        bool warningBln = true;

        //Set the path of the program & save files.
        private string xmlFileName = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";

        //Create the employeesData object.
        MainMenu.EmployeesData employeesData = new MainMenu.EmployeesData();

        private ArrayList ActiveArl;
        private ArrayList InactiveArl;
        private ArrayList PreTaxAdjustmentsArl;
        #endregion

        #region Constructor
        public Employee()
        {
            InitializeComponent();
            employeesData = new MainMenu.EmployeesData();
            FillSavedEmployeesLst(true);

            //Make form non-resizable.
            //this.MinimumSize = this.MaximumSize = this.Size;
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;

            //Fill the employee data from XML.
            FillEmployeeDataFromXML();
            CompanyNameData.Text = MainMenu.CompanyStr;
        }
        #endregion

        #region Form Functions.
        #region KeyDown Functions (For Enter-As-Tab Functionality)
        private void codeTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void nameTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void address1Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void address2Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void city2Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void state2Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void zip2Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void ssn2Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void hourlyWageTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void saveBtn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                saveBtn_Click(null, null);
        }
        private void Group_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        //private void saveEditedBtn_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        saveEditedBtn_Click(null, null);
        //}
        //private void newPreTaxAdjustmentCodeTxt_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        this.GetNextControl((Control)sender, true).Focus();
        //}
        //private void newPreTaxAdjustmentDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        this.GetNextControl((Control)sender, true).Focus();
        //}
        //private void newPreTaxAdjustmentAmountTxt_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        this.GetNextControl((Control)sender, true).Focus();
        //}
        //private void saveNewPreTaxAdjustmentBtn_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        saveNewPreTaxAdjustmentBtn_Click(null, null);
        //}
        //private void editPreTaxAdjustmentCodeTxt_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        this.GetNextControl((Control)sender, true).Focus();
        //}
        //private void editPreTaxAdjustmentDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        this.GetNextControl((Control)sender, true).Focus();
        //}
        //private void editPreTaxAdjustmentAmountTxt_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        this.GetNextControl((Control)sender, true).Focus();
        //}
        //private void saveEditedPreTaxAdjustmentBtn_KeyDown(object sender, KeyEventArgs e)
        //{
        //    if (e.KeyCode.ToString() == "Return")
        //        saveEditedPreTaxAdjustmentBtn_Click(null, null);
        //}
        #endregion

        #region Input Validation Functions (Checking for proper data type inputs).
        private void codeTxt_Leave(object sender, EventArgs e)
        {
            if (!InputStringContainsChar(codeTxt, delimiter))
            {
                //Check for a unique employee code.
                foreach (MainMenu.EmployeeData tempEmployeeData in employeesData.EmployeesDataArl)
                {
                    if (tempEmployeeData.CodeStr == this.codeTxt.Text.Trim())
                    {
                        MessageBox.Show("This employee code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //Focus on the line the user needs to change.
                        this.codeTxt.Focus();
                        this.codeTxt.Text = string.Empty;

                        break;
                    }
                }
            }
        }
        private void editCodeTxt_Leave(object sender, EventArgs e)
        {/*af
            if (!InputStringContainsChar(editCodeTxt, delimiter))
            {
                //Check for a unique employee code.
                for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
                {
                    MainMenu.EmployeeData tempEmployeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                    if (tempEmployeeData.CodeStr == this.editCodeTxt.Text.Trim() && tempEmployeeData.KeyInt.ToString() != this.keyTxt.Text)
                    {
                        MessageBox.Show("This employee code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        //Focus on the line the user needs to change.
                        this.editCodeTxt.Focus();
                        this.editCodeTxt.SelectAll();

                        break;
                    }
                }
            }*/
        }
        private void newPreTaxAdjustmentCodeTxt_Leave(object sender, EventArgs e)
        {/*af
			if (!InputStringContainsChar(newPreTaxAdjustmentCodeTxt, delimiter))
			{
				foreach (MainMenu.EmployeeData tempEmployeeData in employeesData.EmployeesDataArl)
				{
					if (tempEmployeeData.KeyInt.ToString() == this.keyTxt.Text)
					{
						//Check for a unique employee code.
						foreach (MainMenu.EmployeePreTaxAdjustmentData tempEmployeePreTaxAdjustmentData in tempEmployeeData.EmployeePreTaxAdjustmentsDataArl)
						{
							if (tempEmployeePreTaxAdjustmentData.CodeStr == this.newPreTaxAdjustmentCodeTxt.Text.Trim())
							{
								MessageBox.Show("This pre-tax adjustment code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

								//Focus on the line the user needs to change.
								this.newPreTaxAdjustmentCodeTxt.Focus();
								this.newPreTaxAdjustmentCodeTxt.Text = string.Empty;

								break;
							}
						}

						break;
					}
				}
			}*/
        }
        private void editPreTaxAdjustmentCodeTxt_Leave(object sender, EventArgs e)
        {/*af
			if (!InputStringContainsChar(editPreTaxAdjustmentCodeTxt, delimiter))
			{
				//Check for a unique employee code.
				for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
				{
					MainMenu.EmployeeData tempEmployeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

					if (tempEmployeeData.KeyInt.ToString() == this.keyTxt.Text)
					{
						//Check for a unique employee code.
						for (int j = 0; j < tempEmployeeData.EmployeePreTaxAdjustmentsDataArl.Count; j++)
						{
							MainMenu.EmployeePreTaxAdjustmentData tempEmployeePreTaxAdjustmentData = (MainMenu.EmployeePreTaxAdjustmentData)tempEmployeeData.EmployeePreTaxAdjustmentsDataArl[j];

							if (tempEmployeePreTaxAdjustmentData.CodeStr == this.editPreTaxAdjustmentCodeTxt.Text.Trim() && tempEmployeePreTaxAdjustmentData.KeyInt.ToString() != this.preTaxAdjustmentKeyTxt.Text)
							{
								MessageBox.Show("This pre-tax adjustment code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

								//Focus on the line the user needs to change.
								this.editPreTaxAdjustmentCodeTxt.Focus();
								this.editPreTaxAdjustmentCodeTxt.SelectAll();

								break;
							}
						}

						break;
					}
				}
			}*/
        }
        #endregion

        #region List Box Functions
        private void savedEmployeesLst_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (savedEmployeesLst.SelectedIndex != -1)
            {
                //Clear out the new entry spaces.
                this.codeTxt.Text = string.Empty;
                this.nameTxt.Text = string.Empty;
                this.address1Txt.Text = string.Empty;
                this.address2Txt.Text = string.Empty;
                this.city2Txt.Text = string.Empty;
                this.state2Txt.Text = string.Empty;
                this.zip2Txt.Text = string.Empty;
                this.SSN2Txt.Text = string.Empty;
                this.hourlyWageTxt.Text = string.Empty;
                this.groupTxt.Text = string.Empty;
                editBln = true;
                warningBln = true;

                if (activeRdo.Checked)
                {
                    //MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)ActiveArl[savedEmployeesLst.SelectedIndex];
                    MainMenu.EmployeeData employeeData = new MainMenu.EmployeeData();
                    //for loop on EmployeeArl and match Code to first part of employeeCbx.SelectedItem

                    for (int i = 0; i < ActiveArl.Count; i++)
                    {
                        MainMenu.EmployeeData tempData = (MainMenu.EmployeeData)ActiveArl[i];

                        if (tempData.CodeStr == ParseTildaString(savedEmployeesLst.SelectedItem.ToString(), 0).Trim())
                        {
                            employeeData = tempData;
                            break;
                        }
                    }
                    //Fill the Edit entry boxes.
                    codeTxt.Text = employeeData.CodeStr;
                    nameTxt.Text = employeeData.NameStr;
                    address1Txt.Text = employeeData.Address1Str;
                    address2Txt.Text = employeeData.Address2Str;
                    city2Txt.Text = employeeData.CityStr;
                    state2Txt.Text = employeeData.StateStr;
                    zip2Txt.Text = employeeData.ZipStr;
                    SSN2Txt.Text = employeeData.SSNStr;
                    hourlyWageTxt.Text = DisplayFormat((decimal)employeeData.HourlyWageDbl);
                    groupTxt.Text = employeeData.GroupStr;
                    if (employeeData.OvertimePerEmployeeBln)
                    {
                        overtimeYesRdo.Checked = true;
                        overtimeNoRdo.Checked = false;
                    }
                    else
                    {
                        overtimeYesRdo.Checked = false;
                        overtimeNoRdo.Checked = true;
                    }
                    keyTxt.Text = employeeData.KeyInt.ToString();
                }
                else
                {
                    //MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)InactiveArl[savedEmployeesLst.SelectedIndex];
                    MainMenu.EmployeeData employeeData = new MainMenu.EmployeeData();
                    //for loop on EmployeeArl and match Code to first part of employeeCbx.SelectedItem

                    for (int i = 0; i < InactiveArl.Count; i++)
                    {
                        MainMenu.EmployeeData tempData = (MainMenu.EmployeeData)InactiveArl[i];

                        if (tempData.CodeStr == ParseTildaString(savedEmployeesLst.SelectedItem.ToString(), 0).Trim())
                        {
                            employeeData = tempData;
                            break;
                        }
                    }

                    //Fill the Edit entry boxes.
                    codeTxt.Text = employeeData.CodeStr;
                    nameTxt.Text = employeeData.NameStr;
                    address1Txt.Text = employeeData.Address1Str;
                    address2Txt.Text = employeeData.Address2Str;
                    city2Txt.Text = employeeData.CityStr;
                    state2Txt.Text = employeeData.StateStr;
                    zip2Txt.Text = employeeData.ZipStr;
                    SSN2Txt.Text = employeeData.SSNStr;
                    hourlyWageTxt.Text = DisplayFormat((decimal)employeeData.HourlyWageDbl);
                    groupTxt.Text = employeeData.GroupStr;
                    //overtimeByAverageWageChk.Checked = employeeData.OvertimeByAverageWageBln;
                    if (employeeData.OvertimePerEmployeeBln)
                    {
                        overtimeYesRdo.Checked = true;
                        overtimeNoRdo.Checked = false;
                    }
                    else
                    {
                        overtimeYesRdo.Checked = false;
                        overtimeNoRdo.Checked = true;
                    }
                    keyTxt.Text = employeeData.KeyInt.ToString();
                }

                //Extrapolate the selected code & description.
                string selectedEntry = savedEmployeesLst.SelectedItem.ToString();
                string[] entryPieces = selectedEntry.Split(delimiter);
                string selectedCode = entryPieces[0].Trim();
                string selectedName = entryPieces[1].Trim();

                //Fill the Edit entry boxes.
                codeTxt.Text = selectedCode;
                nameTxt.Text = selectedName;

                //Enable the entry boxes & button.
                //editCodeTxt.Enabled = true;
                //editCodeLbl.Enabled = true;
                //editDescriptionTxt.Enabled = true;
                //editDescriptionLbl.Enabled = true;
                //editHourlyWageLbl.Enabled = true;
                //editHourlyWageTxt.Enabled = true;
                //saveEditedBtn.Enabled = true;
                //enableDisableBtn.Enabled = true;

                //savedPreTaxAdjustmentsLst.Enabled = true;
                //saveNewPreTaxAdjustmentBtn.Enabled = true;
                //newPreTaxAdjustmentCodeTxt.Enabled = true;
                //newPreTaxAdjustmentDescriptionTxt.Enabled = true;
                //newPreTaxAdjustmentAmountTxt.Enabled = true;

                //savedPreTaxAdjustmentsLbl.Enabled = true;
                //newPreTaxAdjustmentCodeLbl.Enabled = true;
                //newPreTaxAdjustmentDescriptionLbl.Enabled = true;
                //newPreTaxAdjustmentAmountLbl.Enabled = true;

                //FillSavedPreTaxAdjustmentsLst();
            }
            else
            {
                //Clear the Edit entry boxes.
                this.codeTxt.Text = string.Empty;
                this.nameTxt.Text = string.Empty;
                this.address1Txt.Text = string.Empty;
                this.address2Txt.Text = string.Empty;
                this.city2Txt.Text = string.Empty;
                this.state2Txt.Text = string.Empty;
                this.zip2Txt.Text = string.Empty;
                this.SSN2Txt.Text = string.Empty;
                this.hourlyWageTxt.Text = string.Empty;
                this.groupTxt.Text = string.Empty;
                //overtimeByAverageWageChk.Checked = false;
                keyTxt.Text = string.Empty;

                //Disable the entry boxes & button.
                //editCodeTxt.Enabled = false;
                //editCodeLbl.Enabled = false;
                //editDescriptionTxt.Enabled = false;
                //editDescriptionLbl.Enabled = false;
                //editHourlyWageLbl.Enabled = false;
                //editHourlyWageTxt.Enabled = false;
                //saveEditedBtn.Enabled = false;
                //enableDisableBtn.Enabled = false;

                //savedPreTaxAdjustmentsLst.Items.Clear();
                //savedPreTaxAdjustmentsLst.SelectedIndex = -1;
                //savedPreTaxAdjustmentsLst.ClearSelected();

                //savedPreTaxAdjustmentsLst.Enabled = false;
                //saveNewPreTaxAdjustmentBtn.Enabled = false;
                //saveEditedPreTaxAdjustmentBtn.Enabled = false;
                //deletePreTaxAdjustmentBtn.Enabled = false;
                //newPreTaxAdjustmentCodeTxt.Enabled = false;
                //newPreTaxAdjustmentDescriptionTxt.Enabled = false;
                //newPreTaxAdjustmentAmountTxt.Enabled = false;
                //editPreTaxAdjustmentCodeTxt.Enabled = false;
                //editPreTaxAdjustmentDescriptionTxt.Enabled = false;
                //editPreTaxAdjustmentAmountTxt.Enabled = false;

                //newPreTaxAdjustmentCodeTxt.Text = string.Empty;
                //newPreTaxAdjustmentDescriptionTxt.Text = string.Empty;
                //newPreTaxAdjustmentAmountTxt.Text = string.Empty;
                //editPreTaxAdjustmentCodeTxt.Text = string.Empty;
                //editPreTaxAdjustmentDescriptionTxt.Text = string.Empty;
                //editPreTaxAdjustmentAmountTxt.Text = string.Empty;
                //preTaxAdjustmentKeyTxt.Text = string.Empty;

                //savedPreTaxAdjustmentsLbl.Enabled = false;
                //newPreTaxAdjustmentCodeLbl.Enabled = false;
                //newPreTaxAdjustmentDescriptionLbl.Enabled = false;
                //newPreTaxAdjustmentAmountLbl.Enabled = false;
                //editPreTaxAdjustmentCodeLbl.Enabled = false;
                //editPreTaxAdjustmentDescriptionLbl.Enabled = false;
                //editPreTaxAdjustmentAmountLbl.Enabled = false;
            }
        }
        private void activeRdo_CheckedChanged(object sender, EventArgs e)
        {
            if (activeRdo.Checked)
            {
                deleteBtn.Text = "Delete";

                FillSavedEmployeesLst(activeRdo.Checked);
            }
            else
            {
                deleteBtn.Text = "Enable";

                FillSavedEmployeesLst(activeRdo.Checked);
            }
        }
        private void inactiveRdo_CheckedChanged(object sender, EventArgs e)
        {
            if (inactiveRdo.Checked)
            {
                deleteBtn.Text = "Enable";

                FillSavedEmployeesLst(activeRdo.Checked);
            }
            else
            {
                deleteBtn.Text = "Delete";

                FillSavedEmployeesLst(activeRdo.Checked);
            }
        }
        private void overtimeYesRdo_CheckedChanged(object sender, EventArgs e)
        {
            if (overtimeYesRdo.Checked)
            {
                overtimeNoRdo.Checked = false;
            }
            else
            {
                overtimeNoRdo.Checked = true;
            }
        }
        private void overtimeNoRdo_CheckedChanged(object sender, EventArgs e)
        {
            if (overtimeNoRdo.Checked)
            {
                overtimeYesRdo.Checked = false;
            }
            else
            {
                overtimeYesRdo.Checked = true;
            }

        }
        private void savedPreTaxAdjustmentsLst_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (savedPreTaxAdjustmentsLst.SelectedIndex != -1)
            {
                //Clear out the new entry spaces.
                this.newPreTaxAdjustmentCodeTxt.Text = string.Empty;
                this.newPreTaxAdjustmentDescriptionTxt.Text = string.Empty;
                this.newPreTaxAdjustmentAmountTxt.Text = string.Empty;

                MainMenu.EmployeePreTaxAdjustmentData employeePreTaxAdjustmentData = (MainMenu.EmployeePreTaxAdjustmentData)PreTaxAdjustmentsArl[savedPreTaxAdjustmentsLst.SelectedIndex];

                //Fill the Edit entry boxes.
                editPreTaxAdjustmentCodeTxt.Text = employeePreTaxAdjustmentData.CodeStr;
                editPreTaxAdjustmentDescriptionTxt.Text = employeePreTaxAdjustmentData.DescriptionStr;
                editPreTaxAdjustmentAmountTxt.Text = DisplayFormat((decimal)employeePreTaxAdjustmentData.AmountDbl);
                preTaxAdjustmentKeyTxt.Text = employeePreTaxAdjustmentData.KeyInt.ToString();

                //Enable the entry boxes & button.
                editPreTaxAdjustmentCodeTxt.Enabled = true;
                editPreTaxAdjustmentDescriptionTxt.Enabled = true;
                editPreTaxAdjustmentAmountTxt.Enabled = true;

                editPreTaxAdjustmentCodeLbl.Enabled = true;
                editPreTaxAdjustmentDescriptionLbl.Enabled = true;
                editPreTaxAdjustmentAmountLbl.Enabled = true;

                saveEditedPreTaxAdjustmentBtn.Enabled = true;
                deletePreTaxAdjustmentBtn.Enabled = true;
            }
            else
            {
                //Clear the Edit entry boxes.
                editPreTaxAdjustmentCodeTxt.Text = string.Empty;
                editPreTaxAdjustmentDescriptionTxt.Text = string.Empty;
                editPreTaxAdjustmentAmountTxt.Text = string.Empty;
                preTaxAdjustmentKeyTxt.Text = string.Empty;

                //Disable the entry boxes & button.
                editPreTaxAdjustmentCodeTxt.Enabled = false;
                editPreTaxAdjustmentDescriptionTxt.Enabled = false;
                editPreTaxAdjustmentAmountTxt.Enabled = false;

                editPreTaxAdjustmentCodeLbl.Enabled = false;
                editPreTaxAdjustmentDescriptionLbl.Enabled = false;
                editPreTaxAdjustmentAmountLbl.Enabled = false;

                saveEditedPreTaxAdjustmentBtn.Enabled = false;
                deletePreTaxAdjustmentBtn.Enabled = false;
            }
        }
        #endregion

        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion


        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (!editBln)
            {
                //add new employee
                //if (hourlyWageTxt.Text != "")
                //{
                    //Check the code and description to make sure the user hasn't entered the delimiter 
                    //  character we're using to parse from the listbox.
                    if (!InputStringContainsChar(this.codeTxt, delimiter))
                    {
                        if (!InputStringContainsChar(this.nameTxt, delimiter))
                        {
                            //Fill the list from XML.
                            FillEmployeeDataFromXML();

                            //Add the new Employee Data into the list.
                            MainMenu.EmployeeData employeeData = MainMenu.FindOrAddEmployeeData(employeesData, this.codeTxt.Text.Trim(), this.nameTxt.Text.Trim(), this.address1Txt.Text.Trim(), this.address2Txt.Text.Trim(), this.city2Txt.Text.Trim(), this.state2Txt.Text.Trim(), this.zip2Txt.Text.Trim(), this.SSN2Txt.Text.Trim(), employeesData.NextKeyInt, this.groupTxt.Text.Trim());
                            //employeeData.HourlyWageDbl = Convert.ToDouble(hourlyWageTxt.Text);
                            //employeeData.OvertimeByAverageWageBln = overtimeByAverageWageChk.Checked;
                            employeeData.ActiveBln = activeRdo.Checked;

                            //Save the new data to XML.
                            SaveEmployeeDataToXML();

                            //Refill the GUI list.
                            FillSavedEmployeesLst(activeRdo.Checked);

                            //Clear the freshly added new code & description.
                            codeTxt.Clear();
                            nameTxt.Clear();
                            address1Txt.Clear();
                            address2Txt.Clear();
                            city2Txt.Clear();
                            state2Txt.Clear();
                            zip2Txt.Clear();
                            SSN2Txt.Clear();
                            hourlyWageTxt.Clear();
                            groupTxt.Clear();
                            //overtimeByAverageWageChk.Checked = false;

                            //Clear the edit entry spaces.
                            savedEmployeesLst.SelectedIndex = -1;

                            //Put focus back on 'Code' entry box.
                            codeTxt.Focus();
                        }
                    }
                //}
                //else
                //{
                //    MessageBox.Show("Hourly Wage cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //    hourlyWageTxt.Focus();
                //    hourlyWageTxt.SelectAll();
                //}
            }
            else
            {
                //Show warning panel on Edits
                //if (warningBln)
                //{
                //    warningPnl.Visible = true;
                //    saveBtn.Enabled = false;
                //    codeTxt.Enabled = false;
                //    nameTxt.Enabled = false;
                //    address1Txt.Enabled = false;
                //    address2Txt.Enabled = false;
                //    cityTxt.Enabled = false;
                //    stateTxt.Enabled = false;
                //    zipTxt.Enabled = false;
                //    SSNTxt.Enabled = false;
                //    hourlyWageTxt.Enabled = false;
                //    savedEmployeesLst.Enabled = false;
                //}
                //else
                //{
                //edit exixting employees
                //if (hourlyWageTxt.Text != "")
                //{
                    //Check the code and description to make sure the user hasn't entered the delimiter 
                    //  character we're using to parse from the listbox.
                    if (!InputStringContainsChar(this.codeTxt, delimiter) && this.keyTxt.Text != "")
                    {
                        if (!InputStringContainsChar(this.nameTxt, delimiter))
                        {
                            //Fill the list from XML.
                            FillEmployeeDataFromXML();

                            //Save the edited employee.
                            MainMenu.SaveEditedEmployeeData(employeesData, this.codeTxt.Text.Trim(), this.nameTxt.Text.Trim(), this.address1Txt.Text.Trim(), this.address2Txt.Text.Trim(), this.city2Txt.Text.Trim(), this.state2Txt.Text.Trim(), this.zip2Txt.Text.Trim(), this.SSN2Txt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), Convert.ToDouble(hourlyWageTxt.Text), overtimeYesRdo.Checked, activeRdo.Checked, this.groupTxt.Text);

                            //Save the new data to XML.
                            SaveEmployeeDataToXML();

                            //Refill the GUI list.
                            FillSavedEmployeesLst(activeRdo.Checked);

                            //Clear the edit entry spaces.
                            savedEmployeesLst.SelectedIndex = -1;

                            //Put focus back on 'Code' entry box.
                            codeTxt.Focus();
                        }
                    }
                //}
                //else
                //{
                //    MessageBox.Show("Hourly Wage cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                //    hourlyWageTxt.Focus();
                //    hourlyWageTxt.SelectAll();
                //}
                //}
            }
            editBln = false;
        }

        #region Employee Functions.
        private void codeTxt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void nameTxt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void address1Txt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void address2Txt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void city2Txt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void state2Txt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void zip2Txt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void SSN2Txt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void hourlyWageTxt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        private void hourlyWageTxt_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != "")
            {
                ValidateQuantity((TextBox)sender, "Hourly Wage");
            }
        }
        private void payOvertime_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;
        }
        #endregion

        #region Edit Employee Functions.
        //private void saveEditedBtn_Click(object sender, EventArgs e)
        //{
        //    if (hourlyWageTxt.Text != "")
        //    {
        //        //Check the code and description to make sure the user hasn't entered the delimiter 
        //        //  character we're using to parse from the listbox.
        //        if (!InputStringContainsChar(this.codeTxt, delimiter) && this.keyTxt.Text != "")
        //        {
        //            if (!InputStringContainsChar(this.nameTxt, delimiter))
        //            {
        //                //Fill the list from XML.
        //                FillEmployeeDataFromXML();

        //                //Save the edited employee.
        //                MainMenu.SaveEditedEmployeeData(employeesData, this.codeTxt.Text.Trim(), this.nameTxt.Text.Trim(), this.address1Txt.Text.Trim(), this.address2Txt.Text.Trim(), this.cityTxt.Text.Trim(), this.stateTxt.Text.Trim(), this.zipTxt.Text.Trim(), this.SSNTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), Convert.ToDouble(hourlyWageTxt.Text), overtimeByAverageWageChk.Checked, activeRdo.Checked);

        //                //Save the new data to XML.
        //                SaveEmployeeDataToXML();

        //                //Refill the GUI list.
        //                FillSavedEmployeesLst(activeRdo.Checked);

        //                //Clear the edit entry spaces.
        //                savedEmployeesLst.SelectedIndex = -1;

        //                //Put focus back on 'Code' entry box.
        //                codeTxt.Focus();
        //            }
        //        }
        //    }
        //    else
        //    {
        //        MessageBox.Show("Hourly Wage cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

        //        hourlyWageTxt.Focus();
        //        hourlyWageTxt.SelectAll();
        //    }
        //}
        //private void enableDisableBtn_Click(object sender, EventArgs e)
        //{
        //    if (this.keyTxt.Text != "")
        //    {
        //        //Fill the list from XML.
        //        FillEmployeeDataFromXML();

        //        //Enable/Disable out the selected employee.
        //        MainMenu.EmployeeData employeeData = MainMenu.FindEmployeeData(employeesData, this.editCodeTxt.Text, this.editDescriptionTxt.Text, Convert.ToInt32(this.keyTxt.Text));
        //        employeeData.ActiveBln = !activeRdo.Checked;

        //        //Save the new data to XML.
        //        SaveEmployeeDataToXML();

        //        //Refill the GUI list.
        //        FillSavedEmployeesLst(activeRdo.Checked);

        //        //Clear the edit entry spaces.
        //        savedEmployeesLst.SelectedIndex = -1;

        //        //Put focus back on 'code' entry box.
        //        codeTxt.Focus();
        //    }
        //}
        //private void editHourlyWageTxt_Leave(object sender, EventArgs e)
        //{
        //    if (((TextBox)sender).Text != "")
        //    {
        //        ValidateQuantity((TextBox)sender, "Hourly Wage");
        //    }
        //}
        #endregion

        #region New Pre-Tax Adjustment Functions
        private void saveNewPreTaxAdjustmentBtn_Click(object sender, EventArgs e)
        {
            if (newPreTaxAdjustmentAmountTxt.Text != "")
            {
                //Check the code and description to make sure the user hasn't entered the delimiter 
                //  character we're using to parse from the listbox.
                if (!InputStringContainsChar(this.newPreTaxAdjustmentCodeTxt, delimiter))
                {
                    if (!InputStringContainsChar(this.newPreTaxAdjustmentDescriptionTxt, delimiter))
                    {
                        for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
                        {
                            MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                            if (employeeData.KeyInt.ToString() == this.keyTxt.Text)
                            {
                                //Add the new Employee Data into the list.
                                MainMenu.EmployeePreTaxAdjustmentData employeePreTaxAdjustmentData = MainMenu.FindOrAddEmployeePreTaxAdjustmentData(employeeData, this.newPreTaxAdjustmentCodeTxt.Text.Trim(), this.newPreTaxAdjustmentDescriptionTxt.Text.Trim(), employeeData.NextKeyInt);
                                employeePreTaxAdjustmentData.AmountDbl += Convert.ToDouble(newPreTaxAdjustmentAmountTxt.Text);

                                //Save the new data to XML.
                                SaveEmployeeDataToXML();

                                //Refill the GUI list.
                                FillSavedPreTaxAdjustmentsLst();

                                //Clear the freshly added new code & description.
                                newPreTaxAdjustmentCodeTxt.Clear();
                                newPreTaxAdjustmentDescriptionTxt.Clear();
                                newPreTaxAdjustmentAmountTxt.Clear();

                                //Clear the edit entry spaces.
                                savedPreTaxAdjustmentsLst.SelectedIndex = -1;

                                //Put focus back on 'Code' entry box.
                                newPreTaxAdjustmentCodeTxt.Focus();

                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Pre-Tax Adjustment amount cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                newPreTaxAdjustmentAmountTxt.Focus();
                newPreTaxAdjustmentAmountTxt.SelectAll();
            }
        }
        private void newPreTaxAdjustmentCodeTxt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedPreTaxAdjustmentsLst.SelectedIndex = -1;
        }
        private void newPreTaxAdjustmentDescriptionTxt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedPreTaxAdjustmentsLst.SelectedIndex = -1;
        }
        private void newPreTaxAdjustmentAmountTxt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces.
            savedPreTaxAdjustmentsLst.SelectedIndex = -1;
        }
        private void newPreTaxAdjustmentAmountTxt_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != "")
            {
                ValidateQuantity((TextBox)sender, "Amount");
            }
        }
        #endregion

        #region Edit Pre-Tax Adjustment Functions
        private void editPreTaxAdjustmentAmountTxt_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != "")
            {
                ValidateQuantity((TextBox)sender, "Amount");
            }
        }
        private void saveEditedPreTaxAdjustmentBtn_Click(object sender, EventArgs e)
        {
            if (editPreTaxAdjustmentAmountTxt.Text != "")
            {
                //Check the code and description to make sure the user hasn't entered the delimiter 
                //  character we're using to parse from the listbox.
                if (!InputStringContainsChar(this.editPreTaxAdjustmentCodeTxt, delimiter) && this.preTaxAdjustmentKeyTxt.Text != "")
                {
                    if (!InputStringContainsChar(this.editPreTaxAdjustmentDescriptionTxt, delimiter))
                    {
                        for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
                        {
                            MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                            if (employeeData.KeyInt.ToString() == this.keyTxt.Text)
                            {
                                //Save the edited employee.
                                MainMenu.SaveEditedEmployeePreTaxAdjustmentData(employeeData, this.editPreTaxAdjustmentCodeTxt.Text.Trim(), this.editPreTaxAdjustmentDescriptionTxt.Text.Trim(), Convert.ToInt32(this.preTaxAdjustmentKeyTxt.Text), Convert.ToDouble(editPreTaxAdjustmentAmountTxt.Text));

                                //Save the new data to XML.
                                SaveEmployeeDataToXML();

                                //Refill the GUI list.
                                FillSavedPreTaxAdjustmentsLst();

                                //Clear the edit entry spaces.
                                savedPreTaxAdjustmentsLst.SelectedIndex = -1;

                                //Put focus back on 'Code' entry box.
                                newPreTaxAdjustmentCodeTxt.Focus();

                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Pre-Tax Adjustment amount cannot be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                editPreTaxAdjustmentAmountTxt.Focus();
                editPreTaxAdjustmentAmountTxt.SelectAll();
            }
        }
        private void deletePreTaxAdjustmentBtn_Click(object sender, EventArgs e)
        {
            if (this.preTaxAdjustmentKeyTxt.Text != "")
            {
                for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
                {
                    MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                    if (employeeData.KeyInt.ToString() == this.keyTxt.Text)
                    {
                        for (int j = 0; j < employeeData.EmployeePreTaxAdjustmentsDataArl.Count; j++)
                        {
                            MainMenu.EmployeePreTaxAdjustmentData employeePreTaxAdjustmentData = (MainMenu.EmployeePreTaxAdjustmentData)employeeData.EmployeePreTaxAdjustmentsDataArl[j];

                            if (employeePreTaxAdjustmentData.KeyInt.ToString() == this.preTaxAdjustmentKeyTxt.Text)
                            {
                                employeeData.EmployeePreTaxAdjustmentsDataArl.RemoveAt(j);

                                break;
                            }
                        }

                        break;
                    }
                }

                //Save the new data to XML.
                SaveEmployeeDataToXML();

                //Refill the GUI list.
                FillSavedPreTaxAdjustmentsLst();

                //Clear the edit entry spaces.
                savedPreTaxAdjustmentsLst.SelectedIndex = -1;

                //Put focus back on 'Code' entry box.
                newPreTaxAdjustmentCodeTxt.Focus();
            }
        }
        #endregion

        #region Helper Functions.
        private void FillSavedEmployeesLst(bool activeBln)
        {
            //Clear out the Saved Employees List.
            savedEmployeesLst.ClearSelected();
            savedEmployeesLst.Items.Clear();
            savedEmployeesLst.SelectedIndex = -1;

            ActiveArl = new ArrayList();
            InactiveArl = new ArrayList();

            FileInfo fileInfo = new FileInfo(xmlFileName);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                MainMenu.EmployeesData employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
                fileStream.Close();
                employeesData.EmployeesDataArl.Sort();
                for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
                {
                    MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                    if (employeeData.ActiveBln == activeBln)
                    {
                        savedEmployeesLst.Items.Add(employeeData.CodeStr + " ~ " + employeeData.NameStr);

                        savedEmployeesLst.SelectedValue = employeeData.KeyInt.ToString();
                    }

                    if (employeeData.ActiveBln)
                    {
                        ActiveArl.Add(employeeData);
                    }
                    else
                    {
                        InactiveArl.Add(employeeData);
                    }
                }
            }
        }
        private void FillSavedPreTaxAdjustmentsLst()
        {/*af
			//Clear out the Saved Pre-Tax Adjustments List.
			savedPreTaxAdjustmentsLst.ClearSelected();
			savedPreTaxAdjustmentsLst.Items.Clear();
			savedPreTaxAdjustmentsLst.SelectedIndex = -1;

			PreTaxAdjustmentsArl = new ArrayList();

			for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
			{
				MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

				if (employeeData.KeyInt.ToString() == this.keyTxt.Text)
				{
					for (int j = 0; j < employeeData.EmployeePreTaxAdjustmentsDataArl.Count; j++)
					{
						MainMenu.EmployeePreTaxAdjustmentData employeePreTaxAdjustmentData = (MainMenu.EmployeePreTaxAdjustmentData)employeeData.EmployeePreTaxAdjustmentsDataArl[j];

						savedPreTaxAdjustmentsLst.Items.Add(employeePreTaxAdjustmentData.CodeStr + " ~ " + employeePreTaxAdjustmentData.NameStr + " ~ " + employeePreTaxAdjustmentData.AmountDbl.ToString());

						PreTaxAdjustmentsArl.Add(employeePreTaxAdjustmentData);
					}
				}
			}*/
        }
        private void FillEmployeeDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName);

            if (fileInfo.Exists)
            {

                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void SaveEmployeeDataToXML()
        {
            //Serialize (convert an object instance to an XML document):
            XmlSerializer xmlSerializer = new XmlSerializer(employeesData.GetType());
            // Create an XmlTextWriter using a FileStream.
            Stream fileStream2 = new FileStream(xmlFileName, FileMode.Create);
            XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
            // Serialize using the XmlTextWriter.
            xmlSerializer.Serialize(xmlWriter, employeesData);
            xmlWriter.Flush();
            xmlWriter.Close();
        }
        //Validation Function(s).
        private bool InputStringContainsChar(TextBox textBoxToCheck, char charToCheckFor)
        {
            if (textBoxToCheck.Text.Contains(charToCheckFor))
            {
                MessageBox.Show("Input cannot contain '" + charToCheckFor + "' characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                textBoxToCheck.Focus();
                textBoxToCheck.SelectAll();

                return true;
            }
            else
                return false;
        }
        private void ValidateQuantity(TextBox textBoxToCheck, string textBoxName)
        {
            try
            {
                decimal temp = decimal.Parse(textBoxToCheck.Text);

                if (textBoxToCheck.Text != "" && textBoxToCheck.Text.IndexOf(",") == -1)
                {
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
                else if (textBoxToCheck.Text.IndexOf(",") != -1)
                {
                    textBoxToCheck.Text.Replace(",", "");
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show(this, "Invalid " + textBoxName + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxToCheck.Focus();
                textBoxToCheck.SelectAll();
            }
        }

        //Formating Function(s).
        public static string DisplayFormat(decimal value)
        {
            if (value == 0)
                return value.ToString();
            else
                return decimal.Round(value, 2).ToString("#,###,##0.##");
        }
        public static string ParseTildaString(string stringToParse, int field)
        {
            string[] stringSplit = stringToParse.Split('~');

            if (stringSplit.Length <= field)
                return "";
            else
                return stringSplit[field];
        }
        #endregion

        private void warnYesBtn_Click(object sender, EventArgs e)
        {
            warningBln = false;
            warningPnl.Visible = false;
            editBln = true;
            saveBtn.Enabled = true;
            deleteBtn.Enabled = true;
            codeTxt.Enabled = true;
            nameTxt.Enabled = true;
            address1Txt.Enabled = true;
            address2Txt.Enabled = true;
            city2Txt.Enabled = true;
            state2Txt.Enabled = true;
            zip2Txt.Enabled = true;
            SSN2Txt.Enabled = true;
            hourlyWageTxt.Enabled = true;
            savedEmployeesLst.Enabled = true;

            //saveBtn_Click(sender, e);
            //

            //Fill the list from XML.
            FillEmployeeDataFromXML();

            //Enable/Disable out the selected field.
            //MainMenu.EmployeeData employeeData = MainMenu.FindEmployeeData(employeesData, this.codeTxt.Text.Trim(), this.nameTxt.Text.Trim(), this.address1Txt.Text.Trim(), this.address2Txt.Text.Trim(), this.city2Txt.Text.Trim(), this.state2Txt.Text.Trim(), this.zip2Txt.Text.Trim(), this.SSN2Txt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), this.groupTxt.Text.Trim());
            for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
          
            {
                MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];
                if (employeeData.KeyInt == Convert.ToInt32(this.keyTxt.Text))
                {
                    employeeData.ActiveBln = !activeRdo.Checked;
                }
            }


            //employeeData.HourlyWageDbl = Convert.ToDouble(hourlyWageTxt.Text);
            //employeeData.ActiveBln = !activeRdo.Checked;

            //Save the new data to XML.
            SaveEmployeeDataToXML();

            //Refill the GUI list.
            FillSavedEmployeesLst(activeRdo.Checked);

            //Clear the freshly added new code & description.
            codeTxt.Clear();
            nameTxt.Clear();
            address1Txt.Clear();
            address2Txt.Clear();
            city2Txt.Clear();
            state2Txt.Clear();
            zip2Txt.Clear();
            SSN2Txt.Clear();
            hourlyWageTxt.Clear();
            //overtimeByAverageWageChk.Checked = false;
            overtimeYesRdo.Checked = false;
            overtimeNoRdo.Checked = true;


            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;

            //Put focus back on 'NewCode' entry box.
            codeTxt.Focus();
            editBln = false;  // this did not help
        }

        private void warnNoBtn_Click(object sender, EventArgs e)
        {
            warningPnl.Visible = false;
            editBln = true;
            warningBln = true;
            warningPnl.Visible = false;
            editBln = true;
            saveBtn.Enabled = true;
            deleteBtn.Enabled = true;
            codeTxt.Enabled = true;
            nameTxt.Enabled = true;
            address1Txt.Enabled = true;
            address2Txt.Enabled = true;
            city2Txt.Enabled = true;
            state2Txt.Enabled = true;
            zip2Txt.Enabled = true;
            SSN2Txt.Enabled = true;
            hourlyWageTxt.Enabled = true;
            savedEmployeesLst.Enabled = true;

            //Put focus back on 'Code' entry box.
            codeTxt.Focus();

        }

        private void newEmployeeBtn_Click(object sender, EventArgs e)
        {
            //Refill the GUI list.
            FillSavedEmployeesLst(activeRdo.Checked);

            //Clear the freshly added new code & description.
            codeTxt.Clear();
            nameTxt.Clear();
            address1Txt.Clear();
            address2Txt.Clear();
            city2Txt.Clear();
            state2Txt.Clear();
            zip2Txt.Clear();
            SSN2Txt.Clear();
            hourlyWageTxt.Clear();
            warningPnl.Visible = false;
            //overtimeByAverageWageChk.Checked = false;
            overtimeYesRdo.Checked = false;
            overtimeNoRdo.Checked = true;

            saveBtn.Enabled = true;
            deleteBtn.Enabled = false;
            codeTxt.Enabled = true;
            nameTxt.Enabled = true;
            address1Txt.Enabled = true;
            address2Txt.Enabled = true;
            city2Txt.Enabled = true;
            state2Txt.Enabled = true;
            zip2Txt.Enabled = true;
            SSN2Txt.Enabled = true;
            hourlyWageTxt.Enabled = true;
            savedEmployeesLst.Enabled = true;

            //Clear the edit entry spaces.
            savedEmployeesLst.SelectedIndex = -1;

            //Put focus back on 'Code' entry box.
            codeTxt.Focus();
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("Are you sure", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            warningPnl.Visible = true;
            saveBtn.Enabled = false;
            deleteBtn.Enabled = false;
            codeTxt.Enabled = false;
            nameTxt.Enabled = false;
            address1Txt.Enabled = false;
            address2Txt.Enabled = false;
            city2Txt.Enabled = false;
            state2Txt.Enabled = false;
            zip2Txt.Enabled = false;
            SSN2Txt.Enabled = false;
            hourlyWageTxt.Enabled = false;
            savedEmployeesLst.Enabled = false;
        }

        private void employeeListBtn_Click(object sender, EventArgs e)
        {
            #region Make PDF
            string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Customer Data\";
            //string fileNameStr = "Pay_Data_Report_" + timeStampStr + ".pdf";
            string fileNameStr = "Customer_Data.pdf";
            FillEmployeeDataFromXML();
            EmployeeReport employeeReport = new EmployeeReport();
            employeeReport.Main(saveFolderStr, fileNameStr, employeesData);
            #endregion
            #region Open PDF
            Process.Start(saveFolderStr + fileNameStr);
            #endregion
        }




        private void MakeExcelBtn_Click(object sender, EventArgs e)
        {
            string timeStampCodeStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".").Replace(" ", "_");
            string saveFolderExcelStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Customer Data\" + "CustomerList" + timeStampCodeStr + ".csv";
            MakeExcel(saveFolderExcelStr);
            #region Open Excel
            Process.Start(saveFolderExcelStr);
            #endregion
        }


        public void MakeExcel(string excelFile)
        {
            #region Vars
           

            string employeeNameStr = string.Empty;
            string employeeAddress1Str = string.Empty;
            string employeeAddress2Str = string.Empty;
            string employeeCityStr = string.Empty;
            string employeeStateStr = string.Empty;
            string employeeZipStr = string.Empty;
            string employeeSSNStr = string.Empty;
            string employeeHourlyWageStr = string.Empty;
            //string employeeOverTimeByAvgWageStr = string.Empty;
            string employeeOverTimeByEmployeeStr = string.Empty;
            string employeeActiveStr = string.Empty;
            string employeeGroupStr = string.Empty;
            string employeeSubInt1Str = string.Empty;
            string employeeSubStr1Str = string.Empty;
            string employeeSubInt2Str = string.Empty;
            string employeeSubStr2Str = string.Empty;
            string employeeSubInt3Str = string.Empty;
            string employeeSubStr3Str = string.Empty;
            string employeeSubInt4Str = string.Empty;
            string employeeSubStr4Str = string.Empty;
            string employeeSubInt5Str = string.Empty;
            string employeeSubStr5Str = string.Empty;

           
            #endregion

            StreamWriter fileToWriteTo = null;

            fileToWriteTo = new StreamWriter(excelFile);

            string lineToWriteToFile = "";
            lineToWriteToFile = "Customer,Name,Address1,Adress2,City,State,Zip,Other1,Other2,Other3,Active,Group,EMSubInt1,EMSubStr1,EMSubInt2,EMSubStr2,EMSubInt3,EMSubStr3,EMSubInt4,EMSubStr4,EMSubInt5,EMSubStr5";
                                
            fileToWriteTo.WriteLine(lineToWriteToFile);

            
                #region Employees
                string employeeStr = "";

                for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
                {
                    MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];
                 
                        employeeStr = employeeData.CodeStr;
                        //for Excel doc
                        employeeNameStr = employeeData.NameStr;
                        employeeAddress1Str = employeeData.Address1Str;
                        employeeAddress2Str = employeeData.Address2Str;
                        employeeCityStr = employeeData.CityStr;
                        employeeStateStr = employeeData.StateStr;
                        employeeZipStr = employeeData.ZipStr;
                        employeeSSNStr = employeeData.SSNStr;
                        employeeHourlyWageStr = employeeData.HourlyWageDbl.ToString();
                        employeeOverTimeByEmployeeStr = employeeData.OvertimePerEmployeeBln.ToString();
                        employeeActiveStr = employeeData.ActiveBln.ToString();
                        employeeGroupStr = employeeData.GroupStr;
                        employeeSubInt1Str = employeeData.SubInt1.ToString();
                        if (employeeSubInt1Str == "-1")
                        {
                            employeeSubInt1Str = string.Empty;
                        }
                        employeeSubStr1Str = employeeData.SubStr1;
                        employeeSubInt2Str = employeeData.SubInt1.ToString();
                        if (employeeSubInt2Str == "-1")
                        {
                            employeeSubInt2Str = string.Empty;
                        }
                        employeeSubStr2Str = employeeData.SubStr2;
                        employeeSubInt3Str = employeeData.SubInt3.ToString();
                        if (employeeSubInt3Str == "-1")
                        {
                            employeeSubInt3Str = string.Empty;
                        }
                        employeeSubStr3Str = employeeData.SubStr3;
                        employeeSubInt4Str = employeeData.SubInt4.ToString();
                        if (employeeSubInt4Str == "-1")
                        {
                            employeeSubInt4Str = string.Empty;
                        }
                        employeeSubStr4Str = employeeData.SubStr4;
                        employeeSubInt5Str = employeeData.SubInt5.ToString();
                        if (employeeSubInt5Str == "-1")
                        {
                            employeeSubInt5Str = string.Empty;
                        }
                        employeeSubStr5Str = employeeData.SubStr5;

                       lineToWriteToFile = employeeStr.Replace(",","") + "," +
                                    employeeNameStr.Replace(",", "") + "," +
                                    employeeAddress1Str.Replace(",", "") + "," +
                                    employeeAddress2Str.Replace(",", "") + "," +
                                    employeeCityStr.Replace(",", "") + "," +
                                    employeeStateStr.Replace(",", "") + "," +
                                    employeeZipStr.Replace(",", "") + "," +
                                    employeeSSNStr.Replace(",", "") + "," +
                                    employeeHourlyWageStr + "," +
                                    employeeOverTimeByEmployeeStr + "," +
                                    employeeActiveStr + "," +
                                    employeeGroupStr + "," +
                                    employeeSubInt1Str + "," +
                                    employeeSubStr1Str + "," +
                                    employeeSubInt2Str + "," +
                                    employeeSubStr2Str + "," +
                                    employeeSubInt3Str + "," +
                                    employeeSubStr3Str + "," +
                                    employeeSubInt4Str + "," +
                                    employeeSubStr4Str + "," +
                                    employeeSubInt5Str + "," +
                                    employeeSubStr5Str;
                                    

                fileToWriteTo.WriteLine(lineToWriteToFile);
                    
                }
                #endregion

                

                
            

            #region Close file streams
            if (fileToWriteTo != null)
            {
                fileToWriteTo.Close();
            }
            #endregion

        }

        private void titleLbl_Click(object sender, EventArgs e)
        {

        }
    }
}
