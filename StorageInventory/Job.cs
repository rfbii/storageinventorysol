﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Collections;
using System.Diagnostics;

namespace StorageInventory
{
	public partial class Job : Form
	{
		#region Vars
		//Set the global variables for delimiter character.
		char delimiter = '~';

		//Set the path of the program & save files.
		private string xmlFileName = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";

		//Create the jobsData object.
		MainMenu.JobsData jobsData = new MainMenu.JobsData();

		private ArrayList ActiveArl;
		private ArrayList InactiveArl;
		#endregion

		#region Constructor
		public Job()
		{
			InitializeComponent();
			jobsData = new MainMenu.JobsData();
			FillSavedJobsLst(true);

			//Make form non-resizable.
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;

			//Fill the job data from XML.
			FillJobDataFromXML();
            CompanyNameData.Text = MainMenu.CompanyStr;
		}
		#endregion

		#region Form Functions.
		#region KeyDown Functions (For Enter-As-Tab Functionality)
		private void newCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void newDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveNewBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveBtn_Click(null, null);
		}
		private void editCodeTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editDescriptionTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void saveEditedBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				saveEditedBtn_Click(null, null);
		}
		#endregion

		#region Input Validation Functions (Checking for proper data type inputs).

		private void newCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(newCodeTxt, delimiter))
			{
				//Check for a unique job code.
				foreach (MainMenu.JobData tempJobData in jobsData.JobsDataArl)
				{
					if (tempJobData.CodeStr == this.newCodeTxt.Text.Trim())
					{
						MessageBox.Show("This job code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

						//Focus on the line the user needs to change.
						this.newCodeTxt.Focus();
						this.newCodeTxt.Text = string.Empty;

						break;
					}
				}
			}

		}

		private void editCodeTxt_Leave(object sender, EventArgs e)
		{
			if (!InputStringContainsChar(editCodeTxt, delimiter))
			{
				//Check for a unique job code.
				for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
				{
					MainMenu.JobData tempJobData = (MainMenu.JobData)jobsData.JobsDataArl[i];

					if (tempJobData.CodeStr == this.editCodeTxt.Text.Trim() && tempJobData.KeyInt.ToString() != this.keyTxt.Text)
					{
						MessageBox.Show("This job code is already used.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

						//Focus on the line the user needs to change.
						this.editCodeTxt.Focus();
						this.editCodeTxt.SelectAll();

						break;
					}
				}
			}
		}
		#endregion

		#region List Box Functions
		private void savedJobsLst_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (savedJobsLst.SelectedIndex != -1)
			{
				//Clear out the new entry spaces.
				this.newCodeTxt.Text = string.Empty;
				this.newDescriptionTxt.Text = string.Empty;

				if (activeRdo.Checked)
				{
					MainMenu.JobData jobData = (MainMenu.JobData)ActiveArl[savedJobsLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = jobData.CodeStr;
					editDescriptionTxt.Text = jobData.DescriptionStr;
					keyTxt.Text = jobData.KeyInt.ToString();
				}
				else
				{
					MainMenu.JobData jobData = (MainMenu.JobData)InactiveArl[savedJobsLst.SelectedIndex];

					//Fill the Edit entry boxes.
					editCodeTxt.Text = jobData.CodeStr;
					editDescriptionTxt.Text = jobData.DescriptionStr;
					keyTxt.Text = jobData.KeyInt.ToString();
				}

				//Extrapolate the selected code & description.
				//string selectedEntry = savedJobsLst.SelectedItem.ToString();
				//string[] entryPieces = selectedEntry.Split(delimiter);
				//string selectedCode = entryPieces[0].Trim();
				//string selectedDescription = entryPieces[1].Trim();

				//Fill the Edit entry boxes.
				//editCodeTxt.Text = selectedCode;
				//editDescriptionTxt.Text = selectedDescription;

				//Enable the entry boxes & button.
				editCodeTxt.Enabled = true;
				editCodeLbl.Enabled = true;
				editDescriptionTxt.Enabled = true;
				editDescriptionLbl.Enabled = true;
				saveEditedBtn.Enabled = true;
				enableDisableBtn.Enabled = true;
			}
			else
			{
				//Clear the Edit entry boxes.
				editCodeTxt.Text = string.Empty;
				editDescriptionTxt.Text = string.Empty;
				keyTxt.Text = string.Empty;

				//Disable the entry boxes & button.
				editCodeTxt.Enabled = false;
				editCodeLbl.Enabled = false;
				editDescriptionTxt.Enabled = false;
				editDescriptionLbl.Enabled = false;
				saveEditedBtn.Enabled = false;
				enableDisableBtn.Enabled = false;
			}
		}
		private void activeRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (activeRdo.Checked)
			{
				enableDisableBtn.Text = "&Disable";

				FillSavedJobsLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "En&able";

				FillSavedJobsLst(activeRdo.Checked);
			}
		}
		private void inactiveRdo_CheckedChanged(object sender, EventArgs e)
		{
			if (inactiveRdo.Checked)
			{
				enableDisableBtn.Text = "En&able";

				FillSavedJobsLst(activeRdo.Checked);
			}
			else
			{
				enableDisableBtn.Text = "&Disable";

				FillSavedJobsLst(activeRdo.Checked);
			}
		}
		#endregion

		private void exitBtn_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		#endregion

		#region New Job Functions.
		private void saveBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//  character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.newCodeTxt, delimiter))
			{
				if (!InputStringContainsChar(this.newDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
					FillJobDataFromXML();

					//Add the new Job Data into the list.
					MainMenu.JobData jobData = MainMenu.FindOrAddJobData(jobsData, this.newCodeTxt.Text.Trim(), this.newDescriptionTxt.Text.Trim(), jobsData.NextKeyInt);
					jobData.ActiveBln = activeRdo.Checked;

					//Save the new data to XML.
					SaveJobDataToXML();

					//Refill the GUI list.
					FillSavedJobsLst(activeRdo.Checked);

					//Clear the freshly added new code & description.
					newCodeTxt.Clear();
					newDescriptionTxt.Clear();

					//Clear the edit entry spaces.
					savedJobsLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void newCodeTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedJobsLst.SelectedIndex = -1;
		}
		private void newDescriptionTxt_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces.
			savedJobsLst.SelectedIndex = -1;
		}
		#endregion

		#region Edit Job Functions.
		private void saveEditedBtn_Click(object sender, EventArgs e)
		{
			//Check the code and description to make sure the user hasn't entered the delimiter 
			//  character we're using to parse from the listbox.
			if (!InputStringContainsChar(this.editCodeTxt, delimiter) && this.keyTxt.Text != "")
			{
				if (!InputStringContainsChar(this.editDescriptionTxt, delimiter))
				{
					//Fill the list from XML.
					FillJobDataFromXML();

					//Save the edited job.
					MainMenu.SaveEditedJobData(jobsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), activeRdo.Checked);

					//Save the new data to XML.
					SaveJobDataToXML();

					//Refill the GUI list.
					FillSavedJobsLst(activeRdo.Checked);

					//Clear the edit entry spaces.
					savedJobsLst.SelectedIndex = -1;

					//Put focus back on 'NewCode' entry box.
					newCodeTxt.Focus();
				}
			}
		}
		private void enableDisableBtn_Click(object sender, EventArgs e)
		{
			if (this.keyTxt.Text != "")
			{
				//Fill the list from XML.
				FillJobDataFromXML();

				//Enable/Disable out the selected job.
				if (activeRdo.Checked)
                {
                    MainMenu.SaveEditedJobData(jobsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), false);
                }
                else
                {
                    MainMenu.SaveEditedJobData(jobsData, this.editCodeTxt.Text.Trim(), this.editDescriptionTxt.Text.Trim(), Convert.ToInt32(this.keyTxt.Text), true);
                }
				//Save the new data to XML.
				SaveJobDataToXML();

				//Refill the GUI list.
				FillSavedJobsLst(activeRdo.Checked);

				//Clear the edit entry spaces.
				savedJobsLst.SelectedIndex = -1;

				//Put focus back on 'NewCode' entry box.
				newCodeTxt.Focus();
			}
		}
		#endregion

		#region Helper Functions.
		private void FillSavedJobsLst(bool activeBln)
		{
			//Clear out the Saved Jobs List.
			savedJobsLst.ClearSelected();
			savedJobsLst.Items.Clear();
			savedJobsLst.SelectedIndex = -1;

			ActiveArl = new ArrayList();
			InactiveArl = new ArrayList();

			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				MainMenu.JobsData jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
                jobsData.JobsDataArl.Sort();
				for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
				{
					MainMenu.JobData jobData = (MainMenu.JobData)jobsData.JobsDataArl[i];

					if (jobData.ActiveBln == activeBln)
					{
						savedJobsLst.Items.Add(jobData.CodeStr + " ~ " + jobData.DescriptionStr);
					}

					if (jobData.ActiveBln)
					{
						ActiveArl.Add(jobData);
					}
					else
					{
						InactiveArl.Add(jobData);
					}
				}
			}
		}
		private void FillJobDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName);

			if (fileInfo.Exists)
			{

				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void SaveJobDataToXML()
		{
			//Serialize (convert an object instance to an XML document):
			XmlSerializer xmlSerializer = new XmlSerializer(jobsData.GetType());
			// Create an XmlTextWriter using a FileStream.
			Stream fileStream2 = new FileStream(xmlFileName, FileMode.Create);
			XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
			// Serialize using the XmlTextWriter.
			xmlSerializer.Serialize(xmlWriter, jobsData);
			xmlWriter.Flush();
			xmlWriter.Close();
		}
		private bool InputStringContainsChar(TextBox textBoxToCheck, char charToCheckFor)
		{
			if (textBoxToCheck.Text.Contains(charToCheckFor))
			{
				MessageBox.Show("Input cannot contain '" + charToCheckFor + "' characters.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

				textBoxToCheck.Focus();
				textBoxToCheck.SelectAll();

				return true;
			}
			else
				return false;
		}
		#endregion

        private void CompanyNameData_Click(object sender, EventArgs e)
        {

        }

        private void jobListBtn_Click(object sender, EventArgs e)
        {
            #region Make PDF
            string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Box Type Data\";
            string fileNameStr = "Box_Type_Data.pdf";
            JobReport jobReport = new JobReport();
            jobReport.Main(saveFolderStr, fileNameStr, jobsData);
            #endregion
            #region Open PDF
            Process.Start(saveFolderStr + fileNameStr);
            #endregion
        }
	}
}
