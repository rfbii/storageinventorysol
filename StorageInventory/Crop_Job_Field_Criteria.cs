﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
namespace PayrollFieldDataDesktop
{
	public partial class Crop_Job_Field_Criteria : Form
    {
        #region Vars
        private ArrayList ReportDetailsArl;
        private ArrayList SelectedPayDetailsArl;
        public string commentStr = string.Empty;
        #endregion

        #region Constructor
        public Crop_Job_Field_Criteria()
		{
			//DEFAULT - Initialize Form.
			InitializeComponent();
            
            //Fill Arraylists
            FillAvailable_Crop();
            FillAvailable_Job();
            FillAvailable_Field();
            FillAvailable_Employee();

			//Initialize the DatePicker.
			ConstructDatePicker();

            Construct_PayDetailData();
            CompanyNameData.Text = MainMenu.CompanyStr;
            
			//Make form non-resizable.
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;
		}
		#endregion

		#region Form Functions
        private void runBtn_Click(object sender, EventArgs e)
        {
            #region Fill SelectedPayDetailDataArl
            SelectedPayDetailsDataArl = new ArrayList();

            for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
            {
                MainMenu.PayDetailData payDetailData = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                if (payDetailData.ActiveBln)
                {
                    bool addPayDetailDataBln = true;

                    if (addPayDetailDataBln && SelectedEmployeesArl.Count > 0)
                    {
                        for (int j = 0; j < SelectedEmployeesArl.Count; j++)
                        {
                            MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)SelectedEmployeesArl[j];

                            if (payDetailData.EmployeeKeyInt == employeeData.KeyInt)
                            {
                                addPayDetailDataBln = true;

                                break;
                            }
                            else
                            {
                                addPayDetailDataBln = false;
                            }
                        }
                    }

                    if (addPayDetailDataBln && SelectedCropsArl.Count > 0)
                    {
                        for (int j = 0; j < SelectedCropsArl.Count; j++)
                        {
                            MainMenu.CropData cropData = (MainMenu.CropData)SelectedCropsArl[j];

                            if (payDetailData.CropKeyInt == cropData.KeyInt)
                            {
                                addPayDetailDataBln = true;

                                break;
                            }
                            else
                            {
                                addPayDetailDataBln = false;
                            }
                        }
                    }

                    if (addPayDetailDataBln && SelectedJobsArl.Count > 0)
                    {
                        for (int j = 0; j < SelectedJobsArl.Count; j++)
                        {
                            MainMenu.JobData jobData = (MainMenu.JobData)SelectedJobsArl[j];

                            if (payDetailData.JobKeyInt == jobData.KeyInt)
                            {
                                addPayDetailDataBln = true;

                                break;
                            }
                            else
                            {
                                addPayDetailDataBln = false;
                            }
                        }
                    }

                    if (addPayDetailDataBln && SelectedFieldsArl.Count > 0)
                    {
                        for (int j = 0; j < SelectedFieldsArl.Count; j++)
                        {
                            MainMenu.FieldData fieldData = (MainMenu.FieldData)SelectedFieldsArl[j];

                            if (payDetailData.FieldKeyInt == fieldData.KeyInt)
                            {
                                addPayDetailDataBln = true;

                                break;
                            }
                            else
                            {
                                addPayDetailDataBln = false;
                            }
                        }
                    }

                    if (addPayDetailDataBln && DatePickerFromDateTxt.Text.Trim() != "" && DatePickerThruDateTxt.Text.Trim() != "")
                    {
                        if (Convert.ToDateTime(DatePickerFromDateTxt.Text.Trim()) <= payDetailData.DateDtm &&
                            Convert.ToDateTime(DatePickerThruDateTxt.Text.Trim()) >= payDetailData.DateDtm)
                        {
                            addPayDetailDataBln = true;
                        }
                        else
                        {
                            addPayDetailDataBln = false;
                        }
                    }

                    if (addPayDetailDataBln)
                    {
                        SelectedPayDetailsDataArl.Add(payDetailData);
                    }

                }
            }
            #endregion

            #region Fill Report Details
            ReportDetailsArl = new ArrayList();

            for (int x = 0; x < SelectedPayDetailsDataArl.Count; x++)
            {
                MainMenu.PayDetailData tempPayDetailData = (MainMenu.PayDetailData)SelectedPayDetailsDataArl[x];
                ReportDetails tempReportDetails = new ReportDetails();

                tempReportDetails.keyInt = tempPayDetailData.KeyInt;
                tempReportDetails.dateStr = tempPayDetailData.DateDtm.ToShortDateString();
                tempReportDetails.startTimeStr = tempPayDetailData.StartTimeDtm.ToShortTimeString();
                tempReportDetails.endTimeStr = tempPayDetailData.EndTimeDtm.ToShortTimeString();
                tempReportDetails.employeeStr = Get_Employee(tempPayDetailData.EmployeeKeyInt);
                tempReportDetails.hoursDbl = tempPayDetailData.HoursDbl;
                tempReportDetails.cropStr = Get_Crop(tempPayDetailData.CropKeyInt);
                tempReportDetails.jobStr = Get_Job(tempPayDetailData.JobKeyInt);
                tempReportDetails.fieldStr = Get_Field(tempPayDetailData.FieldKeyInt);
                tempReportDetails.payUnitDbl = tempPayDetailData.PayUnitDbl;
                tempReportDetails.yieldUnitDbl = tempPayDetailData.YieldUnitDbl;
                tempReportDetails.priceDbl = tempPayDetailData.PriceDbl;
                tempReportDetails.summaryNoStr = tempPayDetailData.SummaryNumberInt.ToString();
                tempReportDetails.typeStr = tempPayDetailData.TypeStr;
                tempReportDetails.reportDateStr = tempPayDetailData.ReportDateDtm.ToShortDateString();
                tempReportDetails.amountDbl = tempPayDetailData.PriceDbl * tempPayDetailData.PayUnitDbl;

                ReportDetailsArl.Add(tempReportDetails);
            }
            #endregion
            #region get list of all Employee and pay dates
            ArrayList tempEmployeeDetails = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReport = (ReportDetails)ReportDetailsArl[i];
                FindOrAddEmployeeDetail(tempEmployeeDetails, tempReport.employeeStr, tempReport.reportDateStr, tempReport.summaryNoStr);
            }
            #endregion
            
            for (int h = 0; h < tempEmployeeDetails.Count; h++)
            {
                EmployeePayDetails tempEmployee = (EmployeePayDetails)tempEmployeeDetails[h];
                #region Find total per Employee per pay date and total Min and Over time.
                double totalpaidDbl = 0;
                double totalExtraDbl = 0;
               // ArrayList tempDbl = 0;
                ArrayList tempExtraArl = new ArrayList();
                for (int i = 0; i < ReportDetailsArl.Count; i++)
                {
                    ReportDetails tempReport = (ReportDetails)ReportDetailsArl[i];
                    if (tempEmployee.codeStr == tempReport.employeeStr && tempEmployee.dateStr == tempReport.reportDateStr && tempEmployee.summaryNumberStr == tempReport.summaryNoStr)
                    {
                        // finding regular pay and extra pay (minimum and overtime)
                        if (tempReport.typeStr == "P")
                        {
                            totalpaidDbl += tempReport.amountDbl;
                        }
                        else
                        {
                            totalExtraDbl += tempReport.amountDbl;
                            //Adding lines to temp arrraylist to remove later
                            tempExtraArl.Add(tempReport);
                        }
                    }
                }
                #endregion
                if (totalExtraDbl != 0)
                {
                    #region place mim  wage adjustments
                    ArrayList tempArl = new ArrayList();
                    bool foundBln = false;
                    for (int i = 0; i < ReportDetailsArl.Count; i++)
                    {
                        ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];
                        if (tempReportDetails.typeStr == "P" && tempEmployee.codeStr == tempReportDetails.employeeStr && tempEmployee.dateStr == tempReportDetails.reportDateStr && tempEmployee.summaryNumberStr == tempReportDetails.summaryNoStr)
                        {
                            ReportDetails tempItem = new ReportDetails();
                            tempItem.cropStr = tempReportDetails.cropStr;
                            tempItem.dateStr = tempReportDetails.dateStr;
                            tempItem.employeeStr = tempReportDetails.employeeStr;
                            tempItem.fieldStr = tempReportDetails.fieldStr;
                            tempItem.jobStr = tempReportDetails.jobStr;
                            tempItem.reportDateStr = tempReportDetails.reportDateStr;
                            tempItem.typeStr = "P";
                            tempItem.amountDbl = (totalExtraDbl * (tempReportDetails.amountDbl / totalpaidDbl));
                            foundBln = true;
                            //Save lines to add later
                            tempArl.Add(tempItem);
                        }
                    }
                    
                    //place line back into main Arraylist
                    for (int i = 0; i < tempArl.Count; i++)
                    {
                        ReportDetails tempReportDetail1s = (ReportDetails)tempArl[i];
                        ReportDetailsArl.Add(tempReportDetail1s);
                    }
                    if (foundBln)
                    {
                        //Place Negitve line in to reverse Extra line
                        for (int i = ReportDetailsArl.Count - 1; i > 0; i--)
                        {
                            ReportDetails tempReport = (ReportDetails)ReportDetailsArl[i];
                            if (tempEmployee.codeStr == tempReport.employeeStr && tempEmployee.dateStr == tempReport.reportDateStr && tempEmployee.summaryNumberStr == tempReport.summaryNoStr)
                            {
                                // finding regular pay and extra pay (minimum and overtime)
                                if (tempReport.typeStr != "P")
                                {
                                    ReportDetailsArl.RemoveAt(i);
                                }

                            }
                        }
                    }
                    #endregion
                }
            }
            #region Fill CropDetailsArl
            ArrayList CropDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                CropDetail tempCropDetail = FindOrAddCropDetail(CropDetailsArl, tempReportDetails.cropStr);

                tempCropDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempCropDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempCropDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempCropDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < CropDetailsArl.Count; i++)
            {
                CropDetail tempCropDetail = (CropDetail)CropDetailsArl[i];

                if (tempCropDetail.hoursDbl == 0 && tempCropDetail.payUnitDbl == 0 && tempCropDetail.yieldUnitDbl == 0 && Math.Round(tempCropDetail.amountDbl) == 0)
                {
                    CropDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill JobDetailsArl
            ArrayList JobDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                JobDetail tempJobDetail = FindOrAddJobDetail(JobDetailsArl, tempReportDetails.jobStr);

                tempJobDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempJobDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempJobDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempJobDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < JobDetailsArl.Count; i++)
            {
                JobDetail tempJobDetail = (JobDetail)JobDetailsArl[i];

                if (tempJobDetail.hoursDbl == 0 && tempJobDetail.payUnitDbl == 0 && tempJobDetail.yieldUnitDbl == 0 && Math.Round(tempJobDetail.amountDbl) == 0)
                {
                    JobDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill FieldDetailsArl
            ArrayList FieldDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                FieldDetail tempFieldDetail = FindOrAddFieldDetail(FieldDetailsArl, tempReportDetails.fieldStr);

                tempFieldDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempFieldDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempFieldDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempFieldDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < FieldDetailsArl.Count; i++)
            {
                FieldDetail tempFieldDetail = (FieldDetail)FieldDetailsArl[i];

                if (tempFieldDetail.hoursDbl == 0 && tempFieldDetail.payUnitDbl == 0 && tempFieldDetail.yieldUnitDbl == 0 && Math.Round(tempFieldDetail.amountDbl) == 0)
                {
                    FieldDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill CropJobDetailsArl
            ArrayList CropJobDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                CropJobDetail tempCropJobDetail = FindOrAddCropJobDetail(CropJobDetailsArl, tempReportDetails.cropStr, tempReportDetails.jobStr);

                tempCropJobDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempCropJobDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempCropJobDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempCropJobDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < CropJobDetailsArl.Count; i++)
            {
                CropJobDetail tempCropJobDetail = (CropJobDetail)CropJobDetailsArl[i];

                if (tempCropJobDetail.hoursDbl == 0 && tempCropJobDetail.payUnitDbl == 0 && tempCropJobDetail.yieldUnitDbl == 0 && Math.Round(tempCropJobDetail.amountDbl) == 0)
                {
                    CropJobDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill CropFieldDetailsArl
            ArrayList CropFieldDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                CropFieldDetail tempCropFieldDetail = FindOrAddCropFieldDetail(CropFieldDetailsArl, tempReportDetails.cropStr, tempReportDetails.fieldStr);

                tempCropFieldDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempCropFieldDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempCropFieldDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempCropFieldDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < CropFieldDetailsArl.Count; i++)
            {
                CropFieldDetail tempCropFieldDetail = (CropFieldDetail)CropFieldDetailsArl[i];

                if (tempCropFieldDetail.hoursDbl == 0 && tempCropFieldDetail.payUnitDbl == 0 && tempCropFieldDetail.yieldUnitDbl == 0 && Math.Round(tempCropFieldDetail.amountDbl) == 0)
                {
                    CropFieldDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill JobFieldDetailsArl
            ArrayList JobFieldDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                JobFieldDetail tempJobFieldDetail = FindOrAddJobFieldDetail(JobFieldDetailsArl, tempReportDetails.jobStr, tempReportDetails.fieldStr);

                tempJobFieldDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempJobFieldDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempJobFieldDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempJobFieldDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < JobFieldDetailsArl.Count; i++)
            {
                JobFieldDetail tempJobFieldDetail = (JobFieldDetail)JobFieldDetailsArl[i];

                if (tempJobFieldDetail.hoursDbl == 0 && tempJobFieldDetail.payUnitDbl == 0 && tempJobFieldDetail.yieldUnitDbl == 0 && Math.Round(tempJobFieldDetail.amountDbl) == 0)
                {
                    JobFieldDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill CropJobFieldDetailsArl
            ArrayList CropJobFieldDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                CropJobFieldDetail tempCropJobFieldDetail = FindOrAddCropJobFieldDetail(CropJobFieldDetailsArl, tempReportDetails.cropStr, tempReportDetails.jobStr, tempReportDetails.fieldStr);

                tempCropJobFieldDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempCropJobFieldDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempCropJobFieldDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempCropJobFieldDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < CropJobFieldDetailsArl.Count; i++)
            {
                CropJobFieldDetail tempCropJobFieldDetail = (CropJobFieldDetail)CropJobFieldDetailsArl[i];

                if (tempCropJobFieldDetail.hoursDbl == 0 && tempCropJobFieldDetail.payUnitDbl == 0 && tempCropJobFieldDetail.yieldUnitDbl == 0 && Math.Round(tempCropJobFieldDetail.amountDbl) == 0)
                {
                    CropJobFieldDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill JobCropFieldDetailsArl
            ArrayList JobCropFieldDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                JobCropFieldDetail tempJobCropFieldDetail = FindOrAddJobCropFieldDetail(JobCropFieldDetailsArl, tempReportDetails.jobStr, tempReportDetails.cropStr, tempReportDetails.fieldStr);

                tempJobCropFieldDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempJobCropFieldDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempJobCropFieldDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempJobCropFieldDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < JobCropFieldDetailsArl.Count; i++)
            {
                JobCropFieldDetail tempJobCropFieldDetail = (JobCropFieldDetail)JobCropFieldDetailsArl[i];

                if (tempJobCropFieldDetail.hoursDbl == 0 && tempJobCropFieldDetail.payUnitDbl == 0 && tempJobCropFieldDetail.yieldUnitDbl == 0 && Math.Round(tempJobCropFieldDetail.amountDbl) == 0)
                {
                    JobCropFieldDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            #region Fill FieldCropJobDetailsArl
            ArrayList FieldCropJobDetailsArl = new ArrayList();
            for (int i = 0; i < ReportDetailsArl.Count; i++)
            {
                ReportDetails tempReportDetails = (ReportDetails)ReportDetailsArl[i];

                FieldCropJobDetail tempFieldCropJobDetail = FindOrAddFieldCropJobDetail(FieldCropJobDetailsArl, tempReportDetails.fieldStr, tempReportDetails.cropStr, tempReportDetails.jobStr);

                tempFieldCropJobDetail.hoursDbl += tempReportDetails.hoursDbl;
                tempFieldCropJobDetail.payUnitDbl += tempReportDetails.payUnitDbl;
                tempFieldCropJobDetail.yieldUnitDbl += tempReportDetails.yieldUnitDbl;
                tempFieldCropJobDetail.amountDbl += tempReportDetails.amountDbl;
            }

            //Remove lines with zero value
            for (int i = 0; i < FieldCropJobDetailsArl.Count; i++)
            {
                FieldCropJobDetail tempFieldCropJobDetail = (FieldCropJobDetail)FieldCropJobDetailsArl[i];

                if (tempFieldCropJobDetail.hoursDbl == 0 && tempFieldCropJobDetail.payUnitDbl == 0 && tempFieldCropJobDetail.yieldUnitDbl == 0 && Math.Round(tempFieldCropJobDetail.amountDbl) == 0)
                {
                    FieldCropJobDetailsArl.RemoveAt(i);
                }
            }
            #endregion

            if (ReportDetailsArl.Count > 0)
            {
                #region Comment String - Date Range of report.
                StringBuilder comment = new StringBuilder();

                if (DatePickerFromDateTxt.Text.ToString() != "")
                {
                    comment.Append(DatePickerFromDateTxt.Text.ToString());
                }

                    
                
                if (DatePickerThruDateTxt.Text.ToString() != "")
                {
                    comment.Append(" TO ");
                    comment.Append(DatePickerThruDateTxt.Text.ToString());

                }
                commentStr = comment.ToString();
                #endregion

                #region Make PDF
                string timeStampStr = DateTime.Now.ToString().Replace("/", "-").Replace("\\", "-").Replace(":", ".").Replace(" ", "_");
                string saveFolderStr = Path.GetDirectoryName(Application.ExecutablePath) + @"\Reports\Crop_Job_Field\";
                string fileNameStr = "Crop_Job_Field_Report_" + timeStampStr + ".pdf";
                //string fileNameStr = "Crop_Job_Field_Report.pdf";

                Crop_Job_Field_Report cropJobField_Report = new Crop_Job_Field_Report();
                cropJobField_Report.Main(saveFolderStr, fileNameStr, commentStr, CropDetailsArl, JobDetailsArl, FieldDetailsArl, CropJobDetailsArl, CropFieldDetailsArl, JobFieldDetailsArl, CropJobFieldDetailsArl, JobCropFieldDetailsArl, FieldCropJobDetailsArl);
                #endregion

                #region Open PDF
                Process.Start(saveFolderStr + fileNameStr);
                #endregion

                //#region Update XML
                ////Serialize (convert an object instance to an XML document):
                //XmlSerializer xmlSerializer = new XmlSerializer(payDetailsData.GetType());
                //// Create an XmlTextWriter using a FileStream.
                //Stream fileStream = new FileStream(PayDetailDataXML, FileMode.Create);
                //XmlWriter xmlWriter = new XmlTextWriter(fileStream, Encoding.Unicode);
                //// Serialize using the XmlTextWriter.
                //xmlSerializer.Serialize(xmlWriter, payDetailsData);
                //xmlWriter.Flush();
                //xmlWriter.Close();
                //#endregion
            }
            else
            {
                MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void exitBtn_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
        
		#region DatePicker
		// ~!~!~   INSTRUCTIONS....   ~!~!~ 
		//
		// To use this DatePicker on another form:
		//      1. Copy this whole DatePicker #region to the new form.
		//      2. Call this constructor -"ConstructDatePicker()" to the new form's constructor.
		//      3. Copy the Grouped Box from this designer view to the new form's designer view.

		#region Constructor
		protected void ConstructDatePicker()
		{
		    //Fill the Combobox (DropdownList) with preset date ranges.
			DatePicker_presetsCbx_FillPresetDateRanges();

			//Link the GUI actions/events to the proper functions.
			this.DatePickerPresetsCbx.SelectedIndexChanged += new System.EventHandler(this.DatePicker_presetsCbx_SelectedIndexChanged);
			this.DatePickerEndWeekTxt.TextChanged += new System.EventHandler(this.DatePicker_endYearOrWeek_TextChanged);
			this.DatePickerEndYearTxt.TextChanged += new System.EventHandler(this.DatePicker_endYearOrWeek_TextChanged);
			this.DatePickerStartWeekTxt.TextChanged += new System.EventHandler(this.DatePicker_startYearOrWeek_TextChanged);
			this.DatePickerStartYearTxt.TextChanged += new System.EventHandler(this.DatePicker_startYearOrWeek_TextChanged);
		}
		#endregion

		#region Functions
		#region Events
		private void DatePicker_presetsCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			DatePickerFromDateTxt.Text = " ";
			DatePickerThruDateTxt.Text = " ";

			switch (DatePickerPresetsCbx.SelectedIndex)
			{
				case 0: /*Custom*/
					break;
				case 1: /*Today*/
					DatePickerFromDateTxt.Text = System.DateTime.Now.ToShortDateString();
					DatePickerThruDateTxt.Text = System.DateTime.Now.ToShortDateString();
					ClearCustomEntries();
					break;
				case 2: /*Yesterday*/
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					ClearCustomEntries();
					break;
				case 3: /*Tomorrow*/
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
					ClearCustomEntries();
					break;
				case 4: /*This Week*/
					ThisWeek();
					ClearCustomEntries();
					break;
				case 5: /*This Week Thru Today*/
					ThisWeekThruToday();
					ClearCustomEntries();
					break;
				case 6: /*Last Week*/
					LastWeek();
					ClearCustomEntries();
					break;
				case 7: /*Last 2 Weeks*/
					Last2Weeks();
					ClearCustomEntries();
					break;
				case 8: /*Last 4 Weeks*/
					Last4Weeks();
					ClearCustomEntries();
					break;
				case 9: /*Last 13 Weeks*/
					Last13Weeks();
					ClearCustomEntries();
					break;
				case 10: /*Last 26 Weeks*/
					Last26Weeks();
					ClearCustomEntries();
					break;
				case 11: /*Last 52 Weeks*/
					Last52Weeks();
					ClearCustomEntries();
					break;
				case 12: /*This Month*/
					ThisMonth();
					ClearCustomEntries();
					break;
				case 13: /*This Month thru Today*/
					ThisMonthThruToday();
					ClearCustomEntries();
					break;
				case 14: /*This Month Year Ago*/
					ThisMonthYearAgo();
					ClearCustomEntries();
					break;
				case 15: /*Last Month"*/
					LastMonth();
					ClearCustomEntries();
					break;
				case 16: /*Last Month Year Ago*/
					LastMonthYearAgo();
					ClearCustomEntries();
					break;
				case 17: /*This Year*/
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "12/31/" + (System.DateTime.Today.Year);
					ClearCustomEntries();
					break;
				case 18: /*This Year Thru Today*/
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = System.DateTime.Now.ToShortDateString();
					ClearCustomEntries();
					break;
				case 19: /*Last Year*/
					DatePickerFromDateTxt.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
					ClearCustomEntries();
					break;
			}
		}
		private void DatePicker_startYearOrWeek_TextChanged(object senderIn, EventArgs e)
		{
			//Setup validatable variables.
			int yearEntered, weekEntered;

			//Try to Parse Integers from what the user has entered in the text field.
			if (int.TryParse(DatePickerStartWeekTxt.Text, out weekEntered) && int.TryParse(DatePickerStartYearTxt.Text, out yearEntered)
				&& weekEntered <= 52 && weekEntered >= 1 && yearEntered >= 1 && yearEntered <= 3000)
			{
				//Set the object type.
				TextBox sender = (TextBox)senderIn;

				if (sender.Text != "")/*Set the preset list to 'custom'. */
					DatePickerPresetsCbx.SelectedIndex = 0;

				//Create the proper date variable.
				DateTime startDate = new DateTime(yearEntered, 1, 1);/*Set to January 1st of whatever year*/
				startDate = startDate.AddDays(FirstMondayOfTheYear(startDate));
				startDate = startDate.AddDays((weekEntered - 1) * 7);
				startDate.AddDays(weekEntered * 7);

				//Set the GUI with a string from this date.
				DatePickerFromDateTxt.Text = startDate.ToShortDateString();
			}
		}
		private void DatePicker_endYearOrWeek_TextChanged(object senderIn, EventArgs e)
		{
			//Setup validatable variables.
			int yearEntered, weekEntered;

			//Try to Parse Integers from what the user has entered in the text field.
			if (int.TryParse(DatePickerEndWeekTxt.Text, out weekEntered) && int.TryParse(DatePickerEndYearTxt.Text, out yearEntered)
				&& weekEntered <= 52 && weekEntered >= 1 && yearEntered >= 1 && yearEntered <= 3000)
			{
				//Set the object type.
				TextBox sender = (TextBox)senderIn;

				if (sender.Text != "")/*Set the preset list to 'custom'. */
					DatePickerPresetsCbx.SelectedIndex = 0;

				//Create the proper date variable.
				DateTime endDate = new DateTime(yearEntered, 1, 1);/*Set to January 1st of whatever year*/
				endDate = endDate.AddDays(FirstMondayOfTheYear(endDate));
				endDate = endDate.AddDays((weekEntered - 1) * 7);
				endDate.AddDays(weekEntered * 7);

				//Set the GUI with a string from this date.
				DatePickerThruDateTxt.Text = endDate.ToShortDateString();
			}
		}
		#endregion

		#region Date Getter Functions
		private void ThisWeek()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+6)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+5)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+4)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+3)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+2)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(+1)).ToShortDateString();
					break;
			}
		}
		private void ThisWeekThruToday()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
			}
		}
		private void LastWeek()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-13)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-8)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-9)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-10)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-11)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-12)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last2Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-20)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-14)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-15)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-16)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-17)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-18)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-19)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last4Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-34)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-28)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-29)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-30)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-31)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-32)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-33)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last13Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-97)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-91)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-92)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-93)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-94)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-95)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-96)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last26Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-188)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-182)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-183)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-184)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-184)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-186)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-187)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void Last52Weeks()
		{
			switch (System.DateTime.Today.DayOfWeek)
			{
				case System.DayOfWeek.Sunday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-370)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-7)).ToShortDateString();
					break;
				case System.DayOfWeek.Monday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-364)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-1)).ToShortDateString();
					break;
				case System.DayOfWeek.Tuesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-365)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-2)).ToShortDateString();
					break;
				case System.DayOfWeek.Wednesday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-366)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-3)).ToShortDateString();
					break;
				case System.DayOfWeek.Thursday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-367)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-4)).ToShortDateString();
					break;
				case System.DayOfWeek.Friday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-368)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-5)).ToShortDateString();
					break;
				case System.DayOfWeek.Saturday:
					DatePickerFromDateTxt.Text = (System.DateTime.Today.AddDays(-369)).ToShortDateString();
					DatePickerThruDateTxt.Text = (System.DateTime.Today.AddDays(-6)).ToShortDateString();
					break;
			}
		}
		private void ThisMonth()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "1/31/" + (System.DateTime.Today.Year);
					break;
				case 2:
					if (System.DateTime.IsLeapYear(System.DateTime.Today.Year))
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/29/" + (System.DateTime.Today.Year);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/28/" + (System.DateTime.Today.Year);
					}
					break;
				case 3:
					DatePickerFromDateTxt.Text = "3/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "3/31/" + (System.DateTime.Today.Year);
					break;
				case 4:
					DatePickerFromDateTxt.Text = "4/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "4/30/" + (System.DateTime.Today.Year);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "5/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "5/31/" + (System.DateTime.Today.Year);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "6/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "6/30/" + (System.DateTime.Today.Year);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "7/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "7/31/" + (System.DateTime.Today.Year);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "8/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "8/31/" + (System.DateTime.Today.Year);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "9/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "9/30/" + (System.DateTime.Today.Year);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "10/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "10/31/" + (System.DateTime.Today.Year);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "11/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "11/30/" + (System.DateTime.Today.Year);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "12/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "12/31/" + (System.DateTime.Today.Year);
					break;
			}
		}
		private void ThisMonthThruToday()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 2:
					DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 3:
					DatePickerFromDateTxt.Text = "3/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 4:
					DatePickerFromDateTxt.Text = "4/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 5:
					DatePickerFromDateTxt.Text = "5/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 6:
					DatePickerFromDateTxt.Text = "6/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 7:
					DatePickerFromDateTxt.Text = "7/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 8:
					DatePickerFromDateTxt.Text = "8/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 9:
					DatePickerFromDateTxt.Text = "9/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 10:
					DatePickerFromDateTxt.Text = "10/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 11:
					DatePickerFromDateTxt.Text = "11/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
				case 12:
					DatePickerFromDateTxt.Text = "12/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = (System.DateTime.Today).ToShortDateString();
					break;
			}
		}
		private void ThisMonthYearAgo()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "1/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 2:
					if (System.DateTime.IsLeapYear((System.DateTime.Today.Year) - 1))
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/29/" + ((System.DateTime.Today.Year) - 1);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/28/" + ((System.DateTime.Today.Year) - 1);
					}
					break;
				case 3:
					DatePickerFromDateTxt.Text = "3/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "3/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 4:
					DatePickerFromDateTxt.Text = "4/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "4/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "5/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "5/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "6/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "6/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "7/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "7/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "8/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "8/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "9/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "9/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "10/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "10/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "11/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "11/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "12/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
					break;
			}
		}
		private void LastMonth()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "12/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 2:
					DatePickerFromDateTxt.Text = "1/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "1/31/" + (System.DateTime.Today.Year);
					break;
				case 3:
					if (System.DateTime.IsLeapYear(System.DateTime.Today.Year))
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/29/" + (System.DateTime.Today.Year);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + (System.DateTime.Today.Year);
						DatePickerThruDateTxt.Text = "2/28/" + (System.DateTime.Today.Year);
					}
					break;
				case 4:
					DatePickerFromDateTxt.Text = "3/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "3/31/" + (System.DateTime.Today.Year);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "4/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "4/30/" + (System.DateTime.Today.Year);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "5/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "5/31/" + (System.DateTime.Today.Year);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "6/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "6/30/" + (System.DateTime.Today.Year);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "7/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "7/31/" + (System.DateTime.Today.Year);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "8/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "8/31/" + (System.DateTime.Today.Year);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "9/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "9/30/" + (System.DateTime.Today.Year);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "10/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "10/31/" + (System.DateTime.Today.Year);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "11/1/" + (System.DateTime.Today.Year);
					DatePickerThruDateTxt.Text = "11/30/" + (System.DateTime.Today.Year);
					break;
			}
		}
		private void LastMonthYearAgo()
		{
			switch (System.DateTime.Today.Month)
			{
				case 1:
					DatePickerFromDateTxt.Text = "12/1/" + ((System.DateTime.Today.Year) - 2);
					DatePickerThruDateTxt.Text = "12/31/" + ((System.DateTime.Today.Year) - 2);
					break;
				case 2:
					DatePickerFromDateTxt.Text = "1/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "1/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 3:
					if (System.DateTime.IsLeapYear((System.DateTime.Today.Year) - 1))
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/29/" + ((System.DateTime.Today.Year) - 1);
					}
					else
					{
						DatePickerFromDateTxt.Text = "2/1/" + ((System.DateTime.Today.Year) - 1);
						DatePickerThruDateTxt.Text = "2/28/" + ((System.DateTime.Today.Year) - 1);
					}
					break;
				case 4:
					DatePickerFromDateTxt.Text = "3/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "3/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 5:
					DatePickerFromDateTxt.Text = "4/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "4/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 6:
					DatePickerFromDateTxt.Text = "5/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "5/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 7:
					DatePickerFromDateTxt.Text = "6/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "6/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 8:
					DatePickerFromDateTxt.Text = "7/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "7/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 9:
					DatePickerFromDateTxt.Text = "8/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "8/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 10:
					DatePickerFromDateTxt.Text = "9/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "9/30/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 11:
					DatePickerFromDateTxt.Text = "10/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "10/31/" + ((System.DateTime.Today.Year) - 1);
					break;
				case 12:
					DatePickerFromDateTxt.Text = "11/1/" + ((System.DateTime.Today.Year) - 1);
					DatePickerThruDateTxt.Text = "11/30/" + ((System.DateTime.Today.Year) - 1);
					break;
			}
		}
		private int FirstMondayOfTheYear(DateTime date)
		{
			DateTime jan1 = date.AddDays(-(date.DayOfYear - 1));

			if (jan1.DayOfWeek == 0)
				return 1;
			else if (((int)jan1.DayOfWeek) < 5)
				return -(((int)jan1.DayOfWeek) - 1);
			else
				return 8 - ((int)jan1.DayOfWeek);
		}
		#endregion

		#region Helper / General Functions
		private void DatePicker_presetsCbx_FillPresetDateRanges()
		{
			DatePickerPresetsCbx.Items.Add("Custom");
			DatePickerPresetsCbx.Items.Add("Today");
			DatePickerPresetsCbx.Items.Add("Yesterday");
			DatePickerPresetsCbx.Items.Add("Tomorrow");
			DatePickerPresetsCbx.Items.Add("This Week");
			DatePickerPresetsCbx.Items.Add("This Week Thru Today");
			DatePickerPresetsCbx.Items.Add("Last Week");
			DatePickerPresetsCbx.Items.Add("Last 2 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 4 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 13 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 26 Weeks");
			DatePickerPresetsCbx.Items.Add("Last 52 Weeks");
			DatePickerPresetsCbx.Items.Add("This Month");
			DatePickerPresetsCbx.Items.Add("This Month thru Today");
			DatePickerPresetsCbx.Items.Add("This Month Year Ago");
			DatePickerPresetsCbx.Items.Add("Last Month");
			DatePickerPresetsCbx.Items.Add("Last Month Year Ago");
			DatePickerPresetsCbx.Items.Add("This Year");
			DatePickerPresetsCbx.Items.Add("This Year Thru Today");
			DatePickerPresetsCbx.Items.Add("Last Year");

			DatePickerPresetsCbx.SelectedIndex = 0;
		}
		private void ClearCustomEntries()
		{
			this.DatePickerStartYearTxt.Text = "";
			this.DatePickerStartWeekTxt.Text = "";
			this.DatePickerEndWeekTxt.Text = "";
			this.DatePickerEndYearTxt.Text = "";
		}
		#endregion
		#endregion
		#endregion

        #region Get Crop Data
        // ~!~!~   INSTRUCTIONS....   ~!~!~ 
        //
        // To use this Selector for another data type or on another form:
        //      1. Copy this whole Selector (Crop) #region to the new form.
        //      2. Find & Rename all 'Crop' & 'crop' to match the new data.
        //      3. Call this constructor -"ConstructSelector_Crop()" to the new form's constructor.
        //      4. Copy the Grouped Box from this designer view to the new form's designer view.
        //      5. Rename all of the objects in the designer view - change 'Crop' to match new data.
        //      6. Depending - You may have to change the XML file name in the 'Vars' region.

        #region Vars
        string CropXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
        MainMenu.CropsData cropsData;

        private ArrayList AvailableCropsArl = new ArrayList();
        private ArrayList SelectedCropsArl = new ArrayList();
        #endregion

        #region Constructor
        protected void ConstructSelector_Crop()
        {
            //Fill the available crops.
            FillAvailable_Crop();
        }
        #endregion

        #region Functions
        protected void FillAvailable_Crop()
        {
            FileInfo fileInfo = new FileInfo(CropXML);

            if (fileInfo.Exists)
            {
                //Fill 'cropsData' from XML.

                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(CropXML, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
                fileStream.Close();

                //Fill the list from the data.
                for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
                {
                    MainMenu.CropData tempCropData = (MainMenu.CropData)cropsData.CropsDataArl[i];

                    if (tempCropData.ActiveBln)
                    {
                        AvailableCropsArl.Add(tempCropData);
                    }
                }
            }
        }
        
        protected string Get_Crop(int cropKey)
        {
            string ret = string.Empty;

                //Find crop description from the data.
                for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
                {
                    MainMenu.CropData tempCropData = (MainMenu.CropData)cropsData.CropsDataArl[i];

                    if (tempCropData.KeyInt == cropKey)
                    {
                        ret = tempCropData.DescriptionStr;
                        break;
                    }
                }
            
            return ret;
        }
        #endregion
        #endregion

        #region Get Job Data
        // ~!~!~   INSTRUCTIONS....   ~!~!~ 
        //
        // To use this Selector for another data type or on another form:
        //      1. Copy this whole Selector (Job) #region to the new form.
        //      2. Find & Rename all 'Job' & 'job' to match the new data.
        //      3. Call this constructor -"ConstructSelector_Job()" to the new form's constructor.
        //      4. Copy the Grouped Box from this designer view to the new form's designer view.
        //      5. Rename all of the objects in the designer view - change 'Job' to match new data.
        //      6. Depending - You may have to change the XML file name in the 'Vars' region.

        #region Vars
        string JobXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
        MainMenu.JobsData jobsData;

        private ArrayList AvailableJobsArl = new ArrayList();
        private ArrayList SelectedJobsArl = new ArrayList();
        #endregion

        #region Constructor
        protected void ConstructSelector_Job()
        {
            //Fill the available jobs.
            FillAvailable_Job();
        }
        #endregion

        #region Functions
        protected void FillAvailable_Job()
        {
            FileInfo fileInfo = new FileInfo(JobXML);

            if (fileInfo.Exists)
            {
                //Fill 'jobsData' from XML.

                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(JobXML, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
                fileStream.Close();

                //Fill the list from the data.
                for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
                {
                    MainMenu.JobData tempJobData = (MainMenu.JobData)jobsData.JobsDataArl[i];

                    if (tempJobData.ActiveBln)
                    {
                        AvailableJobsArl.Add(tempJobData);
                    }
                }
            }
        }
        protected string Get_Job(int jobKey)
        {
            string ret = string.Empty;

                //Find job description from the data.
                for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
                {
                    MainMenu.JobData tempJobData = (MainMenu.JobData)jobsData.JobsDataArl[i];

                    if (tempJobData.KeyInt == jobKey)
                    {
                        ret = tempJobData.DescriptionStr;
                        break;
                    }
                }
            
            return ret;
        }
        
        #endregion
        #endregion

        #region Get Field Data
        // ~!~!~   INSTRUCTIONS....   ~!~!~ 
        //
        // To use this Selector for another data type or on another form:
        //      1. Copy this whole Selector (Field) #region to the new form.
        //      2. Find & Rename all 'Field' & 'field' to match the new data.
        //      3. Call this constructor -"ConstructSelector_Field()" to the new form's constructor.
        //      4. Copy the Grouped Box from this designer view to the new form's designer view.
        //      5. Rename all of the objects in the designer view - change 'Field' to match new data.
        //      6. Depending - You may have to change the XML file name in the 'Vars' region.

        #region Vars
        string FieldXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
        MainMenu.FieldsData fieldsData;

        private ArrayList AvailableFieldsArl = new ArrayList();
        private ArrayList SelectedFieldsArl = new ArrayList();
        #endregion

        #region Constructor
        protected void ConstructSelector_Field()
        {
            //Fill the available fields.
            FillAvailable_Field();
        }
        #endregion

        #region Functions
        protected void FillAvailable_Field()
        {
            FileInfo fileInfo = new FileInfo(FieldXML);

            if (fileInfo.Exists)
            {
                //Fill 'fieldsData' from XML.

                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(FieldXML, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
                fileStream.Close();

                //Fill the list from the data.
                for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
                {
                    MainMenu.FieldData tempFieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

                    if (tempFieldData.ActiveBln)
                    {
                        AvailableFieldsArl.Add(tempFieldData);
                    }
                }
            }
        }
        protected string Get_Field(int fieldKey)
        {
            string ret = string.Empty;

            //Find field description from the data.
            for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
            {
                MainMenu.FieldData tempFieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

                if (tempFieldData.KeyInt == fieldKey)
                {
                    ret = tempFieldData.DescriptionStr;
                    break;
                }
            }

            return ret;
        }
        #endregion
        #endregion

		#region Get Pay Detail Data
		#region Vars
        string PayDetailDataXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayDetailData.xml";
        MainMenu.PayDetailsData payDetailsData;

        private ArrayList AvailablePayDetailsDataArl = new ArrayList();//not really needed
        private ArrayList SelectedPayDetailsDataArl = new ArrayList();
		#endregion

		#region Constructor
        protected void Construct_PayDetailData()
        {
            FileInfo fileInfo = new FileInfo(PayDetailDataXML);

            if (fileInfo.Exists)
            {
                //Fill 'payDetailsData' from XML.

                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayDetailsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(PayDetailDataXML, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                payDetailsData = (MainMenu.PayDetailsData)serializer.Deserialize(xmlReader);
                fileStream.Close();

                //Fill the list from the data.
                for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
                {
                    MainMenu.PayDetailData tempPayDetailData = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];


                    AvailablePayDetailsDataArl.Add(tempPayDetailData);

                }
            }
		}
		#endregion
		#endregion

        #region Get Employee Data
        // ~!~!~   INSTRUCTIONS....   ~!~!~ 
        //
        // To use this Selector for another data type or on another form:
        //      1. Copy this whole Selector (Employee) #region to the new form.
        //      2. Find & Rename all 'Employee' & 'employee' to match the new data.
        //      3. Call this constructor -"ConstructSelector_Employee()" to the new form's constructor.
        //      4. Copy the Grouped Box from this designer view to the new form's designer view.
        //      5. Rename all of the objects in the designer view - change 'Employee' to match new data.
        //      6. Depending - You may have to change the XML file name in the 'Vars' region.

        #region Vars
        string EmployeeXML = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
        MainMenu.EmployeesData employeesData;

        private ArrayList AvailableEmployeesArl = new ArrayList();
        private ArrayList SelectedEmployeesArl = new ArrayList();
        #endregion

        #region Constructor
        protected void ConstructSelector_Employee()
        {
            //Fill the available employees.
            FillAvailable_Employee();
        }
        #endregion

        #region Functions
        protected void FillAvailable_Employee()
        {
            FileInfo fileInfo = new FileInfo(EmployeeXML);

            if (fileInfo.Exists)
            {
                //Fill 'employeesData' from XML.

                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(EmployeeXML, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
                fileStream.Close();

                //Fill the list from the data.
                for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
                {
                    MainMenu.EmployeeData tempEmployeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                    if (tempEmployeeData.ActiveBln)
                    {
                        AvailableEmployeesArl.Add(tempEmployeeData);
                    }
                }
            }
        }
        protected string Get_Employee(int employeeKey)
        {
            string ret = string.Empty;

            //Find employee description from the data.
            for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
            {
                MainMenu.EmployeeData tempEmployeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                if (tempEmployeeData.KeyInt == employeeKey)
                {
                    ret = tempEmployeeData.NameStr;
                    break;
                }
            }

            return ret;
        }
       #endregion
        #endregion

        #endregion

        #region Public Helper
        private EmployeePayDetails FindOrAddEmployeeDetail(ArrayList list, string codeStr, string dateStr, string summaryNumberStr)
        {
            EmployeePayDetails tempEmployeeDetail = new EmployeePayDetails(codeStr, dateStr, summaryNumberStr);
            int tempEmployeeDetailIdx = list.BinarySearch(tempEmployeeDetail);

            if (tempEmployeeDetailIdx < 0)
                list.Insert(Math.Abs(tempEmployeeDetailIdx) - 1, tempEmployeeDetail);
            else
                tempEmployeeDetail = (EmployeePayDetails)list[tempEmployeeDetailIdx];

            return tempEmployeeDetail;
        }
        private CropDetail FindOrAddCropDetail(ArrayList list, string cropStr)
        {
            CropDetail tempCropDetail = new CropDetail(cropStr);
            int tempCropDetailIdx = list.BinarySearch(tempCropDetail);

            if (tempCropDetailIdx < 0)
                list.Insert(Math.Abs(tempCropDetailIdx) - 1, tempCropDetail);
            else
                tempCropDetail = (CropDetail)list[tempCropDetailIdx];

            return tempCropDetail;
        }
        private JobDetail FindOrAddJobDetail(ArrayList list, string jobStr)
        {
            JobDetail tempJobDetail = new JobDetail(jobStr);
            int tempJobDetailIdx = list.BinarySearch(tempJobDetail);

            if (tempJobDetailIdx < 0)
                list.Insert(Math.Abs(tempJobDetailIdx) - 1, tempJobDetail);
            else
                tempJobDetail = (JobDetail)list[tempJobDetailIdx];

            return tempJobDetail;
        }
        private FieldDetail FindOrAddFieldDetail(ArrayList list, string fieldStr)
       {
           FieldDetail tempFieldDetail = new FieldDetail(fieldStr);
           int tempFieldDetailIdx = list.BinarySearch(tempFieldDetail);

           if (tempFieldDetailIdx < 0)
               list.Insert(Math.Abs(tempFieldDetailIdx) - 1, tempFieldDetail);
           else
               tempFieldDetail = (FieldDetail)list[tempFieldDetailIdx];

           return tempFieldDetail;
       }
        private CropJobDetail FindOrAddCropJobDetail(ArrayList list, string cropStr, string jobStr)
       {
           CropJobDetail tempCropJobDetail = new CropJobDetail(cropStr, jobStr);
           int tempCropJobDetailIdx = list.BinarySearch(tempCropJobDetail);

           if (tempCropJobDetailIdx < 0)
               list.Insert(Math.Abs(tempCropJobDetailIdx) - 1, tempCropJobDetail);
           else
               tempCropJobDetail = (CropJobDetail)list[tempCropJobDetailIdx];

           return tempCropJobDetail;
       }
        private CropFieldDetail FindOrAddCropFieldDetail(ArrayList list, string cropStr, string fieldStr)
       {
           CropFieldDetail tempCropFieldDetail = new CropFieldDetail(cropStr, fieldStr);
           int tempCropFieldDetailIdx = list.BinarySearch(tempCropFieldDetail);

           if (tempCropFieldDetailIdx < 0)
               list.Insert(Math.Abs(tempCropFieldDetailIdx) - 1, tempCropFieldDetail);
           else
               tempCropFieldDetail = (CropFieldDetail)list[tempCropFieldDetailIdx];

           return tempCropFieldDetail;
       }
        private JobFieldDetail FindOrAddJobFieldDetail(ArrayList list, string jobStr, string fieldStr)
       {
           JobFieldDetail tempJobFieldDetail = new JobFieldDetail(jobStr, fieldStr);
           int tempJobFieldDetailIdx = list.BinarySearch(tempJobFieldDetail);

           if (tempJobFieldDetailIdx < 0)
               list.Insert(Math.Abs(tempJobFieldDetailIdx) - 1, tempJobFieldDetail);
           else
               tempJobFieldDetail = (JobFieldDetail)list[tempJobFieldDetailIdx];

           return tempJobFieldDetail;
       }
        private CropJobFieldDetail FindOrAddCropJobFieldDetail(ArrayList list, string cropStr, string jobStr, string fieldStr)
       {
           CropJobFieldDetail tempCropJobFieldDetail = new CropJobFieldDetail(cropStr, jobStr, fieldStr);
           int tempCropJobFieldDetailIdx = list.BinarySearch(tempCropJobFieldDetail);

           if (tempCropJobFieldDetailIdx < 0)
               list.Insert(Math.Abs(tempCropJobFieldDetailIdx) - 1, tempCropJobFieldDetail);
           else
               tempCropJobFieldDetail = (CropJobFieldDetail)list[tempCropJobFieldDetailIdx];

           return tempCropJobFieldDetail;
       }
        private JobCropFieldDetail FindOrAddJobCropFieldDetail(ArrayList list, string jobStr, string cropStr, string fieldStr)
       {
           JobCropFieldDetail tempJobCropFieldDetail = new JobCropFieldDetail(jobStr, cropStr, fieldStr);
           int tempJobCropFieldDetailIdx = list.BinarySearch(tempJobCropFieldDetail);

           if (tempJobCropFieldDetailIdx < 0)
               list.Insert(Math.Abs(tempJobCropFieldDetailIdx) - 1, tempJobCropFieldDetail);
           else
               tempJobCropFieldDetail = (JobCropFieldDetail)list[tempJobCropFieldDetailIdx];

           return tempJobCropFieldDetail;
       }
        private FieldCropJobDetail FindOrAddFieldCropJobDetail(ArrayList list, string fieldStr, string cropStr, string jobStr)
       {
           FieldCropJobDetail tempFieldCropJobDetail = new FieldCropJobDetail(fieldStr, cropStr, jobStr);
           int tempFieldCropJobDetailIdx = list.BinarySearch(tempFieldCropJobDetail);

           if (tempFieldCropJobDetailIdx < 0)
               list.Insert(Math.Abs(tempFieldCropJobDetailIdx) - 1, tempFieldCropJobDetail);
           else
               tempFieldCropJobDetail = (FieldCropJobDetail)list[tempFieldCropJobDetailIdx];

           return tempFieldCropJobDetail;
       }
        #endregion

        #region CBO
        public class ReportDetails : IComparable
        {
            #region Vars
            public int keyInt;
            public string dateStr;
            public string startTimeStr;
            public string endTimeStr;
            public string employeeStr;
            public double hoursDbl;
            public string cropStr;
            public string jobStr;
            public string fieldStr;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double priceDbl;
            public string summaryNoStr;
            public string typeStr;
            public string reportDateStr;
            public double amountDbl;
            #endregion

            public ReportDetails()
            {
                keyInt = 0;
                dateStr = string.Empty;
                startTimeStr = string.Empty;
                endTimeStr = string.Empty;
                employeeStr = string.Empty;
                hoursDbl = 0.0f;
                cropStr = string.Empty;
                jobStr = string.Empty;
                fieldStr = string.Empty;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                priceDbl = 0.0f;
                summaryNoStr = string.Empty;
                typeStr = string.Empty;
                reportDateStr = string.Empty;
                amountDbl = 0.0f;
            }
            public ReportDetails(Int32 KeyInt, string DateStr, string StartTimeStr, string EndTimeStr, string EmployeeStr, double HoursDbl, string CropStr, string JobStr, string FieldStr, double PayUnitDbl, double YieldUnitDbl, double PriceDbl, string SummaryNoStr, string TypeStr, string ReportDateStr, double AmountDbl)
            {
                keyInt = KeyInt;
                dateStr = DateStr;
                startTimeStr = StartTimeStr;
                endTimeStr = EndTimeStr;
                employeeStr = EmployeeStr;
                hoursDbl = HoursDbl;
                cropStr = CropStr;
                jobStr = JobStr;
                fieldStr = FieldStr;
                payUnitDbl = PayUnitDbl;
                yieldUnitDbl = YieldUnitDbl;
                priceDbl = PriceDbl;
                summaryNoStr = SummaryNoStr;
                typeStr = TypeStr;
                reportDateStr = ReportDateStr;
                amountDbl = AmountDbl;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                ReportDetails Y = (ReportDetails)obj;

                //Compare dateStr
                tempCompare = this.dateStr.CompareTo(Y.dateStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare startTimeStr
                tempCompare = this.startTimeStr.CompareTo(Y.startTimeStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare endTimeStr.
                tempCompare = this.endTimeStr.CompareTo(Y.endTimeStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare employeeStr.
                tempCompare = this.employeeStr.CompareTo(Y.employeeStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare cropStr.
                tempCompare = this.cropStr.CompareTo(Y.cropStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare jobStr.
                tempCompare = this.jobStr.CompareTo(Y.jobStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare fieldStr.
                tempCompare = this.fieldStr.CompareTo(Y.fieldStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare summaryNoStr.
                tempCompare = this.summaryNoStr.CompareTo(Y.summaryNoStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare typeStr.
                tempCompare = this.typeStr.CompareTo(Y.typeStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare reportDateStr.
                tempCompare = this.reportDateStr.CompareTo(Y.reportDateStr);
                if (tempCompare != 0)
                    return tempCompare;
                
                return 0;
            }
            #endregion
        }
        public class EmployeePayDetails : IComparable
        {
            #region Vars
            public string codeStr;
            public string dateStr;
            public string summaryNumberStr;
            #endregion

            public EmployeePayDetails()
            {
                codeStr = string.Empty;
                dateStr = string.Empty;
                summaryNumberStr = string.Empty;
            }
            public EmployeePayDetails(string CodeStr, string Datestr, string SummaryNumberStr)
            {
                codeStr = CodeStr;
                dateStr = Datestr;
                summaryNumberStr = SummaryNumberStr;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                EmployeePayDetails Y = (EmployeePayDetails)obj;

                //Compare codeStr
                tempCompare = this.codeStr.CompareTo(Y.codeStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare DateStr
                tempCompare = this.dateStr.CompareTo(Y.dateStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare SummaryNumberStr
                tempCompare = this.summaryNumberStr.CompareTo(Y.summaryNumberStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class CropDetail : IComparable
        {
            #region Vars
            public string cropStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            public CropDetail()
            {
                cropStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public CropDetail(string CropStr)
            {
                cropStr = CropStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f; 
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                CropDetail Y = (CropDetail)obj;

                //Compare cropStr
                tempCompare = this.cropStr.CompareTo(Y.cropStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class JobDetail : IComparable
        {
            #region Vars
            public string jobStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            public JobDetail()
            {
                jobStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f; 
            }
            public JobDetail(string JobStr)
            {
                jobStr = JobStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                JobDetail Y = (JobDetail)obj;

                //Compare jobStr
                tempCompare = this.jobStr.CompareTo(Y.jobStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class FieldDetail : IComparable
        {
            #region Vars
            public string fieldStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            public FieldDetail()
            {
                fieldStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public FieldDetail(string FieldStr)
            {
                fieldStr = FieldStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                FieldDetail Y = (FieldDetail)obj;

                //Compare fieldStr
                tempCompare = this.fieldStr.CompareTo(Y.fieldStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class CropJobDetail : IComparable
        {
            #region Vars
            public string cropStr;
            public string jobStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            public CropJobDetail()
            {
                cropStr = string.Empty;
                jobStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public CropJobDetail(string CropStr, string Jobstr)
            {
                cropStr = CropStr;
                jobStr = Jobstr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                CropJobDetail Y = (CropJobDetail)obj;

                //Compare cropStr
                tempCompare = this.cropStr.CompareTo(Y.cropStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare jobStr
                tempCompare = this.jobStr.CompareTo(Y.jobStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class CropFieldDetail : IComparable
        {
            #region Vars
            public string cropStr;
            public string fieldStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            public CropFieldDetail()
            {
                cropStr = string.Empty;
                fieldStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public CropFieldDetail(string CropStr, string FieldStr)
            {
                cropStr = CropStr;
                fieldStr = FieldStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                CropFieldDetail Y = (CropFieldDetail)obj;

                //Compare cropStr
                tempCompare = this.cropStr.CompareTo(Y.cropStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare fieldStr
                tempCompare = this.fieldStr.CompareTo(Y.fieldStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class JobFieldDetail : IComparable
        {
            #region Vars
            public string jobStr;
            public string fieldStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            public JobFieldDetail()
            {
                jobStr = string.Empty;
                fieldStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public JobFieldDetail(string JobStr, string FieldStr)
            {
                jobStr = JobStr;
                fieldStr = FieldStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                JobFieldDetail Y = (JobFieldDetail)obj;

                //Compare jobStr
                tempCompare = this.jobStr.CompareTo(Y.jobStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare fieldStr
                tempCompare = this.fieldStr.CompareTo(Y.fieldStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class CropJobFieldDetail : IComparable
        {
            #region Vars
            public string cropStr;
            public string jobStr;
            public string fieldStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            public CropJobFieldDetail()
            {
                cropStr = string.Empty;
                jobStr = string.Empty;
                fieldStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public CropJobFieldDetail(string CropStr, string JobStr, string FieldStr)
            {
                cropStr = CropStr;
                jobStr = JobStr;
                fieldStr = FieldStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                CropJobFieldDetail Y = (CropJobFieldDetail)obj;

                //Compare cropStr
                tempCompare = this.cropStr.CompareTo(Y.cropStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare jobStr
                tempCompare = this.jobStr.CompareTo(Y.jobStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare fieldStr
                tempCompare = this.fieldStr.CompareTo(Y.fieldStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class JobCropFieldDetail : IComparable
        {
            #region Vars
            public string jobStr;
            public string cropStr;
            public string fieldStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            #region Constructor
            public JobCropFieldDetail()
            {
                jobStr = string.Empty;
                cropStr = string.Empty;
                fieldStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public JobCropFieldDetail(string JobStr, string CropStr, string FieldStr)
            {
                jobStr = JobStr;
                cropStr = CropStr;                
                fieldStr = FieldStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            } 
            #endregion

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                JobCropFieldDetail Y = (JobCropFieldDetail)obj;

                //Compare jobStr
                tempCompare = this.jobStr.CompareTo(Y.jobStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare cropStr
                tempCompare = this.cropStr.CompareTo(Y.cropStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare fieldStr
                tempCompare = this.fieldStr.CompareTo(Y.fieldStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        public class FieldCropJobDetail : IComparable
        {
            #region Vars
            public string fieldStr;
            public string cropStr;
            public string jobStr;
            public double hoursDbl;
            public double payUnitDbl;
            public double yieldUnitDbl;
            public double amountDbl;
            #endregion

            #region Constructor
            public FieldCropJobDetail()
            {
                fieldStr = string.Empty;
                cropStr = string.Empty;
                jobStr = string.Empty;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            public FieldCropJobDetail(string FieldStr, string CropStr, string JobStr) 
            {
                fieldStr = FieldStr;
                cropStr = CropStr;
                jobStr = JobStr;
                hoursDbl = 0.0f;
                payUnitDbl = 0.0f;
                yieldUnitDbl = 0.0f;
                amountDbl = 0.0f;
            }
            #endregion

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                FieldCropJobDetail Y = (FieldCropJobDetail)obj;

                //Compare fieldStr
                tempCompare = this.fieldStr.CompareTo(Y.fieldStr);
                if (tempCompare != 0)
                    return tempCompare;
               
                //Compare cropStr
                tempCompare = this.cropStr.CompareTo(Y.cropStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare jobStr
                tempCompare = this.jobStr.CompareTo(Y.jobStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        #endregion

       

    }
}