﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Diagnostics;
using System.Management;
using SoftekBarcodeMakerLib2;
using System.Drawing.Printing;


namespace StorageInventory
{
    public partial class PayrollOptions : Form
    {
        #region Vars

        private MainMenu.CropsData cropsData;
        private MainMenu.JobsData jobsData;
        private MainMenu.FieldsData fieldsData;
        private MainMenu.PayrollOptionsData payrollOptionsData;

        private ArrayList CropsArl;
        private ArrayList FieldsArl;
        private ArrayList JobsArl;

        private string xmlFileName_Crops;
        private string xmlFileName_Jobs;
        private string xmlFileName_Fields;
        private string xmlFileName_payrollOptionsData;

        //Program option variables.
        private char delimiter;
        private string delimiterWithSpaces;
        #endregion

        #region Constructor
        public PayrollOptions()
        {
            //Initialize the program option variables.
            delimiter = '~';
            delimiterWithSpaces = " " + delimiter.ToString() + " ";

            //Default-Initialize.
            InitializeComponent();

            //Make form non-resizable.
            //this.MinimumSize = this.MaximumSize = this.Size;
            //this.FormBorderStyle = FormBorderStyle.FixedSingle;

            //Initialize object variables.
            cropsData = new MainMenu.CropsData();
            cropsData = new MainMenu.CropsData();
            jobsData = new MainMenu.JobsData();
            fieldsData = new MainMenu.FieldsData();
            payrollOptionsData = new MainMenu.PayrollOptionsData();
            FillPrintersComboBoxes();
            //Set the XML file names.
            xmlFileName_Crops = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
            xmlFileName_Jobs = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
            xmlFileName_Fields = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
            xmlFileName_payrollOptionsData = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayrollOptionsData.xml";

            //Load all of our data from XML.
            FillCropDataFromXML();
            FillFieldDataFromXML();
            FillJobDataFromXML();
            FillPayrollOptionsDataFromXML();

            //Fill the dropdown lists.
            //FillCropsDropdown();
            //FillJobsDropdown();
            //FillFieldsDropdown();

            //Fill the saved payroll options.
            FillSavedPayrollOptions();
            CompanyNameData.Text = MainMenu.CompanyStr;
        }
        #endregion

        #region Form Functions
        #region Button Click Functions.
        private void saveBtn_Click(object sender, EventArgs e)
        {
            //Refill the PayDetail object from XML.
            FillPayrollOptionsDataFromXML();


            payrollOptionsData.LabelPrinterStr = labelsPrinterCbx.SelectedItem.ToString();
            payrollOptionsData.ReportPrinterStr = reportsPrinterCbx.SelectedItem.ToString();

            //Save the Pay Details To XML.
            SavePayDetailDataToXML();

            MessageBox.Show(this, "Printer Options Saved.", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);

            //Refill the GUI list.
            FillSavedPayrollOptions();
        }
        private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        #region KeyDown Functions (For Enter-As-Tab Functionality)
        private void payOvertimeAllRdo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void cropForOvertimeCbx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void payOvertimeEmployeeRdo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void hoursRegularTimeTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void jobForOvertimeCbx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void fieldForOvertimeCbx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void fieldForMinimumWageAdjustmentCbx_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void minimumWageTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        private void saveBtn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
        #endregion

        #region Input Validation Functions (Checking for proper data type inputs).
        private void hoursRegularTimeTxt_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != "")
            {
                ValidateQuantity((TextBox)sender, "Hours Regular Time");
            }
        }
        private void minimumWageTxt_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != "")
            {
                ValidateQuantity((TextBox)sender, "Minimum Wage");
            }
        }
        #endregion
        #endregion

        #region Helper Methods
        //Get Data
        private void FillCropDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Crops);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Crops, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillJobDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Jobs);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Jobs, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillFieldDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Fields);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Fields, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private void FillPayrollOptionsDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_payrollOptionsData);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayrollOptionsData));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_payrollOptionsData, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                payrollOptionsData = (MainMenu.PayrollOptionsData)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }

        //Fill DropDown Functions
        private void FillCropsDropdown()
        {
            ////Clear the existing list.
            //this.cropForOvertimeCbx.Items.Clear();
            //this.cropForOvertimeCbx.SelectedIndex = -1;

            //CropsArl = new ArrayList();

            ////Fill the crop data object.
            //FillCropDataFromXML();

            ////Fill the GUI dropdown list from the data object.
            //for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
            //{
            //    MainMenu.CropData crop = (MainMenu.CropData)cropsData.CropsDataArl[i];

            //    if (crop.ActiveBln)
            //    {
            //        cropForOvertimeCbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
            //        CropsArl.Add(crop);
            //    }
            //}
        }
        private void FillJobsDropdown()
        {
            ////Clear the existing list.
            //this.jobForOvertimeCbx.Items.Clear();
            //this.jobForOvertimeCbx.SelectedIndex = -1;

            //JobsArl = new ArrayList();

            ////Fill the job data object.
            //FillJobDataFromXML();

            ////Fill the GUI dropdown list from the data object.
            //for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
            //{
            //    MainMenu.JobData job = (MainMenu.JobData)jobsData.JobsDataArl[i];

            //    if (job.ActiveBln)
            //    {
            //        jobForOvertimeCbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
            //        JobsArl.Add(job);
            //    }
            //}
        }
        private void FillFieldsDropdown()
        {
            ////Clear the existing list.
            //this.fieldForOvertimeCbx.Items.Clear();
            //this.fieldForOvertimeCbx.SelectedIndex = -1;

            //this.fieldForMinimumWageAdjustmentCbx.Items.Clear();
            //this.fieldForMinimumWageAdjustmentCbx.SelectedIndex = -1;

            //FieldsArl = new ArrayList();

            ////Fill the field data object.
            //FillFieldDataFromXML();

            ////Fill the GUI dropdown list from the data object.
            //for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
            //{
            //    MainMenu.FieldData field = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

            //    if (field.ActiveBln)
            //    {
            //        fieldForOvertimeCbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
            //        fieldForMinimumWageAdjustmentCbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
            //        FieldsArl.Add(field);
            //    }
            //}
        }

        //Fill ListBox Functions
        private void FillSavedPayrollOptions()
        {
            //Clear the existing list.
            //payNoOvertimeRdo.Checked = false;
            //payOvertimeAllRdo.Checked = false;
            //payOvertimeEmployeeRdo.Checked = false;
            //hoursRegularTimeTxt.Text = "0";
            //minimumWageTxt.Text = "0";

            //Fill the pay detail data object.
            FillPayrollOptionsDataFromXML();

            if (payrollOptionsData.PayOvertimeAllBln)
            {
                //payOvertimeAllRdo.Checked = true;
            }

            if (payrollOptionsData.OvertimePerEmployeeBln)
            {
                //payOvertimeEmployeeRdo.Checked = true;
            }

            if ((!payrollOptionsData.PayOvertimeAllBln) && (!payrollOptionsData.OvertimePerEmployeeBln))
            {
                //payNoOvertimeRdo.Checked = true;
            }

            //hoursRegularTimeTxt.Text = payrollOptionsData.RegularHoursDbl.ToString();
            //minimumWageTxt.Text = payrollOptionsData.MinimumWageDbl.ToString();
            for (int i = 0; i < labelsPrinterCbx.Items.Count; i++)
            {
                if (payrollOptionsData.LabelPrinterStr.Trim() == labelsPrinterCbx.Items[i].ToString())
                {
                    labelsPrinterCbx.SelectedIndex = i;
                    break;
                }
            }
            for (int i = 0; i < reportsPrinterCbx.Items.Count; i++)
            {
                if (payrollOptionsData.ReportPrinterStr.Trim() == reportsPrinterCbx.Items[i].ToString())
                {
                    reportsPrinterCbx.SelectedIndex = i;
                    break;
                }
            }

        }
        private void payNoOvertimeRdo_CheckedChanged(object sender, EventArgs e)
        {
            //hoursRegularTimeTxt.Text = "0";
        }
        //Save data to XML functions
        private void SavePayDetailDataToXML()
        {
            //Serialize (convert an object instance to an XML document):
            XmlSerializer xmlSerializer = new XmlSerializer(payrollOptionsData.GetType());
            // Create an XmlTextWriter using a FileStream.
            Stream fileStream = new FileStream(xmlFileName_payrollOptionsData, FileMode.Create);
            XmlWriter xmlWriter = new XmlTextWriter(fileStream, Encoding.Unicode);
            // Serialize using the XmlTextWriter.
            xmlSerializer.Serialize(xmlWriter, payrollOptionsData);
            xmlWriter.Flush();
            xmlWriter.Close();
        }

        //Validation Function(s)
        private void ValidateQuantity(TextBox textBoxToCheck, string textBoxName)
        {
            try
            {
                decimal temp = decimal.Parse(textBoxToCheck.Text);

                if (textBoxToCheck.Text != "" && textBoxToCheck.Text.IndexOf(",") == -1)
                {
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
                else if (textBoxToCheck.Text.IndexOf(",") != -1)
                {
                    textBoxToCheck.Text.Replace(",", "");
                    textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
                }
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show(this, "Invalid " + textBoxName + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxToCheck.Focus();
                textBoxToCheck.SelectAll();
            }
        }

        //Formating Function(s)
        private static string DisplayFormat(decimal value)
        {
            if (value == 0)
                return value.ToString();
            else
                return decimal.Round(value, 2).ToString("#,###,##0.##");
        }
        #endregion

        private void FillPrintersComboBoxes()
        {
            labelsPrinterCbx.SelectedIndex = -1;
            labelsPrinterCbx.Items.Clear();
            reportsPrinterCbx.SelectedIndex = -1;
            reportsPrinterCbx.Items.Clear();

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            ManagementObjectCollection results = searcher.Get();
            foreach (ManagementObject printer in results)
            {

                labelsPrinterCbx.Items.Add(printer["Name"].ToString());
                reportsPrinterCbx.Items.Add(printer["Name"].ToString());
            }
        }  
        
    }
}
