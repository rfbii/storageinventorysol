﻿namespace StorageInventory
{
    partial class PayData_Criteria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatePickerGroupBox = new System.Windows.Forms.GroupBox();
            this.DatePickerEndWeekLbl = new System.Windows.Forms.Label();
            this.DatePickerEndYearLbl = new System.Windows.Forms.Label();
            this.DatePickerStartWeekLbl = new System.Windows.Forms.Label();
            this.DatePickerStartYearLbl = new System.Windows.Forms.Label();
            this.DatePickerEndWeekTxt = new System.Windows.Forms.TextBox();
            this.DatePickerEndYearTxt = new System.Windows.Forms.TextBox();
            this.DatePickerStartWeekTxt = new System.Windows.Forms.TextBox();
            this.DatePickerStartYearTxt = new System.Windows.Forms.TextBox();
            this.DatePickerThruDateLbl = new System.Windows.Forms.Label();
            this.DatePickerThruDateTxt = new System.Windows.Forms.TextBox();
            this.DatePickerFromDateTxt = new System.Windows.Forms.TextBox();
            this.DatePickerFromDateLbl = new System.Windows.Forms.Label();
            this.DatePickerPresetsLbl = new System.Windows.Forms.Label();
            this.DatePickerPresetsCbx = new System.Windows.Forms.ComboBox();
            this.exitBtn = new System.Windows.Forms.Button();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.employeesAvailableLst = new System.Windows.Forms.ListBox();
            this.employeesSelectedLst = new System.Windows.Forms.ListBox();
            this.employeeAddBtn = new System.Windows.Forms.Button();
            this.employeeRemoveBtn = new System.Windows.Forms.Button();
            this.employeesAvailableLbl = new System.Windows.Forms.Label();
            this.employeesSelectedLbl = new System.Windows.Forms.Label();
            this.employeeGroupBox = new System.Windows.Forms.GroupBox();
            this.cropGroupBox = new System.Windows.Forms.GroupBox();
            this.cropsAvailableLst = new System.Windows.Forms.ListBox();
            this.cropsSelectedLbl = new System.Windows.Forms.Label();
            this.cropsSelectedLst = new System.Windows.Forms.ListBox();
            this.cropsAvailableLbl = new System.Windows.Forms.Label();
            this.cropAddBtn = new System.Windows.Forms.Button();
            this.cropRemoveBtn = new System.Windows.Forms.Button();
            this.jobGroupBox = new System.Windows.Forms.GroupBox();
            this.jobsAvailableLst = new System.Windows.Forms.ListBox();
            this.jobsSelectedLbl = new System.Windows.Forms.Label();
            this.jobsSelectedLst = new System.Windows.Forms.ListBox();
            this.jobsAvailableLbl = new System.Windows.Forms.Label();
            this.jobAddBtn = new System.Windows.Forms.Button();
            this.jobRemoveBtn = new System.Windows.Forms.Button();
            this.fieldGroupBox = new System.Windows.Forms.GroupBox();
            this.fieldsAvailableLst = new System.Windows.Forms.ListBox();
            this.fieldsSelectedLbl = new System.Windows.Forms.Label();
            this.fieldsSelectedLst = new System.Windows.Forms.ListBox();
            this.fieldsAvailableLbl = new System.Windows.Forms.Label();
            this.fieldAddBtn = new System.Windows.Forms.Button();
            this.fieldRemoveBtn = new System.Windows.Forms.Button();
            this.runBtn = new System.Windows.Forms.Button();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.makePDFBtn = new System.Windows.Forms.Button();
            this.makeExcelBtn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.GrowerBlockGroupBox = new System.Windows.Forms.GroupBox();
            this.growerBlockAvailableLst = new System.Windows.Forms.ListBox();
            this.growerBlockLbl = new System.Windows.Forms.Label();
            this.growerAddBtn = new System.Windows.Forms.Button();
            this.growerRemoveBtn = new System.Windows.Forms.Button();
            this.growerBlockSelectedLst = new System.Windows.Forms.ListBox();
            this.growerSelectedLbl = new System.Windows.Forms.Label();
            this.DatePickerGroupBox.SuspendLayout();
            this.employeeGroupBox.SuspendLayout();
            this.cropGroupBox.SuspendLayout();
            this.jobGroupBox.SuspendLayout();
            this.fieldGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.GrowerBlockGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatePickerGroupBox
            // 
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndWeekLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndYearLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartWeekLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartYearLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndWeekTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndYearTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartWeekTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartYearTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerThruDateLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerThruDateTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerFromDateTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerFromDateLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerPresetsLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerPresetsCbx);
            this.DatePickerGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerGroupBox.Location = new System.Drawing.Point(64, 44);
            this.DatePickerGroupBox.Name = "DatePickerGroupBox";
            this.DatePickerGroupBox.Size = new System.Drawing.Size(684, 95);
            this.DatePickerGroupBox.TabIndex = 0;
            this.DatePickerGroupBox.TabStop = false;
            // 
            // DatePickerEndWeekLbl
            // 
            this.DatePickerEndWeekLbl.AutoSize = true;
            this.DatePickerEndWeekLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndWeekLbl.Location = new System.Drawing.Point(619, 11);
            this.DatePickerEndWeekLbl.Name = "DatePickerEndWeekLbl";
            this.DatePickerEndWeekLbl.Size = new System.Drawing.Size(47, 18);
            this.DatePickerEndWeekLbl.TabIndex = 13;
            this.DatePickerEndWeekLbl.Text = "Week";
            // 
            // DatePickerEndYearLbl
            // 
            this.DatePickerEndYearLbl.AutoSize = true;
            this.DatePickerEndYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndYearLbl.Location = new System.Drawing.Point(518, 11);
            this.DatePickerEndYearLbl.Name = "DatePickerEndYearLbl";
            this.DatePickerEndYearLbl.Size = new System.Drawing.Size(68, 18);
            this.DatePickerEndYearLbl.TabIndex = 12;
            this.DatePickerEndYearLbl.Text = "End Year";
            // 
            // DatePickerStartWeekLbl
            // 
            this.DatePickerStartWeekLbl.AutoSize = true;
            this.DatePickerStartWeekLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartWeekLbl.Location = new System.Drawing.Point(444, 11);
            this.DatePickerStartWeekLbl.Name = "DatePickerStartWeekLbl";
            this.DatePickerStartWeekLbl.Size = new System.Drawing.Size(47, 18);
            this.DatePickerStartWeekLbl.TabIndex = 11;
            this.DatePickerStartWeekLbl.Text = "Week";
            // 
            // DatePickerStartYearLbl
            // 
            this.DatePickerStartYearLbl.AutoSize = true;
            this.DatePickerStartYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartYearLbl.Location = new System.Drawing.Point(340, 11);
            this.DatePickerStartYearLbl.Name = "DatePickerStartYearLbl";
            this.DatePickerStartYearLbl.Size = new System.Drawing.Size(73, 18);
            this.DatePickerStartYearLbl.TabIndex = 9;
            this.DatePickerStartYearLbl.Text = "Start Year";
            // 
            // DatePickerEndWeekTxt
            // 
            this.DatePickerEndWeekTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndWeekTxt.Location = new System.Drawing.Point(615, 33);
            this.DatePickerEndWeekTxt.Name = "DatePickerEndWeekTxt";
            this.DatePickerEndWeekTxt.Size = new System.Drawing.Size(54, 24);
            this.DatePickerEndWeekTxt.TabIndex = 6;
            // 
            // DatePickerEndYearTxt
            // 
            this.DatePickerEndYearTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndYearTxt.Location = new System.Drawing.Point(502, 33);
            this.DatePickerEndYearTxt.Name = "DatePickerEndYearTxt";
            this.DatePickerEndYearTxt.Size = new System.Drawing.Size(105, 24);
            this.DatePickerEndYearTxt.TabIndex = 5;
            // 
            // DatePickerStartWeekTxt
            // 
            this.DatePickerStartWeekTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartWeekTxt.Location = new System.Drawing.Point(440, 33);
            this.DatePickerStartWeekTxt.Name = "DatePickerStartWeekTxt";
            this.DatePickerStartWeekTxt.Size = new System.Drawing.Size(54, 24);
            this.DatePickerStartWeekTxt.TabIndex = 4;
            // 
            // DatePickerStartYearTxt
            // 
            this.DatePickerStartYearTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartYearTxt.Location = new System.Drawing.Point(328, 33);
            this.DatePickerStartYearTxt.Name = "DatePickerStartYearTxt";
            this.DatePickerStartYearTxt.Size = new System.Drawing.Size(105, 24);
            this.DatePickerStartYearTxt.TabIndex = 3;
            // 
            // DatePickerThruDateLbl
            // 
            this.DatePickerThruDateLbl.AutoSize = true;
            this.DatePickerThruDateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerThruDateLbl.Location = new System.Drawing.Point(415, 68);
            this.DatePickerThruDateLbl.Name = "DatePickerThruDateLbl";
            this.DatePickerThruDateLbl.Size = new System.Drawing.Size(73, 18);
            this.DatePickerThruDateLbl.TabIndex = 14;
            this.DatePickerThruDateLbl.Text = "Thru Date";
            // 
            // DatePickerThruDateTxt
            // 
            this.DatePickerThruDateTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerThruDateTxt.Location = new System.Drawing.Point(494, 66);
            this.DatePickerThruDateTxt.Name = "DatePickerThruDateTxt";
            this.DatePickerThruDateTxt.Size = new System.Drawing.Size(144, 24);
            this.DatePickerThruDateTxt.TabIndex = 2;
            // 
            // DatePickerFromDateTxt
            // 
            this.DatePickerFromDateTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerFromDateTxt.Location = new System.Drawing.Point(164, 66);
            this.DatePickerFromDateTxt.Name = "DatePickerFromDateTxt";
            this.DatePickerFromDateTxt.Size = new System.Drawing.Size(144, 24);
            this.DatePickerFromDateTxt.TabIndex = 1;
            // 
            // DatePickerFromDateLbl
            // 
            this.DatePickerFromDateLbl.AutoSize = true;
            this.DatePickerFromDateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerFromDateLbl.Location = new System.Drawing.Point(79, 69);
            this.DatePickerFromDateLbl.Name = "DatePickerFromDateLbl";
            this.DatePickerFromDateLbl.Size = new System.Drawing.Size(79, 18);
            this.DatePickerFromDateLbl.TabIndex = 8;
            this.DatePickerFromDateLbl.Text = "From Date";
            // 
            // DatePickerPresetsLbl
            // 
            this.DatePickerPresetsLbl.AutoSize = true;
            this.DatePickerPresetsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerPresetsLbl.Location = new System.Drawing.Point(6, 11);
            this.DatePickerPresetsLbl.Name = "DatePickerPresetsLbl";
            this.DatePickerPresetsLbl.Size = new System.Drawing.Size(169, 18);
            this.DatePickerPresetsLbl.TabIndex = 7;
            this.DatePickerPresetsLbl.Text = "Date Selections for Date";
            // 
            // DatePickerPresetsCbx
            // 
            this.DatePickerPresetsCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DatePickerPresetsCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerPresetsCbx.FormattingEnabled = true;
            this.DatePickerPresetsCbx.Location = new System.Drawing.Point(6, 33);
            this.DatePickerPresetsCbx.Name = "DatePickerPresetsCbx";
            this.DatePickerPresetsCbx.Size = new System.Drawing.Size(271, 26);
            this.DatePickerPresetsCbx.TabIndex = 0;
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(490, 730);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(95, 35);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Text = "&Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(581, 8);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(188, 20);
            this.subtitleLbl.TabIndex = 8;
            this.subtitleLbl.Text = "Storage Inventory Report";
            // 
            // employeesAvailableLst
            // 
            this.employeesAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeesAvailableLst.FormattingEnabled = true;
            this.employeesAvailableLst.ItemHeight = 18;
            this.employeesAvailableLst.Location = new System.Drawing.Point(6, 27);
            this.employeesAvailableLst.Name = "employeesAvailableLst";
            this.employeesAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.employeesAvailableLst.Sorted = true;
            this.employeesAvailableLst.TabIndex = 0;
            // 
            // employeesSelectedLst
            // 
            this.employeesSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeesSelectedLst.FormattingEnabled = true;
            this.employeesSelectedLst.ItemHeight = 18;
            this.employeesSelectedLst.Location = new System.Drawing.Point(474, 27);
            this.employeesSelectedLst.Name = "employeesSelectedLst";
            this.employeesSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.employeesSelectedLst.Sorted = true;
            this.employeesSelectedLst.TabIndex = 1;
            // 
            // employeeAddBtn
            // 
            this.employeeAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeAddBtn.Location = new System.Drawing.Point(348, 27);
            this.employeeAddBtn.Name = "employeeAddBtn";
            this.employeeAddBtn.Size = new System.Drawing.Size(95, 35);
            this.employeeAddBtn.TabIndex = 2;
            this.employeeAddBtn.Text = ">";
            this.employeeAddBtn.UseVisualStyleBackColor = true;
            // 
            // employeeRemoveBtn
            // 
            this.employeeRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeeRemoveBtn.Location = new System.Drawing.Point(348, 68);
            this.employeeRemoveBtn.Name = "employeeRemoveBtn";
            this.employeeRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.employeeRemoveBtn.TabIndex = 3;
            this.employeeRemoveBtn.Text = "<";
            this.employeeRemoveBtn.UseVisualStyleBackColor = true;
            // 
            // employeesAvailableLbl
            // 
            this.employeesAvailableLbl.AutoSize = true;
            this.employeesAvailableLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeesAvailableLbl.Location = new System.Drawing.Point(3, 8);
            this.employeesAvailableLbl.Name = "employeesAvailableLbl";
            this.employeesAvailableLbl.Size = new System.Drawing.Size(143, 18);
            this.employeesAvailableLbl.TabIndex = 4;
            this.employeesAvailableLbl.Text = "Available Customers";
            // 
            // employeesSelectedLbl
            // 
            this.employeesSelectedLbl.AutoSize = true;
            this.employeesSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.employeesSelectedLbl.Location = new System.Drawing.Point(471, 8);
            this.employeesSelectedLbl.Name = "employeesSelectedLbl";
            this.employeesSelectedLbl.Size = new System.Drawing.Size(143, 18);
            this.employeesSelectedLbl.TabIndex = 5;
            this.employeesSelectedLbl.Text = "Selected Customers";
            // 
            // employeeGroupBox
            // 
            this.employeeGroupBox.Controls.Add(this.employeesAvailableLst);
            this.employeeGroupBox.Controls.Add(this.employeesSelectedLbl);
            this.employeeGroupBox.Controls.Add(this.employeesSelectedLst);
            this.employeeGroupBox.Controls.Add(this.employeesAvailableLbl);
            this.employeeGroupBox.Controls.Add(this.employeeAddBtn);
            this.employeeGroupBox.Controls.Add(this.employeeRemoveBtn);
            this.employeeGroupBox.Location = new System.Drawing.Point(18, 136);
            this.employeeGroupBox.Name = "employeeGroupBox";
            this.employeeGroupBox.Size = new System.Drawing.Size(792, 117);
            this.employeeGroupBox.TabIndex = 1;
            this.employeeGroupBox.TabStop = false;
            // 
            // cropGroupBox
            // 
            this.cropGroupBox.Controls.Add(this.cropsAvailableLst);
            this.cropGroupBox.Controls.Add(this.cropsSelectedLbl);
            this.cropGroupBox.Controls.Add(this.cropsSelectedLst);
            this.cropGroupBox.Controls.Add(this.cropsAvailableLbl);
            this.cropGroupBox.Controls.Add(this.cropAddBtn);
            this.cropGroupBox.Controls.Add(this.cropRemoveBtn);
            this.cropGroupBox.Location = new System.Drawing.Point(18, 251);
            this.cropGroupBox.Name = "cropGroupBox";
            this.cropGroupBox.Size = new System.Drawing.Size(792, 117);
            this.cropGroupBox.TabIndex = 2;
            this.cropGroupBox.TabStop = false;
            // 
            // cropsAvailableLst
            // 
            this.cropsAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsAvailableLst.FormattingEnabled = true;
            this.cropsAvailableLst.ItemHeight = 18;
            this.cropsAvailableLst.Location = new System.Drawing.Point(6, 27);
            this.cropsAvailableLst.Name = "cropsAvailableLst";
            this.cropsAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.cropsAvailableLst.Sorted = true;
            this.cropsAvailableLst.TabIndex = 0;
            // 
            // cropsSelectedLbl
            // 
            this.cropsSelectedLbl.AutoSize = true;
            this.cropsSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsSelectedLbl.Location = new System.Drawing.Point(471, 8);
            this.cropsSelectedLbl.Name = "cropsSelectedLbl";
            this.cropsSelectedLbl.Size = new System.Drawing.Size(110, 18);
            this.cropsSelectedLbl.TabIndex = 5;
            this.cropsSelectedLbl.Text = "Selected Crops";
            // 
            // cropsSelectedLst
            // 
            this.cropsSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsSelectedLst.FormattingEnabled = true;
            this.cropsSelectedLst.ItemHeight = 18;
            this.cropsSelectedLst.Location = new System.Drawing.Point(474, 27);
            this.cropsSelectedLst.Name = "cropsSelectedLst";
            this.cropsSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.cropsSelectedLst.Sorted = true;
            this.cropsSelectedLst.TabIndex = 1;
            // 
            // cropsAvailableLbl
            // 
            this.cropsAvailableLbl.AutoSize = true;
            this.cropsAvailableLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropsAvailableLbl.Location = new System.Drawing.Point(6, 8);
            this.cropsAvailableLbl.Name = "cropsAvailableLbl";
            this.cropsAvailableLbl.Size = new System.Drawing.Size(110, 18);
            this.cropsAvailableLbl.TabIndex = 4;
            this.cropsAvailableLbl.Text = "Available Crops";
            // 
            // cropAddBtn
            // 
            this.cropAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropAddBtn.Location = new System.Drawing.Point(348, 27);
            this.cropAddBtn.Name = "cropAddBtn";
            this.cropAddBtn.Size = new System.Drawing.Size(95, 35);
            this.cropAddBtn.TabIndex = 2;
            this.cropAddBtn.Text = ">";
            this.cropAddBtn.UseVisualStyleBackColor = true;
            // 
            // cropRemoveBtn
            // 
            this.cropRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cropRemoveBtn.Location = new System.Drawing.Point(348, 68);
            this.cropRemoveBtn.Name = "cropRemoveBtn";
            this.cropRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.cropRemoveBtn.TabIndex = 3;
            this.cropRemoveBtn.Text = "<";
            this.cropRemoveBtn.UseVisualStyleBackColor = true;
            // 
            // jobGroupBox
            // 
            this.jobGroupBox.Controls.Add(this.jobsAvailableLst);
            this.jobGroupBox.Controls.Add(this.jobsSelectedLbl);
            this.jobGroupBox.Controls.Add(this.jobsSelectedLst);
            this.jobGroupBox.Controls.Add(this.jobsAvailableLbl);
            this.jobGroupBox.Controls.Add(this.jobAddBtn);
            this.jobGroupBox.Controls.Add(this.jobRemoveBtn);
            this.jobGroupBox.Location = new System.Drawing.Point(18, 366);
            this.jobGroupBox.Name = "jobGroupBox";
            this.jobGroupBox.Size = new System.Drawing.Size(792, 117);
            this.jobGroupBox.TabIndex = 3;
            this.jobGroupBox.TabStop = false;
            // 
            // jobsAvailableLst
            // 
            this.jobsAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsAvailableLst.FormattingEnabled = true;
            this.jobsAvailableLst.ItemHeight = 18;
            this.jobsAvailableLst.Location = new System.Drawing.Point(6, 27);
            this.jobsAvailableLst.Name = "jobsAvailableLst";
            this.jobsAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.jobsAvailableLst.Sorted = true;
            this.jobsAvailableLst.TabIndex = 0;
            // 
            // jobsSelectedLbl
            // 
            this.jobsSelectedLbl.AutoSize = true;
            this.jobsSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsSelectedLbl.Location = new System.Drawing.Point(471, 8);
            this.jobsSelectedLbl.Name = "jobsSelectedLbl";
            this.jobsSelectedLbl.Size = new System.Drawing.Size(142, 18);
            this.jobsSelectedLbl.TabIndex = 5;
            this.jobsSelectedLbl.Text = "Selected Box Labels";
            // 
            // jobsSelectedLst
            // 
            this.jobsSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsSelectedLst.FormattingEnabled = true;
            this.jobsSelectedLst.ItemHeight = 18;
            this.jobsSelectedLst.Location = new System.Drawing.Point(474, 27);
            this.jobsSelectedLst.Name = "jobsSelectedLst";
            this.jobsSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.jobsSelectedLst.Sorted = true;
            this.jobsSelectedLst.TabIndex = 1;
            // 
            // jobsAvailableLbl
            // 
            this.jobsAvailableLbl.AutoSize = true;
            this.jobsAvailableLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobsAvailableLbl.Location = new System.Drawing.Point(3, 8);
            this.jobsAvailableLbl.Name = "jobsAvailableLbl";
            this.jobsAvailableLbl.Size = new System.Drawing.Size(142, 18);
            this.jobsAvailableLbl.TabIndex = 4;
            this.jobsAvailableLbl.Text = "Available Box Labels";
            // 
            // jobAddBtn
            // 
            this.jobAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobAddBtn.Location = new System.Drawing.Point(348, 27);
            this.jobAddBtn.Name = "jobAddBtn";
            this.jobAddBtn.Size = new System.Drawing.Size(95, 35);
            this.jobAddBtn.TabIndex = 2;
            this.jobAddBtn.Text = ">";
            this.jobAddBtn.UseVisualStyleBackColor = true;
            // 
            // jobRemoveBtn
            // 
            this.jobRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jobRemoveBtn.Location = new System.Drawing.Point(348, 68);
            this.jobRemoveBtn.Name = "jobRemoveBtn";
            this.jobRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.jobRemoveBtn.TabIndex = 3;
            this.jobRemoveBtn.Text = "<";
            this.jobRemoveBtn.UseVisualStyleBackColor = true;
            // 
            // fieldGroupBox
            // 
            this.fieldGroupBox.Controls.Add(this.fieldsAvailableLst);
            this.fieldGroupBox.Controls.Add(this.fieldsSelectedLbl);
            this.fieldGroupBox.Controls.Add(this.fieldsSelectedLst);
            this.fieldGroupBox.Controls.Add(this.fieldsAvailableLbl);
            this.fieldGroupBox.Controls.Add(this.fieldAddBtn);
            this.fieldGroupBox.Controls.Add(this.fieldRemoveBtn);
            this.fieldGroupBox.Location = new System.Drawing.Point(18, 481);
            this.fieldGroupBox.Name = "fieldGroupBox";
            this.fieldGroupBox.Size = new System.Drawing.Size(792, 117);
            this.fieldGroupBox.TabIndex = 4;
            this.fieldGroupBox.TabStop = false;
            // 
            // fieldsAvailableLst
            // 
            this.fieldsAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsAvailableLst.FormattingEnabled = true;
            this.fieldsAvailableLst.ItemHeight = 18;
            this.fieldsAvailableLst.Location = new System.Drawing.Point(6, 27);
            this.fieldsAvailableLst.Name = "fieldsAvailableLst";
            this.fieldsAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.fieldsAvailableLst.Sorted = true;
            this.fieldsAvailableLst.TabIndex = 0;
            // 
            // fieldsSelectedLbl
            // 
            this.fieldsSelectedLbl.AutoSize = true;
            this.fieldsSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsSelectedLbl.Location = new System.Drawing.Point(471, 8);
            this.fieldsSelectedLbl.Name = "fieldsSelectedLbl";
            this.fieldsSelectedLbl.Size = new System.Drawing.Size(113, 18);
            this.fieldsSelectedLbl.TabIndex = 5;
            this.fieldsSelectedLbl.Text = "Selected Where";
            this.fieldsSelectedLbl.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fieldsSelectedLst
            // 
            this.fieldsSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsSelectedLst.FormattingEnabled = true;
            this.fieldsSelectedLst.ItemHeight = 18;
            this.fieldsSelectedLst.Location = new System.Drawing.Point(474, 27);
            this.fieldsSelectedLst.Name = "fieldsSelectedLst";
            this.fieldsSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.fieldsSelectedLst.Sorted = true;
            this.fieldsSelectedLst.TabIndex = 1;
            // 
            // fieldsAvailableLbl
            // 
            this.fieldsAvailableLbl.AutoSize = true;
            this.fieldsAvailableLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldsAvailableLbl.Location = new System.Drawing.Point(3, 8);
            this.fieldsAvailableLbl.Name = "fieldsAvailableLbl";
            this.fieldsAvailableLbl.Size = new System.Drawing.Size(113, 18);
            this.fieldsAvailableLbl.TabIndex = 4;
            this.fieldsAvailableLbl.Text = "Available Where";
            // 
            // fieldAddBtn
            // 
            this.fieldAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldAddBtn.Location = new System.Drawing.Point(348, 27);
            this.fieldAddBtn.Name = "fieldAddBtn";
            this.fieldAddBtn.Size = new System.Drawing.Size(95, 35);
            this.fieldAddBtn.TabIndex = 2;
            this.fieldAddBtn.Text = ">";
            this.fieldAddBtn.UseVisualStyleBackColor = true;
            // 
            // fieldRemoveBtn
            // 
            this.fieldRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fieldRemoveBtn.Location = new System.Drawing.Point(348, 68);
            this.fieldRemoveBtn.Name = "fieldRemoveBtn";
            this.fieldRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.fieldRemoveBtn.TabIndex = 3;
            this.fieldRemoveBtn.Text = "<";
            this.fieldRemoveBtn.UseVisualStyleBackColor = true;
            // 
            // runBtn
            // 
            this.runBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runBtn.Location = new System.Drawing.Point(719, 730);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(95, 35);
            this.runBtn.TabIndex = 6;
            this.runBtn.Text = "&Run";
            this.runBtn.UseVisualStyleBackColor = true;
            this.runBtn.Visible = false;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 5);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 9;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // makePDFBtn
            // 
            this.makePDFBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.makePDFBtn.Location = new System.Drawing.Point(240, 730);
            this.makePDFBtn.Name = "makePDFBtn";
            this.makePDFBtn.Size = new System.Drawing.Size(142, 35);
            this.makePDFBtn.TabIndex = 10;
            this.makePDFBtn.Text = "Make PDF Report";
            this.makePDFBtn.UseVisualStyleBackColor = true;
            this.makePDFBtn.Click += new System.EventHandler(this.makePDFBtn_Click);
            // 
            // makeExcelBtn
            // 
            this.makeExcelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.makeExcelBtn.Location = new System.Drawing.Point(388, 730);
            this.makeExcelBtn.Name = "makeExcelBtn";
            this.makeExcelBtn.Size = new System.Drawing.Size(96, 35);
            this.makeExcelBtn.TabIndex = 11;
            this.makeExcelBtn.Text = "Make Excel";
            this.makeExcelBtn.UseVisualStyleBackColor = true;
            this.makeExcelBtn.Click += new System.EventHandler(this.makeExcelBtn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::StorageInventory.Properties.Resources.Key_Central_Logo___100x20_pix_;
            this.pictureBox1.Location = new System.Drawing.Point(315, -12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(213, 57);
            this.pictureBox1.TabIndex = 12;
            this.pictureBox1.TabStop = false;
            // 
            // GrowerBlockGroupBox
            // 
            this.GrowerBlockGroupBox.Controls.Add(this.growerSelectedLbl);
            this.GrowerBlockGroupBox.Controls.Add(this.growerBlockSelectedLst);
            this.GrowerBlockGroupBox.Controls.Add(this.growerRemoveBtn);
            this.GrowerBlockGroupBox.Controls.Add(this.growerAddBtn);
            this.GrowerBlockGroupBox.Controls.Add(this.growerBlockLbl);
            this.GrowerBlockGroupBox.Controls.Add(this.growerBlockAvailableLst);
            this.GrowerBlockGroupBox.Location = new System.Drawing.Point(18, 605);
            this.GrowerBlockGroupBox.Name = "GrowerBlockGroupBox";
            this.GrowerBlockGroupBox.Size = new System.Drawing.Size(792, 119);
            this.GrowerBlockGroupBox.TabIndex = 14;
            this.GrowerBlockGroupBox.TabStop = false;
            // 
            // growerBlockAvailableLst
            // 
            this.growerBlockAvailableLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerBlockAvailableLst.FormattingEnabled = true;
            this.growerBlockAvailableLst.ItemHeight = 18;
            this.growerBlockAvailableLst.Location = new System.Drawing.Point(9, 38);
            this.growerBlockAvailableLst.Name = "growerBlockAvailableLst";
            this.growerBlockAvailableLst.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.growerBlockAvailableLst.Size = new System.Drawing.Size(311, 76);
            this.growerBlockAvailableLst.Sorted = true;
            this.growerBlockAvailableLst.TabIndex = 0;
            // 
            // growerBlockLbl
            // 
            this.growerBlockLbl.AutoSize = true;
            this.growerBlockLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerBlockLbl.Location = new System.Drawing.Point(11, 16);
            this.growerBlockLbl.Name = "growerBlockLbl";
            this.growerBlockLbl.Size = new System.Drawing.Size(161, 18);
            this.growerBlockLbl.TabIndex = 1;
            this.growerBlockLbl.Text = "Available Grower Block";
            // 
            // growerAddBtn
            // 
            this.growerAddBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerAddBtn.Location = new System.Drawing.Point(348, 34);
            this.growerAddBtn.Name = "growerAddBtn";
            this.growerAddBtn.Size = new System.Drawing.Size(95, 35);
            this.growerAddBtn.TabIndex = 2;
            this.growerAddBtn.Text = ">";
            this.growerAddBtn.UseVisualStyleBackColor = true;
            this.growerAddBtn.Click += new System.EventHandler(this.AddClick_GrowerBlock);
            // 
            // growerRemoveBtn
            // 
            this.growerRemoveBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerRemoveBtn.Location = new System.Drawing.Point(348, 73);
            this.growerRemoveBtn.Name = "growerRemoveBtn";
            this.growerRemoveBtn.Size = new System.Drawing.Size(95, 35);
            this.growerRemoveBtn.TabIndex = 3;
            this.growerRemoveBtn.Text = "<";
            this.growerRemoveBtn.UseVisualStyleBackColor = true;
            this.growerRemoveBtn.Click += new System.EventHandler(this.RemoveClick_GrowerBlock);
            // 
            // growerBlockSelectedLst
            // 
            this.growerBlockSelectedLst.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerBlockSelectedLst.FormattingEnabled = true;
            this.growerBlockSelectedLst.ItemHeight = 18;
            this.growerBlockSelectedLst.Location = new System.Drawing.Point(474, 38);
            this.growerBlockSelectedLst.Name = "growerBlockSelectedLst";
            this.growerBlockSelectedLst.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.growerBlockSelectedLst.Size = new System.Drawing.Size(311, 76);
            this.growerBlockSelectedLst.Sorted = true;
            this.growerBlockSelectedLst.TabIndex = 4;
            // 
            // growerSelectedLbl
            // 
            this.growerSelectedLbl.AutoSize = true;
            this.growerSelectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.growerSelectedLbl.Location = new System.Drawing.Point(476, 14);
            this.growerSelectedLbl.Name = "growerSelectedLbl";
            this.growerSelectedLbl.Size = new System.Drawing.Size(162, 22);
            this.growerSelectedLbl.TabIndex = 5;
            this.growerSelectedLbl.Text = "Selected Grower Block";
            this.growerSelectedLbl.UseCompatibleTextRendering = true;
            // 
            // PayData_Criteria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(843, 775);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.makeExcelBtn);
            this.Controls.Add(this.makePDFBtn);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.DatePickerGroupBox);
            this.Controls.Add(this.employeeGroupBox);
            this.Controls.Add(this.cropGroupBox);
            this.Controls.Add(this.jobGroupBox);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.fieldGroupBox);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.GrowerBlockGroupBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PayData_Criteria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Easy Payroll Data Collection - Pay Data Report";
            this.Load += new System.EventHandler(this.PayData_Criteria_Load);
            this.DatePickerGroupBox.ResumeLayout(false);
            this.DatePickerGroupBox.PerformLayout();
            this.employeeGroupBox.ResumeLayout(false);
            this.employeeGroupBox.PerformLayout();
            this.cropGroupBox.ResumeLayout(false);
            this.cropGroupBox.PerformLayout();
            this.jobGroupBox.ResumeLayout(false);
            this.jobGroupBox.PerformLayout();
            this.fieldGroupBox.ResumeLayout(false);
            this.fieldGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.GrowerBlockGroupBox.ResumeLayout(false);
            this.GrowerBlockGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox DatePickerGroupBox;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label DatePickerPresetsLbl;
        private System.Windows.Forms.ComboBox DatePickerPresetsCbx;
        private System.Windows.Forms.TextBox DatePickerThruDateTxt;
        private System.Windows.Forms.TextBox DatePickerFromDateTxt;
        private System.Windows.Forms.Label DatePickerFromDateLbl;
        private System.Windows.Forms.TextBox DatePickerEndWeekTxt;
        private System.Windows.Forms.TextBox DatePickerEndYearTxt;
        private System.Windows.Forms.TextBox DatePickerStartWeekTxt;
        private System.Windows.Forms.TextBox DatePickerStartYearTxt;
		private System.Windows.Forms.Label DatePickerThruDateLbl;
        private System.Windows.Forms.Label DatePickerStartYearLbl;
        private System.Windows.Forms.Label DatePickerEndWeekLbl;
        private System.Windows.Forms.Label DatePickerEndYearLbl;
        private System.Windows.Forms.Label DatePickerStartWeekLbl;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.ListBox employeesAvailableLst;
        private System.Windows.Forms.ListBox employeesSelectedLst;
        private System.Windows.Forms.Button employeeAddBtn;
        private System.Windows.Forms.Button employeeRemoveBtn;
        private System.Windows.Forms.Label employeesAvailableLbl;
        private System.Windows.Forms.Label employeesSelectedLbl;
        private System.Windows.Forms.GroupBox employeeGroupBox;
        private System.Windows.Forms.GroupBox cropGroupBox;
        private System.Windows.Forms.ListBox cropsAvailableLst;
        private System.Windows.Forms.Label cropsSelectedLbl;
        private System.Windows.Forms.ListBox cropsSelectedLst;
        private System.Windows.Forms.Label cropsAvailableLbl;
        private System.Windows.Forms.Button cropAddBtn;
        private System.Windows.Forms.Button cropRemoveBtn;
        private System.Windows.Forms.GroupBox jobGroupBox;
        private System.Windows.Forms.ListBox jobsAvailableLst;
        private System.Windows.Forms.Label jobsSelectedLbl;
        private System.Windows.Forms.ListBox jobsSelectedLst;
        private System.Windows.Forms.Label jobsAvailableLbl;
        private System.Windows.Forms.Button jobAddBtn;
        private System.Windows.Forms.Button jobRemoveBtn;
        private System.Windows.Forms.GroupBox fieldGroupBox;
        private System.Windows.Forms.ListBox fieldsAvailableLst;
        private System.Windows.Forms.Label fieldsSelectedLbl;
        private System.Windows.Forms.ListBox fieldsSelectedLst;
        private System.Windows.Forms.Label fieldsAvailableLbl;
        private System.Windows.Forms.Button fieldAddBtn;
        private System.Windows.Forms.Button fieldRemoveBtn;
        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.Label CompanyNameData;
        private System.Windows.Forms.Button makePDFBtn;
        private System.Windows.Forms.Button makeExcelBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox GrowerBlockGroupBox;
        private System.Windows.Forms.Button growerRemoveBtn;
        private System.Windows.Forms.Button growerAddBtn;
        private System.Windows.Forms.Label growerBlockLbl;
        private System.Windows.Forms.ListBox growerBlockAvailableLst;
        private System.Windows.Forms.ListBox growerBlockSelectedLst;
        private System.Windows.Forms.Label growerSelectedLbl;
    }
}