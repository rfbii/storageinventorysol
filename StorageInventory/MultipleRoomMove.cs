﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace StorageInventory
{
	public partial class MultipleRoomMove : Form
	{
		#region Vars

		private MainMenu.CropsData cropsData;
		private MainMenu.EmployeesData employeesData;
		private MainMenu.JobsData jobsData;
		private MainMenu.FieldsData fieldsData;
		private MainMenu.PayDetailsData payDetailsData;
        private MainMenu.Grower_Blocks_Data growerBlocksData;

        private ArrayList CropsArl;
		private ArrayList EmployeesArl;
		private ArrayList FieldsArl;
        private ArrayList JobsArl;
        private ArrayList GrowerBlockArl;

        private string xmlFileName_Crops;
		private string xmlFileName_Employees;
		private string xmlFileName_Jobs;
		private string xmlFileName_Fields;
		private string xmlFileName_PayDetails;
        private string xmlFileName_Grower_Block;


        //Program option variables.
        private char delimiter;
		private string delimiterWithSpaces;
		private int tabIndexToEndInt;
		private int savedEntriesCharsPerColumn;
        private string VersionTypeStr;
		#endregion

		#region Constructor
		public MultipleRoomMove()
		{
			//Initialize the program option variables
			delimiter = '~';
			delimiterWithSpaces = " " + delimiter.ToString() + " ";
			tabIndexToEndInt = 120;
			savedEntriesCharsPerColumn = 10;

			//Default-Initialize
			InitializeComponent();

			//Setup our Saved Entries Header
			//savedPayDetailsHeaderLbl.Text = MakeHeaderColumns(new List<string> { "Date", "Start Time", "End Time", "Employee", "Hours", "Yield Unit", "Crop", "Job", "Field", "Pay Unit", "Price", "Total" });

			//Make form non-resizable
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;

			//Initialize object variables.
			cropsData = new MainMenu.CropsData();
			employeesData = new MainMenu.EmployeesData();
			cropsData = new MainMenu.CropsData();
			jobsData = new MainMenu.JobsData();
			fieldsData = new MainMenu.FieldsData();
			payDetailsData = new MainMenu.PayDetailsData();
            growerBlocksData = new MainMenu.Grower_Blocks_Data();

            //Set the XML file names
            xmlFileName_Crops = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
			xmlFileName_Employees = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
			xmlFileName_Jobs = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
			xmlFileName_Fields = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
			xmlFileName_PayDetails = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayDetailData.xml";
            xmlFileName_Grower_Block = Path.GetDirectoryName(Application.ExecutablePath) + @"\Grower_Block_Data.xml";

            //Load all of our data from XML
            FillCropDataFromXML();
			FillEmployeeDataFromXML();
			FillFieldDataFromXML();
			FillJobDataFromXML();
			FillPayDetailDataFromXML();
            FillGrowerBlockDataFromXML();

            //Set the dropdown boxes to auto-complete
            employeeCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			employeeCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
			cropCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			cropCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
			jobCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			jobCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
			fieldCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
			fieldCbx.AutoCompleteSource = AutoCompleteSource.ListItems;
            growerCbx.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            growerCbx.AutoCompleteSource = AutoCompleteSource.ListItems;

            //Fill the dropdown lists
            FillEmployeesDropdown();
			FillCropsDropdown();
			FillJobsDropdown();
			FillFieldsDropdown();
            FillGrowerBlocksDropdown();

            //Fill the saved pay details list
            //FillSavedPayDetails();
            CompanyNameData.Text = MainMenu.CompanyStr;
            VersionTypeStr = MainMenu.VersionTypeStr;
            KeyCentralPbx.Visible = true;
           
		}
		#endregion

		#region Form Functions
		#region Button Click Functions
		private void exitBtn_Click(object sender, EventArgs e)
		{
			this.Close();
		}
		
		#endregion

		#region KeyDown Functions (For Enter-As-Tab Functionality)
		private void datePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void startTimePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void endTimePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void employeeCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void hoursTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
        private void yieldUnitTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
		private void cropCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void jobCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void fieldCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void payUnitTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
        private void PriceTxt_Keydown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Return")
                this.GetNextControl((Control)sender, true).Focus();
        }
		private void saveBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}

		private void editDatePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editStartTimePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editEndTimePicker_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editEmployeeCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editHoursTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editCropCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editJobCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editFieldCbx_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editPayUnitTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editYieldUnitTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void editPriceTxt_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		private void deleteEditedBtn_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode.ToString() == "Return")
				this.GetNextControl((Control)sender, true).Focus();
		}
		#endregion

		#region DropDown Functions
		private void employeeCbx_DropDown(object sender, EventArgs e)
		{
			SetComboScrollWidth(sender);
		}
		private void cropCbx_DropDown(object sender, EventArgs e)
		{
			SetComboScrollWidth(sender);
		}
		private void jobCbx_DropDown(object sender, EventArgs e)
		{
			SetComboScrollWidth(sender);
		}
		private void fieldCbx_DropDown(object sender, EventArgs e)
		{
			SetComboScrollWidth(sender);
		}

		private void employeeCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (employeeCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = employeeCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////employeeCbx.SelectedText = code;
				//employeeCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
                MainMenu.EmployeeData employeeData = new MainMenu.EmployeeData();
                //for loop on EmployeeArl and match Code to first part of employeeCbx.SelectedItem

                for (int i = 0; i < EmployeesArl.Count; i++)
                {
                    MainMenu.EmployeeData tempData = (MainMenu.EmployeeData)EmployeesArl[i];

                    if (tempData.CodeStr == ParseTildaString(employeeCbx.SelectedItem.ToString(), 0).Trim())
                    {
                        employeeData = tempData;
                        break;
                    }
                }
               
               
            
            }
        }
		private void cropCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cropCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = cropCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////cropCbx.SelectedText = code;
				//cropCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
			}
		}
		private void jobCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (jobCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = jobCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////jobCbx.SelectedText = code;
				//jobCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
			}
		}
		private void fieldCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (fieldCbx.SelectedIndex != -1)
			{
				//string[] entryPieces = fieldCbx.SelectedItem.ToString().Split(delimiter);

				//string code = entryPieces[0].Trim();
				//string description = entryPieces[1].Trim();

				////fieldCbx.SelectedText = code;
				//fieldCbx.SelectionLength = 0;
				//SendKeys.Send("{HOME}");
			}
		}
        
		private void editFieldCbx_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (editFieldCbx.SelectedIndex != -1)
			{
                saveEditedBtn.Enabled = true;
			}
		}

		private void employeeCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void cropCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void jobCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void fieldCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}

		private void editEmployeeCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void editCropCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void editJobCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		private void editFieldCbx_DropDownClosed(object sender, EventArgs e)
		{
			SendKeys.Send("{HOME}");
		}
		#endregion

		#region Input Validation Functions (Checking for proper data type inputs)
		private void hoursTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Hours");

				
			}
		}
        private void yieldUnitTxt_Leave(object sender, EventArgs e)
        {
            if (((TextBox)sender).Text != "")
            {
                ValidateQuantity((TextBox)sender, "Yield Unit");
            }

           
        }
		private void payUnitTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Pay Unit");
			}
		}
		private void priceTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Price");
			}
		}
		private void editHoursTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Hours");
			}
		}
		private void editPayUnitTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Pay Unit");
			}
		}
		private void editPriceTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Price");
			}
		}
		private void editYieldUnitTxt_Leave(object sender, EventArgs e)
		{
			if (((TextBox)sender).Text != "")
			{
				ValidateQuantity((TextBox)sender, "Yield Unit");
			}

			
		}
		private void comboBox_Leave(object senderIn, EventArgs e)
		{
			if (!exitBtn.Focused && ((ComboBox)senderIn).Text.Trim() != "")/*Allow the user to exit regardless.*/
			{
				//Evaluate the sender
				ComboBox sender = (ComboBox)senderIn;
				string senderStr = sender.Name.Substring(0, sender.Name.Length - 3);

				//Evaluate what the user has entered
				if (!sender.Items.Contains(sender.Text))
				{
					//Give the user a warning.
					MessageBox.Show("Not a valid " + senderStr + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

					//Focus on what they need to change
					sender.Focus();
				}
			}
		}
		#endregion





        private void savedPayDetailsLst_SelectedIndexChanged(object sender, EventArgs e)
        {
            //IF - They have selected an entry on the list.
            //if (savedPayDetailsLst.SelectedIndex != -1)
            //{
            //    //Extrapolate the variables from the selected line in the list.
            //    string selectedEntry = savedPayDetailsLst.SelectedItem.ToString();
            //    string[] entryPieces = selectedEntry.Split('|');

            //    string selectedDate = entryPieces[1].Trim();
            //    //string selectedStartTime = entryPieces[2].Trim();
            //    //string selectedEndTime = entryPieces[3].Trim();
            //    string selectedEmployeeCode = entryPieces[2].Trim();
            //    string selectedEmployeeDescription = FindDescription("employee", selectedEmployeeCode);
            //    //string selectedHours = entryPieces[4].Trim();

            //    string selectedCropCode = entryPieces[4].Trim();
            //    string selectedCropDescription = FindDescription("crop", selectedCropCode);
            //    string selectedJobCode = entryPieces[5].Trim();
            //    string selectedJobDescription = FindDescription("job", selectedJobCode);
            //    string selectedFieldCode = entryPieces[6].Trim();
            //    string selectedFieldDescription = FindDescription("field", selectedFieldCode);
            //    string selectedGrowerBlockCode = entryPieces[7].Trim();
            //    string selectedGorwerBlockDescription = FindDescription("growerblock", selectedGrowerBlockCode);
            //    string selectedBoxLabel = entryPieces[8].Trim();
            //    string selectedReference = entryPieces[9].Trim();
            //    string selectedOther = entryPieces[10].Trim();

            //    string selectedPayUnit = entryPieces[11].Trim();
            //    string selectedYieldUnit = entryPieces[12].Trim();
            //    string selectedSummaryNumber = entryPieces[13].Trim();

                
            //    editFieldCbx.Enabled = true;
            //    editFieldLbl.Enabled = true;
            //    saveEditedBtn.Enabled = true;
                
            //    for (int i = 0; i < editFieldCbx.Items.Count; i++)
            //    {
            //        if (editFieldCbx.Items[i].ToString() == selectedFieldCode + delimiterWithSpaces + selectedFieldDescription)
            //        {
            //            editFieldCbx.SelectedIndex = i;
            //            break;
            //        }
            //    }

            //}
            //else//<-- They have NOT selected an entry on the list.
            //{
                
            //    editFieldCbx.Text = string.Empty;
            //    editFieldCbx.SelectedItem = string.Empty;
            //    editFieldCbx.SelectedIndex = -1;
            //    editFieldCbx.Enabled = false;
            //    editFieldLbl.Enabled = false;
            //    saveEditedBtn.Enabled = false;
            //}
        }
		private void setHoursFromStartAndEndTime(object sender, EventArgs e)
		{
			//
			//This function is called when the user changes the start & end time date pickers.
			//

			//Calculate the hours between the times.
			double hoursDbl = 0;
            TimeSpan span = Convert.ToDateTime(System.DateTime.Now).Subtract(Convert.ToDateTime(System.DateTime.Now));
			hoursDbl += span.Hours;

			//Evaluate the minutes portion (rounds down).
			if (span.Minutes >= 45 && span.Minutes < 60)
				hoursDbl += .75;
			else if (span.Minutes >= 30 && span.Minutes < 45)
				hoursDbl += .5;
			else if (span.Minutes >= 15 && span.Minutes < 30)
				hoursDbl += .25;

			
		}
		#endregion

		#region New Pay Detail Methods
		private void fillScreenBtn_Click(object sender, EventArgs e)
		{
			    //Refill the GUI list
                FillSavedPayDetails();
        }
		private void datePicker_Enter(object sender, EventArgs e)
		{
			//Clear the edit entry spaces
			//savedPayDetailsLst.SelectedIndex = -1;
		}
		private void startTimePicker_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
			//savedPayDetailsLst.SelectedIndex = -1;
		}
		private void endTimePicker_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
			//savedPayDetailsLst.SelectedIndex = -1;
		}
        private void employeeCbx_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
            //savedPayDetailsLst.SelectedIndex = -1;
		}
		private void hoursTxt_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
           // savedPayDetailsLst.SelectedIndex = -1;
        }
        private void yieldUnitTxt_Enter(object sender, EventArgs e)
        {
            //Clear the edit entry spaces
            //savedPayDetailsLst.SelectedIndex = -1;
        }
		private void cropCbx_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
            //savedPayDetailsLst.SelectedIndex = -1;
        }
		private void jobCbx_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
           // savedPayDetailsLst.SelectedIndex = -1;
        }
		private void fieldCbx_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
           // savedPayDetailsLst.SelectedIndex = -1;
        }
		private void payUnitTxt_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
           // savedPayDetailsLst.SelectedIndex = -1;
        }
		private void priceTxt_Enter(object sender, EventArgs e)
		{
            //Clear the edit entry spaces
           // savedPayDetailsLst.SelectedIndex = -1;
        }
        #endregion

        #region Edit Pay Detail Methods
        private void saveEditedBtn_Click(object sender, EventArgs e)
        {
            //Check For Selected Drop Downs
            if (editFieldCbx.SelectedIndex == -1 || editFieldCbx.Text.Trim() == "")
            {
                MessageBox.Show("Please select a where", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                editFieldCbx.Focus();
            }

            //for loop on FieldArl and match Code to first part of FieldCbx.SelectedItem
            MainMenu.FieldData fieldData = new MainMenu.FieldData();
            for (int i = 0; i < FieldsArl.Count; i++)
            {
                MainMenu.FieldData tempFieldData = (MainMenu.FieldData)FieldsArl[i];

                if (tempFieldData.CodeStr == ParseTildaString(editFieldCbx.SelectedItem.ToString(), 0).Trim())
                {
                    fieldData = tempFieldData;
                    break;
                }
            }
            //Update room in data object
            for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
            {
                MainMenu.PayDetailData tempPayDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                for (int j = 0; j < keyLst.Items.Count; j++)
                {
                    int keyInt = Convert.ToInt32(keyLst.Items[j]);
                    if (keyInt == tempPayDetail.KeyInt)
                    {
                        tempPayDetail.FieldKeyInt = fieldData.KeyInt;
                        keyLst.Items.RemoveAt(j);
                        break;
                    }
                }

            }

            //Save the Pay Details To XML
            SavePayDetailDataToXML();

            //Refill the GUI list
            FillSavedPayDetails();

            //Clear the edit entry spaces
            savedPayDetailsLst.SelectedIndex = -1;
            ResetScreen();
        }
		private void deleteEditedBtn_Click(object sender, EventArgs e)
		{
			//Fill the list from XML
			FillPayDetailDataFromXML();
            
            //Delete out the selected Pay Detail
			MainMenu.DeletePayDetailData(GetKey(), payDetailsData.PayDetailsDataArl);

			//Save the new data to XML
			SavePayDetailDataToXML();

			//Refill the GUI list
			FillSavedPayDetails();

			//Clear the edit entry spaces
			savedPayDetailsLst.SelectedIndex = -1;

			//Put focus back on 'datePicker' entry box
			startDatePicker.Focus();
		}
		#endregion

		#region Helper Methods
		//Get Data
		private void FillCropDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Crops);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Crops, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillEmployeeDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Employees);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Employees, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillJobDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Jobs);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Jobs, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillFieldDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_Fields);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_Fields, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
		}
		private void FillPayDetailDataFromXML()
		{
			FileInfo fileInfo = new FileInfo(xmlFileName_PayDetails);

			if (fileInfo.Exists)
			{
				//Deserialize (convert an XML document into an object instance):
				XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayDetailsData));

				// A FileStream is needed to read the XML document.
				FileStream fileStream = new FileStream(xmlFileName_PayDetails, FileMode.Open);
				XmlReader xmlReader = XmlReader.Create(fileStream);

				// Declare an object variable of the type to be deserialized.
				// Use the Deserialize method to restore the object's state.
				payDetailsData = (MainMenu.PayDetailsData)serializer.Deserialize(xmlReader);
				fileStream.Close();
			}
        }
        private void FillGrowerBlockDataFromXML()
        {
            FileInfo fileInfo = new FileInfo(xmlFileName_Grower_Block);

            if (fileInfo.Exists)
            {
                //Deserialize (convert an XML document into an object instance):
                XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.Grower_Blocks_Data));

                // A FileStream is needed to read the XML document.
                FileStream fileStream = new FileStream(xmlFileName_Grower_Block, FileMode.Open);
                XmlReader xmlReader = XmlReader.Create(fileStream);

                // Declare an object variable of the type to be deserialized.
                // Use the Deserialize method to restore the object's state.
                growerBlocksData = (MainMenu.Grower_Blocks_Data)serializer.Deserialize(xmlReader);
                fileStream.Close();
            }
        }
        private int GetKey()
		{
			int key = -1;

			int.TryParse(keyLst.Items[savedPayDetailsLst.SelectedIndex].ToString(), out key);

			return key;
		}

		//Fill DropDown Functions
		private void FillEmployeesDropdown()
		{
			//Clear the existing list
			employeeCbx.Items.Clear();
			employeeCbx.SelectedIndex = -1;

			EmployeesArl = new ArrayList();
            
			//Fill the employee data object
			FillEmployeeDataFromXML();

			//Fill the GUI dropdown list from the data object
			for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
			{
				MainMenu.EmployeeData employee = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

                if (employee.ActiveBln)
                {
                    
                    employeeCbx.Items.Add(employee.CodeStr + delimiterWithSpaces + employee.NameStr);
                    //employeeCbx.DisplayMember = employee.CodeStr + delimiterWithSpaces + employee.NameStr;
                    //employeeCbx.DataSource = employee;
                    //editEmployeeCbx.DisplayMember = employee.CodeStr + delimiterWithSpaces + employee.NameStr;
                    //editEmployeeCbx.DataSource = employee;
                    
                    EmployeesArl.Add(employee);
                }
			}
		}
		private void FillCropsDropdown()
		{
			//Clear the existing list
			cropCbx.Items.Clear();
			cropCbx.SelectedIndex = -1;

			CropsArl = new ArrayList();

			//Fill the crop data object
			FillCropDataFromXML();

			//Fill the GUI dropdown list from the data object
			for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
			{
				MainMenu.CropData crop = (MainMenu.CropData)cropsData.CropsDataArl[i];

				if (crop.ActiveBln)
				{
					cropCbx.Items.Add(crop.CodeStr + delimiterWithSpaces + crop.DescriptionStr);
					CropsArl.Add(crop);
				}
			}
		}
		private void FillJobsDropdown()
		{
			//Clear the existing list
			jobCbx.Items.Clear();
			jobCbx.SelectedIndex = -1;

			JobsArl = new ArrayList();

			//Fill the job data object
			FillJobDataFromXML();

			//Fill the GUI dropdown list from the data object
			for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
			{
				MainMenu.JobData job = (MainMenu.JobData)jobsData.JobsDataArl[i];

				if (job.ActiveBln)
				{
					jobCbx.Items.Add(job.CodeStr + delimiterWithSpaces + job.DescriptionStr);
					JobsArl.Add(job);
				}
			}
		}
		private void FillFieldsDropdown()
		{
			//Clear the existing list
			fieldCbx.Items.Clear();
			fieldCbx.SelectedIndex = -1;

			editFieldCbx.Items.Clear();
			editFieldCbx.SelectedIndex = -1;

			FieldsArl = new ArrayList();

			//Fill the field data object
			FillFieldDataFromXML();

			//Fill the GUI dropdown list from the data object
			for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
			{
				MainMenu.FieldData field = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

				if (field.ActiveBln)
				{
					fieldCbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
					editFieldCbx.Items.Add(field.CodeStr + delimiterWithSpaces + field.DescriptionStr);
					FieldsArl.Add(field);
				}
			}
        }
        private void FillGrowerBlocksDropdown()
        {
            //Clear the existing list
            growerCbx.Items.Clear();
            growerCbx.SelectedIndex = -1;

            GrowerBlockArl = new ArrayList();

            //Fill the crop data object
            FillCropDataFromXML();

            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < growerBlocksData.GrowerBlocksDataArl.Count; i++)
            {
                MainMenu.GrowerBlockData grower = (MainMenu.GrowerBlockData)growerBlocksData.GrowerBlocksDataArl[i];

                if (grower.ActiveBln)
                {
                    growerCbx.Items.Add(grower.CodeStr + delimiterWithSpaces + grower.DescriptionStr);
                    GrowerBlockArl.Add(grower);
                }
            }
        }
        //Fill ListBox Functions
        private void FillSavedPayDetails()
		{
			//Clear the existing list
			savedPayDetailsLst.ClearSelected();
			savedPayDetailsLst.Items.Clear();
			savedPayDetailsLst.SelectedIndex = -1;
            keyLst.Items.Clear();
            //Fill the pay detail data object
			FillPayDetailDataFromXML();
            MainMenu.EmployeeData sortEmployeeData = new MainMenu.EmployeeData();
            MainMenu.CropData sortCropData = new MainMenu.CropData();
            MainMenu.JobData sortJobData = new MainMenu.JobData();
            MainMenu.FieldData sortFieldData = new MainMenu.FieldData();
            MainMenu.GrowerBlockData sortGrowerBlockData = new MainMenu.GrowerBlockData();
            bool addLinebln = false;
            if (employeeCbx.SelectedIndex != -1)
            {
                for (int i = 0; i < EmployeesArl.Count; i++)
                {
                    MainMenu.EmployeeData tempData = (MainMenu.EmployeeData)EmployeesArl[i];

                    if (tempData.CodeStr == ParseTildaString(employeeCbx.SelectedItem.ToString(), 0).Trim())
                    {
                        sortEmployeeData = tempData;
                        break;
                    }
                }
            }
            if (cropCbx.SelectedIndex != -1)
            {
                for (int i = 0; i < CropsArl.Count; i++)
                {
                    MainMenu.CropData tempCropData = (MainMenu.CropData)CropsArl[i];

                    if (tempCropData.CodeStr == ParseTildaString(cropCbx.SelectedItem.ToString(), 0).Trim())
                    {
                        sortCropData = tempCropData;
                        break;
                    }
                }
            }
            if (jobCbx.SelectedIndex != -1)
            {
                for (int i = 0; i < JobsArl.Count; i++)
                {
                    MainMenu.JobData tempJobData = (MainMenu.JobData)JobsArl[i];

                    if (tempJobData.CodeStr == ParseTildaString(jobCbx.SelectedItem.ToString(), 0).Trim())
                    {
                        sortJobData = tempJobData;
                        break;
                    }
                }
            }
            if (fieldCbx.SelectedIndex != -1)
            {
                for (int i = 0; i < FieldsArl.Count; i++)
                {
                    MainMenu.FieldData tempFieldData = (MainMenu.FieldData)FieldsArl[i];

                    if (tempFieldData.CodeStr == ParseTildaString(fieldCbx.SelectedItem.ToString(), 0).Trim())
                    {
                        sortFieldData = tempFieldData;
                        break;
                    }
                }
            }
            if (growerCbx.SelectedIndex != -1)
            {
                for (int i = 0; i < GrowerBlockArl.Count; i++)
                {
                    MainMenu.GrowerBlockData tempGrowerData = (MainMenu.GrowerBlockData)GrowerBlockArl[i];

                    if (tempGrowerData.CodeStr == ParseTildaString(growerCbx.SelectedItem.ToString(), 0).Trim())
                    {
                        sortGrowerBlockData = tempGrowerData;
                        break;
                    }
                }
            }
            //Fill the GUI dropdown list from the data object
            for (int i = 0; i < payDetailsData.PayDetailsDataArl.Count; i++)
			{
				MainMenu.PayDetailData payDetail = (MainMenu.PayDetailData)payDetailsData.PayDetailsDataArl[i];
                addLinebln = true;
                //Check Date Sort
                if (sortbyDateChx.Checked)
                {
                    if (payDetail.DateDtm >= Convert.ToDateTime(startDatePicker.Text.ToString()) && payDetail.DateDtm <= Convert.ToDateTime(endDatePicker.Text.ToString()))
                    {
                       
                    }
                    else
                    {
                        addLinebln = false;
                    }
                }
                //Check Employee sort
                if (employeeCbx.SelectedIndex != -1)
                {
                    if (sortEmployeeData.KeyInt != payDetail.EmployeeKeyInt)
                    {
                        addLinebln = false;
                    }
                }
                //Check Hour sort
                //if (hoursTxt.Text.ToString() != "")
                //{
                //    if (Convert.ToDouble(hoursTxt.Text.ToString()) != payDetail.HoursDbl)
                //    {
                //        addLinebln = false;
                //    }
                    
                //}
                //Check Crop Sort
                if (cropCbx.SelectedIndex != -1)
                {
                    if (sortCropData.KeyInt != payDetail.CropKeyInt)
                    {
                        addLinebln = false;
                    }
                   
                }
                //Check Job Sort
                if (jobCbx.SelectedIndex != -1)
                {
                    if (sortJobData.KeyInt != payDetail.JobKeyInt)
                    {
                        addLinebln = false;
                    }
                }
                //Check Field Sort
                if (fieldCbx.SelectedIndex != -1)
                {
                    if (sortFieldData.KeyInt != payDetail.FieldKeyInt)
                    {
                        addLinebln = false;
                    }
                }
                //Check Grower Block Sort
                if (growerCbx.SelectedIndex != -1)
                {
                    if (sortGrowerBlockData.KeyInt != payDetail.Extra1Int)
                    {
                        addLinebln = false;
                    }
                }
                //Add line only if it passed all Sorts
                string testHashStr = (DateTime.Today.Day * DateTime.Today.Day * DateTime.Today.Month * DateTime.Today.Year).ToString();
                testHashStr = testHashStr.Substring(testHashStr.Length - 3, 3);
                if ((addLinebln && payDetail.ActiveBln && payDetail.ReportDateDtm.ToShortDateString() == "1/1/0001"))
                {
                    #region Add line to List box
                    string EmployeeStr = "";
                    string EmployeeNameStr = "";
					string CropStr = "";
					string JobStr = "";
					string FieldStr = "";
                    string GrowerBlockStr = "";
                    for (int j = 0; j < employeesData.EmployeesDataArl.Count; j++)
					{
						MainMenu.EmployeeData employeeData = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[j];

						if (employeeData.KeyInt == payDetail.EmployeeKeyInt)
						{
							EmployeeStr = employeeData.CodeStr;
                            EmployeeNameStr = employeeData.NameStr;
							break;
						}
					}

					for (int j = 0; j < cropsData.CropsDataArl.Count; j++)
					{
						MainMenu.CropData cropData = (MainMenu.CropData)cropsData.CropsDataArl[j];

						if (cropData.KeyInt == payDetail.CropKeyInt)
						{
							CropStr = cropData.CodeStr;

							break;
						}
					}

					for (int j = 0; j < jobsData.JobsDataArl.Count; j++)
					{
						MainMenu.JobData jobData = (MainMenu.JobData)jobsData.JobsDataArl[j];

						if (jobData.KeyInt == payDetail.JobKeyInt)
						{
							JobStr = jobData.CodeStr;

							break;
						}
					}

					for (int j = 0; j < fieldsData.FieldsDataArl.Count; j++)
					{
						MainMenu.FieldData fieldData = (MainMenu.FieldData)fieldsData.FieldsDataArl[j];

						if (fieldData.KeyInt == payDetail.FieldKeyInt)
						{
							FieldStr = fieldData.CodeStr;

							break;
						}
					}
                    for (int j = 0; j < growerBlocksData.GrowerBlocksDataArl.Count; j++)
                    {
                        MainMenu.GrowerBlockData growerData = (MainMenu.GrowerBlockData)growerBlocksData.GrowerBlocksDataArl[j];

                        if (growerData.KeyInt == payDetail.Extra1Int)
                        {
                            GrowerBlockStr = growerData.CodeStr;

                            break;
                        }
                    }
                    // Sort ListBox Items                 
                    int tempItemInt = 0;
                    for (int j = 0; j < keyLst.Items.Count; j++)
                    {
                        if (Convert.ToInt32(keyLst.Items[j].ToString()) > payDetail.KeyInt)
                        {
                            tempItemInt = j + 1;
                        }
                    }
                    //Store in the right place in ListBox
                    //savedPayDetailsLst.Items.Insert(tempItemInt, MakeColumns(new List<string> { payDetail.DateDtm.ToShortDateString(), EmployeeStr, EmployeeNameStr, CropStr, JobStr, FieldStr, payDetail.PayUnitDbl.ToString(), }));
                    savedPayDetailsLst.Items.Insert(tempItemInt, MakeColumns(new List<string> { payDetail.DateDtm.ToShortDateString(), EmployeeStr, EmployeeNameStr, CropStr, JobStr, FieldStr, GrowerBlockStr, payDetail.UserNameStr.ToString(), payDetail.Extra1Str.ToString(), payDetail.Extra2Str.ToString(), payDetail.PayUnitDbl.ToString(), payDetail.YieldUnitDbl.ToString() }));

                    //Store the key in same index
                    keyLst.Items.Insert(tempItemInt, payDetail.KeyInt);
                    #endregion
                }
			}
            
		}

		//Save data to XML functions
		private void SavePayDetailDataToXML()
		{
			//Serialize (convert an object instance to an XML document):
			XmlSerializer xmlSerializer = new XmlSerializer(payDetailsData.GetType());
			// Create an XmlTextWriter using a FileStream.
			Stream fileStream2 = new FileStream(xmlFileName_PayDetails, FileMode.Create);
			XmlWriter xmlWriter = new XmlTextWriter(fileStream2, Encoding.Unicode);
			// Serialize using the XmlTextWriter.
			xmlSerializer.Serialize(xmlWriter, payDetailsData);
			xmlWriter.Flush();
			xmlWriter.Close();
		}
       
		//Adjust ComboBox dropdown list width
		private static void SetComboScrollWidth(object sender)
		{
			//CREDIT - this code was found at: http://rajeshkm.blogspot.com/2006/11/adjust-combobox-drop-down-list-width-c.html
			//      THANKS Rajesh!
			try
			{
				ComboBox senderComboBox = (ComboBox)sender;
				int width = senderComboBox.Width;
				Graphics g = senderComboBox.CreateGraphics();
				Font font = senderComboBox.Font;

				//checks if a scrollbar will be displayed.
				//If yes, then get its width to adjust the size of the drop down list.
				int vertScrollBarWidth = (senderComboBox.Items.Count > senderComboBox.MaxDropDownItems) ? SystemInformation.VerticalScrollBarWidth : 0;

				//Loop through list items and check size of each items.
				//set the width of the drop down list to the width of the largest item.

				int newWidth;

				foreach (string s in ((ComboBox)sender).Items)
				{
					if (s != null)
					{
						newWidth = (int)g.MeasureString(s.Trim(), font).Width + vertScrollBarWidth;

						if (width < newWidth)
						{
							width = newWidth;
						}
					}
				}

				senderComboBox.DropDownWidth = width;
			}
			catch (Exception objException)
			{
				//Catch objException
			}
		}

		//Validation Function(s)
		private void ValidateQuantity(TextBox textBoxToCheck, string textBoxName)
		{
			try
			{
				decimal temp = decimal.Parse(textBoxToCheck.Text);

				if (textBoxToCheck.Text != "" && textBoxToCheck.Text.IndexOf(",") == -1)
				{
					textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
				}
				else if (textBoxToCheck.Text.IndexOf(",") != -1)
				{
					textBoxToCheck.Text.Replace(",", "");
					textBoxToCheck.Text = DisplayFormat((decimal)Convert.ToDouble(textBoxToCheck.Text));
				}
			}
			catch (System.FormatException ex)
			{
				MessageBox.Show(this, "Invalid " + textBoxName + ".", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				textBoxToCheck.Focus();
				textBoxToCheck.SelectAll();
			}
		}

		//Formating Function(s)
		private static string DisplayFormat(decimal value)
		{
			if (value == 0)
				return value.ToString();
			else
				return decimal.Round(value, 2).ToString("#,###,##0.##");
		}
		private string FindDescription(string dataTypeStr, string codeIn)
		{
			if (dataTypeStr.ToLower() == "crop")
			{
				for (int i = 0; i < cropsData.CropsDataArl.Count; i++)
				{
					MainMenu.CropData crop = (MainMenu.CropData)cropsData.CropsDataArl[i];

					if (crop.CodeStr == codeIn)
					{
						return crop.DescriptionStr;
					}
				}
			}
			else if (dataTypeStr.ToLower() == "employee")
			{
				for (int i = 0; i < employeesData.EmployeesDataArl.Count; i++)
				{
					MainMenu.EmployeeData employee = (MainMenu.EmployeeData)employeesData.EmployeesDataArl[i];

					if (employee.CodeStr == codeIn)
					{
						return employee.NameStr;
					}
				}
			}
			else if (dataTypeStr.ToLower() == "job")
			{
				for (int i = 0; i < jobsData.JobsDataArl.Count; i++)
				{
					MainMenu.JobData job = (MainMenu.JobData)jobsData.JobsDataArl[i];

					if (job.CodeStr == codeIn)
					{
						return job.DescriptionStr;
					}
				}
			}
			else if (dataTypeStr.ToLower() == "field")
			{
				for (int i = 0; i < fieldsData.FieldsDataArl.Count; i++)
				{
					MainMenu.FieldData field = (MainMenu.FieldData)fieldsData.FieldsDataArl[i];

					if (field.CodeStr == codeIn)
					{
						return field.DescriptionStr;
					}
				}
            }
            else if (dataTypeStr.ToLower() == "growerblock")
            {
                for (int i = 0; i < growerBlocksData.GrowerBlocksDataArl.Count; i++)
                {
                    MainMenu.GrowerBlockData grower = (MainMenu.GrowerBlockData)growerBlocksData.GrowerBlocksDataArl[i];

                    if (grower.CodeStr == codeIn)
                    {
                        return grower.DescriptionStr;
                    }
                }
            }

            return "";
		}
		private string MakeHeaderColumns(List<string> stringsLst)
		{
			string outputStr = "|";

			foreach (string str in stringsLst)
			{
				int spaceToFill = savedEntriesCharsPerColumn - str.Length;

				if (spaceToFill % 2 == 0/*Is Even*/)
				{
					for (int i = 0; i < (spaceToFill / 2); i++)
					{
						outputStr += " ";
					}

					outputStr += str;

					for (int i = 0; i < (spaceToFill / 2); i++)
					{
						outputStr += " ";
					}

					outputStr += "|";
				}
				else
				{
					for (int i = 0; i < (((spaceToFill - 1) / 2) + 1); i++)
					{
						outputStr += " ";
					}

					outputStr += str;

					for (int i = 0; i < ((spaceToFill - 1) / 2); i++)
					{
						outputStr += " ";
					}

					outputStr += "|";
				}
			}

			return outputStr;
		}

        private string MakeColumns(List<string> stringsLst)
        {
            string outputStr = "|";

            for (int i = 0; i < stringsLst.Count; i++)
            {
                string str = stringsLst[i];

                switch (i)
                {
                    case 0://Date
                           /*Left Align*/
                        outputStr += str.PadRight(10, ' ').Substring(0, 10);
                        outputStr += "|";
                        break;

                    case 1://Employee
                           /*Left Align*/
                        outputStr += str.PadRight(10, ' ').Substring(0, 10);
                        outputStr += "|";
                        break;

                    case 2://Employee Name
                           /*Left Align*/
                        outputStr += str.PadRight(18, ' ').Substring(0, 18);
                        outputStr += "|";
                        break;

                    case 3://crop
                           /*Right Align*/
                        outputStr += str.PadRight(8, ' ').Substring(0, 8);
                        outputStr += "|";
                        break;

                    case 4://box type
                           /*Right Align*/
                        outputStr += str.PadRight(8, ' ').Substring(0, 8);
                        outputStr += "|";
                        break;

                    case 5://where
                           /*Left Align*/
                        outputStr += str.PadRight(8, ' ').Substring(0, 8);
                        outputStr += "|";
                        break;

                    case 6://grower Block
                           /*Left Align*/
                        outputStr += str.PadRight(12, ' ').Substring(0, 12);
                        outputStr += "|";
                        break;

                    case 7://box label
                           /*Left Align*/
                        outputStr += str.PadRight(11, ' ').Substring(0, 11);
                        outputStr += "|";
                        break;

                    case 8://reference
                           /*Left Align*/
                        outputStr += str.PadRight(11, ' ').Substring(0, 11);
                        outputStr += "|";
                        break;

                    case 9://Other
                           /*Right Align*/
                        outputStr += str.PadRight(7, ' ').Substring(0, 7);
                        outputStr += "|";
                        break;

                    case 10:// amount in
                            /*Right Align*/
                        outputStr += str.PadLeft(10, ' ').Substring(0, 10);
                        outputStr += "|";
                        break;

                    case 11://amount out
                            /*Right Align*/
                        outputStr += str.PadLeft(10, ' ').Substring(0, 10);
                        outputStr += "|";
                        break;
                }
            }

            return outputStr;
        }
        private void EnableAllTextFields()
        {
            startDatePicker.Enabled = true;
            employeeCbx.Enabled = true;
            //hoursTxt.Enabled = true;
            cropCbx.Enabled = true;
            jobCbx.Enabled = true;
            fieldCbx.Enabled = true;
            growerCbx.Enabled = true;
            editFieldCbx.Enabled = true;
            editFieldLbl.Enabled = true;
            //priceTxt.Enabled = true;
        }
        private void ResetScreen()
        {
            startDatePicker.Checked = true;
            endDatePicker.Checked = true;
            employeeCbx.SelectedIndex = -1;
            //hoursTxt.Text = "";
            cropCbx.SelectedIndex = -1;
            jobCbx.SelectedIndex = -1;
            fieldCbx.SelectedIndex = -1;
            growerCbx.SelectedIndex = -1;
            editFieldCbx.SelectedIndex = -1;
            //priceTxt.Text = "";
            savedPayDetailsLst.ClearSelected();
            savedPayDetailsLst.Items.Clear();
            savedPayDetailsLst.SelectedIndex = -1;
            startDateLbl.Visible = false;
            startDatePicker.Visible = false;
            endDateLabel.Visible = false;
            endDatePicker.Visible = false;
            saveEditedBtn.Enabled = false;
        }
        public static string ParseTildaString(string stringToParse, int field)
        {
            string[] stringSplit = stringToParse.Split('~');

            if (stringSplit.Length <= field)
                return "";
            else
                return stringSplit[field];
        }
        #endregion

       public class EmployeeKey : IComparable
       {
           public int valueKey;
           public string codeStr;
           public string nameStr;

            public EmployeeKey()
            {
               valueKey =  0;
               codeStr = string.Empty;
               nameStr = string.Empty;
            }

            public EmployeeKey(Int32 ValueKey)
            {
                valueKey = ValueKey;
                codeStr = string.Empty;
                nameStr = string.Empty;
            }
            public EmployeeKey(Int32 ValueKey, string CodeStr, string NameStr)
            {
               valueKey = ValueKey;
               codeStr = CodeStr;
               nameStr = NameStr;
            }
           public int CompareTo(object obj)
           {
               int tempCompare;
               EmployeeKey Y = (EmployeeKey)obj;

               //Compare codeStr
               tempCompare = this.codeStr.CompareTo(Y.codeStr);
               if (tempCompare != 0)
                   return tempCompare;

               //Compare nameStr
               tempCompare = this.nameStr.CompareTo(Y.nameStr);
               if (tempCompare != 0)
                   return tempCompare;

               return 0;
           }
       
       }

       private void EndEditBtn_Click(object sender, EventArgs e)
       {
           ResetScreen();
       }

       private void clearBtn_Click(object sender, EventArgs e)
       {
           ResetScreen();
       }

       private void sortbyDateChx_CheckedChanged(object sender, EventArgs e)
       {
           if (sortbyDateChx.Checked)
           {
               startDateLbl.Visible = true;
               startDatePicker.Visible = true;
               endDateLabel.Visible = true;
               endDatePicker.Visible = true;
           }
           else
           {
               startDateLbl.Visible = false;
               startDatePicker.Visible = false;
               endDateLabel.Visible = false;
               endDatePicker.Visible = false;
           }
       }
    }
}
