﻿using System;
using System.Windows.Forms;
using System.Drawing.Printing;
using System.Collections.Generic;
using System.Text;

namespace StorageInventory
{
	/// <summary>
	/// Summary description for Printer.
	/// </summary>
	public class Printer
	{
		#region Vars
		//Main objects
		private PrintDocument printDoc = new PrintDocument();

		//Settings objects
		private PrinterSettings printerSettings = new PrinterSettings();
		private PageSettings pageSettings = new PageSettings();

		//Events
		public event PrintPageEventHandler PrintPage;
		#endregion

		#region Constructor
		public Printer()
		{
			//Setup Print Doc
			printDoc.PrintPage += new PrintPageEventHandler(this.printDoc_PrintPage);
			printDoc.DefaultPageSettings = pageSettings;
		}
		#endregion

		#region Events
		private void printDoc_PrintPage(object sender, PrintPageEventArgs e)
		{
			this.PrintPage(sender, e);
		}
		#endregion

		#region Helper Methods
		public bool PrintDirectToPrinter()
		{
			try
			{
				printDoc.Print();
				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		public bool PrintDirectToPrinter(string printerName)
		{
			try
			{
				printDoc.PrinterSettings.PrinterName = printerName;
				printDoc.Print();
				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		public bool PrintWithPrinterDialog()
		{
			try
			{
				PrintDialog printDialog = new PrintDialog();
				printDialog.Document = printDoc;
				printDialog.PrinterSettings = printerSettings;

				if (printDialog.ShowDialog() == DialogResult.OK)
				{
					printDoc.PrinterSettings = printerSettings;
					printDoc.Print();
				}
				else
					return false;

				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		public bool PrintWithPrinterDialog(int numberOfCopies)
		{
			try
			{
				PrintDialog printDialog = new PrintDialog();
				printDialog.Document = printDoc;
				printDialog.PrinterSettings = printerSettings;
				printDialog.PrinterSettings.Copies = Convert.ToInt16(numberOfCopies);

				if (printDialog.ShowDialog() == DialogResult.OK)
				{
					printDoc.PrinterSettings = printerSettings;
					printDoc.Print();
				}
				else
					return false;

				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		public bool PrintWithPrinterDialog(bool landscape)
		{
			try
			{
				PrintDialog printDialog = new PrintDialog();
				printDialog.Document = printDoc;
				printDialog.PrinterSettings = printerSettings;

				if (landscape == true)
					printDoc.PrinterSettings.DefaultPageSettings.Landscape = true;
				else
					printDoc.PrinterSettings.DefaultPageSettings.Landscape = false;

				if (printDialog.ShowDialog() == DialogResult.OK)
				{
					printDoc.PrinterSettings = printerSettings;

					if (landscape == true)
						printDoc.DefaultPageSettings.Landscape = true;
					else
						printDoc.DefaultPageSettings.Landscape = false;

					printDoc.Print();
				}
				else
					return false;

				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		public bool PrintWithPrinterDialog(string printerName)
		{
			try
			{
				PrintDialog printDialog = new PrintDialog();
				printDialog.Document = printDoc;
				printDialog.PrinterSettings = printerSettings;
				printerSettings.PrinterName = printerName;

				if (printDialog.ShowDialog() == DialogResult.OK)
				{
					printDoc.PrinterSettings = printerSettings;
					printDoc.Print();
				}
				else
					return false;

				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		public bool PrintPreview()
		{
			try
			{
				PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();
				printPreviewDialog.Document = printDoc;
				printPreviewDialog.ShowDialog(); // Show the print preview dialog, uses print page event to draw preview screen
				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		public bool PageSettings()
		{
			try
			{
				PageSetupDialog pageSetupDialog = new PageSetupDialog();
				pageSetupDialog.PageSettings = pageSettings;
				pageSetupDialog.PrinterSettings = printerSettings;
				pageSetupDialog.AllowOrientation = true;
				pageSetupDialog.AllowMargins = true;

				pageSetupDialog.ShowDialog();

				return true;
			}
			catch (Exception exp)
			{
				MessageBox.Show(exp.Message, "Printing Error");
				return false;
			}
		}
		#endregion
	}
}