using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;

namespace PayrollFieldDataDesktop
{
	public class Crop_Job_Field_Report
	{
		#region Vars
		private Document document;
		private PdfWriter PDFWriter;
        private double lineCountPerPageDbl = 46;
        private double lineCountDbl = 0;
        #endregion

		#region Constructor
        public Crop_Job_Field_Report()
		{

		}
		#endregion

		#region Display Code
        public void Main(string saveFolderLocationStr, string fileNameStr, string commentStr, ArrayList CropDetailsArl, ArrayList JobDetailsArl, ArrayList FieldDetailsArl, ArrayList CropJobDetailsArl, ArrayList CropFieldDetailsArl, ArrayList JobFieldDetailsArl, ArrayList CropJobFieldDetailsArl, ArrayList JobCropFieldDetailsArl, ArrayList FieldCropJobDetailsArl)
		{
            try
            {
                //if temp folder doesn't already exist on client's computer, create the folder
                DirectoryInfo directoryInfo = new DirectoryInfo(saveFolderLocationStr);

                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }

                document = new Document(PageSize.LETTER.Rotate(), 18, 18, 18, 18);

                // creation of the different writers
                PDFWriter = PdfWriter.GetInstance(document, new FileStream(saveFolderLocationStr + "\\" + fileNameStr, FileMode.Create));
                //PDFWriter.ViewerPreferences = PdfWriter.PageModeUseOutlines;

                document.Open();

                #region Footer
                HeaderFooter footer = new HeaderFooter(new Phrase("Page ", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL)), true);
                footer.Border = iTextSharp.text.Rectangle.NO_BORDER;
                footer.Alignment = Element.ALIGN_CENTER;
                document.Footer = footer;
                #endregion

                #region Header
                PdfPTable pdfPTable = HeaderPdfPTable(commentStr);
                #endregion

                PdfPCell detailCell = new PdfPCell();

                #region Detail Vars
                double dollarsPerHourDbl = 0;
                double dollarsPerUnitDbl = 0;
                double dollarsPerYieldDbl = 0;
                double grandTotalHoursDbl = 0;
                double grandTotalDollarsPerHourDbl = 0;
                double grandTotalPayUnitDbl = 0;
                double grandTotalDollarsPerUnitDbl = 0;
                double grandTotalYieldUnitDbl = 0;
                double grandTotalDollarsPerYieldDbl = 0;
                double grandTotalTotalDbl = 0;
                #endregion

                #region Setting & Printing Detail Vars
                #region CropDetail
                if (CropDetailsArl.Count != 0)
                {
                    for (int i = 0; i < CropDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.CropDetail tempCropDetail = (Crop_Job_Field_Criteria.CropDetail)CropDetailsArl[i];

                        string cropString = tempCropDetail.cropStr;

                        if (tempCropDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempCropDetail.amountDbl / tempCropDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }
                        
                        if (tempCropDetail.payUnitDbl != 0)
                        {
                        dollarsPerUnitDbl = tempCropDetail.amountDbl / tempCropDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempCropDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempCropDetail.amountDbl / tempCropDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }
                        
                        grandTotalTotalDbl += tempCropDetail.amountDbl;
                        grandTotalHoursDbl += tempCropDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempCropDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }
                        
                        grandTotalYieldUnitDbl += tempCropDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }
                        
                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable(commentStr);
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase(tempCropDetail.cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable(commentStr);
                    }
                    #region Print CropDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 10;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell); 
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region JobDetail

                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion

                if (JobDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                         #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                         #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();
                       
                    }
                   for (int i = 0; i < JobDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.JobDetail tempJobDetail = (Crop_Job_Field_Criteria.JobDetail)JobDetailsArl[i];

                        if (tempJobDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempJobDetail.amountDbl / tempJobDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempJobDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempJobDetail.amountDbl / tempJobDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempJobDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempJobDetail.amountDbl / tempJobDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempJobDetail.amountDbl;
                        grandTotalHoursDbl += tempJobDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempJobDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempJobDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }

                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable2();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobDetail.jobStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                   if (lineCountDbl + 2 >= lineCountPerPageDbl)
                   {
                       lineCountDbl = 0;

                       document.Add(pdfPTable);
                       document.NewPage();
                       pdfPTable = HeaderPdfPTable2();
                   }
                    #region JobDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 10;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region FieldDetail

                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion

                if (FieldDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();

                    }
                                           
                    for (int i = 0; i < FieldDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.FieldDetail tempFieldDetail = (Crop_Job_Field_Criteria.FieldDetail)FieldDetailsArl[i];

                        if (tempFieldDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempFieldDetail.amountDbl / tempFieldDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempFieldDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempFieldDetail.amountDbl / tempFieldDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempFieldDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempFieldDetail.amountDbl / tempFieldDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempFieldDetail.amountDbl;
                        grandTotalHoursDbl += tempFieldDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempFieldDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempFieldDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }

                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable2();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldDetail.fieldStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f; 
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable2();
                    }
                    #region FieldDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 10;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region CropJobDetail

                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion

                if (CropJobDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();

                    }

                    for (int i = 0; i < CropJobDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.CropJobDetail tempCropJobDetail = (Crop_Job_Field_Criteria.CropJobDetail)CropJobDetailsArl[i];

                        dollarsPerHourDbl = tempCropJobDetail.amountDbl / tempCropJobDetail.hoursDbl;
                        dollarsPerUnitDbl = tempCropJobDetail.amountDbl / tempCropJobDetail.payUnitDbl;
                        dollarsPerYieldDbl = tempCropJobDetail.amountDbl / tempCropJobDetail.yieldUnitDbl;

                        if (tempCropJobDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempCropJobDetail.amountDbl / tempCropJobDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempCropJobDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempCropJobDetail.amountDbl / tempCropJobDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempCropJobDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempCropJobDetail.amountDbl / tempCropJobDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempCropJobDetail.amountDbl;
                        grandTotalHoursDbl += tempCropJobDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempCropJobDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempCropJobDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }
                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable2();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase(tempCropJobDetail.cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobDetail.jobStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable2();
                    }
                    #region CropJobDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 10;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region CropFieldDetail
                
                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion

                if (CropFieldDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();

                    }

                    for (int i = 0; i < CropFieldDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.CropFieldDetail tempCropFieldDetail = (Crop_Job_Field_Criteria.CropFieldDetail)CropFieldDetailsArl[i];

                        if (tempCropFieldDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempCropFieldDetail.amountDbl / tempCropFieldDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempCropFieldDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempCropFieldDetail.amountDbl / tempCropFieldDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempCropFieldDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempCropFieldDetail.amountDbl / tempCropFieldDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempCropFieldDetail.amountDbl;
                        grandTotalHoursDbl += tempCropFieldDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempCropFieldDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempCropFieldDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }

                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable2();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase(tempCropFieldDetail.cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropFieldDetail.fieldStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropFieldDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropFieldDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropFieldDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropFieldDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        //document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable2();
                    }
                    #region CropFieldDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 10;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region JobFieldDetail
                
                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion

                if (JobFieldDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();

                    }
                    for (int i = 0; i < JobFieldDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.JobFieldDetail tempJobFieldDetail = (Crop_Job_Field_Criteria.JobFieldDetail)JobFieldDetailsArl[i];

                        if (tempJobFieldDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempJobFieldDetail.amountDbl / tempJobFieldDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempJobFieldDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempJobFieldDetail.amountDbl / tempJobFieldDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempJobFieldDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempJobFieldDetail.amountDbl / tempJobFieldDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempJobFieldDetail.amountDbl;
                        grandTotalHoursDbl += tempJobFieldDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempJobFieldDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempJobFieldDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }

                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable2();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobFieldDetail.jobStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobFieldDetail.fieldStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobFieldDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobFieldDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobFieldDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobFieldDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable2();
                    }
                    #region JobFieldDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 10;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region CropJobFieldDetail

                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion

                if (CropJobFieldDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable2();

                    }                   


                    for (int i = 0; i < CropJobFieldDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.CropJobFieldDetail tempCropJobFieldDetail = (Crop_Job_Field_Criteria.CropJobFieldDetail)CropJobFieldDetailsArl[i];

                        if (tempCropJobFieldDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempCropJobFieldDetail.amountDbl / tempCropJobFieldDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempCropJobFieldDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempCropJobFieldDetail.amountDbl / tempCropJobFieldDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempCropJobFieldDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempCropJobFieldDetail.amountDbl / tempCropJobFieldDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempCropJobFieldDetail.amountDbl;
                        grandTotalHoursDbl += tempCropJobFieldDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempCropJobFieldDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempCropJobFieldDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }

                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable2();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase(tempCropJobFieldDetail.cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobFieldDetail.jobStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobFieldDetail.fieldStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobFieldDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobFieldDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobFieldDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempCropJobFieldDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable2();
                    }
                    #region CropJobFieldDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.Colspan = 10;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.BorderWidthLeft = 0f;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region JobCropFieldDetail

                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion

                if (JobCropFieldDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable3();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable3();

                    }

                    for (int i = 0; i < JobCropFieldDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.JobCropFieldDetail tempJobCropFieldDetail = (Crop_Job_Field_Criteria.JobCropFieldDetail)JobCropFieldDetailsArl[i];

                        if (tempJobCropFieldDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempJobCropFieldDetail.amountDbl / tempJobCropFieldDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempJobCropFieldDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempJobCropFieldDetail.amountDbl / tempJobCropFieldDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempJobCropFieldDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempJobCropFieldDetail.amountDbl / tempJobCropFieldDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempJobCropFieldDetail.amountDbl;
                        grandTotalHoursDbl += tempJobCropFieldDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempJobCropFieldDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempJobCropFieldDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }

                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable3();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase(tempJobCropFieldDetail.jobStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobCropFieldDetail.cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobCropFieldDetail.fieldStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobCropFieldDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobCropFieldDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobCropFieldDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempJobCropFieldDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable3();
                    }
                    #region JobCropFieldDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion

                #region FieldCropJobDetail
               
                #region Vars
                dollarsPerHourDbl = 0;
                dollarsPerUnitDbl = 0;
                dollarsPerYieldDbl = 0;
                grandTotalHoursDbl = 0;
                grandTotalDollarsPerHourDbl = 0;
                grandTotalPayUnitDbl = 0;
                grandTotalDollarsPerUnitDbl = 0;
                grandTotalYieldUnitDbl = 0;
                grandTotalDollarsPerYieldDbl = 0;
                grandTotalTotalDbl = 0;
                #endregion
                if (FieldCropJobDetailsArl.Count != 0)
                {
                    if (lineCountDbl + 3 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 2;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable4();
                    }
                    else
                    {
                        #region skip line and add new header
                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 32, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.Colspan = 10;
                        detailCell.BorderWidthLeft = 0f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = 0f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = 0f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 2;
                        #endregion

                        document.Add(pdfPTable);
                        pdfPTable = HeaderPdfPTable4();
                    }

                    for (int i = 0; i < FieldCropJobDetailsArl.Count; i++)
                    {
                        Crop_Job_Field_Criteria.FieldCropJobDetail tempFieldCropJobDetail = (Crop_Job_Field_Criteria.FieldCropJobDetail)FieldCropJobDetailsArl[i];

                        if (tempFieldCropJobDetail.hoursDbl != 0)
                        {
                            dollarsPerHourDbl = tempFieldCropJobDetail.amountDbl / tempFieldCropJobDetail.hoursDbl;
                        }
                        else
                        {
                            dollarsPerHourDbl = 0;
                        }

                        if (tempFieldCropJobDetail.payUnitDbl != 0)
                        {
                            dollarsPerUnitDbl = tempFieldCropJobDetail.amountDbl / tempFieldCropJobDetail.payUnitDbl;
                        }
                        else
                        {
                            dollarsPerUnitDbl = 0;
                        }

                        if (tempFieldCropJobDetail.yieldUnitDbl != 0)
                        {
                            dollarsPerYieldDbl = tempFieldCropJobDetail.amountDbl / tempFieldCropJobDetail.yieldUnitDbl;
                        }
                        else
                        {
                            dollarsPerYieldDbl = 0;
                        }

                        grandTotalTotalDbl += tempFieldCropJobDetail.amountDbl;
                        grandTotalHoursDbl += tempFieldCropJobDetail.hoursDbl;

                        if (grandTotalHoursDbl != 0)
                        {
                            grandTotalDollarsPerHourDbl = grandTotalTotalDbl / grandTotalHoursDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerHourDbl = 0;
                        }

                        grandTotalPayUnitDbl += tempFieldCropJobDetail.payUnitDbl;

                        if (grandTotalPayUnitDbl != 0)
                        {
                            grandTotalDollarsPerUnitDbl = grandTotalTotalDbl / grandTotalPayUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerUnitDbl = 0;
                        }

                        grandTotalYieldUnitDbl += tempFieldCropJobDetail.yieldUnitDbl;

                        if (grandTotalYieldUnitDbl != 0)
                        {
                            grandTotalDollarsPerYieldDbl = grandTotalTotalDbl / grandTotalYieldUnitDbl;
                        }
                        else
                        {
                            grandTotalDollarsPerYieldDbl = 0;
                        }

                        if (lineCountDbl + 1 >= lineCountPerPageDbl)
                        {
                            lineCountDbl = 0;

                            document.Add(pdfPTable);
                            document.NewPage();
                            pdfPTable = HeaderPdfPTable4();
                        }
                        #region Print Details
                        detailCell = new PdfPCell(new Phrase(tempFieldCropJobDetail.fieldStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldCropJobDetail.cropStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldCropJobDetail.jobStr, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_LEFT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldCropJobDetail.hoursDbl.ToString("####,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldCropJobDetail.payUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldCropJobDetail.yieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(dollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);

                        detailCell = new PdfPCell(new Phrase(tempFieldCropJobDetail.amountDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                        detailCell.BorderWidth = 0f;
                        detailCell.BorderWidthLeft = .5f;
                        detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthRight = .5f;
                        detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.BorderWidthBottom = .5f;
                        detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                        detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                        detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                        pdfPTable.AddCell(detailCell);
                        lineCountDbl += 1;
                        #endregion
                    }
                    if (lineCountDbl + 2 >= lineCountPerPageDbl)
                    {
                        lineCountDbl = 0;

                        document.Add(pdfPTable);
                        document.NewPage();
                        pdfPTable = HeaderPdfPTable4();
                    }
                    #region FieldCropJobDetail Grand Total
                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase("Grand Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalHoursDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerHourDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalPayUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerUnitDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalYieldUnitDbl.ToString("###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalDollarsPerYieldDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);

                    detailCell = new PdfPCell(new Phrase(grandTotalTotalDbl.ToString("$###,###.00"), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
                    detailCell.BorderWidth = 0f;
                    detailCell.BorderWidthLeft = .5f;
                    detailCell.BorderColorLeft = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthRight = .5f;
                    detailCell.BorderColorRight = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.BorderWidthBottom = .5f;
                    detailCell.BorderColorBottom = iTextSharp.text.Color.LIGHT_GRAY;
                    detailCell.HorizontalAlignment = Element.ALIGN_RIGHT;
                    detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                    pdfPTable.AddCell(detailCell);
                    lineCountDbl += 1;
                    #endregion
                }
                else
                {
                    //MessageBox.Show(this, "No records found.", "No records found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                #endregion
                #endregion

                document.Add(pdfPTable);

                
            }
            catch (Exception ex)
            {
                //Exception was thrown.
            }
            finally
            {
                if (document != null && document.IsOpen())
                {
                    // we close the document
                    document.Close();
                }
            }
		}


        private PdfPTable HeaderPdfPTable(string commentDateStr)
        {
            #region Header C-J-F with title

            PdfPTable table = new PdfPTable(10);
            table.HorizontalAlignment = Element.ALIGN_LEFT;

            float tableWidth = 0;

            float cropWidth = 96f;
            float jobWidth = 96f;
            float fieldWidth = 96f;
            float hoursWidth = 78f;
            float dollarsPerHourWidth = 48;
            float payUnitWidth = 78f;
            float dollarsPerUnitWidth = 48f;
            float yieldUnitWidth = 78f;
            float dollarsPerYieldWidth = 48f;
            float totalWidth = 78f;

            tableWidth = cropWidth + jobWidth + fieldWidth + hoursWidth + dollarsPerHourWidth + payUnitWidth + dollarsPerUnitWidth + yieldUnitWidth + dollarsPerYieldWidth + totalWidth;

            table.SetWidths(new float[] { cropWidth, jobWidth, fieldWidth, hoursWidth, dollarsPerHourWidth, payUnitWidth, dollarsPerUnitWidth, yieldUnitWidth, dollarsPerYieldWidth, totalWidth });
            table.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table.SplitRows = true;
            table.SplitLate = false;
            table.LockedWidth = true;
            table.DefaultCell.Padding = 0;
            table.DefaultCell.BorderWidth = 1f;
            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            iTextSharp.text.Color tintColor = new iTextSharp.text.Color(220, 230, 241);

            Paragraph paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_LEFT;
            paragraph.Font = FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD);
            paragraph.Add(MainMenu.CompanyStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.BOLD);
            paragraph.Add("Crop - Job - Field Report");
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            paragraph = new Paragraph();
            paragraph.Alignment = Element.ALIGN_CENTER;
            paragraph.Font = FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD);
            paragraph.Add(commentDateStr);
            paragraph.SpacingAfter = 6f;
            paragraph.SpacingBefore = 0f;
            paragraph.Leading = 10f;
            document.Add(paragraph);

            PdfPCell detailCell = new PdfPCell(new Phrase("Crop", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Job", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Field", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Hours", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Hour", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Pay Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Yield Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Yield", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table.DefaultCell.Padding = 0;
            table.AddCell(detailCell);

            //table.HeaderRows = 3; // this is the end of the table header
            lineCountDbl += 3;
            #endregion

            return table;
        }
        private PdfPTable HeaderPdfPTable2()
        {
            #region Header2 C-J-F without title
            
            PdfPTable table2 = new PdfPTable(10);
            table2.HorizontalAlignment = Element.ALIGN_LEFT;

            float tableWidth = 0;

            float cropWidth = 96f;
            float jobWidth = 96f;
            float fieldWidth = 96f;
            float hoursWidth = 78f;
            float dollarsPerHourWidth = 48;
            float payUnitWidth = 78f;
            float dollarsPerUnitWidth = 48f;
            float yieldUnitWidth = 78f;
            float dollarsPerYieldWidth = 48f;
            float totalWidth = 78f;

            tableWidth = cropWidth + jobWidth + fieldWidth + hoursWidth + dollarsPerHourWidth + payUnitWidth + dollarsPerUnitWidth + yieldUnitWidth + dollarsPerYieldWidth + totalWidth;

            table2.SetWidths(new float[] { cropWidth, jobWidth, fieldWidth, hoursWidth, dollarsPerHourWidth, payUnitWidth, dollarsPerUnitWidth, yieldUnitWidth, dollarsPerYieldWidth, totalWidth });
            table2.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table2.SplitRows = true;
            table2.SplitLate = false;
            table2.LockedWidth = true;
            table2.DefaultCell.Padding = 0;
            table2.DefaultCell.BorderWidth = 1f;
            table2.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table2.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            iTextSharp.text.Color tintColor = new iTextSharp.text.Color(220, 230, 241);


            PdfPCell detailCell = new PdfPCell(new Phrase("Crop", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Job", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Field", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Hours", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Hour", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Pay Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Yield Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Yield", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table2.DefaultCell.Padding = 0;
            table2.AddCell(detailCell);

            //table2.HeaderRows = 1; // this is the end of the table header
            lineCountDbl += 2;
            #endregion

            return table2;
        }
        private PdfPTable HeaderPdfPTable3()
        {
            #region Header3 Header J-C-F without title

            PdfPTable table3 = new PdfPTable(10);
            table3.HorizontalAlignment = Element.ALIGN_LEFT;

            float tableWidth = 0;

            float cropWidth = 96f;
            float jobWidth = 96f;
            float fieldWidth = 96f;
            float hoursWidth = 78f;
            float dollarsPerHourWidth = 48;
            float payUnitWidth = 78f;
            float dollarsPerUnitWidth = 48f;
            float yieldUnitWidth = 78f;
            float dollarsPerYieldWidth = 48f;
            float totalWidth = 78f;

            tableWidth = cropWidth + jobWidth + fieldWidth + hoursWidth + dollarsPerHourWidth + payUnitWidth + dollarsPerUnitWidth + yieldUnitWidth + dollarsPerYieldWidth + totalWidth;

            table3.SetWidths(new float[] { cropWidth, jobWidth, fieldWidth, hoursWidth, dollarsPerHourWidth, payUnitWidth, dollarsPerUnitWidth, yieldUnitWidth, dollarsPerYieldWidth, totalWidth });
            table3.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table3.SplitRows = true;
            table3.SplitLate = false;
            table3.LockedWidth = true;
            table3.DefaultCell.Padding = 0;
            table3.DefaultCell.BorderWidth = 1f;
            table3.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table3.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            iTextSharp.text.Color tintColor = new iTextSharp.text.Color(220, 230, 241);


            PdfPCell detailCell = new PdfPCell(new Phrase("Job", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Crop", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Field", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Hours", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Hour", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Pay Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Yield Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Yield", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table3.DefaultCell.Padding = 0;
            table3.AddCell(detailCell);

            //table3.HeaderRows = 1; // this is the end of the table header
            lineCountDbl += 2;
            #endregion

            return table3;
        }
        private PdfPTable HeaderPdfPTable4()
        {
            #region Header4 Header F-C-J without title

            PdfPTable table4 = new PdfPTable(10);
            table4.HorizontalAlignment = Element.ALIGN_LEFT;

            float tableWidth = 0;

            float cropWidth = 96f;
            float jobWidth = 96f;
            float fieldWidth = 96f;
            float hoursWidth = 78f;
            float dollarsPerHourWidth = 48;
            float payUnitWidth = 78f;
            float dollarsPerUnitWidth = 48f;
            float yieldUnitWidth = 78f;
            float dollarsPerYieldWidth = 48f;
            float totalWidth = 78f;

            tableWidth = cropWidth + jobWidth + fieldWidth + hoursWidth + dollarsPerHourWidth + payUnitWidth + dollarsPerUnitWidth + yieldUnitWidth + dollarsPerYieldWidth + totalWidth;

            table4.SetWidths(new float[] { cropWidth, jobWidth, fieldWidth, hoursWidth, dollarsPerHourWidth, payUnitWidth, dollarsPerUnitWidth, yieldUnitWidth, dollarsPerYieldWidth, totalWidth });
            table4.TotalWidth = tableWidth;
            //table.KeepTogether = true;
            table4.SplitRows = true;
            table4.SplitLate = false;
            table4.LockedWidth = true;
            table4.DefaultCell.Padding = 0;
            table4.DefaultCell.BorderWidth = 1f;
            table4.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table4.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            iTextSharp.text.Color tintColor = new iTextSharp.text.Color(220, 230, 241);


            PdfPCell detailCell = new PdfPCell(new Phrase("Field", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Crop", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Job", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Hours", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Hour", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Pay Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Yield Unit", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("$ / Yield", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            detailCell = new PdfPCell(new Phrase("Total", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.BOLD)));
            //cell.Width = 2f;
            detailCell.NoWrap = false;
            //cell.Leading = 0;
            detailCell.HorizontalAlignment = Element.ALIGN_CENTER;
            detailCell.VerticalAlignment = Element.ALIGN_MIDDLE;
            detailCell.BorderColor = iTextSharp.text.Color.LIGHT_GRAY;
            detailCell.BackgroundColor = tintColor;
            table4.DefaultCell.Padding = 0;
            table4.AddCell(detailCell);

            //table4.HeaderRows = 1; // this is the end of the table header
            lineCountDbl += 2;
            #endregion

            return table4;
        }
        #endregion
    }
}
