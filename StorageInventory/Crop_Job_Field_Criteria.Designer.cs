﻿namespace PayrollFieldDataDesktop
{
    partial class Crop_Job_Field_Criteria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DatePickerGroupBox = new System.Windows.Forms.GroupBox();
            this.DatePickerEndWeekLbl = new System.Windows.Forms.Label();
            this.DatePickerEndYearLbl = new System.Windows.Forms.Label();
            this.DatePickerStartWeekLbl = new System.Windows.Forms.Label();
            this.DatePickerStartYearLbl = new System.Windows.Forms.Label();
            this.DatePickerEndWeekTxt = new System.Windows.Forms.TextBox();
            this.DatePickerEndYearTxt = new System.Windows.Forms.TextBox();
            this.DatePickerStartWeekTxt = new System.Windows.Forms.TextBox();
            this.DatePickerStartYearTxt = new System.Windows.Forms.TextBox();
            this.DatePickerThruDateLbl = new System.Windows.Forms.Label();
            this.DatePickerThruDateTxt = new System.Windows.Forms.TextBox();
            this.DatePickerFromDateTxt = new System.Windows.Forms.TextBox();
            this.DatePickerFromDateLbl = new System.Windows.Forms.Label();
            this.DatePickerPresetsLbl = new System.Windows.Forms.Label();
            this.DatePickerPresetsCbx = new System.Windows.Forms.ComboBox();
            this.exitBtn = new System.Windows.Forms.Button();
            this.subtitleLbl = new System.Windows.Forms.Label();
            this.titleLbl = new System.Windows.Forms.Label();
            this.runBtn = new System.Windows.Forms.Button();
            this.CompanyNameData = new System.Windows.Forms.Label();
            this.DatePickerGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // DatePickerGroupBox
            // 
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndWeekLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndYearLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartWeekLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartYearLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndWeekTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerEndYearTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartWeekTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerStartYearTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerThruDateLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerThruDateTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerFromDateTxt);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerFromDateLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerPresetsLbl);
            this.DatePickerGroupBox.Controls.Add(this.DatePickerPresetsCbx);
            this.DatePickerGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerGroupBox.Location = new System.Drawing.Point(70, 73);
            this.DatePickerGroupBox.Name = "DatePickerGroupBox";
            this.DatePickerGroupBox.Size = new System.Drawing.Size(684, 95);
            this.DatePickerGroupBox.TabIndex = 0;
            this.DatePickerGroupBox.TabStop = false;
            // 
            // DatePickerEndWeekLbl
            // 
            this.DatePickerEndWeekLbl.AutoSize = true;
            this.DatePickerEndWeekLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndWeekLbl.Location = new System.Drawing.Point(619, 11);
            this.DatePickerEndWeekLbl.Name = "DatePickerEndWeekLbl";
            this.DatePickerEndWeekLbl.Size = new System.Drawing.Size(47, 18);
            this.DatePickerEndWeekLbl.TabIndex = 13;
            this.DatePickerEndWeekLbl.Text = "Week";
            // 
            // DatePickerEndYearLbl
            // 
            this.DatePickerEndYearLbl.AutoSize = true;
            this.DatePickerEndYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndYearLbl.Location = new System.Drawing.Point(518, 11);
            this.DatePickerEndYearLbl.Name = "DatePickerEndYearLbl";
            this.DatePickerEndYearLbl.Size = new System.Drawing.Size(68, 18);
            this.DatePickerEndYearLbl.TabIndex = 12;
            this.DatePickerEndYearLbl.Text = "End Year";
            // 
            // DatePickerStartWeekLbl
            // 
            this.DatePickerStartWeekLbl.AutoSize = true;
            this.DatePickerStartWeekLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartWeekLbl.Location = new System.Drawing.Point(444, 11);
            this.DatePickerStartWeekLbl.Name = "DatePickerStartWeekLbl";
            this.DatePickerStartWeekLbl.Size = new System.Drawing.Size(47, 18);
            this.DatePickerStartWeekLbl.TabIndex = 11;
            this.DatePickerStartWeekLbl.Text = "Week";
            // 
            // DatePickerStartYearLbl
            // 
            this.DatePickerStartYearLbl.AutoSize = true;
            this.DatePickerStartYearLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartYearLbl.Location = new System.Drawing.Point(340, 11);
            this.DatePickerStartYearLbl.Name = "DatePickerStartYearLbl";
            this.DatePickerStartYearLbl.Size = new System.Drawing.Size(73, 18);
            this.DatePickerStartYearLbl.TabIndex = 9;
            this.DatePickerStartYearLbl.Text = "Start Year";
            // 
            // DatePickerEndWeekTxt
            // 
            this.DatePickerEndWeekTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndWeekTxt.Location = new System.Drawing.Point(615, 33);
            this.DatePickerEndWeekTxt.Name = "DatePickerEndWeekTxt";
            this.DatePickerEndWeekTxt.Size = new System.Drawing.Size(54, 24);
            this.DatePickerEndWeekTxt.TabIndex = 6;
            // 
            // DatePickerEndYearTxt
            // 
            this.DatePickerEndYearTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerEndYearTxt.Location = new System.Drawing.Point(502, 33);
            this.DatePickerEndYearTxt.Name = "DatePickerEndYearTxt";
            this.DatePickerEndYearTxt.Size = new System.Drawing.Size(105, 24);
            this.DatePickerEndYearTxt.TabIndex = 5;
            // 
            // DatePickerStartWeekTxt
            // 
            this.DatePickerStartWeekTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartWeekTxt.Location = new System.Drawing.Point(440, 33);
            this.DatePickerStartWeekTxt.Name = "DatePickerStartWeekTxt";
            this.DatePickerStartWeekTxt.Size = new System.Drawing.Size(54, 24);
            this.DatePickerStartWeekTxt.TabIndex = 4;
            // 
            // DatePickerStartYearTxt
            // 
            this.DatePickerStartYearTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerStartYearTxt.Location = new System.Drawing.Point(328, 33);
            this.DatePickerStartYearTxt.Name = "DatePickerStartYearTxt";
            this.DatePickerStartYearTxt.Size = new System.Drawing.Size(105, 24);
            this.DatePickerStartYearTxt.TabIndex = 3;
            // 
            // DatePickerThruDateLbl
            // 
            this.DatePickerThruDateLbl.AutoSize = true;
            this.DatePickerThruDateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerThruDateLbl.Location = new System.Drawing.Point(415, 68);
            this.DatePickerThruDateLbl.Name = "DatePickerThruDateLbl";
            this.DatePickerThruDateLbl.Size = new System.Drawing.Size(73, 18);
            this.DatePickerThruDateLbl.TabIndex = 14;
            this.DatePickerThruDateLbl.Text = "Thru Date";
            // 
            // DatePickerThruDateTxt
            // 
            this.DatePickerThruDateTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerThruDateTxt.Location = new System.Drawing.Point(494, 66);
            this.DatePickerThruDateTxt.Name = "DatePickerThruDateTxt";
            this.DatePickerThruDateTxt.Size = new System.Drawing.Size(144, 24);
            this.DatePickerThruDateTxt.TabIndex = 2;
            // 
            // DatePickerFromDateTxt
            // 
            this.DatePickerFromDateTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerFromDateTxt.Location = new System.Drawing.Point(164, 66);
            this.DatePickerFromDateTxt.Name = "DatePickerFromDateTxt";
            this.DatePickerFromDateTxt.Size = new System.Drawing.Size(144, 24);
            this.DatePickerFromDateTxt.TabIndex = 1;
            // 
            // DatePickerFromDateLbl
            // 
            this.DatePickerFromDateLbl.AutoSize = true;
            this.DatePickerFromDateLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerFromDateLbl.Location = new System.Drawing.Point(79, 69);
            this.DatePickerFromDateLbl.Name = "DatePickerFromDateLbl";
            this.DatePickerFromDateLbl.Size = new System.Drawing.Size(79, 18);
            this.DatePickerFromDateLbl.TabIndex = 8;
            this.DatePickerFromDateLbl.Text = "From Date";
            // 
            // DatePickerPresetsLbl
            // 
            this.DatePickerPresetsLbl.AutoSize = true;
            this.DatePickerPresetsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerPresetsLbl.Location = new System.Drawing.Point(6, 11);
            this.DatePickerPresetsLbl.Name = "DatePickerPresetsLbl";
            this.DatePickerPresetsLbl.Size = new System.Drawing.Size(169, 18);
            this.DatePickerPresetsLbl.TabIndex = 7;
            this.DatePickerPresetsLbl.Text = "Date Selections for Date";
            // 
            // DatePickerPresetsCbx
            // 
            this.DatePickerPresetsCbx.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.DatePickerPresetsCbx.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DatePickerPresetsCbx.FormattingEnabled = true;
            this.DatePickerPresetsCbx.Location = new System.Drawing.Point(6, 33);
            this.DatePickerPresetsCbx.Name = "DatePickerPresetsCbx";
            this.DatePickerPresetsCbx.Size = new System.Drawing.Size(271, 26);
            this.DatePickerPresetsCbx.TabIndex = 0;
            // 
            // exitBtn
            // 
            this.exitBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exitBtn.Location = new System.Drawing.Point(467, 207);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(95, 35);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click_1);
            // 
            // subtitleLbl
            // 
            this.subtitleLbl.AutoSize = true;
            this.subtitleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subtitleLbl.Location = new System.Drawing.Point(363, 29);
            this.subtitleLbl.Name = "subtitleLbl";
            this.subtitleLbl.Size = new System.Drawing.Size(182, 20);
            this.subtitleLbl.TabIndex = 8;
            this.subtitleLbl.Text = "Crop - Job - Field Report";
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLbl.Location = new System.Drawing.Point(259, 5);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(352, 24);
            this.titleLbl.TabIndex = 7;
            this.titleLbl.Text = "KeyCentral® Easy Payroll Data Collection";
            // 
            // runBtn
            // 
            this.runBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runBtn.Location = new System.Drawing.Point(322, 207);
            this.runBtn.Name = "runBtn";
            this.runBtn.Size = new System.Drawing.Size(139, 35);
            this.runBtn.TabIndex = 6;
            this.runBtn.Text = "Make PDF Report";
            this.runBtn.UseVisualStyleBackColor = true;
            this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
            // 
            // CompanyNameData
            // 
            this.CompanyNameData.Location = new System.Drawing.Point(13, 5);
            this.CompanyNameData.Name = "CompanyNameData";
            this.CompanyNameData.Size = new System.Drawing.Size(248, 23);
            this.CompanyNameData.TabIndex = 9;
            this.CompanyNameData.Text = "label1";
            this.CompanyNameData.UseMnemonic = false;
            // 
            // Crop_Job_Field_Criteria
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(808, 261);
            this.Controls.Add(this.CompanyNameData);
            this.Controls.Add(this.subtitleLbl);
            this.Controls.Add(this.DatePickerGroupBox);
            this.Controls.Add(this.runBtn);
            this.Controls.Add(this.titleLbl);
            this.Controls.Add(this.exitBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Crop_Job_Field_Criteria";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "KeyCentral® Easy Payroll Data Collection - Pay Data Report";
            this.DatePickerGroupBox.ResumeLayout(false);
            this.DatePickerGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox DatePickerGroupBox;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label DatePickerPresetsLbl;
        private System.Windows.Forms.ComboBox DatePickerPresetsCbx;
        private System.Windows.Forms.TextBox DatePickerThruDateTxt;
        private System.Windows.Forms.TextBox DatePickerFromDateTxt;
        private System.Windows.Forms.Label DatePickerFromDateLbl;
        private System.Windows.Forms.TextBox DatePickerEndWeekTxt;
        private System.Windows.Forms.TextBox DatePickerEndYearTxt;
        private System.Windows.Forms.TextBox DatePickerStartWeekTxt;
        private System.Windows.Forms.TextBox DatePickerStartYearTxt;
		private System.Windows.Forms.Label DatePickerThruDateLbl;
        private System.Windows.Forms.Label DatePickerStartYearLbl;
        private System.Windows.Forms.Label DatePickerEndWeekLbl;
        private System.Windows.Forms.Label DatePickerEndYearLbl;
        private System.Windows.Forms.Label DatePickerStartWeekLbl;
        private System.Windows.Forms.Label subtitleLbl;
        private System.Windows.Forms.Label titleLbl;
        private System.Windows.Forms.Button runBtn;
        private System.Windows.Forms.Label CompanyNameData;
    }
}