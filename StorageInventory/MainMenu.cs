﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.Reflection;
using System.Management;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace StorageInventory
{
	[Serializable]
	public partial class MainMenu : Form
	{
		#region Vars
		private IntPtr ProgramMainWindowHandle;
        #region Version Information
        public static string versionStr = "Version FDD230410";
        #endregion

        #region RFBII Company Info
        public static string CompanyStr = "Grand Rapids RFBII Farms LLC";
        public static string CompanyAddressStr = "400 Ann Street";
        public static string CompanyAddress2Str = "Suite 213";
        public static string CompanyAddress3Str = "Grand Rapids MI, 49504";
        public static string CompanyPhoneStr = "616-447-0138";
        public static string CompanyEINStr = "??-???????";
        public static string VersionTypeStr = "Scan";
        #endregion

        #region Customer Company Info
        #region Berrybrook Enterprises Company Info
        //public static string CompanyStr = "Berrybrook Enterprises";
        //public static string CompanyAddressStr = "86997 CR687, ";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Hartford, MI 49057";
        //public static string CompanyPhoneStr = "269-782-5000";
        //public static string CompanyEINStr = "??-???????";
        //public static string VersionTypeStr = "Scan";
        //Berrybrook Label printing is different need to change before release.(in WorkOrder.cs and LabelPrinter.cs)
        #endregion

        #region Centennial Fruit Inc. 03-19-13
        //public static string CompanyStr = "Centennial Fruit Inc.";
        //public static string CompanyAddressStr = "1464 Wilson";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Conklin, MI 49403";
        //public static string CompanyPhoneStr = "616-899-1102";
        //public static string CompanyEINStr = "38-3582237";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #region J & J Morse, Inc. 08-21-13
        //public static string CompanyStr = "J & J Morse, Inc.";
        //public static string CompanyAddressStr = "10350 Fruit Ridge Avenue";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Sparta, MI  49345";
        //public static string CompanyPhoneStr = "616-893-3257";
        //public static string CompanyEINStr = "38-3209460";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #region Kent Fruit Farms 08-07-13
        //public static string CompanyStr = "Kent Fruit Farms";
        //public static string CompanyAddressStr = "PO Box 555";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Casnovia, MI 49318";
        //public static string CompanyPhoneStr = "";
        //public static string CompanyEINStr = "";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #region Leo Dietrich & Sons 12-28-12
        //public static string CompanyStr = "Leo Dietrich & Sons LLC";
        //public static string CompanyAddressStr = "1311 Wilson";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Conklin, MI 49403";
        //public static string CompanyPhoneStr = "616-899-5579";
        //public static string CompanyEINStr = "38-2492859";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #region PG Orchards LLC 08-07-13
        //public static string CompanyStr = "PG Orchards LLC";
        //public static string CompanyAddressStr = "4546 12 Mile Road NW";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Sparta, MI  49345";
        //public static string CompanyPhoneStr = "616-291-8980";
        //public static string CompanyEINStr = "26-1475268";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #region Ridgeview Orchards LLC 09-02-13
        //public static string CompanyStr = "Ridgeview Orchards LLC";
        //public static string CompanyAddressStr = "1567 Wilson";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Conklin, MI 49403";
        //public static string CompanyPhoneStr = "616-899-2317";
        //public static string CompanyEINStr = "38-2583791";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #region Riveridge Land Co LLC 08-07-13
        //public static string CompanyStr = "Riveridge Land Co LLC";
        //public static string CompanyAddressStr = "9000 Fruit Ridge";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Sparta, MI  49345";
        //public static string CompanyPhoneStr = "616-887-6873";
        //public static string CompanyEINStr = "38-3172652";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #region Riveridge Land Co II LLC 08-07-13
        //public static string CompanyStr = "Riveridge Land Co II LLC";
        //public static string CompanyAddressStr = "9000 Fruit Ridge";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Sparta, MI  49345";
        //public static string CompanyPhoneStr = "616-887-6873";
        //public static string CompanyEINStr = "26-4781727";
        //public static string VersionTypeStr = "Scan";
        #endregion

        #endregion


        #region Other Customer Company Info


        #region Dennis W. Umlor Farms 12-28-12
        //public static string CompanyStr = "Dennis W. Umlor Farms LLC";
        //public static string CompanyAddressStr = "2018 Wilson";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Conklin, MI 49403";
        //public static string CompanyPhoneStr = "616-899-2983";
        //public static string CompanyEINStr = "46-1842020";
        //public static string VersionTypeStr = "";
        #endregion

        #region Gavin Orchards 12-28-12
        //public static string CompanyStr = "Gavin Orchards LLC";
        //public static string CompanyAddressStr = "16495 40th Ave.";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Coopersville, MI  49404";
        //public static string CompanyPhoneStr = "616-837-6472";
        //public static string CompanyEINStr = "87-0793486";
        //public static string VersionTypeStr = "";
        #endregion

        #region Gee Orchards 08-05-13
        //public static string CompanyStr = "Gee Orchards, LLC";
        //public static string CompanyAddressStr = "975 Kenowa Avenue";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Bailey, MI  49303";
        //public static string CompanyPhoneStr = "616-675-4217";
        //public static string CompanyEINStr = "45-4077682";
        //public static string VersionTypeStr = "";
        #endregion

        #region Good Fruit Storage LLC 08-12-13
        //public static string CompanyStr = "Good Fruit Storage LLC";
        //public static string CompanyAddressStr = "4574 16 Mile Rd.";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Kent City, MI 49330";
        //public static string CompanyPhoneStr = "616-675-5477";
        //public static string CompanyEINStr = "20-0741356";
        //public static string VersionTypeStr = "";
        #endregion

        #region Hubert Storage 12-28-12
        //public static string CompanyStr = "Hubert Storage LLC";
        //public static string CompanyAddressStr = "4574 16 Mile Rd.";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Kent City, MI 49330";
        //public static string CompanyPhoneStr = "616-675-5477";
        //public static string CompanyEINStr = "38-3255122";
        //public static string VersionTypeStr = "";
        #endregion

        #region K&H Farms 12-28-12
        //public static string CompanyStr = "K&H Farms";
        //public static string CompanyAddressStr = "4574 16 Mile Rd.";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Kent City, MI 49330";
        //public static string CompanyPhoneStr = "616-675-5477";
        //public static string CompanyEINStr = "38-2908459";
        //public static string VersionTypeStr = "";
        #endregion

        #region Karnemaat's 12-28-12
        //public static string CompanyStr = "Karnemaat's L.L.C.";
        //public static string CompanyAddressStr = "5118 W. 72nd Street";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Fremont, MI  49412";
        //public static string CompanyPhoneStr = "(231) 924-0465";
        //public static string CompanyEINStr = "38-3222320";
        //public static string VersionTypeStr = "";
        #endregion

        #region Rasch Family 12-28-12
        //public static string CompanyStr = "Rasch Family Orchards LLC";
        //public static string CompanyAddressStr = "6191 Stage Ave NW";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Grand Rapids, MI  49544";
        //public static string CompanyPhoneStr = "616-784-2715";
        //public static string CompanyEINStr = "56-2644940";
        //public static string VersionTypeStr = "";
        #endregion

        #region Rodney Kober 12-28-12
        //public static string CompanyStr = "Rodney Kober LLC";
        //public static string CompanyAddressStr = "7861 Peach Ridge";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Sparta, MI 49345";
        //public static string CompanyPhoneStr = "616-887-7930";
        //public static string CompanyEINStr = "38-2648513";
        //public static string VersionTypeStr = "";
        #endregion

        #region Ryan Kober Farms 12-28-12
        //public static string CompanyStr = "Ryan Kober Farms LLC";
        //public static string CompanyAddressStr = "7861 Peach Ridge";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Sparta, MI 49345";
        //public static string CompanyPhoneStr = "616-887-7930";
        //public static string CompanyEINStr = "27-0344499";
        //public static string VersionTypeStr = "";
        #endregion

        #region Summit Farms 12-28-12
        //public static string CompanyStr = "Summit Farms Inc.";
        //public static string CompanyAddressStr = "4574 16 Mile Rd.";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Kent City, MI 49330";
        //public static string CompanyPhoneStr = "616-675-5477";
        //public static string CompanyEINStr = "38-3091696";
        //public static string VersionTypeStr = "";
        #endregion

        #region Summit Orchards 12-28-12
        //public static string CompanyStr = "Summit Orchards LLC";
        //public static string CompanyAddressStr = "(Randy Kober)";
        //public static string CompanyAddress2Str = "4574 16 Mile Rd.";
        //public static string CompanyAddress3Str = "Kent City, MI 49330";
        //public static string CompanyPhoneStr = "616-675-5477";
        //public static string CompanyEINStr = "30-0642994";
        //public static string VersionTypeStr = "";
        #endregion

        #region Thome Orchards 1-22-13
        //public static string CompanyStr = "Thome Orchards LLC";
        //public static string CompanyAddressStr = "2137 - 7 Mile Road NW";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Comstock Park, MI 49321";
        //public static string CompanyPhoneStr = "616-784-1009";
        //public static string CompanyEINStr = "20-2114975";
        //public static string VersionTypeStr = "";
        #endregion

        #region Victor Hubert Farms 12-28-12
        //public static string CompanyStr = "Victor Hubert Farms LLC";
        //public static string CompanyAddressStr = "4574 16 Mile Rd.";
        //public static string CompanyAddress2Str = "";
        //public static string CompanyAddress3Str = "Kent City, MI 49330";
        //public static string CompanyPhoneStr = "616-675-5477";
        //public static string CompanyEINStr = "38-2881209";
        //public static string VersionTypeStr = "";
        #endregion
        #endregion

        #endregion

        #region Constructor
        public MainMenu()
		{
			InitializeComponent();
            CompanyNameData.Text = CompanyStr;
            VersionData.Text = versionStr;
            
			////Load all of our data from XML.
			//FillCropDataFromXML();
			//FillEmployeeDataFromXML();
			//FillFieldDataFromXML();
			//FillJobDataFromXML();
			//FillPayDetailDataFromXML();

			////Set the XML file names.
			//xmlFileName_Crops = Path.GetDirectoryName(Application.ExecutablePath) + @"\CropData.xml";
			//xmlFileName_Employees = Path.GetDirectoryName(Application.ExecutablePath) + @"\EmployeeData.xml";
			//xmlFileName_Jobs = Path.GetDirectoryName(Application.ExecutablePath) + @"\JobData.xml";
			//xmlFileName_Fields = Path.GetDirectoryName(Application.ExecutablePath) + @"\FieldData.xml";
			//xmlFileName_PayDetails = Path.GetDirectoryName(Application.ExecutablePath) + @"\PayDetailData.xml";

			//Make form non-resizable.
			//this.MinimumSize = this.MaximumSize = this.Size;
			//this.FormBorderStyle = FormBorderStyle.FixedSingle;

			Process[] processes = Process.GetProcesses();

			foreach (Process p in processes)
			{
				if (p.ProcessName.ToUpper().Contains("PAYROLLDATACOLLECTION"))
				{
					string processOwnerStr = GetProcessOwner(p.Id);
					string currentlyLoggedInUserStr = Environment.UserName;

					if (processOwnerStr == currentlyLoggedInUserStr)
					{
						ProgramMainWindowHandle = p.MainWindowHandle;
					}
				}
			}
		}
		#endregion

		#region Button Click Functions
		//Input / Edit
		private void exitBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
		private void cropBtn_Click(object sender, EventArgs e)
        {
            Crop crop = new Crop();
            this.Hide();
            crop.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
		private void jobBtn_Click(object sender, EventArgs e)
        {
            Job job = new Job();
            this.Hide();
            job.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
		private void fieldBtn_Click(object sender, EventArgs e)
        {
            Field field = new Field();
            this.Hide();
            field.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
		private void employeeBtn_Click(object sender, EventArgs e)
        {
            Employee employee = new Employee();
            this.Hide();
            employee.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
		private void payDetailBtn_Click(object sender, EventArgs e)
        {
            PayDetail payDetail = new PayDetail();
            this.Hide();
            payDetail.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
		private void setPayrollOptionsBtn_Click(object sender, EventArgs e)
        {
            PayrollOptions payrollOptions = new PayrollOptions();
            this.Hide();
            payrollOptions.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }

		//Reports
		private void payDataReportBtn_Click(object sender, EventArgs e)
        {
            PayData_Criteria payData_Criteria = new PayData_Criteria();
            this.Hide();
            payData_Criteria.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
		private void makePayReportBtn_Click(object sender, EventArgs e)
        {
            //MakePayEndDate makepayenddate = new MakePayEndDate();
            //this.Hide();
            //makepayenddate.ShowDialog();
            //this.Show();
            //this.BringToFront();
            //this.Activate();
            //this.Focus();
        }
         private void editInventoryDetailBtn_Click(object sender, EventArgs e)
        {
            EditWorkOrder editWorkOrder = new EditWorkOrder();
            this.Hide();
            editWorkOrder.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();
        }
         private void payDataByEmployeeBtn_Click(object sender, EventArgs e)
         {
             //PayDataByEmployee_Criteria payDataByEmployee_Criteria = new PayDataByEmployee_Criteria();
             //this.Hide();
             //payDataByEmployee_Criteria.ShowDialog();
             //this.Show();
             //this.BringToFront();
             //this.Activate();
             //this.Focus();

             //SetForegroundWindow(ProgramMainWindowHandle);
         }
         private void makePayDueReportBtn_Click(object sender, EventArgs e)
         {
             //MakePayDueEndDate makePayDueEndDate = new MakePayDueEndDate();
             //this.Hide();
             //makePayDueEndDate.ShowDialog();
             //this.Show();
             //this.BringToFront();
             //this.Activate();
             //this.Focus();

             //SetForegroundWindow(ProgramMainWindowHandle);
         }
        private void multipleRoomMoveBtn_Click(object sender, EventArgs e)
        {
            MultipleRoomMove multipleRoomMove = new MultipleRoomMove();
            this.Hide();
            multipleRoomMove.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
        private void LabelPrintingBtn_Click(object sender, EventArgs e)
         {
             LabelPrinter labelPrinter = new LabelPrinter();
             this.Hide();
             labelPrinter.ShowDialog();
             this.Show();
             this.BringToFront();
             this.Activate();
             this.Focus();

             SetForegroundWindow(ProgramMainWindowHandle);
         }
         private void SyncScannerBtn_Click(object sender, EventArgs e)
         {
             //Process.Start(Path.GetDirectoryName(Application.ExecutablePath) + @"\PayrollFieldScannerSyncUpload.exe");
         }

         private void ShippingOrderBtn_Click(object sender, EventArgs e)
         {
             ShippingOrder shippingOrder = new ShippingOrder();
             this.Hide();
             shippingOrder.ShowDialog();
             this.Show();
             this.BringToFront();
             this.Activate();
             this.Focus();
         }
        
        private void WorkOrderBtn_Click(object sender, EventArgs e)
         {

             WorkOrder workOrder = new WorkOrder();
             this.Hide();
             workOrder.ShowDialog();
             this.Show();
             this.BringToFront();
             this.Activate();
             this.Focus();
         }
        
         private void SixEmployeePerPageBtn_Click(object sender, EventArgs e)
         {
             //EmployeesPage eworkOrder = new EmployeesPage();
             //this.Hide();
             //eworkOrder.ShowDialog();
             //this.Show();
             //this.BringToFront();
             //this.Activate();
             //this.Focus();
         }

         private void RoomReportBtn_Click(object sender, EventArgs e)
         {
             RoomReport_Criteria roomReport_Criteria = new RoomReport_Criteria();
             this.Hide();
             roomReport_Criteria.ShowDialog();
             this.Show();
             this.BringToFront();
             this.Activate();
             this.Focus();

             SetForegroundWindow(ProgramMainWindowHandle);
         }

        private void growerBlockBtn_Click(object sender, EventArgs e)
        {
            Grower_Block grower_Block = new Grower_Block();
            this.Hide();
            grower_Block.ShowDialog();
            this.Show();
            this.BringToFront();
            this.Activate();
            this.Focus();

            SetForegroundWindow(ProgramMainWindowHandle);
        }
        #endregion

        #region Helpers
        #region Crop Functions
        public static CropData FindOrAddCropData(CropsData cropDataIn, string codeStr, string descriptionStr, int keyInt)
        {
            CropData tempCropData = new CropData(codeStr, descriptionStr, keyInt);
            int tempCropDataIdx = cropDataIn.CropsDataArl.BinarySearch(tempCropData);

            if (tempCropDataIdx < 0)
                cropDataIn.CropsDataArl.Insert(Math.Abs(tempCropDataIdx) - 1, tempCropData);
            else
                tempCropData = (CropData)cropDataIn.CropsDataArl[tempCropDataIdx];

            return tempCropData;
        }
        public static CropData FindCropData(CropsData cropDataIn, string codeStr, string descriptionStr, int keyInt)
        {
            CropData tempCropData = new CropData(codeStr, descriptionStr, keyInt);
            int tempCropDataIdx = cropDataIn.CropsDataArl.BinarySearch(tempCropData);

            if (tempCropDataIdx >= 0)
                return (CropData)cropDataIn.CropsDataArl[tempCropDataIdx];

            return null;
        }
        public static void SaveEditedCropData(CropsData cropDataIn, string codeStr, string descriptionStr, int keyInt, bool active)
        {
            for (int i = 0; i < cropDataIn.CropsDataArl.Count; i++)
            {
                CropData cropDataInList = (CropData)cropDataIn.CropsDataArl[i];
                if (keyInt == cropDataInList.KeyInt)
                {
                    cropDataIn.CropsDataArl.RemoveAt(i);
                    CropData editedCropData = new CropData(codeStr, descriptionStr, keyInt);
                    editedCropData.ActiveBln = active;
                    cropDataIn.CropsDataArl.Insert(i, editedCropData);

                    break;
                }
            }
        }
        #endregion
		#region Job Functions
		public static JobData FindOrAddJobData(JobsData jobDataIn, string codeStr, string descriptionStr, int keyInt)
		{
			JobData tempJobData = new JobData(codeStr, descriptionStr, keyInt);
			int tempJobDataIdx = jobDataIn.JobsDataArl.BinarySearch(tempJobData);

			if (tempJobDataIdx < 0)
				jobDataIn.JobsDataArl.Insert(Math.Abs(tempJobDataIdx) - 1, tempJobData);
			else
				tempJobData = (JobData)jobDataIn.JobsDataArl[tempJobDataIdx];

			return tempJobData;
		}
		public static JobData FindJobData(JobsData jobDataIn, string codeStr, string descriptionStr, int keyInt)
		{
			JobData tempJobData = new JobData(codeStr, descriptionStr, keyInt);
			int tempJobDataIdx = jobDataIn.JobsDataArl.BinarySearch(tempJobData);

			if (tempJobDataIdx >= 0)
				return (JobData)jobDataIn.JobsDataArl[tempJobDataIdx];

			return null;
		}
		public static void SaveEditedJobData(JobsData jobDataIn, string codeStr, string descriptionStr, int keyInt, bool active)
		{
			for (int i = 0; i < jobDataIn.JobsDataArl.Count; i++)
			{
				JobData jobDataInList = (JobData)jobDataIn.JobsDataArl[i];
				if (keyInt == jobDataInList.KeyInt)
				{
					jobDataIn.JobsDataArl.RemoveAt(i);
					JobData editedJobData = new JobData(codeStr, descriptionStr, keyInt);
					editedJobData.ActiveBln = active;
					jobDataIn.JobsDataArl.Insert(i, editedJobData);

					break;
				}
			}
		}
		#endregion
		#region Field Functions
		public static FieldData FindOrAddFieldData(FieldsData fieldDataIn, string codeStr, string descriptionStr, int keyInt)
		{
			FieldData tempFieldData = new FieldData(codeStr, descriptionStr, keyInt);
			int tempFieldDataIdx = fieldDataIn.FieldsDataArl.BinarySearch(tempFieldData);

			if (tempFieldDataIdx < 0)
				fieldDataIn.FieldsDataArl.Insert(Math.Abs(tempFieldDataIdx) - 1, tempFieldData);
			else
				tempFieldData = (FieldData)fieldDataIn.FieldsDataArl[tempFieldDataIdx];

			return tempFieldData;
		}
		public static FieldData FindFieldData(FieldsData fieldDataIn, string codeStr, string descriptionStr, int keyInt)
		{
			FieldData tempFieldData = new FieldData(codeStr, descriptionStr, keyInt);
			int tempFieldDataIdx = fieldDataIn.FieldsDataArl.BinarySearch(tempFieldData);

			if (tempFieldDataIdx >= 0)
				return (FieldData)fieldDataIn.FieldsDataArl[tempFieldDataIdx];

			return null;
		}
		public static void SaveEditedFieldData(FieldsData fieldDataIn, string codeStr, string descriptionStr, int keyInt, bool active)
		{
			for (int i = 0; i < fieldDataIn.FieldsDataArl.Count; i++)
			{
				FieldData fieldDataInList = (FieldData)fieldDataIn.FieldsDataArl[i];
				if (keyInt == fieldDataInList.KeyInt)
				{
					fieldDataIn.FieldsDataArl.RemoveAt(i);
					FieldData editedFieldData = new FieldData(codeStr, descriptionStr, keyInt);
					editedFieldData.ActiveBln = active;
					fieldDataIn.FieldsDataArl.Insert(i, editedFieldData);

					break;
				}
			}
		}
		#endregion
		#region Employee Functions
		public static EmployeeData FindOrAddEmployeeData(EmployeesData employeeDataIn, string codeStr, string nameStr, string address1Str, string address2Str, string cityStr, string stateStr, string zipStr, string ssnStr, int keyInt, string groupStr)
		{
            EmployeeData tempEmployeeData = new EmployeeData(codeStr, nameStr, address1Str, address2Str, cityStr, stateStr, zipStr, ssnStr, keyInt, groupStr);
			int tempEmployeeDataIdx = employeeDataIn.EmployeesDataArl.BinarySearch(tempEmployeeData);

			if (tempEmployeeDataIdx < 0)
				employeeDataIn.EmployeesDataArl.Insert(Math.Abs(tempEmployeeDataIdx) - 1, tempEmployeeData);
			else
				tempEmployeeData = (EmployeeData)employeeDataIn.EmployeesDataArl[tempEmployeeDataIdx];

			return tempEmployeeData;
		}
        public static EmployeeData FindEmployeeData(EmployeesData employeeDataIn, string codeStr, string nameStr, string address1Str, string address2Str, string cityStr, string stateStr, string zipStr, string ssnStr, int keyInt, string groupStr)
		{
            EmployeeData tempEmployeeData = new EmployeeData(codeStr, nameStr, address1Str, address2Str, cityStr, stateStr, zipStr, ssnStr, keyInt, groupStr);
			int tempEmployeeDataIdx = employeeDataIn.EmployeesDataArl.BinarySearch(tempEmployeeData);

			if (tempEmployeeDataIdx >= 0)
				return (EmployeeData)employeeDataIn.EmployeesDataArl[tempEmployeeDataIdx];

			return null;
		}
        public static void SaveEditedEmployeeData(EmployeesData employeeDataIn, string codeStr, string nameStr, string address1Str, string address2Str, string cityStr, string stateStr, string zipStr, string ssnStr, int keyInt, double hourlyWageDbl, bool overtimePerEmployeeBln, bool active, string groupStr)
		{
			for (int i = 0; i < employeeDataIn.EmployeesDataArl.Count; i++)
			{
				EmployeeData employeeDataInList = (EmployeeData)employeeDataIn.EmployeesDataArl[i];

				if (keyInt == employeeDataInList.KeyInt)
				{
                    EmployeeData editedEmployeeData = new EmployeeData(codeStr, nameStr, address1Str, address2Str, cityStr, stateStr, zipStr, ssnStr, keyInt, groupStr);
					editedEmployeeData.HourlyWageDbl = hourlyWageDbl;
                    editedEmployeeData.OvertimePerEmployeeBln = overtimePerEmployeeBln;
					editedEmployeeData.ActiveBln = active;

					for (int j = 0; j < employeeDataInList.EmployeePreTaxAdjustmentsDataArl.Count; j++)
					{
						EmployeePreTaxAdjustmentData employeePreTaxAdjustmentData = (EmployeePreTaxAdjustmentData)employeeDataInList.EmployeePreTaxAdjustmentsDataArl[j];
						EmployeePreTaxAdjustmentData editedEmployeePreTaxAdjustmentData = new EmployeePreTaxAdjustmentData(employeePreTaxAdjustmentData.CodeStr, employeePreTaxAdjustmentData.DescriptionStr, employeePreTaxAdjustmentData.KeyInt);
						editedEmployeePreTaxAdjustmentData.AmountDbl += employeePreTaxAdjustmentData.AmountDbl;

						editedEmployeeData.EmployeePreTaxAdjustmentsDataArl.Add(editedEmployeePreTaxAdjustmentData);
					}

					employeeDataIn.EmployeesDataArl.RemoveAt(i);

					employeeDataIn.EmployeesDataArl.Insert(i, editedEmployeeData);

					break;
				}
			}
		}

		public static EmployeePreTaxAdjustmentData FindOrAddEmployeePreTaxAdjustmentData(EmployeeData employeeData, string codeStr, string descriptionStr, int keyInt)
		{
			EmployeePreTaxAdjustmentData tempEmployeePreTaxAdjustmentData = new EmployeePreTaxAdjustmentData(codeStr, descriptionStr, keyInt);
			int tempEmployeePreTaxAdjustmentDataIdx = employeeData.EmployeePreTaxAdjustmentsDataArl.BinarySearch(tempEmployeePreTaxAdjustmentData);

			if (tempEmployeePreTaxAdjustmentDataIdx < 0)
				employeeData.EmployeePreTaxAdjustmentsDataArl.Insert(Math.Abs(tempEmployeePreTaxAdjustmentDataIdx) - 1, tempEmployeePreTaxAdjustmentData);
			else
				tempEmployeePreTaxAdjustmentData = (EmployeePreTaxAdjustmentData)employeeData.EmployeePreTaxAdjustmentsDataArl[tempEmployeePreTaxAdjustmentDataIdx];

			return tempEmployeePreTaxAdjustmentData;
		}
		public static EmployeePreTaxAdjustmentData FindEmployeePreTaxAdjustmentData(EmployeeData employeeData, string codeStr, string descriptionStr, int keyInt)
		{
			EmployeePreTaxAdjustmentData tempEmployeePreTaxAdjustmentData = new EmployeePreTaxAdjustmentData(codeStr, descriptionStr, keyInt);
			int tempEmployeePreTaxAdjustmentDataIdx = employeeData.EmployeePreTaxAdjustmentsDataArl.BinarySearch(tempEmployeePreTaxAdjustmentData);

			if (tempEmployeePreTaxAdjustmentDataIdx >= 0)
				return (EmployeePreTaxAdjustmentData)employeeData.EmployeePreTaxAdjustmentsDataArl[tempEmployeePreTaxAdjustmentDataIdx];

			return null;
		}
		public static void SaveEditedEmployeePreTaxAdjustmentData(EmployeeData employeeData, string codeStr, string descriptionStr, int keyInt, double amountDbl)
		{
			for (int i = 0; i < employeeData.EmployeePreTaxAdjustmentsDataArl.Count; i++)
			{
				EmployeePreTaxAdjustmentData employeePreTaxAdjustmentDataInList = (EmployeePreTaxAdjustmentData)employeeData.EmployeePreTaxAdjustmentsDataArl[i];

				if (keyInt == employeePreTaxAdjustmentDataInList.KeyInt)
				{
					EmployeePreTaxAdjustmentData editedEmployeePreTaxAdjustmentData = new EmployeePreTaxAdjustmentData(codeStr, descriptionStr, keyInt);
					editedEmployeePreTaxAdjustmentData.AmountDbl += amountDbl;

					employeeData.EmployeePreTaxAdjustmentsDataArl.RemoveAt(i);

					employeeData.EmployeePreTaxAdjustmentsDataArl.Insert(i, editedEmployeePreTaxAdjustmentData);

					break;
				}
			}
		}
		#endregion
		#region Pay Detail Functions
		public static PayDetailData FindOrAddPayDetailData(ArrayList list, DateTime dateIn, DateTime startTimeIn, DateTime endTimeIn, int employeeIn, int cropIn, int jobIn, int fieldIn, string typeStr)
		{
			PayDetailData tempPayDetailData = new PayDetailData(dateIn, startTimeIn, endTimeIn, employeeIn, cropIn, jobIn, fieldIn);
            tempPayDetailData.TypeStr = typeStr;
            tempPayDetailData.ActiveBln = true;
			int tempPayDetailDataIdx = list.BinarySearch(tempPayDetailData);

			if (tempPayDetailDataIdx < 0)/*Insert a new Pay Detail object.*/
			{
				//Set the key ( "line number" ) for the new entry.
				tempPayDetailData.KeyInt = list.Count;

				//This line will insert the new entry into the list in a chronological position.
				//      This will make the key ( "line number" ) not be in order.
				list.Insert(Math.Abs(tempPayDetailDataIdx) - 1, tempPayDetailData);
			}
			else
			{
				tempPayDetailData = (PayDetailData)list[tempPayDetailDataIdx];
			}

			return tempPayDetailData;
		}
		public static void DeletePayDetailData(int keyInt, ArrayList list)
		{
			for (int i = 0; i < list.Count; i++)
			{
				PayDetailData pdata = (PayDetailData)list[i];
				if (pdata.KeyInt == keyInt)
				{
					pdata.ActiveBln = false;
					break;
				}
			}
		}
		public static void SaveEditedPayDetailData(int keyInt, ArrayList list, DateTime dateIn, DateTime startTimeIn, DateTime endTimeIn, int employeeIn, int cropIn, int jobIn, int fieldIn, string payUnitIn, string yieldUnitIn, string typeStr, string boxLabelStr, string referenceStr, string otherStr, int growerBlockIn)
		{
			PayDetailData tempPayDetailData = new PayDetailData(dateIn, startTimeIn, endTimeIn, employeeIn, cropIn, jobIn, fieldIn);

            //tempPayDetailData.HoursDbl += Convert.ToDouble(hoursIn);
			tempPayDetailData.PayUnitDbl += Convert.ToDouble(payUnitIn);
			tempPayDetailData.YieldUnitDbl += Convert.ToDouble(yieldUnitIn);
            //tempPayDetailData.PriceDbl += Convert.ToDouble(priceIn);
			tempPayDetailData.TypeStr = typeStr;
			tempPayDetailData.KeyInt = keyInt;
            tempPayDetailData.UserNameStr = boxLabelStr;
            tempPayDetailData.Extra1Str = referenceStr;
            tempPayDetailData.Extra2Str = otherStr;
            tempPayDetailData.Extra1Int = growerBlockIn;
			for (int i = 0; i < list.Count; i++)
			{
				PayDetailData payDetailData = (PayDetailData)list[i];

				if (payDetailData.KeyInt == keyInt)
				{
					list.RemoveAt(i);
					list.Insert(i, tempPayDetailData);

					break;
				}
			}
		}
        #endregion
        #region Grower Block Functions
        public static GrowerBlockData FindOrAddGrowerBlockData(Grower_Blocks_Data growerDataIn, string codeStr, string descriptionStr, int keyInt)
        {
            GrowerBlockData tempGrowerData = new GrowerBlockData(codeStr, descriptionStr, keyInt);
            int tempGrowerDataIdx = growerDataIn.GrowerBlocksDataArl.BinarySearch(tempGrowerData);

            if (tempGrowerDataIdx < 0)
                growerDataIn.GrowerBlocksDataArl.Insert(Math.Abs(tempGrowerDataIdx) - 1, tempGrowerData);
            else
                tempGrowerData = (GrowerBlockData)growerDataIn.GrowerBlocksDataArl[tempGrowerDataIdx];

            return tempGrowerData;
        }
        public static GrowerBlockData FindGrowerBlockData(Grower_Blocks_Data growerDataIn, string codeStr, string descriptionStr, int keyInt)
        {
            GrowerBlockData tempGrowerData = new GrowerBlockData(codeStr, descriptionStr, keyInt);
            int tempGrowerDataIdx = growerDataIn.GrowerBlocksDataArl.BinarySearch(tempGrowerData);

            if (tempGrowerDataIdx >= 0)
                return (GrowerBlockData)growerDataIn.GrowerBlocksDataArl[tempGrowerDataIdx];

            return null;
        }
        public static void SaveEditedGrowerBlockData(Grower_Blocks_Data growerDataIn, string codeStr, string descriptionStr, int keyInt, bool active)
        {
            for (int i = 0; i < growerDataIn.GrowerBlocksDataArl.Count; i++)
            {
                GrowerBlockData growerDataInList = (GrowerBlockData)growerDataIn.GrowerBlocksDataArl[i];
                if (keyInt == growerDataInList.KeyInt)
                {
                    growerDataIn.GrowerBlocksDataArl.RemoveAt(i);
                    GrowerBlockData editedGrowerData = new GrowerBlockData(codeStr, descriptionStr, keyInt);
                    editedGrowerData.ActiveBln = active;
                    growerDataIn.GrowerBlocksDataArl.Insert(i, editedGrowerData);

                    break;
                }
            }
        }
        #endregion
        #region Window Handle
        [DllImport("user32.dll")]
		public static extern bool SetForegroundWindow(IntPtr hWnd);
		private static string GetProcessOwner(int processId)
		{
			string query = "Select * From Win32_Process Where ProcessID = " + processId;
			ManagementObjectSearcher searcher = new ManagementObjectSearcher(query);
			ManagementObjectCollection processList = searcher.Get();

			foreach (ManagementObject obj in processList)
			{
				string[] argList = new string[] { string.Empty };
				int returnVal = Convert.ToInt32(obj.InvokeMethod("GetOwner", argList));
				if (returnVal == 0)
					return argList[0];
			}

			return "NO OWNER";
		}
		#endregion

		#region Fill data objects from XML functions
		//public static void FillCropDataFromXML()
		//{
		//    FileInfo fileInfo = new FileInfo(xmlFileName_Crops);

		//    if (fileInfo.Exists)
		//    {
		//        //Deserialize (convert an XML document into an object instance):
		//        XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.CropsData));

		//        // A FileStream is needed to read the XML document.
		//        FileStream fileStream = new FileStream(xmlFileName_Crops, FileMode.Open);
		//        XmlReader xmlReader = XmlReader.Create(fileStream);

		//        // Declare an object variable of the type to be deserialized.
		//        // Use the Deserialize method to restore the object's state.
		//        cropsData = (MainMenu.CropsData)serializer.Deserialize(xmlReader);
		//        fileStream.Close();
		//    }
		//}
		//public static void FillEmployeeDataFromXML()
		//{
		//    FileInfo fileInfo = new FileInfo(xmlFileName_Employees);

		//    if (fileInfo.Exists)
		//    {
		//        //Deserialize (convert an XML document into an object instance):
		//        XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.EmployeesData));

		//        // A FileStream is needed to read the XML document.
		//        FileStream fileStream = new FileStream(xmlFileName_Employees, FileMode.Open);
		//        XmlReader xmlReader = XmlReader.Create(fileStream);

		//        // Declare an object variable of the type to be deserialized.
		//        // Use the Deserialize method to restore the object's state.
		//        employeesData = (MainMenu.EmployeesData)serializer.Deserialize(xmlReader);
		//        fileStream.Close();
		//    }
		//}
		//public static void FillJobDataFromXML()
		//{
		//    FileInfo fileInfo = new FileInfo(xmlFileName_Jobs);

		//    if (fileInfo.Exists)
		//    {
		//        //Deserialize (convert an XML document into an object instance):
		//        XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.JobsData));

		//        // A FileStream is needed to read the XML document.
		//        FileStream fileStream = new FileStream(xmlFileName_Jobs, FileMode.Open);
		//        XmlReader xmlReader = XmlReader.Create(fileStream);

		//        // Declare an object variable of the type to be deserialized.
		//        // Use the Deserialize method to restore the object's state.
		//        jobsData = (MainMenu.JobsData)serializer.Deserialize(xmlReader);
		//        fileStream.Close();
		//    }
		//}
		//public static void FillFieldDataFromXML()
		//{
		//    FileInfo fileInfo = new FileInfo(xmlFileName_Fields);

		//    if (fileInfo.Exists)
		//    {
		//        //Deserialize (convert an XML document into an object instance):
		//        XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.FieldsData));

		//        // A FileStream is needed to read the XML document.
		//        FileStream fileStream = new FileStream(xmlFileName_Fields, FileMode.Open);
		//        XmlReader xmlReader = XmlReader.Create(fileStream);

		//        // Declare an object variable of the type to be deserialized.
		//        // Use the Deserialize method to restore the object's state.
		//        fieldsData = (MainMenu.FieldsData)serializer.Deserialize(xmlReader);
		//        fileStream.Close();
		//    }
		//}
		//public static void FillPayDetailDataFromXML()
		//{
		//    FileInfo fileInfo = new FileInfo(xmlFileName_PayDetails);

		//    if (fileInfo.Exists)
		//    {
		//        //Deserialize (convert an XML document into an object instance):
		//        XmlSerializer serializer = new XmlSerializer(typeof(MainMenu.PayDetailsData));

		//        // A FileStream is needed to read the XML document.
		//        FileStream fileStream = new FileStream(xmlFileName_PayDetails, FileMode.Open);
		//        XmlReader xmlReader = XmlReader.Create(fileStream);

		//        // Declare an object variable of the type to be deserialized.
		//        // Use the Deserialize method to restore the object's state.
		//        payDetailsData = (MainMenu.PayDetailsData)serializer.Deserialize(xmlReader);
		//        fileStream.Close();
		//    }
		//}
		#endregion
		#endregion
        

		#region CBO
		#region Crop
		[Serializable]
		[System.Xml.Serialization.XmlInclude(typeof(CropData))]
		public class CropsData
		{
			#region Vars
			public ArrayList CropsDataArl;

			#region Properties
			public int NextKeyInt
			{
				get
				{
					int highestKeyYet = 0;
					for (int i = 0; i < this.CropsDataArl.Count; i++)
					{
						CropData crop = (CropData)this.CropsDataArl[i];
						if (crop.KeyInt > highestKeyYet)
							highestKeyYet = crop.KeyInt;
					}
					return (highestKeyYet + 1);
				}
			}
			#endregion
			#endregion

			#region Constructor
			public CropsData()
			{
				CropsDataArl = new ArrayList();
			}
			#endregion
		}

		[Serializable]
		public class CropData : IComparable
		{
			#region Vars
			public int KeyInt;
			public string CodeStr;
			public string DescriptionStr;
			public bool ActiveBln;
            public int SubInt1;
            public string SubStr1;
            public int SubInt2;
            public string SubStr2;
            public int SubInt3;
            public string SubStr3;
            public int SubInt4;
            public string SubStr4;
            public int SubInt5;
            public string SubStr5;
			#endregion

			public CropData()
			{
				CodeStr = string.Empty;
				DescriptionStr = string.Empty;
				KeyInt = -1;
				ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}
			public CropData(string codeStr, string descriptionStr, int keyInt)
			{
				CodeStr = codeStr;
				DescriptionStr = descriptionStr;
				KeyInt = keyInt;
                ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}

			#region Properties

			public string Print(string seperator)
			{
				return (this.CodeStr + seperator + this.DescriptionStr);
			}

			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				CropData Y = (CropData)obj;

				//Compare CodeStr
				tempCompare = this.CodeStr.CompareTo(Y.CodeStr);
				if (tempCompare != 0)
					return tempCompare;

				////Compare DescriptionStr
				//tempCompare = this.DescriptionStr.CompareTo(Y.DescriptionStr);
				//if (tempCompare != 0)
				//    return tempCompare;

				//Compare Key.
				tempCompare = this.KeyInt.CompareTo(Y.KeyInt);
				if (tempCompare != 0)
					return tempCompare;

				return 0;
			}
			#endregion
		}
		#endregion

		#region Job
		[Serializable]
		[System.Xml.Serialization.XmlInclude(typeof(JobData))]
		public class JobsData
		{
			#region Vars
			public ArrayList JobsDataArl;

			#region Properties
			public int NextKeyInt
			{
				get
				{
					int highestKeyYet = 0;
					for (int i = 0; i < this.JobsDataArl.Count; i++)
					{
						JobData job = (JobData)this.JobsDataArl[i];
						if (job.KeyInt > highestKeyYet)
							highestKeyYet = job.KeyInt;
					}
					return (highestKeyYet + 1);
				}
			}
			#endregion
			#endregion

			#region Constructor
			public JobsData()
			{
				JobsDataArl = new ArrayList();
			}
			#endregion
		}

		[Serializable]
		public class JobData : IComparable
		{
			#region Vars
			public int KeyInt;
			public string CodeStr;
			public string DescriptionStr;
            public bool ActiveBln;
            public int SubInt1;
            public string SubStr1;
            public int SubInt2;
            public string SubStr2;
            public int SubInt3;
            public string SubStr3;
            public int SubInt4;
            public string SubStr4;
            public int SubInt5;
            public string SubStr5;
			#endregion

			public JobData()
			{
				CodeStr = string.Empty;
				DescriptionStr = string.Empty;
				KeyInt = -1;
				ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}
			public JobData(string codeStr, string descriptionStr, int keyInt)
			{
				CodeStr = codeStr;
				DescriptionStr = descriptionStr;
				KeyInt = keyInt;
				ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}

			#region Properties

			public string Print(string seperator)
			{
				return (this.CodeStr + seperator + this.DescriptionStr);
			}

			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				JobData Y = (JobData)obj;

				//Compare CodeStr
				tempCompare = this.CodeStr.CompareTo(Y.CodeStr);
				if (tempCompare != 0)
					return tempCompare;

				//Compare DescriptionStr
				tempCompare = this.DescriptionStr.CompareTo(Y.DescriptionStr);
				if (tempCompare != 0)
					return tempCompare;

				//Compare Key.
				tempCompare = this.KeyInt.CompareTo(Y.KeyInt);
				if (tempCompare != 0)
					return tempCompare;

				return 0;
			}
			#endregion
		}
		#endregion

		#region Field
		[Serializable]
		[System.Xml.Serialization.XmlInclude(typeof(FieldData))]
		public class FieldsData
		{
			#region Vars
			public ArrayList FieldsDataArl;

			#region Properties
			public int NextKeyInt
			{
				get
				{
					int highestKeyYet = 0;
					for (int i = 0; i < this.FieldsDataArl.Count; i++)
					{
						FieldData field = (FieldData)this.FieldsDataArl[i];
						if (field.KeyInt > highestKeyYet)
							highestKeyYet = field.KeyInt;
					}
					return (highestKeyYet + 1);
				}
			}
			#endregion
			#endregion

			#region Constructor
			public FieldsData()
			{
				FieldsDataArl = new ArrayList();
			}
			#endregion
		}

		[Serializable]
		public class FieldData : IComparable
		{
			#region Vars
			public int KeyInt;
			public string CodeStr;
			public string DescriptionStr;
            public bool ActiveBln;
            public int SubInt1;
            public string SubStr1;
            public int SubInt2;
            public string SubStr2;
            public int SubInt3;
            public string SubStr3;
            public int SubInt4;
            public string SubStr4;
            public int SubInt5;
            public string SubStr5;
			#endregion

			public FieldData()
			{
				CodeStr = string.Empty;
				DescriptionStr = string.Empty;
				KeyInt = -1;
                ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}
			public FieldData(string codeStr, string descriptionStr, int keyInt)
			{
				CodeStr = codeStr;
				DescriptionStr = descriptionStr;
				KeyInt = keyInt;
                ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}

			#region Properties

			public string Print(string seperator)
			{
				return (this.CodeStr + seperator + this.DescriptionStr);
			}

			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				FieldData Y = (FieldData)obj;

				//Compare CodeStr
				tempCompare = this.CodeStr.CompareTo(Y.CodeStr);
				if (tempCompare != 0)
					return tempCompare;

				//Compare DescriptionStr
				tempCompare = this.DescriptionStr.CompareTo(Y.DescriptionStr);
				if (tempCompare != 0)
					return tempCompare;

				//Compare Key.
                tempCompare = this.KeyInt.CompareTo(Y.KeyInt);
                if (tempCompare != 0)
                    return tempCompare;

				return 0;
			}
			#endregion
		}
		#endregion

		#region Employee
		[Serializable]
		[System.Xml.Serialization.XmlInclude(typeof(EmployeeData))]
		public class EmployeesData
		{
			#region Vars
			public ArrayList EmployeesDataArl;

			#region Properties
			public int NextKeyInt
			{
				get
				{
					int highestKeyYet = 0;

					for (int i = 0; i < this.EmployeesDataArl.Count; i++)
					{
						EmployeeData employee = (EmployeeData)this.EmployeesDataArl[i];

						if (employee.KeyInt > highestKeyYet)
							highestKeyYet = employee.KeyInt;
					}

					return (highestKeyYet + 1);
				}
			}
			#endregion
			#endregion

			#region Constructor
			public EmployeesData()
			{
				EmployeesDataArl = new ArrayList();
			}
			#endregion
		}

		[Serializable]
		[System.Xml.Serialization.XmlInclude(typeof(EmployeePreTaxAdjustmentData))]
		public class EmployeeData : IComparable
		{
			#region Vars
			public int KeyInt;
			public string CodeStr;
            public string NameStr;
            public string Address1Str;
            public string Address2Str;
            public string CityStr;
            public string StateStr;
            public string ZipStr;
            public string SSNStr;
            public double HourlyWageDbl;
			//public bool OvertimeByAverageWageBln;
            public bool OvertimePerEmployeeBln;
			public bool ActiveBln;
            public ArrayList EmployeePreTaxAdjustmentsDataArl;
            public string GroupStr;
            public int SubInt1;
            public string SubStr1;
            public int SubInt2;
            public string SubStr2;
            public int SubInt3;
            public string SubStr3;
            public int SubInt4;
            public string SubStr4;
            public int SubInt5;
            public string SubStr5;

			#region Properties
			public int NextKeyInt
			{
				get
				{
					int highestKeyYet = 0;

					for (int i = 0; i < this.EmployeePreTaxAdjustmentsDataArl.Count; i++)
					{
						EmployeePreTaxAdjustmentData employeePreTaxAdjustmentData = (EmployeePreTaxAdjustmentData)this.EmployeePreTaxAdjustmentsDataArl[i];

						if (employeePreTaxAdjustmentData.KeyInt > highestKeyYet)
							highestKeyYet = employeePreTaxAdjustmentData.KeyInt;
					}

					return (highestKeyYet + 1);
				}
			}
			#endregion
			#endregion

			#region Constructor
			public EmployeeData()
			{
				CodeStr = string.Empty;
                NameStr = string.Empty;
                Address1Str = string.Empty;
                Address2Str = string.Empty;
                CityStr = string.Empty;
                StateStr = string.Empty;
                ZipStr = string.Empty;
                SSNStr = string.Empty;
                KeyInt = -1;
				HourlyWageDbl = 0;
				//OvertimeByAverageWageBln = false;
                OvertimePerEmployeeBln = false;
				ActiveBln = false;
                EmployeePreTaxAdjustmentsDataArl = new ArrayList();
                GroupStr = string.Empty;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}
            public EmployeeData(string codeStr, string nameStr, string address1Str, string address2Str, string cityStr, string stateStr, string zipStr, string ssnStr, int keyInt, string groupStr)
			{
				CodeStr = codeStr;
                NameStr = nameStr;
                Address1Str = address1Str;
                Address2Str = address2Str;
                CityStr = cityStr;
                StateStr = stateStr;
                ZipStr = zipStr;
                SSNStr = ssnStr;
				KeyInt = keyInt;
				HourlyWageDbl = 0;
				//OvertimeByAverageWageBln = false;
                OvertimePerEmployeeBln = false;
				ActiveBln = false;
                EmployeePreTaxAdjustmentsDataArl = new ArrayList();
                GroupStr = groupStr;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}
			#endregion

			#region Properties
			public string Print(string seperator)
			{
				return (this.CodeStr + seperator + this.NameStr);
			}
			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				EmployeeData Y = (EmployeeData)obj;

				//Compare CodeStr
				tempCompare = this.CodeStr.CompareTo(Y.CodeStr);
				if (tempCompare != 0)
					return tempCompare;

                //Compare NameStr
                tempCompare = this.NameStr.CompareTo(Y.NameStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare Address1Str
                tempCompare = this.Address1Str.CompareTo(Y.Address1Str);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare Address2Str
                tempCompare = this.Address2Str.CompareTo(Y.Address2Str);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare CityStr
                tempCompare = this.CityStr.CompareTo(Y.CityStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare StateStr
                tempCompare = this.StateStr.CompareTo(Y.StateStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare ZipStr
                tempCompare = this.ZipStr.CompareTo(Y.ZipStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare SSNStr
                tempCompare = this.SSNStr.CompareTo(Y.SSNStr);
                if (tempCompare != 0)
                    return tempCompare;

				//Compare Key.
				tempCompare = this.KeyInt.CompareTo(Y.KeyInt);
				if (tempCompare != 0)
					return tempCompare;

				return 0;
			}
			#endregion
		}

		[Serializable]
		public class EmployeePreTaxAdjustmentData : IComparable
		{
			#region Vars
			public int KeyInt;
			public string CodeStr;
			public string DescriptionStr;
			public double AmountDbl;
			#endregion

			#region Constructor
			public EmployeePreTaxAdjustmentData()
			{
				CodeStr = string.Empty;
				DescriptionStr = string.Empty;
				KeyInt = -1;
				AmountDbl = 0;
			}
			public EmployeePreTaxAdjustmentData(string codeStr, string descriptionStr, int keyInt)
			{
				CodeStr = codeStr;
				DescriptionStr = descriptionStr;
				KeyInt = keyInt;
				AmountDbl = 0;
			}
			#endregion

			#region Properties
			public string Print(string seperator)
			{
				return (this.CodeStr + seperator + this.DescriptionStr);
			}
			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				EmployeePreTaxAdjustmentData Y = (EmployeePreTaxAdjustmentData)obj;

				//Compare CodeStr
				tempCompare = this.CodeStr.CompareTo(Y.CodeStr);
				if (tempCompare != 0)
					return tempCompare;

				//Compare DescriptionStr
				tempCompare = this.DescriptionStr.CompareTo(Y.DescriptionStr);
				if (tempCompare != 0)
					return tempCompare;

				//Compare Key.
				tempCompare = this.KeyInt.CompareTo(Y.KeyInt);
				if (tempCompare != 0)
					return tempCompare;

				return 0;
			}
			#endregion
		}
		#endregion

		#region Payroll Options
		[Serializable]
		public class PayrollOptionsData
		{
			#region Vars
			public bool PayOvertimeAllBln;
			public bool OvertimePerEmployeeBln;
			public double RegularHoursDbl;
			public int CropForOvertimeKeyInt;
			public int JobForOvertimeKeyInt;
			public int FieldForOvertimeKeyInt;
			public int FieldForMinimumWageAdjustmentKeyInt;
			public double MinimumWageDbl;
            public string LabelPrinterStr;
            public string ReportPrinterStr;
			#endregion

			#region Constructor
			public PayrollOptionsData()
			{
                PayOvertimeAllBln = false;
                OvertimePerEmployeeBln = false;
				RegularHoursDbl = 0;
				CropForOvertimeKeyInt = -1;
				JobForOvertimeKeyInt = -1;
				FieldForOvertimeKeyInt = -1;
				FieldForMinimumWageAdjustmentKeyInt = -1;
				MinimumWageDbl = 0;
                LabelPrinterStr = "";
                ReportPrinterStr = "";
			}
            public PayrollOptionsData(bool payOvertimeBln, bool overtimePerEmployeeBln, double regularHoursDbl, int cropForOvertimeKeyInt, int jobForOvertimeKeyInt, int fieldForOvertimeKeyInt, int fieldForMinimumWageAdjustmentKeyInt, double minimumWageDbl, string labelPrinterStr, string reportPrinterStr)
			{
                PayOvertimeAllBln = payOvertimeBln;
                OvertimePerEmployeeBln = overtimePerEmployeeBln;
				RegularHoursDbl = regularHoursDbl;
				CropForOvertimeKeyInt = cropForOvertimeKeyInt;
				JobForOvertimeKeyInt = jobForOvertimeKeyInt;
				FieldForOvertimeKeyInt = fieldForOvertimeKeyInt;
				FieldForMinimumWageAdjustmentKeyInt = fieldForMinimumWageAdjustmentKeyInt;
				MinimumWageDbl = minimumWageDbl;
                LabelPrinterStr = labelPrinterStr;
                ReportPrinterStr = reportPrinterStr;
			}
			#endregion
		}
		#endregion

		#region Pay Detail
		[Serializable]
		[System.Xml.Serialization.XmlInclude(typeof(PayDetailData))]
		public class PayDetailsData
		{
			#region Vars
			public ArrayList PayDetailsDataArl;

			#region Properties
			public int NextSummaryNumberInt
			{
				get
				{
					int highestSummaryNumberYet = 0;

					for (int i = 0; i < this.PayDetailsDataArl.Count; i++)
					{
						PayDetailData payDetailData = (PayDetailData)this.PayDetailsDataArl[i];

						if (payDetailData.SummaryNumberInt > highestSummaryNumberYet)
							highestSummaryNumberYet = payDetailData.SummaryNumberInt;
					}

					return (highestSummaryNumberYet + 1);
				}
			}
			#endregion
			#endregion

			#region Constructor
			public PayDetailsData()
			{
				PayDetailsDataArl = new ArrayList();
			}
			#endregion
		}

		[Serializable]
		public class PayDetailData : IComparable
		{
			#region Vars
			public int KeyInt;
			public DateTime DateDtm;
			public DateTime StartTimeDtm;
			public DateTime EndTimeDtm;
			public int EmployeeKeyInt;
			public double HoursDbl;
			public int CropKeyInt;
			public int JobKeyInt;
			public int FieldKeyInt;
			public double PayUnitDbl;
			public double YieldUnitDbl;
			public double PriceDbl;
			public int SummaryNumberInt;
			public bool ActiveBln;
			public string TypeStr;
            public DateTime ReportDateDtm;
            public string UserNameStr;
            public DateTime EnterDtm;
            public DateTime LastEditDtm;
            public DateTime ImportFileDtm;
            public string ImportFileNameStr;
            public string Extra1Str;
            public string Extra2Str;
            public string Extra3Str;
            public double Extra1Dbl;
            public double Extra2Dbl;
            public double Extra3Dbl;
            public int Extra1Int;
            public int Extra2Int;
            public int Extra3Int;
            public DateTime Extra1Dtm;
            public DateTime Extra2Dtm;
            public DateTime Extra3Dtm;

			#endregion

			#region Properties
			public bool EnabledBln
			{
				//This getter will check if the crop, job, field, or emplooyee are disabled and return false if any are.
				get
				{
					return true;
				}
			}
			public string TotalDollarsStr
			{
				get
				{
					return (this.PriceDbl * this.PayUnitDbl).ToString("$###,###,##0.00");
				}
			}
			#endregion

			#region Constructor
			public PayDetailData()
			{
				KeyInt = -1;
				DateDtm = new DateTime();
				StartTimeDtm = new DateTime();
				EndTimeDtm = new DateTime();
				EmployeeKeyInt = -1;
				HoursDbl = 0;
				CropKeyInt = -1;
				JobKeyInt = -1;
				FieldKeyInt = -1;
				PayUnitDbl = 0;
				YieldUnitDbl = 0;
				PriceDbl = 0;
				SummaryNumberInt = 0;
				ActiveBln = true;
				TypeStr = string.Empty;
                ReportDateDtm = new DateTime();
                UserNameStr = string.Empty;
                EnterDtm = new DateTime();
                LastEditDtm = new DateTime();
                ImportFileDtm = new DateTime();
                ImportFileNameStr = string.Empty;
                Extra1Str = string.Empty;
                Extra2Str = string.Empty;
                Extra3Str = string.Empty;
                Extra1Dbl = 0;
                Extra2Dbl = 0;
                Extra3Dbl = 0;
                Extra1Int = 0;
                Extra2Int = 0;
                Extra3Int = 0;
                Extra1Dtm = new DateTime();
                Extra2Dtm = new DateTime();
                Extra3Dtm = new DateTime();

			}
			public PayDetailData(DateTime dateIn, DateTime startTimeIn, DateTime endTimeIn, int employeeIn, int cropIn, int jobIn, int fieldIn)
			{
				KeyInt = -1;
				DateDtm = dateIn;
				StartTimeDtm = startTimeIn;
				EndTimeDtm = endTimeIn;
				EmployeeKeyInt = employeeIn;
				HoursDbl = 0;
				CropKeyInt = cropIn;
				JobKeyInt = jobIn;
				FieldKeyInt = fieldIn;
				PayUnitDbl = 0;
				YieldUnitDbl = 0;
				PriceDbl = 0;
				SummaryNumberInt = 0;
				ActiveBln = true;
				TypeStr = string.Empty;
                ReportDateDtm = new DateTime();
                UserNameStr = string.Empty;
                EnterDtm = new DateTime();
                LastEditDtm = new DateTime();
                ImportFileDtm = new DateTime();
                ImportFileNameStr = string.Empty;
                Extra1Str = string.Empty;
                Extra2Str = string.Empty;
                Extra3Str = string.Empty;
                Extra1Dbl = 0;
                Extra2Dbl = 0;
                Extra3Dbl = 0;
                Extra1Int = 0;
                Extra2Int = 0;
                Extra3Int = 0;
                Extra1Dtm = new DateTime();
                Extra2Dtm = new DateTime();
                Extra3Dtm = new DateTime();
			}
			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
				PayDetailData Y = (PayDetailData)obj;

				////Compare Key
				//tempCompare = this.Key.CompareTo(Y.Key);
				//if (tempCompare != 0)
				//    return tempCompare;

                //Compare Active
                tempCompare = this.ActiveBln.CompareTo(Y.ActiveBln);
                if (tempCompare != 0)
                    return tempCompare;

				//Compare Date
				tempCompare = this.DateDtm.CompareTo(Y.DateDtm);
				if (tempCompare != 0)
					return tempCompare;

				//Compare StartTime
				tempCompare = this.StartTimeDtm.CompareTo(Y.StartTimeDtm);
				if (tempCompare != 0)
					return tempCompare;

				//Compare EndTime
				tempCompare = this.EndTimeDtm.CompareTo(Y.EndTimeDtm);
				if (tempCompare != 0)
					return tempCompare;

				//Compare Employee
				tempCompare = this.EmployeeKeyInt.CompareTo(Y.EmployeeKeyInt);
				if (tempCompare != 0)
					return tempCompare;

				//Compare Crop
				tempCompare = this.CropKeyInt.CompareTo(Y.CropKeyInt);
				if (tempCompare != 0)
					return tempCompare;

				//Compare Job
				tempCompare = this.JobKeyInt.CompareTo(Y.JobKeyInt);
				if (tempCompare != 0)
					return tempCompare;

				//Compare Field
				tempCompare = this.FieldKeyInt.CompareTo(Y.FieldKeyInt);
				if (tempCompare != 0)
					return tempCompare;

				return 0;
			}
			#endregion
		}
		#endregion

        #region Pay Report Data By Employee
        public class PayReportDataByEmployee : IComparable
        {
            #region Vars
            public string codeStr;
            public string nameStr;
            public double totalPayDbl;
            public double totalHoursDbl;
            public double totalPayUnitsDbl;
            public double totalYieldUnitsDbl;
            #endregion

            public PayReportDataByEmployee()
            {
                codeStr = string.Empty;
                nameStr = string.Empty;
                totalPayDbl = 0.0f;
                totalHoursDbl = 0.0f;
                totalPayUnitsDbl = 0.0f;
                totalYieldUnitsDbl = 0.0f;
            }
            public PayReportDataByEmployee(string CodeStr, string NameStr)
            {
                codeStr = CodeStr;
                nameStr = NameStr;
                totalPayDbl = 0.0f;
                totalHoursDbl = 0.0f;
                totalPayUnitsDbl = 0.0f;
                totalYieldUnitsDbl = 0.0f;
            }

            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                PayReportDataByEmployee Y = (PayReportDataByEmployee)obj;

                //Compare codeStr
                tempCompare = this.codeStr.CompareTo(Y.codeStr);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare nameStr
                tempCompare = this.nameStr.CompareTo(Y.nameStr);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion
        }
        #endregion

        #region Grower_Block
		[Serializable]
		[System.Xml.Serialization.XmlInclude(typeof(GrowerBlockData))]
		public class Grower_Blocks_Data
        {
			#region Vars
			public ArrayList GrowerBlocksDataArl;

			#region Properties
			public int NextKeyInt
			{
				get
				{
					int highestKeyYet = 0;
					for (int i = 0; i < this.GrowerBlocksDataArl.Count; i++)
					{
                        GrowerBlockData growerBlock = (GrowerBlockData)this.GrowerBlocksDataArl[i];
						if (growerBlock.KeyInt > highestKeyYet)
							highestKeyYet = growerBlock.KeyInt;
					}
					return (highestKeyYet + 1);
				}
			}
			#endregion
			#endregion

			#region Constructor
			public Grower_Blocks_Data()
			{
                GrowerBlocksDataArl = new ArrayList();
			}
			#endregion
		}

		[Serializable]
		public class GrowerBlockData : IComparable
		{
			#region Vars
			public int KeyInt;
			public string CodeStr;
			public string DescriptionStr;
			public bool ActiveBln;
            public int SubInt1;
            public string SubStr1;
            public int SubInt2;
            public string SubStr2;
            public int SubInt3;
            public string SubStr3;
            public int SubInt4;
            public string SubStr4;
            public int SubInt5;
            public string SubStr5;
			#endregion

			public GrowerBlockData()
			{
				CodeStr = string.Empty;
				DescriptionStr = string.Empty;
				KeyInt = -1;
				ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}
			public GrowerBlockData(string codeStr, string descriptionStr, int keyInt)
			{
				CodeStr = codeStr;
				DescriptionStr = descriptionStr;
				KeyInt = keyInt;
                ActiveBln = false;
                SubInt1 = -1;
                SubStr1 = string.Empty;
                SubInt2 = -1;
                SubStr2 = string.Empty;
                SubInt3 = -1;
                SubStr3 = string.Empty;
                SubInt4 = -1;
                SubStr4 = string.Empty;
                SubInt5 = -1;
                SubStr5 = string.Empty;
			}

			#region Properties

			public string Print(string seperator)
			{
				return (this.CodeStr + seperator + this.DescriptionStr);
			}

			#endregion

			#region IComparable Members
			public int CompareTo(object obj)
			{
				int tempCompare;
                GrowerBlockData Y = (GrowerBlockData)obj;

				//Compare CodeStr
				tempCompare = this.CodeStr.CompareTo(Y.CodeStr);
				if (tempCompare != 0)
					return tempCompare;

				////Compare DescriptionStr
				//tempCompare = this.DescriptionStr.CompareTo(Y.DescriptionStr);
				//if (tempCompare != 0)
				//    return tempCompare;

				//Compare Key.
				tempCompare = this.KeyInt.CompareTo(Y.KeyInt);
				if (tempCompare != 0)
					return tempCompare;

				return 0;
			}
			#endregion
		}
		#endregion
        
        public class ReceiverSend : IComparable
        {
            #region Vars
            public string cropLabelStr;
            public string jobLabelStr;
            public string fieldLabelStr;
            public string growerLabelStr;
            public string boxLabelLabelStr;
            public Int32 countLabelInt;
            #endregion

            public ReceiverSend()
            {
                cropLabelStr = string.Empty;
                jobLabelStr = string.Empty;
                fieldLabelStr = string.Empty;
                growerLabelStr = string.Empty;
                boxLabelLabelStr = string.Empty;
                countLabelInt = 0;
                
            }
            #region IComparable Members
            public int CompareTo(object obj)
            {
                //int tempCompare;
                //PayReportDataByEmployee Y = (PayReportDataByEmployee)obj;

                ////Compare codeStr
                //tempCompare = this.codeStr.CompareTo(Y.codeStr);
                //if (tempCompare != 0)
                //    return tempCompare;

                ////Compare nameStr
                //tempCompare = this.nameStr.CompareTo(Y.nameStr);
                //if (tempCompare != 0)
                //    return tempCompare;

                return 0;
            }
            #endregion
           
        }

        public class RoomItem : IComparable
        {
            #region Vars
            public Int32 roomInt;
            public Int32 cropInt;
            public Int32 customerInt;
            public Int32 growerBlockInt;
            public string roomName;
            public string cropName;
            public string customerName;
            public string growerBlockName;
            public double InDbl;
            public double OutDbl;
            #endregion

            public RoomItem()
            {
                roomInt = -1;
                cropInt = -1;
                customerInt = -1;
                growerBlockInt = -1;
                roomName = "";
                cropName = "";
                customerName = "";
                growerBlockName = "";
                InDbl = 0;
                OutDbl = 0;
            }
            #region IComparable Members
            public int CompareTo(object obj)
            {
                int tempCompare;
                RoomItem Y = (RoomItem)obj;

                //Compare Room
                tempCompare = this.roomName.CompareTo(Y.roomName);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare Customer
                tempCompare = this.customerName.CompareTo(Y.customerName);
                if (tempCompare != 0)
                    return tempCompare;

                //Compare Crop
                tempCompare = this.cropName.CompareTo(Y.cropName);
                if (tempCompare != 0)
                    return tempCompare;
                //Compare Grower Block
                tempCompare = this.growerBlockName.CompareTo(Y.growerBlockName);
                if (tempCompare != 0)
                    return tempCompare;

                return 0;
            }
            #endregion

        }









        #endregion

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
